---
title: KDE Megarelease 6
subtitle: KDE’s next mega release is here!
hidden: true
layout: megarelease-6
date: 2023-02-28
scssFiles:
  - /scss/megarelease-6.scss
images:
 - /announcements/megarelease/6/desktop.png

authors:
  - SPDX-FileCopyrightText: 2024 Carl Schwan <carl@carlschwan.eu>
SPDX-License-Identifier: CC-BY-4.0

intro:
  author: The KDE community proudly present Plasma 6, Frameworks 6 and Gear 24.02
  date: Released %s

plasma_intro:
  title: Plasma 6
  description: |
    KDE Plasma is a modern, feature-rich desktop environment for Linux-based operating systems.
    Known for its sleek design, customizable interface, and extensive set of applications, it
    is also open source, devoid of ads, and makes protecting your privacy and personal data a
    priority.

    [Install Plasma](https://kde.org/distributions/) on your current laptop and enjoy all the
    perks of a state of the art computing experience, even if your hardware is not supported by
    Windows 11. You can also opt for [some very elegant machines](https://kde.org/hardware/)
    such as the Steam Deck, or many laptops and stylish ultrabooks from manufacturers that
    deliver their products with Plasma pre-installed.

overview:
  title: New Overview Effect
  alt: "New overview effect"
  description: "We combined the Overview effect and the desktop Grid into one and made a new effect with better touchpad support."

colors:
  title: |
    **Colors**
  description: |
    Plasma on Wayland now has partial support for [High Dynamic Range
    (HDR)](https://en.wikipedia.org/wiki/High_dynamic_range). This will provide you with richer,
    deeper colors and contrast for your games, videos and visual creations for supported
    monitors and games.

    Set an [ICC profile](https://zamundaaa.github.io/wayland/2023/12/18/update-on-hdr-and-colormanagement-in-plasma.html)
    for each screen individually and Plasma will adjust the colors accordingly. Applications are
    still limited to the sRGB color space, but we are working on increasing the number of supported
    color spaces soon.

    To improve Plasma's accessibility, we added support for [color blindness correction
    filters](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/3354). This helps
    with protanopia, deuteranopia or tritanopia.

panel:
  title: Floating Panel
  description: |
    With Plasma 6, the panel now floats by default.

  description2: |
    Don't like floating panels? No worries, we also redesigned the panel settings to make it
    easier to configure everything to your liking.

  description3: |
    Panels now have an Intelligent auto-hide mode that hides it when windows touch it.

breeze:
  title: Refreshed **Breeze**
  description: |
    Breeze has been overhauled to give it a more modern look and feel, with less
    frames and more consistent spacing.

  apps:
   - name: Kate
     icon: kate.svg
     new: /announcements/megarelease/6/kate-new.png
     old: /announcements/megarelease/6/kate-old.png

   - name: KTorrent
     icon: ktorrent.svg
     new: /announcements/megarelease/6/ktorrent-new.png
     old: /announcements/megarelease/6/ktorrent-old.png

   - name: KAddressBook
     icon: kaddressbook.svg
     new: /announcements/megarelease/6/kaddressbook-new.png
     old: /announcements/megarelease/6/kaddressbook-old.png

wallpaper:
  title: 'New Wallpaper'
  description: |
    Transform your desktop with the mesmerizing 'Scarlet Tree' wallpaper by axo1otl.
    Capture the dynamic interplay between the sun's vibrant energy and the comet's celestial
    dance.

  description_set: |
    Changing your wallpaper can be done from System Settings. Choose whether you
    want to change the wallpaper for one specific screen or all of them at the same time.

settings:
  title: Reorganized Settings
  description: |
    We've revamped the Settings app making it more user-friendly and with fewer nested pages.

    Within the Settings page, numerous buttons have been relocated to the toolbar, enhancing
    consistency with our other applications and creating additional space for the actual
    content.

    You now have the option to customize your sound theme, and a new default sound theme
    named "Ocean" introduced to replace the "Oxygen" sound theme.

  description2: |
    Another change is that you can configure more default application types.

cube:
  title: The Cube is Back!
  description: |
    Removed two years ago, the cube effect is making a comeback with Plasma 6. This effect is
    perfect to get a clear visualisation of the concept of what multiple desktops are all about...
    And also to show off to your friends and family how cool Plasma is.

krunner:
  title: Plasma Search
  description: |
    Plasma Search lets you use a custom search ordering performance: Due to some
    [refactoring and profiling](https://write.as/alexander-lohnau/profiling-and-optimizing-krunner),
    Plasma Search is generally faster and in particular searching for an application or settings
    modules is a lot faster while also using less CPU cycles.

  timezone: |
    Plasma Search lets you convert between timezones. This is handy if you travel
    frequently or need to keep in touch with colleagues, friends and family in other parts of the
    world. [Check out more ways how KDE can help you during your travels](https://kde.org/for/travelers/).

  more: More Plasma Search
  items:
    - name: Trigger "Hybrid Sleep" from KRunner
      url: https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/3227

    - name: Search in codeberg and PyPi from KRunner
      url: https://invent.kde.org/frameworks/kio/-/merge_requests/1462

    - name: Search now matches both English and your interface language
      url: https://bugs.kde.org/show_bug.cgi?id=467924

default:
  title: New Defaults!
  description: |
    Plasma 6.0 is a major new version and we decided to change a few default settings
    to make it easier to use. Plasma is still as configurable as ever, and you can change these
    settings if you need to.

  items:
    - name: Files and folders are now selected with a single-click and opened with a double-click
      url: https://invent.kde.org/plasma/plasma-desktop/-/issues/72

    - name: Touchpad tap-to-click is enabled by default on Wayland
      url: https://invent.kde.org/plasma/plasma-desktop/-/issues/97

    - name: Wayland is the default graphical mode
      url: https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/2188

    - name: '"Thumbnail Grid" is the new Task Switcher style'
      url: https://invent.kde.org/plasma/plasma-desktop/-/issues/53

    - name: Clicking on the scrollbar track now scrolls to the clicked location
      url: https://invent.kde.org/plasma/plasma-desktop/-/issues/92

    - name: Scrolling on the desktop no longer switches virtual desktops
      url: https://invent.kde.org/plasma/plasma-desktop/-/issues/55

    - name: Panels float by default
      url: https://invent.kde.org/plasma/plasma-desktop/-/issues/73

plasma_more:
  title: And More Plasma.
  items:
    - title: Improved fingerprint unlock from lockscreen
      description: |
        On prior versions you chose between either password or fingerprint
        authentication for the lockscreen. In Plasma 6 both are supported at the same time.
    - title: Support for more Islamic Calendars
      description: |
        We now support the Astronomical and Umm al-Qura Calendar in the calendar
        applet. This is in addition to the already existing Julian, Milankovic, Chinese Lunar,
        Indian, Hebrew, Persian and Islamic Civil calendars.

mobile:
  title: Plasma Mobile
  description: |
    The mobile version of Plasma has undergone several enhancements. The updated default home
    screen allows users to organize their preferred applications in a grid and group them.

    We added a welcoming screen to the first launch. This will guide you through configuring
    your WiFi, timezone, device scaling, and mobile connection settings.

gearintro:
  title: Applications
  description: |
    KDE Gear 24.02 brings many applications to Qt 6. In addition to the changes in
    Breeze, many applications adopted a more frameless look for their interface.

tokodon:
  title: Tokodon
  description: |
    Tokodon brings the Mastodon federated social media platform to your fingertips. With Tokodon
    you can read, post, and message easily. In this release, writing new posts is even easier.
    You can set the focal points that your followers will see when viewing your post in their
    timeline. Images can be copy-pasted or dropped in the composer. When discarding your draft,
    a configuration popup will prevent you from deleting your post by mistake.

  description_control: |
    We give you more control over what you see in Mastodon. You can hide boosts and replies in
    your main timeline. Reporting and muting accounts is now more accessible, with these actions
    directly available from the timeline. You can also leave comments when your report an
    offending account.

  description_login: |
    We completely redesigned the first launch experience, allowing you to learn
    more about Mastodon, log in to your existing account, and register an
    account on a server. Additionally, Tokodon will give you some explanation
    about errors encountered during login and give you the possibility to try
    again.

  description_profile: |
    We also redesigned the profile pages and now show an excellent background. The new profile
    page also allows filtering posts on a profile by their featured tags.

  more: And much more!

  items:
   - name: Color themes support on Android
     url: https://invent.kde.org/network/tokodon/-/merge_requests/319
   - name: Granular notification control
     url: https://invent.kde.org/network/tokodon/-/merge_requests/372
   - name: TweetDeck-style cross account actions
     url: https://invent.kde.org/network/tokodon/-/merge_requests/381
   - name: System integration of sharing with Tokodon
     url: https://invent.kde.org/network/tokodon/-/merge_requests/324
   - name: List support
     url: https://invent.kde.org/network/tokodon/-/merge_requests/434
   - name: Push notifications
     url: https://invent.kde.org/network/tokodon/-/merge_requests/350
   - name: Granular notification controls
     url: https://invent.kde.org/network/tokodon/-/merge_requests/372
   - name: Moderation tool for admins
     url: https://invent.kde.org/network/tokodon/-/merge_requests/420
   - name: Visible post edited status
     url: https://invent.kde.org/network/tokodon/-/merge_requests/396

plasmatube:
  title: PlasmaTube

  description: |
    [PlasmaTube](https://apps.kde.org/plasmatube) is a YouTube client via [Invidious](https://invidious.io/).
    This new version supports syncing your watch history with your Invidious account and searching
    for channels and playlists. It also comes with new queue system for the playlists.

    PlasmaTube 24.02 introduces a Picture-in-Picture (PiP) mode, which allows you to watch videos
    in a floating window which stays on top of all the others, so you can keep an eye on what
    you’re watching while working on other tasks.

    <img src="/announcements/megarelease/6/plasmatube-pip-2.png" class="d-block d-lg-none">

    We added limited support for videos from [Peertube](https://joinpeertube.org/) and
    [Piped](https://github.com/TeamPiped/Piped) too.

    ![](/announcements/megarelease/6/plasmatube.png)

  more: And much more!

  items:
    - name: Open a video directly by passing it as a command line argument
      url: https://invent.kde.org/multimedia/plasmatube/-/merge_requests/56
    - name: MPRIS2 support
      url: https://invent.kde.org/multimedia/plasmatube/-/merge_requests/70
    - name: Inhibit sleep when playing a fullscreen video
      url: https://invent.kde.org/multimedia/plasmatube/-/merge_requests/57
    - name: Basic Android support
      url: https://invent.kde.org/multimedia/plasmatube/-/merge_requests/65
    - name: Improved video player UX
      url: https://invent.kde.org/multimedia/plasmatube/-/merge_requests/69

pim:
  title: Groupware

  description_security: |
    The Kontact suite received a lot of improvements related to security and privacy.

    The KMail email viewer removes ads and tracking code from your emails using a more
    powerful ad blocker written in Rust. The email composer now directly shows the validity
    and the trust level of the [OpenPGP](https://librepgp.org/) keys of your recipients
    and warns you when one of their keys is almost expired or expired.

    Korganizer now lets you send encrypted and signed email invitations -- important if your work
    requires confidentiality.

    Kleopatra, the GnuPG frontend, now lets you view encrypted emails downloaded from web
    clients like Gmail that don't support OpenPGP. This will also benefit the many users
    of Gpg4win, as Kleopatra is an important part of this set if security tools.

  description_kmail: |
    In addition of a variety of pre-existing online service, KMail now supports a few
    offline and open source AI features. Translations can be configured to use the
    [Bergamot](https://browser.mt/) translation model, also used by Firefox. It can
    also use the ([vosk-api engine](https://github.com/alphacep/vosk-api)) to convert
    speech to text.

    Note that by default, all these AI features are disabled and you need to explicitly
    enable them in the KMail settings.

itinerary:
  title: Itinerary
  description: |
    Itinerary is KDE's travel assistant.

    We improved the usability of Itinerary on phones by making common actions
    available on a bottom navigation bar which makes it easy to switch to your
    passes and your current trip.

    The main timeline was redesigned and now entries can be expanded inline to
    show the intermediate stops and ongoing delays and disruptions. Speaking of
    events, it is now possible to add and edit manually entries and to share
    entire trips or single reservations to your phone via KDE Connect.

    The indoor map in Itinerary now lists all amenities in the building or area
    you are currently looking at. This helps with things like finding a place to grab a
    coffee in a big train station, or searching for a place to get food in the
    vicinity of your hotel. This feature is powered by OpenStreetMap, works without
    the need to connect to the internet and no search terms ever leave your device.

    When viewing a trip details, in addition to being able to consult the list of
    stops, it can look at a map of the trip.

spectacle:
  title: Spectacle
  description: |
    Spectacle is KDE's default screenshot utility.

    The recently introduced recording features in Spectacle have been improved
    and show an a system tray icon while recording. You can hover over the
    icon to see the time recorded. Click the icon and the recording will finish.

    Apart from capturing the whole screen or an application window, you can also
    capture an arbitrary part of the screen.

  description_2: |

    New recording shortcuts:
      - <kbd>Meta</kbd>+<kbd>R</kbd> or <kbd>Meta</kbd>+<kbd>Shift</kbd>+<kbd>R</kbd> for capturing a region
      - <kbd>Meta</kbd>+<kbd>Alt</kbd>+<kbd>R</kbd> for screen
      - <kbd>Meta</kbd>+<kbd>Ctrl</kbd>+<kbd>R</kbd> for window

    Note that we changed the default location for saving screenshots and screencasts and you will find them in
    `~/Pictures/Screenshots` and `~/Videos/Screencasts` respectively.

  more: And More!

  items:
    - name: Recording quality settings
    - name: VP9 encoding for video recording
    - name: Improved handling of multiple displays with different scale factors
    - name: Added CLI options for recording
    - name: New output filename template options and format

dolphin:
  title: Dolphin
  description: |
    Dolphin is KDE's file and folder explorer... That can do much more.

    Apart from Dolphin's overall redesign, the settings received some special care
    and were reorganized to make them easier to navigate.

    Accessibility improvements are also present in this new version. Toolbar
    buttons and the disk space in the status bar are now keyboard accessible.
    You can press <kbd>F10</kbd> to open the main menu and <kbd>Shift</kbd>+<kbd>F10</kbd>
    to open context menus. Some of these improvements were also applied to
    many other applications.

    Right clicking on a folder will now show an option to open the folder in a
    split view.

neochat:
  title: NeoChat
  subtitle: KDE's Matrix Client
  description_welcome: |
    NeoChat is a chat app that lets you take full advantage of the Matrix
    network.

    In the newest version, when launching the app, you will get a welcome page
    that lets you choose which account you want to use and lets you log in to other
    accounts. The welcome screen will also warn you when NeoChat cannot load an
    account.

    NeoChat will also let you register a new account directly from the app itself.
    Deactivating your Matrix account is also possible from within NeoChat.

  description_space: |
    Spaces are a relatively new feature of Matrix that let you group
    chat channels together. This is used to improve the discoverability of
    rooms, manage large communities, or just tidy all the channels you are in.
    You can now do all this without leaving NeoChat.

  description:
    NeoChat won't let you miss any new notifications anymore. We added a new
    page that includes all your recent notifications and when NeoChat is
    closed, you will still be able to receive
    [push notifications](https://invent.kde.org/network/neochat/-/merge_requests/1416).

    The main timeline will let you know when more messages are loading and when you
    reach the end of it.

  more: More NeoChat Goodies
  items:
    - name: QR Codes to share contacts
      url: https://invent.kde.org/network/neochat/-/merge_requests/1395
    - name: Improved room upgrades
      url: https://invent.kde.org/network/neochat/-/merge_requests/1349
    - name: Added button to reject invitation and ignore user
      url: https://invent.kde.org/network/neochat/-/merge_requests/1261
    - name: Display device security details
      url: https://invent.kde.org/network/neochat/-/merge_requests/1380
    - name: Added room security settings
      url: https://invent.kde.org/network/neochat/-/merge_requests/1413

education:
  title: KDE Edu

  description: |
    Several [KDE Edu](https://apps.kde.org/education) applications were updated to Qt 6,
    and as part of the porting, their interface got redesigned to meet the current KDE standards.

    [KHangMan](https://apps.kde.org/khangman), an adaptation of the [Hangman game](https://en.wikipedia.org/wiki/Hangman_(game)),
    was ported to [Kirigami](https://develop.kde.org/frameworks/kirigami/). [Kiten](https://kde.org/kiten),
    an for learning Japanese, has a search dialog in its Kanji Browser. [KWordQuiz](https://apps.kde.org/kwordquiz),
    a flash card app, includes audio controls for your audio files. [Kalzium](https://apps.kde.org/kalzium)
    doesn't use gradients anymore and the colors of the periodic table elements offer better contrast
    with your current color scheme.

kdenlive:
  title: Kdenlive

  description: |
    The big news for KDE's powerful video editor is support for replacing the audio or the video
    of a clip in a timeline. This new feature allows you to extract the audio of a clip, modify it
    in another program and then update the existing clip with the new audio.

    Kdenlive has further enhanced its subtitle feature, allowing you to incorporate multiple
    subtitles onto a single track. This is especially useful for videos that demand subtitles in
    several languages. You also have the option to import and export subtitles.

    Kdenlive will automatically let you know when there is an update available without even requiring
    an internet connection.

    The clip monitor will list the recently opened clips and the document checker dialog that appears
    when opening a project with missing clips, has been completely rewritten.

    ![Missing clip dialog in Kdenlive](/announcements/megarelease/6/kdenlive-missing-clip.png)

  more: More Kdenlive Features
  items:
    - name: New MTL easing modes for keyframes, like bounce, exponential, etc
      url: https://invent.kde.org/multimedia/kdenlive/-/commit/fbef83b0fb68407919e4fea9d9c384b827bef57a
    - name: Allow setting the default interpolation method for scaling on render
      url: https://invent.kde.org/multimedia/kdenlive/-/issues/1766
    - name: Added a button in timeline clips to easily enable/disable effects
      url: https://invent.kde.org/multimedia/kdenlive/-/merge_requests/448
    - name: Easily toggle clip effects in the timeline
      url: https://invent.kde.org/multimedia/kdenlive/-/merge_requests/448

kate:
  title: Kate

  description: |
    Kate is one of KDE's powerful text editora/IDEa. Kate's LSP uses a new JSON parser called
    RapidJSON and now gets completion results faster and with less memory usage. LSP support
    is also now more complete and will display dialogs when [`the server asks for it`](https://invent.kde.org/utilities/kate/-/merge_requests/1284).

  more: More Kate goodies
  items:
    - name: Automatic LSP setup for CSS, SCSS, LESS, XML, Julia and PHP
      url: https://invent.kde.org/utilities/kate/-/merge_requests/1331
    - name: Automatic DAP setup for Dart and Flutter
      url: https://invent.kde.org/utilities/kate/-/merge_requests/1243
    - name: Synchronize the scrolling for split views
      url: https://invent.kde.org/utilities/kate/-/merge_requests/1203
    - name: Text to speech support
      url: https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/606
      accessibility: true

konsole:
  title: Konsole and Yakuake

  description: |
    In Gear 24.02, apart from being ported to Qt 6, we redesigned the settings of
    Konsole and Yakuake and Konsole's rendering is now more performant with
    [50% less memory allocations](https://invent.kde.org/utilities/konsole/-/merge_requests/913).

    For people using Konsole with Chinese, Japanese or Korean languages, you will
    be happy to hear that text selection is now working correctly.

    Konsole puts every tab in a separate [cgroup](https://en.wikipedia.org/wiki/Cgroups).
    This avoids the entire application getting killed when the system is suffers from high memory load
    and needs to save some memory.

other:
  title: But wait, there's even more

  apps:
    - name: KClock
      description: KClock will pause any media sources (media players, streaming services, etc.) when an alarm or timer rings, and resumes them when dismissed.
      url: https://apps.kde.org/kclock/
    - name: KDE Connect
      description: |
        KDE Connect can now connect to other devices via Bluetooth. This may be
        less reliable than the current Wi-Fi backend but can be useful when your two
        devices are not connected to the same Wi-Fi network or when there is a
        firewall in between them.

        In terms of discoverability, KDE Connect now also
        supports [mDNS](https://en.wikipedia.org/wiki/Multicast_DNS) which is
        better at finding other devices in your network and less often blocked
        by the network configuration.
      url: https://kdeconnect.kde.org/
    - name: Kasts, Podcast Player
      description: |
        Fetching new feed updates is now dramatically faster by not
        parsing RSS feeds if they haven't changed.
    - name: Elisa, Music Player
      description: |
        The starting point of the file view can now be configured to another directory
        or storage. Various metadata dialogs got improved on mobile. We switched to a stack
        based history mode for the navigation.

community:
  title: Community Project

  description: |
    This release would have not been possible without the countless hours spent
    by contributors from all around the world. Help us create software that
    gives you full freedom and protects your privacy, and be a part of a great
    community, by becoming a supporting member of KDE.

    Your donation benefits the rest of humanity as well! With it, KDE has the
    resources to promote a spectacular catalogue of Free Software to
    professionals, [artists](/for/artists), [software developers](/for/developers),
    [children](/for/children), public institutions, [non-profits](/for/activists),
    and companies around the globe.

  more: "If you'd prefer a one-time donation, [click here](https://kde.org/community/donations/)."
---
