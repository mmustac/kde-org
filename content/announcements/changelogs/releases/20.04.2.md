---
aliases:
- ../../fulllog_releases-20.04.2
hidden: true
release: true
releaseDate: 2020-06
title: Release Service 20.04.2 Full Log Page
type: fulllog
version: 20.04.2
---

<h3><a name='akonadi' href='https://cgit.kde.org/akonadi.git'>akonadi</a> <a href='#akonadi' onclick='toggle("ulakonadi", this)'>[Hide]</a></h3>
<ul id='ulakonadi' style='display: block'>
<li>Akonadi searches for file mysql-global.conf at wrong path. <a href='http://commits.kde.org/akonadi/911c2394f7ce01d1f22c63d9159b0ee4726b7069'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/422079'>#422079</a></li>
<li>Allow ptrace for akonadiserver for mysqld_akonadi. <a href='http://commits.kde.org/akonadi/abf18d0d43b08f80ddf45a7f54455e3e34037723'>Commit.</a> </li>
<li>AppArmor DBus rules for AkonadiServer. <a href='http://commits.kde.org/akonadi/39cb8ec4faa320e9c95512db25f296b7f352e84b'>Commit.</a> </li>
</ul>
<h3><a name='akonadi-notes' href='https://cgit.kde.org/akonadi-notes.git'>akonadi-notes</a> <a href='#akonadi-notes' onclick='toggle("ulakonadi-notes", this)'>[Hide]</a></h3>
<ul id='ulakonadi-notes' style='display: block'>
<li>[NoteMessageWrapper] Use unicode also for main body. <a href='http://commits.kde.org/akonadi-notes/3166059dba523a543cabbec61831a87bf3147e6a'>Commit.</a> </li>
</ul>
<h3><a name='akregator' href='https://cgit.kde.org/akregator.git'>akregator</a> <a href='#akregator' onclick='toggle("ulakregator", this)'>[Hide]</a></h3>
<ul id='ulakregator' style='display: block'>
<li>Fix Bug 422193 - start page don't display properly with qt 5.15. <a href='http://commits.kde.org/akregator/e7a75745baae5a9fab8b3e032ba9507d16f52714'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/422193'>#422193</a></li>
</ul>
<h3><a name='cantor' href='https://cgit.kde.org/cantor.git'>cantor</a> <a href='#cantor' onclick='toggle("ulcantor", this)'>[Hide]</a></h3>
<ul id='ulcantor' style='display: block'>
<li>Fix bug with converting EpsResult to Jupyter notebook format (.ipynb). <a href='http://commits.kde.org/cantor/0b1e6e89cb260528dfe7dfce394176da1bc3ad75'>Commit.</a> </li>
<li>Fix bug (missing metadata) with converting EpsResult to Jupyter .ipynb format. <a href='http://commits.kde.org/cantor/557757a7d352280d8ca3f7e55abf2b21575c4ffd'>Commit.</a> </li>
</ul>
<h3><a name='dolphin' href='https://cgit.kde.org/dolphin.git'>dolphin</a> <a href='#dolphin' onclick='toggle("uldolphin", this)'>[Hide]</a></h3>
<ul id='uldolphin' style='display: block'>
<li>Show progress when duplicating items. <a href='http://commits.kde.org/dolphin/70b3f61e1efde2837d53a86e39368f44cbef5d19'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/422335'>#422335</a></li>
<li>Use KSycoca for updating OpenPreferredSearchTool action. <a href='http://commits.kde.org/dolphin/29e6cf01df755724a629203964c9b61cd2f383c4'>Commit.</a> See bug <a href='https://bugs.kde.org/420911'>#420911</a></li>
<li>Left-elide file/folders while keeping their extension visible. <a href='http://commits.kde.org/dolphin/99cf24c03def1c0722ba8dbd86a27b9dbc521f43'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/404955'>#404955</a></li>
</ul>
<h3><a name='elisa' href='https://cgit.kde.org/elisa.git'>elisa</a> <a href='#elisa' onclick='toggle("ulelisa", this)'>[Hide]</a></h3>
<ul id='ulelisa' style='display: block'>
<li>Fix most issues in Elisa own code detected by Clazy. <a href='http://commits.kde.org/elisa/7ebf4fd605c5a16c7a73ddad0ca96d14458a9cc1'>Commit.</a> </li>
<li>Avoid Elisa prevent session close. <a href='http://commits.kde.org/elisa/04510d1630bb8cf5837189197380a1e426f1a8a9'>Commit.</a> </li>
<li>Fix qml interface not showing up on Windows due to platform specific qml. <a href='http://commits.kde.org/elisa/e8004657ec147a3979907b83f4fe28f9e2edcf5e'>Commit.</a> </li>
</ul>
<h3><a name='incidenceeditor' href='https://cgit.kde.org/incidenceeditor.git'>incidenceeditor</a> <a href='#incidenceeditor' onclick='toggle("ulincidenceeditor", this)'>[Hide]</a></h3>
<ul id='ulincidenceeditor' style='display: block'>
<li>Fix Bug 421873: Korganizes crashes when deleting event template. <a href='http://commits.kde.org/incidenceeditor/d507a187355fa84dfcc520e0768e693d0d6e587a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/421873'>#421873</a></li>
</ul>
<h3><a name='kalarm' href='https://cgit.kde.org/kalarm.git'>kalarm</a> <a href='#kalarm' onclick='toggle("ulkalarm", this)'>[Hide]</a></h3>
<ul id='ulkalarm' style='display: block'>
<li>Fix wrong Undo/Redo being performed when selected from list. <a href='http://commits.kde.org/kalarm/961ff1aa0d7b774b792b6c8d2759f56208d01250'>Commit.</a> </li>
<li>Set resource ID in event after adding it to the resource. <a href='http://commits.kde.org/kalarm/424098c70140f5ac13231af9f7dcfd6e94d39e28'>Commit.</a> </li>
<li>Bug 412181: Fix occasional crash on opening alarm edit dialogue. <a href='http://commits.kde.org/kalarm/815c32b1c5de414b2e93dd03aa9b1e60818f499d'>Commit.</a> </li>
<li>Fix failure to execute command alarms in a terminal window. <a href='http://commits.kde.org/kalarm/625d7a1de1f8c058930f7fe2886901aadca8dd03'>Commit.</a> </li>
<li>QFile -> QSaveFile to reliably save autostart file. <a href='http://commits.kde.org/kalarm/2d2d0a365d346932476427133b779eefaf771d52'>Commit.</a> </li>
<li>Fix failure to set no-autostart for non-KDE desktops, if a writable autostart directory exists. <a href='http://commits.kde.org/kalarm/ef896972807ec2143c36fcccbc1778ba25bac06f'>Commit.</a> </li>
<li>Fix evaluation of empty status for calendars. <a href='http://commits.kde.org/kalarm/f948be0e86a84115cb6717464ecee2f9395cf3d8'>Commit.</a> </li>
<li>Bug 417108: Make multiple KAlarm invocations work with latest Qt, Frameworks. <a href='http://commits.kde.org/kalarm/c907ac805bfc605e29a79455d78aad0dde4acf71'>Commit.</a> </li>
</ul>
<h3><a name='kcalutils' href='https://cgit.kde.org/kcalutils.git'>kcalutils</a> <a href='#kcalutils' onclick='toggle("ulkcalutils", this)'>[Hide]</a></h3>
<ul id='ulkcalutils' style='display: block'>
<li>Remove time-of-day from Event Viewer display of all-day to-dos. <a href='http://commits.kde.org/kcalutils/c12e9b3709d3e00a979b60d894c4e3f9e575942c'>Commit.</a> </li>
</ul>
<h3><a name='kdeconnect-kde' href='https://cgit.kde.org/kdeconnect-kde.git'>kdeconnect-kde</a> <a href='#kdeconnect-kde' onclick='toggle("ulkdeconnect-kde", this)'>[Hide]</a></h3>
<ul id='ulkdeconnect-kde' style='display: block'>
<li>Fix findthisdevice sound URL. <a href='http://commits.kde.org/kdeconnect-kde/4df8dfc882ea3d5acc4ff0c97090fdaffd5ce3f6'>Commit.</a> </li>
<li>Remove windows CI in stable too. <a href='http://commits.kde.org/kdeconnect-kde/dc9da25fc9b71370633d05f482003911b24b9436'>Commit.</a> </li>
</ul>
<h3><a name='kdenetwork-filesharing' href='https://cgit.kde.org/kdenetwork-filesharing.git'>kdenetwork-filesharing</a> <a href='#kdenetwork-filesharing' onclick='toggle("ulkdenetwork-filesharing", this)'>[Hide]</a></h3>
<ul id='ulkdenetwork-filesharing' style='display: block'>
<li>Don't disable table view when "Enable guests" checkbox is checked. <a href='http://commits.kde.org/kdenetwork-filesharing/56d7727dcf71df68bb95320e218296c8f4933428'>Commit.</a> </li>
</ul>
<h3><a name='kdenlive' href='https://cgit.kde.org/kdenlive.git'>kdenlive</a> <a href='#kdenlive' onclick='toggle("ulkdenlive", this)'>[Hide]</a></h3>
<ul id='ulkdenlive' style='display: block'>
<li>Fix crash on disabled clip undo / redo insert. <a href='http://commits.kde.org/kdenlive/7dd81585a0b1ba99dcef23e1afdb48721e83c995'>Commit.</a> </li>
<li>Cleaner monitor ticks in ruler. <a href='http://commits.kde.org/kdenlive/ba1f644031fe72c65e0b5487a4f3cbe3463111fc'>Commit.</a> </li>
<li>Fix lag caused by monitor ruler code when resizing the last clip of a long project. <a href='http://commits.kde.org/kdenlive/fddf890fda646b3418426c79c2ef3c5eeb7b8354'>Commit.</a> </li>
<li>Fix all icons failing to load. <a href='http://commits.kde.org/kdenlive/c993b6ce6f1a195ffba0d97d4aec2e2555663fcc'>Commit.</a> </li>
<li>Switch subtitle filter to use av.filename parameter that is now better supported in MLT. <a href='http://commits.kde.org/kdenlive/a4a3a73aecd092e3cc4afaca6e043ec700847588'>Commit.</a> </li>
<li>Correctly initialize timeline snapping. <a href='http://commits.kde.org/kdenlive/6ed1f5f0778b43328e66ad7bd41d249fb879aace'>Commit.</a> </li>
<li>Fix loop clip always disabled. <a href='http://commits.kde.org/kdenlive/d906769bfb5ab4ee47ba794f7f87a057599126f1'>Commit.</a> </li>
<li>Fix moving single clip in group on another track with meta. <a href='http://commits.kde.org/kdenlive/c77cb396fff22469ce9513e11207d986d41cbc8c'>Commit.</a> </li>
<li>Fix crash on project open and possible guides loss on recovery. <a href='http://commits.kde.org/kdenlive/9a06f627996e3a90e9b5854de4d2af00779dca9b'>Commit.</a> </li>
<li>When resizing a geometry effect, like transform, keep item centered. <a href='http://commits.kde.org/kdenlive/141c8848e5e63ecf0208d982fd2c23a65f3c8f99'>Commit.</a> </li>
<li>Ensure newly created folder is active so that added clips go in it. <a href='http://commits.kde.org/kdenlive/d38c2549d9328ef19703feef3ec3442c31c13b2f'>Commit.</a> </li>
<li>Make effect keyframe view follow cursor when zoomed. <a href='http://commits.kde.org/kdenlive/300b03ac7cdb1b18e47a72f774ef0c15d485d39d'>Commit.</a> </li>
<li>Clip proxy: drop data and subtitle streams. <a href='http://commits.kde.org/kdenlive/2a792bfe8dd8973aba0446bae966c026f6ad95da'>Commit.</a> </li>
<li>Fix cannot paste clip from project containing more tracks. <a href='http://commits.kde.org/kdenlive/f29a3194af5ea0b621534d63b5771d3575bd7d83'>Commit.</a> </li>
<li>Various timeline preview fixes (resize clip did not invalidate, undo/redo sometimes restoring invalid chunks). <a href='http://commits.kde.org/kdenlive/79d33eaae41f4da7fa65bf3ea29fa0c71f580820'>Commit.</a> </li>
<li>Archive project: fix clip with speed effect url not updated, resulting in missing clip message. <a href='http://commits.kde.org/kdenlive/63175b60f97f73ce827b234effaa53ce731a4c8e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/422135'>#422135</a></li>
<li>Reset current project name if opening backup file fails to prevent erasing original. <a href='http://commits.kde.org/kdenlive/7c6e28eae8609a8ae877f84abc451dc59c6a615e'>Commit.</a> </li>
<li>Keyframe view zoom bar: don't allow invalid size (x > y). <a href='http://commits.kde.org/kdenlive/0ec236206d1cf0ef3d97c41787da2f6ba9834d68'>Commit.</a> </li>
<li>Ensure no double entries in generators menu. <a href='http://commits.kde.org/kdenlive/263d474b613e6e2ec839fa091c00c83f7d2e3084'>Commit.</a> </li>
<li>Fix sort order not correctly restored on open. <a href='http://commits.kde.org/kdenlive/089002467c6174a682c60756a9727ba5e7bd52ed'>Commit.</a> </li>
<li>Update bin sort menu when clicking in Bin headers. <a href='http://commits.kde.org/kdenlive/4c16d144b649b8b7d065bfaccfa3a08da428bea1'>Commit.</a> </li>
<li>Fix set audio reference incorrectly disabled on AV grouped clips. <a href='http://commits.kde.org/kdenlive/ec3dd32136e465ca648d8dd3d3806763a00a0ce2'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/421669'>#421669</a></li>
<li>Fix keyframe view imprecision on high zoom. <a href='http://commits.kde.org/kdenlive/be349045307d9b4a56704e653a0fd771e171caf6'>Commit.</a> </li>
<li>Fix clicking on guide not seeking to exact guide position. <a href='http://commits.kde.org/kdenlive/845a55dfcc0b2e25b7abffcd8c82704f392cbb97'>Commit.</a> </li>
<li>Fix timeline ruler incorrectly cut on small zoom. <a href='http://commits.kde.org/kdenlive/bb99e9126c25034d08fc235ab1e2317c84f992d0'>Commit.</a> </li>
<li>Fix crash on extract clip with shortcut. <a href='http://commits.kde.org/kdenlive/f093dddda10a54fab15c973963d81e81861d33e0'>Commit.</a> </li>
<li>Ensure we start with the correct fps when default profile is not 25 fps. <a href='http://commits.kde.org/kdenlive/549f89545ef7fdad94dd0355d74bf2839acb7b99'>Commit.</a> See bug <a href='https://bugs.kde.org/420580'>#420580</a></li>
<li>Improve timeline position on zoom. <a href='http://commits.kde.org/kdenlive/c46b8b3c89711afef0e06183072f6623d0f81a2c'>Commit.</a> </li>
<li>Locate clip: select file in file manager. <a href='http://commits.kde.org/kdenlive/acbe4fe517cd3daa0494d42c0dc9701089ddce14'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/421365'>#421365</a></li>
</ul>
<h3><a name='kdepim-addons' href='https://cgit.kde.org/kdepim-addons.git'>kdepim-addons</a> <a href='#kdepim-addons' onclick='toggle("ulkdepim-addons", this)'>[Hide]</a></h3>
<ul id='ulkdepim-addons' style='display: block'>
<li>Fix crash when encountering pkpass types we cannot render. <a href='http://commits.kde.org/kdepim-addons/51be4d2a71be17f66a6535f8a414505457117939'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/422325'>#422325</a></li>
<li>Fix potential problem with qt5.15. <a href='http://commits.kde.org/kdepim-addons/5841e23f7a51d4cdfbc5c3a1ccd4a14c40d1dae7'>Commit.</a> </li>
</ul>
<h3><a name='kget' href='https://cgit.kde.org/kget.git'>kget</a> <a href='#kget' onclick='toggle("ulkget", this)'>[Hide]</a></h3>
<ul id='ulkget' style='display: block'>
<li>Remove duplicate % sign in progress bar. <a href='http://commits.kde.org/kget/b832e046013c0bf50cb35d683fd22abf89b841af'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/421471'>#421471</a></li>
</ul>
<h3><a name='khelpcenter' href='https://cgit.kde.org/khelpcenter.git'>khelpcenter</a> <a href='#khelpcenter' onclick='toggle("ulkhelpcenter", this)'>[Hide]</a></h3>
<ul id='ulkhelpcenter' style='display: block'>
<li>Revert 'Contents tree: add fallback to URL without fragment'. <a href='http://commits.kde.org/khelpcenter/a5e28c6e064a170a5a0ceb70d933c33d5951e7fe'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/422089'>#422089</a></li>
</ul>
<h3><a name='kig' href='https://cgit.kde.org/kig.git'>kig</a> <a href='#kig' onclick='toggle("ulkig", this)'>[Hide]</a></h3>
<ul id='ulkig' style='display: block'>
<li>Cmake: remove duplicate lines. <a href='http://commits.kde.org/kig/f3f5d9ae505f8f63911e8f4b83ca2c1a608e668c'>Commit.</a> </li>
<li>Remove old script for releasing from CVS. <a href='http://commits.kde.org/kig/e31a8926e0eb135f85bac38dadab447106524636'>Commit.</a> </li>
<li>Remove the old kfile plugins. <a href='http://commits.kde.org/kig/a787359daa79bf5e643225fe45e2be5237dd08ee'>Commit.</a> </li>
</ul>
<h3><a name='kio-extras' href='https://cgit.kde.org/kio-extras.git'>kio-extras</a> <a href='#kio-extras' onclick='toggle("ulkio-extras", this)'>[Hide]</a></h3>
<ul id='ulkio-extras' style='display: block'>
<li>Fix kio-extras compilation with -DQT_NO_CAST_TO_ASCII. <a href='http://commits.kde.org/kio-extras/73c53d987dcb825a0b21d5441eb665c0382a42c4'>Commit.</a> </li>
<li>Smb: reshuffle discovery systems. <a href='http://commits.kde.org/kio-extras/4e41727c9626e714477b91cf14bd9a1efa1e8c8b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/421624'>#421624</a></li>
<li>Sftp: map sftp_open error to kio error. <a href='http://commits.kde.org/kio-extras/07e44cb1b53646e3867137aa92eefca818303e8e'>Commit.</a> </li>
<li>Sftp: break large writes into multiple requests. <a href='http://commits.kde.org/kio-extras/1df6174834bb3a126f630d4477bcdbda95f45a8b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/404890'>#404890</a></li>
</ul>
<h3><a name='kmail' href='https://cgit.kde.org/kmail.git'>kmail</a> <a href='#kmail' onclick='toggle("ulkmail", this)'>[Hide]</a></h3>
<ul id='ulkmail' style='display: block'>
<li>Add obviously missing assignment (of mimeype name). <a href='http://commits.kde.org/kmail/2e0971c20ee0a0a74b486dd2b24407a496f5bf71'>Commit.</a> </li>
</ul>
<h3><a name='kmime' href='https://cgit.kde.org/kmime.git'>kmime</a> <a href='#kmime' onclick='toggle("ulkmime", this)'>[Hide]</a></h3>
<ul id='ulkmime' style='display: block'>
<li>Add CRtoLF method. <a href='http://commits.kde.org/kmime/bd6207685644052c4c5a6769dca4a43bf1c2e112'>Commit.</a> </li>
<li>Add autotest for bug 392239. <a href='http://commits.kde.org/kmime/65372b5304a504446259ec1504aa5e2856105e46'>Commit.</a> See bug <a href='https://bugs.kde.org/392239'>#392239</a></li>
<li>Fix parsing headers with a name that is a prefix of a well-known header. <a href='http://commits.kde.org/kmime/b57079dd674ab737ebb4cee04b755b4ab58e6d9f'>Commit.</a> </li>
</ul>
<h3><a name='kmplot' href='https://cgit.kde.org/kmplot.git'>kmplot</a> <a href='#kmplot' onclick='toggle("ulkmplot", this)'>[Hide]</a></h3>
<ul id='ulkmplot' style='display: block'>
<li>Get rid of pixelation on HiDPI displays. <a href='http://commits.kde.org/kmplot/5fa5072f644199b35fd956b98973bcb5e11bf23a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/419527'>#419527</a></li>
</ul>
<h3><a name='konsole' href='https://cgit.kde.org/konsole.git'>konsole</a> <a href='#konsole' onclick='toggle("ulkonsole", this)'>[Hide]</a></h3>
<ul id='ulkonsole' style='display: block'>
<li>Fix lambda capture 'this' is not used. <a href='http://commits.kde.org/konsole/7d1e286102d438a9305b084c93ae2d5076f50ee0'>Commit.</a> </li>
<li>Notify current cursor position might be changed to input methods. <a href='http://commits.kde.org/konsole/399a551b02d8f33ea553984ab715a3d0762fffce'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/420799'>#420799</a></li>
<li>Fix SearchTask memleak. <a href='http://commits.kde.org/konsole/197e8f22fd39c2788bc127e426da7899b49bf9d8'>Commit.</a> </li>
<li>Fix override switch-to-tab action in Yakuake. <a href='http://commits.kde.org/konsole/13697e07d9290f3c14d57ec27884bb9756999f27'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/420503'>#420503</a></li>
<li>[SessionController] Fix crash caused by text encoding menu. <a href='http://commits.kde.org/konsole/27ee6a27380c91b7e98d9fa09915a9d33ee03ea1'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/419526'>#419526</a></li>
<li>Fix konsolepart segfault when closing after showing context menu. <a href='http://commits.kde.org/konsole/d40e3c72f2ad9262d6028bc62bc2837067b824a0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/415762'>#415762</a></li>
<li>Fix crash when closing session in KonsolePart via menu. <a href='http://commits.kde.org/konsole/76f3764b47985f5fe50532d46b2e043ba8e31e4a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/420817'>#420817</a>. Fixes bug <a href='https://bugs.kde.org/420695'>#420695</a>. Fixes bug <a href='https://bugs.kde.org/415762'>#415762</a></li>
</ul>
<h3><a name='kontact' href='https://cgit.kde.org/kontact.git'>kontact</a> <a href='#kontact' onclick='toggle("ulkontact", this)'>[Hide]</a></h3>
<ul id='ulkontact' style='display: block'>
<li>Fix Bug 422193 - start page don't display properly with qt 5.15. <a href='http://commits.kde.org/kontact/1ebcda0aafa28c0a06bf241286ce58fb18662600'>Commit.</a> See bug <a href='https://bugs.kde.org/422193'>#422193</a></li>
</ul>
<h3><a name='korganizer' href='https://cgit.kde.org/korganizer.git'>korganizer</a> <a href='#korganizer' onclick='toggle("ulkorganizer", this)'>[Hide]</a></h3>
<ul id='ulkorganizer' style='display: block'>
<li>Korgac: re-add 10 days check. <a href='http://commits.kde.org/korganizer/84494bcb1db225a87f96265f6e41123c3ad95a00'>Commit.</a> </li>
<li>Korgac: on first run, show alarms from the last 10 days. <a href='http://commits.kde.org/korganizer/3e55b0494f6a86d760ba61feeb079d1d8259a21b'>Commit.</a> </li>
</ul>
<h3><a name='kpat' href='https://cgit.kde.org/kpat.git'>kpat</a> <a href='#kpat' onclick='toggle("ulkpat", this)'>[Hide]</a></h3>
<ul id='ulkpat' style='display: block'>
<li>Change some verbose debugs from using ifndef NDEBUG. <a href='http://commits.kde.org/kpat/9b1d9abdc56f5037e713e1fdefa22e58d5c2087a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/407854'>#407854</a></li>
</ul>
<h3><a name='kpimtextedit' href='https://cgit.kde.org/kpimtextedit.git'>kpimtextedit</a> <a href='#kpimtextedit' onclick='toggle("ulkpimtextedit", this)'>[Hide]</a></h3>
<ul id='ulkpimtextedit' style='display: block'>
<li>Add more autotest. <a href='http://commits.kde.org/kpimtextedit/1b523d7597770aaaaf1f7a70ce9bbb6adc9e003c'>Commit.</a> </li>
<li>Continue to fix Bug 421908 - HTML content - mangled generation. <a href='http://commits.kde.org/kpimtextedit/8ebf1186705f14fc7f015d12c7d13998afa872b8'>Commit.</a> See bug <a href='https://bugs.kde.org/421908'>#421908</a></li>
<li>Start to fix HTML content - mangled generation. <a href='http://commits.kde.org/kpimtextedit/f8dd428f24e7c41a3b01346766ab001b96ce98df'>Commit.</a> See bug <a href='https://bugs.kde.org/421908'>#421908</a></li>
</ul>
<h3><a name='ksudoku' href='https://cgit.kde.org/ksudoku.git'>ksudoku</a> <a href='#ksudoku' onclick='toggle("ulksudoku", this)'>[Hide]</a></h3>
<ul id='ulksudoku' style='display: block'>
<li>Fix saving over an existing save file. <a href='http://commits.kde.org/ksudoku/5a4871302cb64afc2475083e97e4ac5bf3a3e400'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/421332'>#421332</a></li>
</ul>
<h3><a name='ksystemlog' href='https://cgit.kde.org/ksystemlog.git'>ksystemlog</a> <a href='#ksystemlog' onclick='toggle("ulksystemlog", this)'>[Hide]</a></h3>
<ul id='ulksystemlog' style='display: block'>
<li>Disconnect `QtabWidget::currentChanged` signal on window close event. <a href='http://commits.kde.org/ksystemlog/eeab31a116535efc6d4757f027897265736ed161'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/409375'>#409375</a></li>
</ul>
<h3><a name='libkomparediff2' href='https://cgit.kde.org/libkomparediff2.git'>libkomparediff2</a> <a href='#libkomparediff2' onclick='toggle("ullibkomparediff2", this)'>[Hide]</a></h3>
<ul id='ullibkomparediff2' style='display: block'>
<li>Fix broken error emission if blendOriginalIntoModelList went wrong. <a href='http://commits.kde.org/libkomparediff2/6710d9be50f454c001ff4adfcd93cbd71a953002'>Commit.</a> </li>
</ul>
<h3><a name='libksieve' href='https://cgit.kde.org/libksieve.git'>libksieve</a> <a href='#libksieve' onclick='toggle("ullibksieve", this)'>[Hide]</a></h3>
<ul id='ullibksieve' style='display: block'>
<li>Fix crash when we close sieveeditor. <a href='http://commits.kde.org/libksieve/7454926a18d5f8a5ec4874ebc51e3586f0e09baa'>Commit.</a> </li>
<li>Fix autoload image by default. <a href='http://commits.kde.org/libksieve/aef9fbf3e670f82ca06173dcdcbc59a4f3c1f605'>Commit.</a> </li>
</ul>
<h3><a name='messagelib' href='https://cgit.kde.org/messagelib.git'>messagelib</a> <a href='#messagelib' onclick='toggle("ulmessagelib", this)'>[Hide]</a></h3>
<ul id='ulmessagelib' style='display: block'>
<li>Fix save keys. We can't have multi keys. <a href='http://commits.kde.org/messagelib/d3b5ffa8f9937c67fe4ade64fad0ee84293f32fb'>Commit.</a> </li>
<li>Allow to load cid element. <a href='http://commits.kde.org/messagelib/429cdb48f28d699d2aa83a19d6b07143501f7b69'>Commit.</a> </li>
<li>Add cidschemehandler. <a href='http://commits.kde.org/messagelib/2e2bcd3fde6396b28338564cee4e3fd86cefb6fb'>Commit.</a> </li>
<li>Continue to fix loading image from cid scheme. <a href='http://commits.kde.org/messagelib/5b95039188921d9fe198cabe51f75989626ac9d4'>Commit.</a> </li>
<li>Make sure that resourceType is a QWebEngineUrlRequestInfo::ResourceTypeImage. <a href='http://commits.kde.org/messagelib/249c03017d62851cba8616ad1233c2c3d3c8d51c'>Commit.</a> </li>
<li>Fix Start to fix HTML content - mangled generation. <a href='http://commits.kde.org/messagelib/e0d7ef9b891101961cd4d0d0a36f137fa30334a8'>Commit.</a> See bug <a href='https://bugs.kde.org/421908'>#421908</a></li>
<li>Disable line too. <a href='http://commits.kde.org/messagelib/84a46f4d25f9fcf9ba089f54082bbfd49b2cfd61'>Commit.</a> </li>
<li>Fix load header with qt5.15. <a href='http://commits.kde.org/messagelib/1f2548805df60707ffba2bba27d35d441232d140'>Commit.</a> </li>
</ul>
<h3><a name='okular' href='https://cgit.kde.org/okular.git'>okular</a> <a href='#okular' onclick='toggle("ulokular", this)'>[Hide]</a></h3>
<ul id='ulokular' style='display: block'>
<li>Allow unicode filenames to work on Windows, too. <a href='http://commits.kde.org/okular/3ad9fc7028bba05f6d9fbee7f5e3c18bcdd42264'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/422500'>#422500</a></li>
<li>Pdf: fix memory leak when listing fonts. <a href='http://commits.kde.org/okular/8460d34f34477a2ba30e6f838838e34c797b3e5c'>Commit.</a> </li>
<li>Don't attach to another instance if we're going to exit. <a href='http://commits.kde.org/okular/eee401946bf6245b2334164dfb47008f6ba49c26'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/419982'>#419982</a></li>
<li>Fix indentation. <a href='http://commits.kde.org/okular/98570d3633851a64781d0a989da26ef028553c13'>Commit.</a> </li>
<li>Fix ZoomIn getting stuck when on facing pages. <a href='http://commits.kde.org/okular/040e11c52baa95566f3700cda784bd81772eae54'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/420824'>#420824</a></li>
<li>CI: Remove python-yaml. <a href='http://commits.kde.org/okular/a0d6a377cb0cc1704c1a9f93b626a6e6dc136907'>Commit.</a> </li>
<li>Disable scrolling overshoot. <a href='http://commits.kde.org/okular/ca1d62372909f6310f437985cc7aab301c1b7165'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/421482'>#421482</a></li>
<li>Fix filtering items in the contents treeview. <a href='http://commits.kde.org/okular/793f1692db244621a0a603a2a7c404cc1757b9ab'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/421469'>#421469</a></li>
<li>CHM: Fix memory leak every time we open a file. <a href='http://commits.kde.org/okular/bb8de23468c8475a96691f46c100bae5484301fc'>Commit.</a> </li>
</ul>
<h3><a name='pim-data-exporter' href='https://cgit.kde.org/pim-data-exporter.git'>pim-data-exporter</a> <a href='#pim-data-exporter' onclick='toggle("ulpim-data-exporter", this)'>[Hide]</a></h3>
<ul id='ulpim-data-exporter' style='display: block'>
<li>We need to ckeck keyList not groupList. Fix importing folders settings. <a href='http://commits.kde.org/pim-data-exporter/c26fc8d5c7c492605b3c3742f16cc670b4202422'>Commit.</a> </li>
<li>Fix import filter. <a href='http://commits.kde.org/pim-data-exporter/8b1ae0630d66b94417fd69ca29fb6dcdc2d4a5a5'>Commit.</a> </li>
<li>Fix restore "Automatic Add Contacts". <a href='http://commits.kde.org/pim-data-exporter/85b35625449f0133bfad503290097dba2bab0b65'>Commit.</a> </li>
<li>Port identity too. <a href='http://commits.kde.org/pim-data-exporter/771f7bde70374845ea8bec33f25ef72ac28d7771'>Commit.</a> </li>
<li>Fix generate identity with uniqueIdentityName. <a href='http://commits.kde.org/pim-data-exporter/ead08a1ea68e65d4f3b3fafffdb97973ab84cb05'>Commit.</a> </li>
<li>Use directly KIdentityManagement::IdentityManager. <a href='http://commits.kde.org/pim-data-exporter/2fdfc5755e4518e0fdf95e9a40ac3243e1ae2efa'>Commit.</a> </li>
<li>Don't create twice resource. <a href='http://commits.kde.org/pim-data-exporter/25f9bade73fdd24374fa1817db79e0e790ed880d'>Commit.</a> </li>
<li>Fix import notes. <a href='http://commits.kde.org/pim-data-exporter/e961ea69ce61db114760e3fbd2ac587301fe84cb'>Commit.</a> </li>
<li>Fix import note. <a href='http://commits.kde.org/pim-data-exporter/7f2bd934a29ab3308fa8bf62f373ca5bc0157d02'>Commit.</a> </li>
<li>Use correct path for storing mail data. <a href='http://commits.kde.org/pim-data-exporter/55e2cf778024f5be71db1df927009204bcfb9d51'>Commit.</a> </li>
</ul>
<h3><a name='step' href='https://cgit.kde.org/step.git'>step</a> <a href='#step' onclick='toggle("ulstep", this)'>[Hide]</a></h3>
<ul id='ulstep' style='display: block'>
<li>I18n: use dummy source for objinfo and example catalogs. <a href='http://commits.kde.org/step/40735c939fde6e822fb4f72515b0ec6ab8579e50'>Commit.</a> </li>
</ul>
<h3><a name='umbrello' href='https://cgit.kde.org/umbrello.git'>umbrello</a> <a href='#umbrello' onclick='toggle("ulumbrello", this)'>[Hide]</a></h3>
<ul id='ulumbrello' style='display: block'>
<li>Address https://bugs.kde.org/show_bug.cgi?id=56184#c94. <a href='http://commits.kde.org/umbrello/c1aadd7c3333332c220eb8bce90fcdd4a0ad13e9'>Commit.</a> See bug <a href='https://bugs.kde.org/56184'>#56184</a></li>
<li>Fix 'Umbrello code importer does not import c++ header file wrapped with __cplusplus macro'. <a href='http://commits.kde.org/umbrello/592064a33e5563575ae946ca44366c4bbac1eec7'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/422356'>#422356</a></li>
</ul>
