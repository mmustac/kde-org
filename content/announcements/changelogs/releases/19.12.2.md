---
aliases:
- ../../fulllog_releases-19.12.2
hidden: true
release: true
releaseDate: 2020-02
title: Release Service 19.12.2 Full Log Page
type: fulllog
version: 19.12.2
---

<h3><a name='artikulate' href='https://cgit.kde.org/artikulate.git'>artikulate</a> <a href='#artikulate' onclick='toggle("ulartikulate", this)'>[Hide]</a></h3>
<ul id='ulartikulate' style='display: block'>
<li>Fix download page background color. <a href='http://commits.kde.org/artikulate/4c77415be00a53e0aa4b12b90423a3af15d5bd29'>Commit.</a> </li>
<li>Fix DownloadPage use of KNS.ItemsModel. <a href='http://commits.kde.org/artikulate/3cf71f8383717229f451cb7917d067cd5ca656ef'>Commit.</a> </li>
</ul>
<h3><a name='dolphin' href='https://cgit.kde.org/dolphin.git'>dolphin</a> <a href='#dolphin' onclick='toggle("uldolphin", this)'>[Hide]</a></h3>
<ul id='uldolphin' style='display: block'>
<li>Change drop indicator color. <a href='http://commits.kde.org/dolphin/178eb5927c8699c89b3d91b0ebffc00ddaa11a58'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/415010'>#415010</a></li>
<li>Hide tooltip instantly on filter change. <a href='http://commits.kde.org/dolphin/f729f6f5b1d1bbc1b4fab2f925f16f2c2c7128d5'>Commit.</a> </li>
</ul>
<h3><a name='elisa' href='https://cgit.kde.org/elisa.git'>elisa</a> <a href='#elisa' onclick='toggle("ulelisa", this)'>[Hide]</a></h3>
<ul id='ulelisa' style='display: block'>
<li>Fix warnings due to missing enum values in two switch statements. <a href='http://commits.kde.org/elisa/da2b7542f6871b2fe89a3286604c6cdaff863ea4'>Commit.</a> </li>
<li>Implement loading tracks from genre. <a href='http://commits.kde.org/elisa/8e1967d7a91057a04a30383987803eab601da397'>Commit.</a> </li>
<li>Disable playlist animations to fix display corruption. <a href='http://commits.kde.org/elisa/1691274e52b8a76f760bad1333746fde4f5973c7'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/398093'>#398093</a>. Fixes bug <a href='https://bugs.kde.org/406524'>#406524</a></li>
<li>Do not show cover when creating a radio. <a href='http://commits.kde.org/elisa/df7bc03ab325f5c7e22d9fc11438cb5bdac4a17b'>Commit.</a> </li>
<li>Fix radio metadata dialog. <a href='http://commits.kde.org/elisa/a8df563c98e35da382562f191646fb70435251b6'>Commit.</a> </li>
<li>Fix assert when displaying list of radios. <a href='http://commits.kde.org/elisa/e4448985f01d8b3f948da7a401f34bd6a0f23624'>Commit.</a> </li>
<li>Fix loading track metadata in TrackMetaDataModel. <a href='http://commits.kde.org/elisa/3a103b02fb47feea0c7fd35cb5fb4999ca090b5b'>Commit.</a> </li>
<li>Remove needed import in GridBrowserDelegate.qml. <a href='http://commits.kde.org/elisa/5fb8a944f8acbc6ed7c517ca409e6f1759f22a5d'>Commit.</a> </li>
<li>Allow database to contain and get tracks without metadata. <a href='http://commits.kde.org/elisa/0c042a86095fdf8550ca1211dd9d5db73156b34b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/415180'>#415180</a></li>
<li>Do not reimport tracks without metadata at start. <a href='http://commits.kde.org/elisa/6fb031bcf82673fc530ad23bb66c411344075d76'>Commit.</a> </li>
<li>Fix insertion in playlist of list of elements not being tracks. <a href='http://commits.kde.org/elisa/433490bac11707b8dbbee8c3888179fcb8789dd7'>Commit.</a> </li>
<li>Let playlist get the full track data when possible. <a href='http://commits.kde.org/elisa/ce3df0bbdc4790f4bc45b18d6daff36a9a2a96f7'>Commit.</a> See bug <a href='https://bugs.kde.org/398093'>#398093</a></li>
<li>Add some debug output. <a href='http://commits.kde.org/elisa/f37e0eb4f7ef3e3a1d5041d80679a9ba65d4186a'>Commit.</a> </li>
<li>Do not signal property change for position via MPRIS. <a href='http://commits.kde.org/elisa/baa589f73411bb41ad9319b24fc79eae66667d41'>Commit.</a> See bug <a href='https://bugs.kde.org/416179'>#416179</a></li>
<li>Fix enqueue from FileBrowserView with Qt 5.14. <a href='http://commits.kde.org/elisa/4a47e32dadbfaf0066bb5e700ea1f2e29987f5e7'>Commit.</a> </li>
<li>Fix enqueue not working for views that display list of tracks. <a href='http://commits.kde.org/elisa/6d0454526fe4be1892812db2c0bc324f6a5669b7'>Commit.</a> </li>
<li>Fix problems with wrong type passed from qml to c++. <a href='http://commits.kde.org/elisa/fcb76aec4fbd74c218c4e6e17c20a9e3c433f55e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/415827'>#415827</a></li>
<li>Remove code duplication by moving signal to base class. <a href='http://commits.kde.org/elisa/f8cd299e7bacec2843688dc9e05fd7cb51ab4160'>Commit.</a> </li>
<li>Fixc docs location in AppData file. <a href='http://commits.kde.org/elisa/6f470597d57356bdb8c22f4418b54d001b522dab'>Commit.</a> </li>
</ul>
<h3><a name='gwenview' href='https://cgit.kde.org/gwenview.git'>gwenview</a> <a href='#gwenview' onclick='toggle("ulgwenview", this)'>[Hide]</a></h3>
<ul id='ulgwenview' style='display: block'>
<li>Fix divide-by-zero crash. <a href='http://commits.kde.org/gwenview/fdd5ecc344edc7444bd57a080c3d9ec91b245592'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/414631'>#414631</a></li>
</ul>
<h3><a name='k3b' href='https://cgit.kde.org/k3b.git'>k3b</a> <a href='#k3b' onclick='toggle("ulk3b", this)'>[Hide]</a></h3>
<ul id='ulk3b' style='display: block'>
<li>Fix adding multiple symlinks to a data project. <a href='http://commits.kde.org/k3b/3cd2d2111fd7d897edbc6af202fba07bc819174a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/416487'>#416487</a></li>
</ul>
<h3><a name='kaccounts-providers' href='https://cgit.kde.org/kaccounts-providers.git'>kaccounts-providers</a> <a href='#kaccounts-providers' onclick='toggle("ulkaccounts-providers", this)'>[Hide]</a></h3>
<ul id='ulkaccounts-providers' style='display: block'>
<li>Remove the extra comma. <a href='http://commits.kde.org/kaccounts-providers/5952bf25358b8b2545cfdeb898461f82a7b17971'>Commit.</a> </li>
<li>Google provider: limit requested OAuth scopes. <a href='http://commits.kde.org/kaccounts-providers/0a71da4e3caae0defe200a85954fc7e2012010c1'>Commit.</a> </li>
</ul>
<h3><a name='kalarm' href='https://cgit.kde.org/kalarm.git'>kalarm</a> <a href='#kalarm' onclick='toggle("ulkalarm", this)'>[Hide]</a></h3>
<ul id='ulkalarm' style='display: block'>
<li>Fix resourcesPopulated() signal not being emitted if disabled resources exist. <a href='http://commits.kde.org/kalarm/45ed2f802ae68a74c7950c003644da8bfcdd766c'>Commit.</a> </li>
<li>Fix message captions when called from KAlarmApp constructor. <a href='http://commits.kde.org/kalarm/6feae011f577d0ce3bf082b8c51c9864d470c282'>Commit.</a> </li>
<li>Fix message captions when called from KAlarmApp constructor. <a href='http://commits.kde.org/kalarm/cb7ed20257191b14bcbb87266dc8df35266ca359'>Commit.</a> </li>
<li>Fix message captions when called from KAlarmApp constructor. <a href='http://commits.kde.org/kalarm/4a533142a6908fa72b820e2093e7d284510bac18'>Commit.</a> </li>
<li>Fix message captions when called from KAlarmApp constructor. <a href='http://commits.kde.org/kalarm/9d44f9eb5bc337ecdb3a3019290cd8b8e5f004c9'>Commit.</a> </li>
<li>Fix crash if mainMainWindow() is called from KAlarmApp constructor. <a href='http://commits.kde.org/kalarm/d8b81358ff6467ffe4484d7fc464b94a0e8db9c7'>Commit.</a> </li>
<li>Add Show/Hide Menubar menu option; change New Email Alarm shortcut. <a href='http://commits.kde.org/kalarm/eaf461396e05038523ebcdd3699f5afc6642867d'>Commit.</a> </li>
</ul>
<h3><a name='kate' href='https://cgit.kde.org/kate.git'>kate</a> <a href='#kate' onclick='toggle("ulkate", this)'>[Hide]</a></h3>
<ul id='ulkate' style='display: block'>
<li>Fix usage of QtSingleApplication on Windows and Apple. <a href='http://commits.kde.org/kate/a6d1c8d3872c01c3610aadfc6c297d611f99a691'>Commit.</a> </li>
<li>Snippets knsrc file: change ProvidersUrl to non-depr. autoconfig.kde.org. <a href='http://commits.kde.org/kate/de5a43c895d2f34670779916df8544c6a455e7c2'>Commit.</a> </li>
<li>Import further optimized SVG by ndavis from breeze-icons. <a href='http://commits.kde.org/kate/2216e09fcc0b9e8d8707ad0d36e612817fd2a9b1'>Commit.</a> </li>
<li>Import further optimized SVG by ndavis from breeze-icons. <a href='http://commits.kde.org/kate/8cc6391a59a0af5b94fd29a3caeceda73ade50c9'>Commit.</a> </li>
<li>Cut PNG part from SVG. <a href='http://commits.kde.org/kate/f7e22896175209532cfa7defc3bb1514e222db1a'>Commit.</a> </li>
<li>Update generated pngs. <a href='http://commits.kde.org/kate/f795188b0deed2b1e517a9a9b482e3e728d2bd3a'>Commit.</a> </li>
<li>Try to create a breeze icon. <a href='http://commits.kde.org/kate/0897c4ce3abd794f44658cf3d3d615ba31b5f89c'>Commit.</a> </li>
<li>Archive orginial new SVGs from Tyson. <a href='http://commits.kde.org/kate/1ec9aa12c4fb40a520fba417bf42af4545462e01'>Commit.</a> </li>
<li>Only allow the new pass through policy for windows. <a href='http://commits.kde.org/kate/577b74c7d8d4163c76c8d5f1511ce44520e03c85'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/416078'>#416078</a></li>
<li>Try to use Breeze style on mac and win, like we patch it in craft. <a href='http://commits.kde.org/kate/0cbc018671c5975f458aa02d1e384622702c2525'>Commit.</a> </li>
<li>Undo icon name change to kate-editor, back to kate. <a href='http://commits.kde.org/kate/9d9c35876ec6d4df720edd9342016518f3af5b36'>Commit.</a> </li>
<li>Redo png exports. <a href='http://commits.kde.org/kate/cd76c53a08cd06fa3109ab62b5d9cfd253cd2cd4'>Commit.</a> </li>
<li>Ensure the SVG is quadratic. <a href='http://commits.kde.org/kate/d51044378d95bba9337d62ec63ae013b84ac73ce'>Commit.</a> </li>
<li>Ensure proper Kate icon is used. <a href='http://commits.kde.org/kate/718ac48159009bda19abe62420e9c5d39b974a06'>Commit.</a> </li>
<li>Introduce new Kate application icon. <a href='http://commits.kde.org/kate/736214497de2001b420ddfc6d1b7b9ae80c2309b'>Commit.</a> </li>
<li>Try to fix embedding of icon for windows. <a href='http://commits.kde.org/kate/50bddb9e2c3b86ee52c3d82cbc00907848cbdd3b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/415260'>#415260</a></li>
<li>Use non-deprecated KDEInstallDirs variables. <a href='http://commits.kde.org/kate/421998d5600d03c6979572da7d96368eab19d247'>Commit.</a> </li>
<li>Add oars content rating. <a href='http://commits.kde.org/kate/7049dcb3d62aba036379ace3a413cf8747c48200'>Commit.</a> </li>
<li>Allow fractional scaling even on Windows. <a href='http://commits.kde.org/kate/e251ad96c9d70e4982dbdb095027cdd24468dffd'>Commit.</a> </li>
</ul>
<h3><a name='kde-dev-scripts' href='https://cgit.kde.org/kde-dev-scripts.git'>kde-dev-scripts</a> <a href='#kde-dev-scripts' onclick='toggle("ulkde-dev-scripts", this)'>[Hide]</a></h3>
<ul id='ulkde-dev-scripts' style='display: block'>
<li>Add support for K_DOXYGEN to old API docs generation scripts. <a href='http://commits.kde.org/kde-dev-scripts/c5840220d9ccdcb0acc50e385c8ab9db49132f90'>Commit.</a> </li>
<li>Add my relicensing agreements (mjansen). <a href='http://commits.kde.org/kde-dev-scripts/229c13206b728b862a24cecd5208250cd70eda36'>Commit.</a> </li>
</ul>
<h3><a name='kdenetwork-filesharing' href='https://cgit.kde.org/kdenetwork-filesharing.git'>kdenetwork-filesharing</a> <a href='#kdenetwork-filesharing' onclick='toggle("ulkdenetwork-filesharing", this)'>[Hide]</a></h3>
<ul id='ulkdenetwork-filesharing' style='display: block'>
<li>Only list uids from the actual user uid range. <a href='http://commits.kde.org/kdenetwork-filesharing/424facd195a878c91ae9bb14b15a40512cedd09a'>Commit.</a> </li>
<li>Make min/maxuid detection more portable. <a href='http://commits.kde.org/kdenetwork-filesharing/8c4928c0db09d39af5d3a03a7f9260638db77490'>Commit.</a> </li>
</ul>
<h3><a name='kdenlive' href='https://cgit.kde.org/kdenlive.git'>kdenlive</a> <a href='#kdenlive' onclick='toggle("ulkdenlive", this)'>[Hide]</a></h3>
<ul id='ulkdenlive' style='display: block'>
<li>Cleaner deletion order on exit. <a href='http://commits.kde.org/kdenlive/7d1c301c85235c3e3f1151fc9a86cdcb7fa65743'>Commit.</a> </li>
<li>Fix crash on new project with Qt 5.14. <a href='http://commits.kde.org/kdenlive/4493cbbcf2a3f4b82871f978fc9c7909d11f316a'>Commit.</a> </li>
<li>Fix index corruption on track deletion. <a href='http://commits.kde.org/kdenlive/3ef3b4547c6ea1ac38787acd0f23a84ef9f25d93'>Commit.</a> See bug <a href='https://bugs.kde.org/416677'>#416677</a></li>
<li>Sort subclips in chronological order when sorting by date. <a href='http://commits.kde.org/kdenlive/38881faba3ee93757f53ae6f7aeec0ddde1434e0'>Commit.</a> </li>
<li>Fine tune timeline clip elements on smaller track size. <a href='http://commits.kde.org/kdenlive/196178f2ab4327362b4e7251c6f1f260d4c52c79'>Commit.</a> </li>
<li>Cleanup resize and other clip handles (fades, add composition, keyframes). <a href='http://commits.kde.org/kdenlive/c5becebd6b21da0186c3f79878e15494c381e226'>Commit.</a> </li>
<li>Clean up and fix possible corruption on missing bin clip id. <a href='http://commits.kde.org/kdenlive/0f874a1f2066757227d2d611fd662e71b21e94c4'>Commit.</a> </li>
<li>Restore opening of clips from command line. <a href='http://commits.kde.org/kdenlive/6c5f196072785175df07b2af46a5217a0679ac3b'>Commit.</a> See bug <a href='https://bugs.kde.org/416404'>#416404</a></li>
<li>Fix effect with long names prevent easy access to effect actions. <a href='http://commits.kde.org/kdenlive/be08502e002c9cf0b68efd3c3146a4ef65d0f621'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/416420'>#416420</a></li>
<li>Hide option to overlay audio info from Project monitor (not supported). <a href='http://commits.kde.org/kdenlive/e0cc5b07c05867fc8fa9dae29d16eb6c5c103916'>Commit.</a> </li>
<li>Fix one empty frame left when trying to put 2 clips together. <a href='http://commits.kde.org/kdenlive/7a60a330d49197749799fd31b9bd6c4560d5ae8a'>Commit.</a> </li>
<li>Fix i18n warning on startup. <a href='http://commits.kde.org/kdenlive/65c0f59eb00c01b516bf8a02939ea864af319f9b'>Commit.</a> </li>
<li>Improvements to composition duration on drop. <a href='http://commits.kde.org/kdenlive/c0be97aed499b282f8a68787d62c24faa6a73d3b'>Commit.</a> </li>
</ul>
<h3><a name='kgeography' href='https://cgit.kde.org/kgeography.git'>kgeography</a> <a href='#kgeography' onclick='toggle("ulkgeography", this)'>[Hide]</a></h3>
<ul id='ulkgeography' style='display: block'>
<li>Use official sources (instead of Wikipedia) for the Norwegian flags. <a href='http://commits.kde.org/kgeography/0a56be7adf975e461653445d91c8421aeaaf785a'>Commit.</a> </li>
<li>Updated flag (coat of arms) for Viken county in Norway. <a href='http://commits.kde.org/kgeography/e816788ba97ffe5dd4f015d1288820ab2c7b9920'>Commit.</a> </li>
<li>Updated map, flags and metadata for the new counties of Norway. <a href='http://commits.kde.org/kgeography/5913631c9c94372481eeb6a05af981bed088e1ca'>Commit.</a> </li>
</ul>
<h3><a name='khelpcenter' href='https://cgit.kde.org/khelpcenter.git'>khelpcenter</a> <a href='#khelpcenter' onclick='toggle("ulkhelpcenter", this)'>[Hide]</a></h3>
<ul id='ulkhelpcenter' style='display: block'>
<li>Reload the page after changing the font size. <a href='http://commits.kde.org/khelpcenter/c56b981f44d64823edcf558d787a995e11a5e29b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/415192'>#415192</a></li>
</ul>
<h3><a name='kitinerary' href='https://cgit.kde.org/kitinerary.git'>kitinerary</a> <a href='#kitinerary' onclick='toggle("ulkitinerary", this)'>[Hide]</a></h3>
<ul id='ulkitinerary' style='display: block'>
<li>Fix merging of flight reservations with different IATA BCBP data. <a href='http://commits.kde.org/kitinerary/40e7abdcba2746088813fc77675bcb1c68b583f6'>Commit.</a> </li>
<li>Deal with the German language variant of Trenitalia tickets. <a href='http://commits.kde.org/kitinerary/41ef01c1c30b2859fe3d509424a458ed1305f763'>Commit.</a> </li>
<li>Move extractor engine capabilities string to the library. <a href='http://commits.kde.org/kitinerary/f64edaefef19db6271225ebe2c4357f25b624e11'>Commit.</a> </li>
<li>Add walking distance accuracy level for the location comparison. <a href='http://commits.kde.org/kitinerary/08d8157254c5abcad4a7eba354fe3908563dc39b'>Commit.</a> </li>
</ul>
<h3><a name='kmail' href='https://cgit.kde.org/kmail.git'>kmail</a> <a href='#kmail' onclick='toggle("ulkmail", this)'>[Hide]</a></h3>
<ul id='ulkmail' style='display: block'>
<li>Fix option 'h' is already defined for help (now we can see this option). <a href='http://commits.kde.org/kmail/de23c361e6c3e9883834cc530f2ca2e4b97da145'>Commit.</a> </li>
<li>Fix create mail from command line with body as utf8. <a href='http://commits.kde.org/kmail/dae422490279f7b9ff538b29aa85408bf9b7cbf3'>Commit.</a> </li>
<li>Make sure that we show active windows. <a href='http://commits.kde.org/kmail/ab72445133a89956639bf054a2a01162bc9a344b'>Commit.</a> </li>
<li>Fix show configure kmail. <a href='http://commits.kde.org/kmail/03564e7ee9de0ea07683368033e29ab76425ae3d'>Commit.</a> </li>
<li>Fix change settings. KPIM::PIMMessageBox::fourBtnMsgBox return a QDialogButtonBox::. <a href='http://commits.kde.org/kmail/121a6b0a74bff439ab245b8dbb29cd64e086e75d'>Commit.</a> </li>
</ul>
<h3><a name='kmail-account-wizard' href='https://cgit.kde.org/kmail-account-wizard.git'>kmail-account-wizard</a> <a href='#kmail-account-wizard' onclick='toggle("ulkmail-account-wizard", this)'>[Hide]</a></h3>
<ul id='ulkmail-account-wizard' style='display: block'>
<li>Use correct ProvideUrl as requested. <a href='http://commits.kde.org/kmail-account-wizard/d2cc161c0b8e3f2c100420bf0d738c696442b407'>Commit.</a> </li>
<li>Add more debug info. <a href='http://commits.kde.org/kmail-account-wizard/83ab5326a3d605aaf99b58141ef688a938aa4e12'>Commit.</a> See bug <a href='https://bugs.kde.org/416403'>#416403</a></li>
</ul>
<h3><a name='krdc' href='https://cgit.kde.org/krdc.git'>krdc</a> <a href='#krdc' onclick='toggle("ulkrdc", this)'>[Hide]</a></h3>
<ul id='ulkrdc' style='display: block'>
<li>Fix crash in full screen requests when the window shows no host list. <a href='http://commits.kde.org/krdc/f16e40db3baca67b2e133f9a20d12b72ad983d55'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/416156'>#416156</a></li>
</ul>
<h3><a name='ksirk' href='https://cgit.kde.org/ksirk.git'>ksirk</a> <a href='#ksirk' onclick='toggle("ulksirk", this)'>[Hide]</a></h3>
<ul id='ulksirk' style='display: block'>
<li>Fix blue text on almost blue background. <a href='http://commits.kde.org/ksirk/5a1a53dc97489d2468d0fe68b7a0a7eb5ea0b34f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/411083'>#411083</a></li>
<li>Fix warning about double help and version options being added. <a href='http://commits.kde.org/ksirk/dd0caf003b28ef1c0f26fad015dcedb77e62c1a8'>Commit.</a> </li>
<li>Fix a few warnings about copy constructors but no assingment operator or viceversa. <a href='http://commits.kde.org/ksirk/e8b4179756d76d35115458d14e6e06cc7f7c8599'>Commit.</a> </li>
</ul>
<h3><a name='kwordquiz' href='https://cgit.kde.org/kwordquiz.git'>kwordquiz</a> <a href='#kwordquiz' onclick='toggle("ulkwordquiz", this)'>[Hide]</a></h3>
<ul id='ulkwordquiz' style='display: block'>
<li>Fix reading a file always only giving you 20 rows. <a href='http://commits.kde.org/kwordquiz/a21b31c0005b62afc46dc81038aa55d03320aadf'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/416090'>#416090</a></li>
<li>Make it a real table and not an infite table. <a href='http://commits.kde.org/kwordquiz/25ed7434502fb258710967fa198437d6f5757366'>Commit.</a> </li>
<li>Fix warning about help and version already being added. <a href='http://commits.kde.org/kwordquiz/fe7366c7243adb1ee2a9a5f686fb4bcbee1c33b6'>Commit.</a> </li>
</ul>
<h3><a name='libkgapi' href='https://cgit.kde.org/libkgapi.git'>libkgapi</a> <a href='#libkgapi' onclick='toggle("ullibkgapi", this)'>[Hide]</a></h3>
<ul id='ullibkgapi' style='display: block'>
<li>Handle 410 Gone errors during Calendar sync. <a href='http://commits.kde.org/libkgapi/17fedad11cba3a1bb76ed20caa3b8d935bfec064'>Commit.</a> </li>
</ul>
<h3><a name='lokalize' href='https://cgit.kde.org/lokalize.git'>lokalize</a> <a href='#lokalize' onclick='toggle("ullokalize", this)'>[Hide]</a></h3>
<ul id='ullokalize' style='display: block'>
<li>Clean up small compilation warnings. <a href='http://commits.kde.org/lokalize/81bb7da5231ec5819b718d3f2fc96b1b9642cd04'>Commit.</a> </li>
</ul>
<h3><a name='mailcommon' href='https://cgit.kde.org/mailcommon.git'>mailcommon</a> <a href='#mailcommon' onclick='toggle("ulmailcommon", this)'>[Hide]</a></h3>
<ul id='ulmailcommon' style='display: block'>
<li>Fix support for compose key in foldertreewidget. <a href='http://commits.kde.org/mailcommon/655f28d868a773730acdfe492ce79083f07af846'>Commit.</a> </li>
<li>Fix support for dead keys in foldertreewidget filtering. <a href='http://commits.kde.org/mailcommon/081d57ce53d0a4cdad72f3e77db91ac0e139fdae'>Commit.</a> See bug <a href='https://bugs.kde.org/415850'>#415850</a></li>
<li>Fix: Bug 415850 - "Move message to folder" dialog does not accept certain characters. <a href='http://commits.kde.org/mailcommon/e727ec8c7562ba49a09c7da5375c21ae52470c2f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/415850'>#415850</a></li>
</ul>
<h3><a name='messagelib' href='https://cgit.kde.org/messagelib.git'>messagelib</a> <a href='#messagelib' onclick='toggle("ulmessagelib", this)'>[Hide]</a></h3>
<ul id='ulmessagelib' style='display: block'>
<li>Use correct ProvideUrl as requested. <a href='http://commits.kde.org/messagelib/9cb7999262d36f2058433e918ebdf592aad61ca3'>Commit.</a> </li>
<li>Disable reply with quote text as this bug is back in qt5.14.x. <a href='http://commits.kde.org/messagelib/22482e3299e3f6a7e5f4e7f995183054044500ba'>Commit.</a> See bug <a href='https://bugs.kde.org/394784'>#394784</a></li>
<li>Fix Bug 416369 - attachments lost draft message. <a href='http://commits.kde.org/messagelib/c3828a873a75ed7f54465e7d6acf88e81720d46f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/416369'>#416369</a></li>
<li>Fix check signature size. <a href='http://commits.kde.org/messagelib/213cd56511d14f03eca4445562cba3f864b8aecd'>Commit.</a> </li>
</ul>
<h3><a name='minuet' href='https://cgit.kde.org/minuet.git'>minuet</a> <a href='#minuet' onclick='toggle("ulminuet", this)'>[Hide]</a></h3>
<ul id='ulminuet' style='display: block'>
<li>Move the addHelp/VersionOption to android-only. <a href='http://commits.kde.org/minuet/0c72009114871be788f8af23fa17d3382a88cbbc'>Commit.</a> </li>
</ul>
<h3><a name='okular' href='https://cgit.kde.org/okular.git'>okular</a> <a href='#okular' onclick='toggle("ulokular", this)'>[Hide]</a></h3>
<ul id='ulokular' style='display: block'>
<li>Fix race condition in generator.cpp. <a href='http://commits.kde.org/okular/378e99d719aacc1d8c06bf07588bbe61fd5aba15'>Commit.</a> </li>
<li>Fix render stop and high load due to timing issue. <a href='http://commits.kde.org/okular/593803b0a1d98eba64aac38316aa521130b4ae78'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/396137'>#396137</a>. Fixes bug <a href='https://bugs.kde.org/396087'>#396087</a>. See bug <a href='https://bugs.kde.org/403643'>#403643</a></li>
<li>Fix PartTest::testAnnotWindow. <a href='http://commits.kde.org/okular/d7e9be4f8ce090446776ceac5d36a44006bd8c94'>Commit.</a> </li>
<li>"Fix" PartTest::testAdditionalActionTriggers. <a href='http://commits.kde.org/okular/45303be62c55a9267d3bdd4d8de16df3fdb5bbe1'>Commit.</a> </li>
</ul>
<h3><a name='spectacle' href='https://cgit.kde.org/spectacle.git'>spectacle</a> <a href='#spectacle' onclick='toggle("ulspectacle", this)'>[Hide]</a></h3>
<ul id='ulspectacle' style='display: block'>
<li>Tests: make sure testNumbering works. <a href='http://commits.kde.org/spectacle/655a5a536d98fc997445f9bcda1157aa3f50bf8d'>Commit.</a> </li>
</ul>
<h3><a name='step' href='https://cgit.kde.org/step.git'>step</a> <a href='#step' onclick='toggle("ulstep", this)'>[Hide]</a></h3>
<ul id='ulstep' style='display: block'>
<li>Fix compilation for locales with '@'. <a href='http://commits.kde.org/step/c908843b68a1c4f0cf2c3e2d9e1f96b6ed262d47'>Commit.</a> </li>
</ul>
<h3><a name='umbrello' href='https://cgit.kde.org/umbrello.git'>umbrello</a> <a href='#umbrello' onclick='toggle("ulumbrello", this)'>[Hide]</a></h3>
<ul id='ulumbrello' style='display: block'>
<li>Improvement to import of incomplete Java source code:. <a href='http://commits.kde.org/umbrello/8c1bfa64f1c822e2853e5f248a24a00adfda6ccb'>Commit.</a> See bug <a href='https://bugs.kde.org/416178'>#416178</a></li>
<li>Improve creation of generalization with parent provided as string:. <a href='http://commits.kde.org/umbrello/f23904618a4f40d54092152614a0b1df37f0b5f8'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/416178'>#416178</a></li>
<li>Optmization in JavaImport: Skip object creation for method "void" return. <a href='http://commits.kde.org/umbrello/616c0ec319241b8742b2e88b356e66fd63ff7b3f'>Commit.</a> See bug <a href='https://bugs.kde.org/416178'>#416178</a></li>
<li>Fix crash on importing Java code attached to bug 368453:. <a href='http://commits.kde.org/umbrello/372b1e0203847cc8ff7dc79a2a799d9a79b215f9'>Commit.</a> See bug <a href='https://bugs.kde.org/368453'>#368453</a></li>
<li>Followup to commit 0459a73 addresses comment #2 :. <a href='http://commits.kde.org/umbrello/2a01d5346b7c118111105654fe560b049af64b77'>Commit.</a> See bug <a href='https://bugs.kde.org/374124'>#374124</a></li>
<li>Fix broken file selection for PHP and SQL in code import wizard:. <a href='http://commits.kde.org/umbrello/0459a7368321d69b8cb567142ad82601477575c1'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/335566'>#335566</a>. Fixes bug <a href='https://bugs.kde.org/374124'>#374124</a></li>
<li>Fix endless loop on importing mysql schema:. <a href='http://commits.kde.org/umbrello/cd594e1bc3db4e5272ce8712de6ba98398c2f21a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/398082'>#398082</a></li>
<li>Fix 'Missing entries in the New context menu entry for the logical view'. <a href='http://commits.kde.org/umbrello/6f35d385c3307879c005289531c5ca0098c0924f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/416053'>#416053</a></li>
<li>Fix 'Umbrello Crash On Moving the Seconds element in the Class Diagram'. <a href='http://commits.kde.org/umbrello/21c34d60617211a5131ba8713af83fb9d8fbd8ce'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/416013'>#416013</a></li>
<li>Fix 'No association update after changing a class attribute type from class to class pointer or address and vice versa'. <a href='http://commits.kde.org/umbrello/5f681444044aa0e3ff175d0684a05575be21e04f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/416006'>#416006</a></li>
<li>Fix 'Duplicate Composition Association when rename it's variable in the tree View'. <a href='http://commits.kde.org/umbrello/afaca964b8807e4c824b9149bc725a5b5879571a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/415992'>#415992</a></li>
</ul>
