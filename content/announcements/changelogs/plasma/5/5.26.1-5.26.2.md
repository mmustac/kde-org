---
title: Plasma 5.26.2 complete changelog
version: 5.26.2
hidden: true
plasma: true
type: fulllog
---
{{< details title="Discover" href="https://commits.kde.org/discover" >}}
+ Flatpak: make sure we look up the correct resource. [Commit.](http://commits.kde.org/discover/4fb75399665274d027e5fb0b4a0202544fc55dfc) Fixes bug [#460365](https://bugs.kde.org/460365)
+ Flatpak: Only show the beta information for apps. [Commit.](http://commits.kde.org/discover/a5e76d8d071d65d5f5a92448d36902ac556434bb) Fixes bug [#459131](https://bugs.kde.org/459131)
+ Flatpak: Properly render if the beta package is too old. [Commit.](http://commits.kde.org/discover/8660ecc34c7366232690d5236ba634273f186db2) 
+ Snap: Port away from Kirigami.ItemViewHeader. [Commit.](http://commits.kde.org/discover/e01bbb34ff1332a73b2642f246df26dfc03a81fe) Fixes bug [#460391](https://bugs.kde.org/460391)
{{< /details >}}

{{< details title="Dr Konqi" href="https://commits.kde.org/drkonqi" >}}
+ Check telemetrymode. [Commit.](http://commits.kde.org/drkonqi/6bd18d9f187895d5e116fd1bf9e0cde00f639e23) 
+ Use qdbus to query for the hostname. [Commit.](http://commits.kde.org/drkonqi/f3ff06db6b008133e7cbe4453c8c0274ea2aef75) 
+ Allow using debuginfod for symbol resolution with gdb12. [Commit.](http://commits.kde.org/drkonqi/40f8c11579362755c5242f46ab2e257480d3d06e) Fixes bug [#454063](https://bugs.kde.org/454063)
{{< /details >}}

{{< details title="KDE Window Decoration Library" href="https://commits.kde.org/kdecoration" >}}
+ Floor mouse positions rather than rounding. [Commit.](http://commits.kde.org/kdecoration/243ea3f264217021d9ebdaab982c2598213b1fa8) 
{{< /details >}}

{{< details title="Plasma Addons" href="https://commits.kde.org/kdeplasma-addons" >}}
+ Runners/spellchecker: Fix config group name mismatch. [Commit.](http://commits.kde.org/kdeplasma-addons/2d1f7f4280fe5c5b2dc76a61f1c57488af359562) Fixes bug [#460899](https://bugs.kde.org/460899)
{{< /details >}}

{{< details title="Info Center" href="https://commits.kde.org/kinfocenter" >}}
+ CommandOutputKCM: Fix text sizing and make it selectable. [Commit.](http://commits.kde.org/kinfocenter/51e7c59f815330eec79639a69df9ff42d83c93be) 
{{< /details >}}

{{< details title="KWin" href="https://commits.kde.org/kwin" >}}
+ Replace some manual floored QPointF->QPoint conversion with flooredPoint calls. [Commit.](http://commits.kde.org/kwin/77f6e8680dcb9a29d4224fe6144d5f5578089aa2) 
+ DecorationInputFilter: Use QPointF instead of QPoint for events. [Commit.](http://commits.kde.org/kwin/e43851045a368816e24bb785d1d07d6ab30e9a02) 
+ Window: Floor rather than round when doing hitTest for the decoration. [Commit.](http://commits.kde.org/kwin/d78187005432ed02159963fd76554678a9db2e67) Fixes bug [#460686](https://bugs.kde.org/460686)
+ Add a helper function to convert QPointF to QPoint using floor instead of round. [Commit.](http://commits.kde.org/kwin/da10d8481af24f7ff7092e5a2da5c5262fdd4011) 
+ Wayland: Fix missing relative motion events. [Commit.](http://commits.kde.org/kwin/3c07db610bf70796f612016c6c5354ca5c565ebd) Fixes bug [#444510](https://bugs.kde.org/444510)
+ Implement a enableRequested signal for text-input-v3. [Commit.](http://commits.kde.org/kwin/f28e547330ae4c3c6c4ac97cd6fb7930d8e2e148) 
+ Kcmkwin/kwindecoration: use Kirigami.ActionToolBar for the footer actions. [Commit.](http://commits.kde.org/kwin/91eea817d628f151b99f2c8abe137d9f6a4beefa) Fixes bug [#460793](https://bugs.kde.org/460793)
+ Autotests/integration: make inputmethodtest more realstic. [Commit.](http://commits.kde.org/kwin/1e5d30cc61ba247c8f90fa7f5ad031a262594c7e) 
+ Inputpanelv1window: never hide overlay panels. [Commit.](http://commits.kde.org/kwin/870aee8cdc82fdae7440589ea61dfdb0c79f84e9) 
+ Inputmethod: reset m_shouldShowPanel when the tracked window changes. [Commit.](http://commits.kde.org/kwin/ece41a7b32576a18d09b7bbb83e7e332621a30c9) 
+ Inputpanelv1window: show window when client maps it after setting the mode. [Commit.](http://commits.kde.org/kwin/efb27c78cbf221334b33b5a6f4c8d5c8573a475f) Fixes bug [#460537](https://bugs.kde.org/460537)
+ X11window: don't change size for centering windows with maximization. [Commit.](http://commits.kde.org/kwin/ae714ac6d00ba80a869c345d495efbe4a7797eb1) 
+ Output: don't round geometry as often. [Commit.](http://commits.kde.org/kwin/dadf93f87ae338d9b3765507d3fd1b3e8c9fa440) See bug [#459373](https://bugs.kde.org/459373)
+ Effects/blur: Fix clipping when sliding virtual desktops. [Commit.](http://commits.kde.org/kwin/7802d25b642f45156b7aafc278d11b2066c190f4) Fixes bug [#460382](https://bugs.kde.org/460382)
+ Fix potential race condition when text input state change and focus change happened at the same time. [Commit.](http://commits.kde.org/kwin/defe740fabc2456837cd9477372833f11c94fea2) 
+ Fix wording in action 'Switch to Screen'. [Commit.](http://commits.kde.org/kwin/454ca8e1bfb70b443f586483f90ce1431b51bf34) 
+ Backends/drm: don't crash if connector has no modes. [Commit.](http://commits.kde.org/kwin/8456ec02615cfc33d98ff2643fd27ee8acd3ac98) 
{{< /details >}}

{{< details title="libkscreen" href="https://commits.kde.org/libkscreen" >}}
+ Make error messages translatable. [Commit.](http://commits.kde.org/libkscreen/b5163394764ea8ff4df0abd979c91d47e1363ddf) 
{{< /details >}}

{{< details title="Plasma Desktop" href="https://commits.kde.org/plasma-desktop" >}}
+ Kcms/tablet: Fix dragging components. [Commit.](http://commits.kde.org/plasma-desktop/32ac1394c7d3d70a18751b7343851991763942e4) Fixes bug [#460376](https://bugs.kde.org/460376)
+ Revert "applets/taskmanager: make it harder to accidentally start a drag". [Commit.](http://commits.kde.org/plasma-desktop/56e25dafcd235536188ae6969d22ef25400852f0) Fixes bug [#460809](https://bugs.kde.org/460809). See bug [#402376](https://bugs.kde.org/402376)
+ [applets/digitalclock] Fix font size change when migrating from 5.25. [Commit.](http://commits.kde.org/plasma-desktop/18870d13d8eed284d50efaaf88888a728d6226e7) Fixes bug [#460415](https://bugs.kde.org/460415)
+ Kcms/mouse: Set preventStealing on the button capture. [Commit.](http://commits.kde.org/plasma-desktop/1d714844301f129e7e7d4b3cd8ede3d1d046a0ae) See bug [#460345](https://bugs.kde.org/460345)
+ Use KeySequenceItem.captureFinished to notify a binding has been entered. [Commit.](http://commits.kde.org/plasma-desktop/873025e29946a75e1a13a5aa4cebcfdb4e473396) See bug [#459322](https://bugs.kde.org/459322)
{{< /details >}}

{{< details title="plasma-mobile" href="https://commits.kde.org/plasma-mobile" >}}
+ Mmplugin: Cleanup signals. [Commit.](http://commits.kde.org/plasma-mobile/db53c102ad5b552d28f0b847917e39f03ca6c3c9) 
+ Dataproviders: Fix mobile provider label. [Commit.](http://commits.kde.org/plasma-mobile/c3f182e5aefa9b0ba5c958950ef1c6da90a2c448) 
+ Mmplugin: Ensure variables are reset properly. [Commit.](http://commits.kde.org/plasma-mobile/2151b7543ac3358e707a0126de78a23785b821c3) 
+ Mmplugin: Flesh out update signals and behaviour. [Commit.](http://commits.kde.org/plasma-mobile/67b7dd88c3f4716485fa70e9235853884e4ddc99) 
{{< /details >}}

{{< details title="Plasma Remotecontrollers" href="https://commits.kde.org/plasma-remotecontrollers" >}}
+ Fix desktop file. [Commit.](http://commits.kde.org/plasma-remotecontrollers/3b1c1b52d81ccc6d49b6b7bd912e191520384059) Fixes bug [#460924](https://bugs.kde.org/460924)
{{< /details >}}

{{< details title="plasma-vault" href="https://commits.kde.org/plasma-vault" >}}
+ Fix crash in vault listing callback. [Commit.](http://commits.kde.org/plasma-vault/3d66625a5f625b50dadfe79f89a9da5ef2674082) 
{{< /details >}}

{{< details title="Plasma Workspace" href="https://commits.kde.org/plasma-workspace" >}}
+ Revert "applets/notifications: allow screen reader to read notification body in FullRepresentation". [Commit.](http://commits.kde.org/plasma-workspace/80c5928fa2a26503dd28a4766c14148815884be7) Fixes bug [#460895](https://bugs.kde.org/460895)
+ Runners/kill: Fix config group name mismatch. [Commit.](http://commits.kde.org/plasma-workspace/bfdcaa503d2b19e9084f802253446aead14e09da) Fixes bug [#460899](https://bugs.kde.org/460899)
+ [kcms/kcm_regionandlanguage] fix config not saved after clicking 'defaults' and 'save'. [Commit.](http://commits.kde.org/plasma-workspace/3d321a242b4d984778d84b7b62669a24e92e62b5) Fixes bug [#460842](https://bugs.kde.org/460842)
+ Disable automatic portal launching early on. [Commit.](http://commits.kde.org/plasma-workspace/8c8c110a934a9abd7e69762f89f4029bea7f9c4a) Fixes bug [#458865](https://bugs.kde.org/458865)
+ Wallpapers/image: disable animated wallpaper on X11. [Commit.](http://commits.kde.org/plasma-workspace/304633713bee125a5b4b9caec79a6d66e68d1b9a) 
+ Wallpapers/image: fall back to default wallpaper when url is empty. [Commit.](http://commits.kde.org/plasma-workspace/c15596dbc8e3d90de6f64a8e6545b482ff694579) Fixes bug [#460692](https://bugs.kde.org/460692)
+ Systemtray: Avoid dbus calls after exit. [Commit.](http://commits.kde.org/plasma-workspace/89eb2eb653ff05de3c07029296d886cc059df824) Fixes bug [#460814](https://bugs.kde.org/460814)
+ Save layout immediately after a resolution change triggered relayout. [Commit.](http://commits.kde.org/plasma-workspace/4a6e8f7f241b518fc0ac7f7cf049029d770c0443) 
+ Remove unnecessary heuristic relayout function call. [Commit.](http://commits.kde.org/plasma-workspace/4496172b59afac1b06def7ef5a859fcd1a378eaf) 
+ Use KeySequenceItem.captureFinished to notify a binding has been entered. [Commit.](http://commits.kde.org/plasma-workspace/56a125da2b0514e2da9e621823fcd6283e6f05d4) Fixes bug [#459322](https://bugs.kde.org/459322)
{{< /details >}}

{{< details title="System Settings" href="https://commits.kde.org/systemsettings" >}}
+ Recognize SystemSettingsExternalApp again. [Commit.](http://commits.kde.org/systemsettings/75293d6a8835d63a280eda6283d001eac8edd165) 
{{< /details >}}

