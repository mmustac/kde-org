---
title: Plasma 5.26.4 complete changelog
version: 5.26.4
hidden: true
plasma: true
type: fulllog
---
{{< details title="Aura Browser" href="https://commits.kde.org/aura-browser" >}}
+ Fix app .desktop file category. [Commit.](http://commits.kde.org/aura-browser/284fbcf5a5686e826a34d1e675268c658ab64167) 
{{< /details >}}

{{< details title="Bluedevil" href="https://commits.kde.org/bluedevil" >}}
+ Install translated documentation from po/ too. [Commit.](http://commits.kde.org/bluedevil/62d1bba67a90635817564ca4ce74da2b68981895) 
{{< /details >}}

{{< details title="Discover" href="https://commits.kde.org/discover" >}}
+ Prefer openining the installed version of an app. [Commit.](http://commits.kde.org/discover/4549356075552941b64ba0d1066f325a55801688) Fixes bug [#461664](https://bugs.kde.org/461664)
+ Packagekit: check free space when update size changes. [Commit.](http://commits.kde.org/discover/cb421186f365ac279ff16c4aa3c7dc491bf51a64) 
+ Pk: Only issue a resolve if something changed. [Commit.](http://commits.kde.org/discover/d371b8454907f5440398444e9a6d4853b2b459fc) Fixes bug [#461813](https://bugs.kde.org/461813)
+ ProgressView: disable highlight effect for real. [Commit.](http://commits.kde.org/discover/eb91b30c329abb8fe021d249addd0eb6de4f30e3) Fixes bug [#461812](https://bugs.kde.org/461812)
{{< /details >}}

{{< details title="kde-cli-tools" href="https://commits.kde.org/kde-cli-tools" >}}
+ Install translated documentation from po/ too. [Commit.](http://commits.kde.org/kde-cli-tools/86aed08de85f4dfc3d6b8a8951903d0c92c98d71) 
{{< /details >}}

{{< details title="Plasma Addons" href="https://commits.kde.org/kdeplasma-addons" >}}
+ Wallpapers/potd: allow force refresh when no cached image is available. [Commit.](http://commits.kde.org/kdeplasma-addons/d1a731a24008123b385236dbb1dd4575f1fa9344) 
+ Applets/weather: Fix system tray detection. [Commit.](http://commits.kde.org/kdeplasma-addons/79facfca9d554f4eb9bc82f770b146370f22a664) 
{{< /details >}}

{{< details title="Gamma Monitor Calibration Tool" href="https://commits.kde.org/kgamma5" >}}
+ Install translated documentation from po/ too. [Commit.](http://commits.kde.org/kgamma5/1892756e6a3010d44cbffb9c8bbfcb6bfaef123f) 
{{< /details >}}

{{< details title="KDE Hotkeys" href="https://commits.kde.org/khotkeys" >}}
+ Install translated documentation from po/ too. [Commit.](http://commits.kde.org/khotkeys/2f1b2313ac8fbe1a5e9d4bd38dc11705d7a89e57) 
{{< /details >}}

{{< details title="Info Center" href="https://commits.kde.org/kinfocenter" >}}
+ Install translated documentation from po/ too. [Commit.](http://commits.kde.org/kinfocenter/cec4b79c405165bf9141214e83fef9a15bdc714f) 
{{< /details >}}

{{< details title="KMenuEdit" href="https://commits.kde.org/kmenuedit" >}}
+ Install translated documentation from po/ too. [Commit.](http://commits.kde.org/kmenuedit/7f81f87fc0d1acb09dada24da91380882c7d4430) 
{{< /details >}}

{{< details title="KScreen" href="https://commits.kde.org/kscreen" >}}
+ Fix right to right snapping. [Commit.](http://commits.kde.org/kscreen/fab815b0564ea08202b5bd3429f580c6c5a742d3) See bug [#455394](https://bugs.kde.org/455394)
{{< /details >}}

{{< details title="KWin" href="https://commits.kde.org/kwin" >}}
+ Install translated documentation from po/ too. [Commit.](http://commits.kde.org/kwin/48842864e0185337e5937337ce85ab9ec48fc687) 
+ Utils/serviceutils: Improve Exec key parsing. [Commit.](http://commits.kde.org/kwin/828e90e85730ff21256e290867e6125fc60d7300) 
+ Effects/kscreen: don't stop effect when fade out animation is done. [Commit.](http://commits.kde.org/kwin/45c2530565c209ccbbeeb20b33a8cdc14a48703c) Fixes bug [#460902](https://bugs.kde.org/460902)
+ Backends/libinput: update screens on Workspace::outputsChanged. [Commit.](http://commits.kde.org/kwin/8fe8d0a1ac969fbaae196bb41dd7bb36655d93e8) Fixes bug [#461901](https://bugs.kde.org/461901)
+ Update defiintion of customised Gitlab CI job to include appropriate flags to keep Git happy. [Commit.](http://commits.kde.org/kwin/6e6378df68e2e4fb9006992103cb5fb4a703c6b5) 
+ Screencast: Don't scale the cursor. [Commit.](http://commits.kde.org/kwin/93b0ef5fd9a036b8560606a8d95d6c29bcc33ed7) 
+ X11: Reset SurfaceItemX11::m_isDamaged when the damage handle is destroyed. [Commit.](http://commits.kde.org/kwin/75a13d7d445a75b4930b7a7c836831d74a5939cc) 
+ Activation: Guard against not having any active window. [Commit.](http://commits.kde.org/kwin/5b76e32251462417a07d169bfae93abe955ada35) 
+ Wayland: Make debug console placeable. [Commit.](http://commits.kde.org/kwin/630990164fe8d0a7dcf132df530ad1031aea686f) Fixes bug [#453920](https://bugs.kde.org/453920)
+ Screencast: Fix how we tell pipewire that the stream has been resized. [Commit.](http://commits.kde.org/kwin/19e7ed61e038744277284197fae05032703cb40d) Fixes bug [#461590](https://bugs.kde.org/461590)
+ Backends/drm: generate modes even if some common modes are available. [Commit.](http://commits.kde.org/kwin/bb99fb8aa4dd5c1ce0630081e424f1038c6b3dae) 
{{< /details >}}

{{< details title="libksysguard" href="https://commits.kde.org/libksysguard" >}}
+ QML error: digist -> digits. [Commit.](http://commits.kde.org/libksysguard/2f84b8895b65909d354f4f99a4561b9fb045d888) 
{{< /details >}}

{{< details title="Plasma Desktop" href="https://commits.kde.org/plasma-desktop" >}}
+ Install translated documentation from po/ too. [Commit.](http://commits.kde.org/plasma-desktop/a3630aa79a45516f82cfe59dec33543f7f56f9c4) 
+ Panel: Avoid setting solid background when compositing is disabled. [Commit.](http://commits.kde.org/plasma-desktop/3e24f14d873b7d753fc848fb8fcafd2e1c83b9c8) Fixes bug [#451451](https://bugs.kde.org/451451)
+ Applets/kickoff: fix huge icons in category list. [Commit.](http://commits.kde.org/plasma-desktop/1673711dd27eefeea4c636d9c8fc56ee1f6a70fc) Fixes bug [#460349](https://bugs.kde.org/460349)
+ Kcms/runners: Fix org.kde.kconfig.notify DBus signal getting wrong plugin id. [Commit.](http://commits.kde.org/plasma-desktop/ea04ba432599e3012dc2f31829c707600f0e2638) Fixes bug [#461422](https://bugs.kde.org/461422)
+ [kcms/tablet] Reload output combobox when outputs change. [Commit.](http://commits.kde.org/plasma-desktop/2bef275debf796bbb3e190d21cfddaaea434f8c3) Fixes bug [#461560](https://bugs.kde.org/461560)
{{< /details >}}

{{< details title="plasma-mobile" href="https://commits.kde.org/plasma-mobile" >}}
+ Actiondrawer: Fix call to non-existent property. [Commit.](http://commits.kde.org/plasma-mobile/2e5a92b725e5b5b9fa8d502cd8af522da2b21f4d) 
+ Shell: Update to new usage of FormComboBoxDelegate. [Commit.](http://commits.kde.org/plasma-mobile/d07040b324f59fbf86298b9fbb07eb4d3f4f94de) 
+ Kcm: Update to new usage of FormComboBoxDelegate. [Commit.](http://commits.kde.org/plasma-mobile/4af53edc0cab8f3a00bd3303f72fe42a7b138d33) 
+ Homescreens/halcyon: Improve gridview scrolling performance. [Commit.](http://commits.kde.org/plasma-mobile/84a0ccd8235aa68d0f51633f6d63806b54b737db) 
+ Revert "look-and-feel: Switch to Breeze Dark by default". [Commit.](http://commits.kde.org/plasma-mobile/06ab0286cddd6df30f2367ef2f8e38210880c52b) 
+ Homescreens/halcyon: Cap grid icon size relative to app name labels. [Commit.](http://commits.kde.org/plasma-mobile/790f15bfeab1be817c5f21a449435722c6124913) 
+ Quicksettings: Limit to 5 rows maximum on one page. [Commit.](http://commits.kde.org/plasma-mobile/95ebc15290c64ff0d9d5bdd573d892475b9fa645) 
+ Look-and-feel: Switch to Breeze Dark by default. [Commit.](http://commits.kde.org/plasma-mobile/b396faf4a8666476392b51b5262c16e2c2b21f55) 
{{< /details >}}

{{< details title="Plasma Networkmanager (plasma-nm)" href="https://commits.kde.org/plasma-nm" >}}
+ Show current MAC for wired connections. [Commit.](http://commits.kde.org/plasma-nm/7dc3b4adae3964c104855b24e9700c3a49b69793) See bug [#456119](https://bugs.kde.org/456119)
+ Show current MAC address instead of permanent MAC address in details view. [Commit.](http://commits.kde.org/plasma-nm/758c651a29adc64d77c0c6c2eb018b0119b0f16f) Fixes bug [#456119](https://bugs.kde.org/456119)
{{< /details >}}

{{< details title="Plasma Audio Volume Control" href="https://commits.kde.org/plasma-pa" >}}
+ Install translated documentation from po/ too. [Commit.](http://commits.kde.org/plasma-pa/f221febbb6fe0161a0e5299733c58e3598d7ad9d) 
{{< /details >}}

{{< details title="Plasma SDK" href="https://commits.kde.org/plasma-sdk" >}}
+ Revert "Revert "Install translated documentation from po/ too"". [Commit.](http://commits.kde.org/plasma-sdk/acb97a096ef9a8b01475cceb05b4ec80c62745ee) 
+ Revert "Install translated documentation from po/ too". [Commit.](http://commits.kde.org/plasma-sdk/692760c1fd110eacc7ed189ee959e557a96e8399) 
+ Install translated documentation from po/ too. [Commit.](http://commits.kde.org/plasma-sdk/af2e17ef9d155120f71ea2e248f568dd4dd86a1e) 
{{< /details >}}

{{< details title="Plasma Workspace" href="https://commits.kde.org/plasma-workspace" >}}
+ Shell: Don't force basic render loop for Plasma Mobile. [Commit.](http://commits.kde.org/plasma-workspace/d9bc5e457ab18f06427e6185fbb8e3b397b55bcd) 
+ Do not query all available image plugins when determining background type. [Commit.](http://commits.kde.org/plasma-workspace/fe738f146f7a1ffafb4d242ac130760b97f2ef32) Fixes bug [#462308](https://bugs.kde.org/462308)
+ Install translated documentation from po/ too. [Commit.](http://commits.kde.org/plasma-workspace/d907c44a70f13d3f1194e609aa6435575fd939aa) 
+ Panel: Revert making the mask smaller by 1px. [Commit.](http://commits.kde.org/plasma-workspace/631841e00e204ba37da2367fe6b221ba6ab795b4) Fixes bug [#460896](https://bugs.kde.org/460896). See bug [#417511](https://bugs.kde.org/417511)
+ Remove negative inset of plasmoid heading in notifications since there's no margin in those. [Commit.](http://commits.kde.org/plasma-workspace/70ba188d62fa9864318dabe2fc6b55ab182268bf) Fixes bug [#452347](https://bugs.kde.org/452347)
+ Lockscreen: Consider info messages to be prompts. [Commit.](http://commits.kde.org/plasma-workspace/20c51d95798e953acd3299354a5b37a1216b6343) Fixes bug [#454706](https://bugs.kde.org/454706)
+ Shell: Use the basic scene graph rendering loop on wayland. [Commit.](http://commits.kde.org/plasma-workspace/0fff87982b7164a442b549509fa9fa792007880a) Fixes bug [#447717](https://bugs.kde.org/447717)
+ Fix wrong example values, return correct paper size value. [Commit.](http://commits.kde.org/plasma-workspace/0e882f92f71aedd5b6f0134fc0c7011951f3b767) 
+ Wallpapers/image: save last wallpaper in slideshow mode. [Commit.](http://commits.kde.org/plasma-workspace/51fdcd20a50712d6f5c4a88cbc02f167b3718528) Fixes bug [#461490](https://bugs.kde.org/461490)
+ SystemDialog: Fix display when there is no component. [Commit.](http://commits.kde.org/plasma-workspace/5b5bbb26473a1d940e752323800152c580914ccb) 
+ Applets/notifications: unload ListView when plasmoid is not expanded. [Commit.](http://commits.kde.org/plasma-workspace/37be022bf2ebd50edaf4144a3b070ff6a8c93feb) See bug [#460895](https://bugs.kde.org/460895)
+ Applets/mediacontroller: avoid flashing application icon. [Commit.](http://commits.kde.org/plasma-workspace/48a32ad19d63017e96a59d62c93b370df396f34b) Fixes bug [#461496](https://bugs.kde.org/461496)
+ SystemDialog: Make sure the window has a Qt.Window type. [Commit.](http://commits.kde.org/plasma-workspace/589b8032e2ff3055a6f196d834246141bda8886f) 
+ Notifications: Explicitly uncheck paused button when switching to running state. [Commit.](http://commits.kde.org/plasma-workspace/cd09c4782afe9074adb4a392642680cfd1f1a570) 
+ Shell: revert unrelated changes in d79a927a4c7a8b489b985a6ae94bd9d8f082db93. [Commit.](http://commits.kde.org/plasma-workspace/a77f1bda64114712603a8c25bc2c678dc6957676) See bug [#439159](https://bugs.kde.org/439159)
+ [shell] Always call load after init. [Commit.](http://commits.kde.org/plasma-workspace/8056e09e08b37e19b1f55179fc8bfa960eea8e24) Fixes bug [#439159](https://bugs.kde.org/439159)
+ Applets/notifications: remove one unused import. [Commit.](http://commits.kde.org/plasma-workspace/7e94254b4f483208f1995bfb83650e5fcdc37631) 
+ Applets/notifications: remove workaround for QTBUG-67007. [Commit.](http://commits.kde.org/plasma-workspace/a58c22e8a2b535b5ba9dac95129763db160adfad) 
+ Startkde/plasma-session: fix playing startup sound. [Commit.](http://commits.kde.org/plasma-workspace/c27f2a5ef1156ffc10018327426cbf03de9945a1) See bug [#422948](https://bugs.kde.org/422948)
{{< /details >}}

{{< details title="Powerdevil" href="https://commits.kde.org/powerdevil" >}}
+ Install translated documentation from po/ too. [Commit.](http://commits.kde.org/powerdevil/499282c002009b612192047d9e85504f57240886) 
{{< /details >}}

{{< details title="qqc2-breeze-style" href="https://commits.kde.org/qqc2-breeze-style" >}}
+ DialogButtonBox: Remove separator to be consistent with desktop style. [Commit.](http://commits.kde.org/qqc2-breeze-style/585943559c9f439433a98a1cc84ffc681033fa65) 
{{< /details >}}

{{< details title="System Settings" href="https://commits.kde.org/systemsettings" >}}
+ Install translated documentation from po/ too. [Commit.](http://commits.kde.org/systemsettings/84f44e0e89ee1e4d9b1b06f672d1bbd6726e2e44) 
{{< /details >}}

