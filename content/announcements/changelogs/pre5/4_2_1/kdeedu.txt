------------------------------------------------------------------------
r914651 | scripty | 2009-01-21 13:42:47 +0000 (Wed, 21 Jan 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r914951 | asimha | 2009-01-22 08:04:03 +0000 (Thu, 22 Jan 2009) | 12 lines

Backporting i18n fix to 4.2 branch.

Quoting from the original commit log:
Fix i18n issue in Time Dialog. ? ... : should not be used in a i18nc
call because the macro doesn't know what to do with it.

Note that this introduces a new string into 4.2 branch.

CCMAIL: kstars-devel@kde.org
CCMAIL: kde-i18n-doc@kde.org


------------------------------------------------------------------------
r915155 | annma | 2009-01-22 14:13:33 +0000 (Thu, 22 Jan 2009) | 4 lines

patch from Nadav Vinik to allow integers to be entered as integers even in mixed numbers mode.
Dirk can you integrate in release please? Thanks!
CCMAIL=mueller@kde.org

------------------------------------------------------------------------
r915158 | annma | 2009-01-22 14:21:21 +0000 (Thu, 22 Jan 2009) | 4 lines

Those 2 files were put on top instead of the right dirs.
Can you include in release please Dirk? Thanks!
CCMAIL=mueller@kde.org

------------------------------------------------------------------------
r915242 | annma | 2009-01-22 16:29:37 +0000 (Thu, 22 Jan 2009) | 2 lines

load and read the Answer: Mixed Number setting

------------------------------------------------------------------------
r915245 | annma | 2009-01-22 16:37:35 +0000 (Thu, 22 Jan 2009) | 2 lines

not needed, duplicated rows

------------------------------------------------------------------------
r915407 | jakselsen | 2009-01-22 23:15:17 +0000 (Thu, 22 Jan 2009) | 1 line


------------------------------------------------------------------------
r916420 | chehrlic | 2009-01-25 12:06:30 +0000 (Sun, 25 Jan 2009) | 1 line

it's openbabel-2 on windows (backport from trunk)
------------------------------------------------------------------------
r916553 | rahn | 2009-01-25 14:37:49 +0000 (Sun, 25 Jan 2009) | 3 lines

- Fix first part of Bug 181865


------------------------------------------------------------------------
r916821 | rahn | 2009-01-26 05:32:31 +0000 (Mon, 26 Jan 2009) | 3 lines

Bug 181435: Fix by egk865 to prevent float items from moving while dragging the map.


------------------------------------------------------------------------
r916871 | jakselsen | 2009-01-26 11:13:31 +0000 (Mon, 26 Jan 2009) | 1 line

Replace/update. Tree SVGs and one new. Backported for 4.2.
------------------------------------------------------------------------
r916889 | mlaurent | 2009-01-26 12:23:18 +0000 (Mon, 26 Jan 2009) | 2 lines

Backport: fix warning about shortcut

------------------------------------------------------------------------
r916892 | mlaurent | 2009-01-26 12:30:27 +0000 (Mon, 26 Jan 2009) | 2 lines

Backport: fix warning error about shortcut

------------------------------------------------------------------------
r916894 | mlaurent | 2009-01-26 12:32:25 +0000 (Mon, 26 Jan 2009) | 2 lines

Backport: fix warning about shortcut

------------------------------------------------------------------------
r916899 | mlaurent | 2009-01-26 12:43:22 +0000 (Mon, 26 Jan 2009) | 2 lines

Backport: fix warning about shortcut

------------------------------------------------------------------------
r917020 | asimha | 2009-01-26 17:47:22 +0000 (Mon, 26 Jan 2009) | 16 lines

Backporting fix for an i18n bug. The original commit log is as
follows:

Fixing i18n bug which prevented the Sun and Moon from being selected
in the conjunctions tool. The bug was preventing me from calculating
today's solar eclipse. Things now work.

I think the amount of obscuration shown by KStars seems to be lesser
than observed, and this is yet another thing to investigate.

We should really, really do something about the way translated names
are handled.

CCMAIL: kstars-devel@kde.org        
                                                                  

------------------------------------------------------------------------
r917115 | pino | 2009-01-26 22:11:35 +0000 (Mon, 26 Jan 2009) | 6 lines

backport SVN commit 917113 by pino:

make it work again
- separate better the pkg-config stuff, and really make use of it
- less win32/not-win32 unneeded differences

------------------------------------------------------------------------
r918209 | annma | 2009-01-29 16:42:44 +0000 (Thu, 29 Jan 2009) | 4 lines

backport of fix for Greek input
Sorry Teofilos to have waited, the report arrived when I was on vacation and I missed it. Thanks tampakrap from Gentoo to tell me about it!
BUG=178114

------------------------------------------------------------------------
r918857 | nielsslot | 2009-01-30 21:09:16 +0000 (Fri, 30 Jan 2009) | 2 lines

Backport revision 918855. Fix for for-loops with negative step. Fixes bug 181938.

------------------------------------------------------------------------
r919509 | scripty | 2009-02-01 08:44:27 +0000 (Sun, 01 Feb 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r920011 | chehrlic | 2009-02-02 06:07:29 +0000 (Mon, 02 Feb 2009) | 2 lines

fix libavogadro install location on windows
BUG 182797
------------------------------------------------------------------------
r920473 | rahn | 2009-02-02 23:29:55 +0000 (Mon, 02 Feb 2009) | 7 lines



n't crash if Marble doesn't find its plugins


M    PluginManager.cpp

------------------------------------------------------------------------
r920689 | rahn | 2009-02-03 16:03:10 +0000 (Tue, 03 Feb 2009) | 4 lines


- Backporting BC Bugfixes from trunk from rev. 916524 and 917218


------------------------------------------------------------------------
r920772 | sengels | 2009-02-03 19:16:15 +0000 (Tue, 03 Feb 2009) | 3 lines

change runner implementation
hopefully this also removes the crashes in the designer
CCBUG:178530
------------------------------------------------------------------------
r920779 | rahn | 2009-02-03 19:29:47 +0000 (Tue, 03 Feb 2009) | 3 lines

- Backporting thread fixes for PntMapLoader and PlaceMarkLoader ...


------------------------------------------------------------------------
r920786 | sengels | 2009-02-03 19:39:06 +0000 (Tue, 03 Feb 2009) | 1 line

make it 4.3 proof (although it *might* leak this way)
------------------------------------------------------------------------
r920796 | rahn | 2009-02-03 19:54:56 +0000 (Tue, 03 Feb 2009) | 3 lines

- This fixes the reported issue behind 182866 -- still a crash away from a full fix ...


------------------------------------------------------------------------
r920800 | sengels | 2009-02-03 20:02:05 +0000 (Tue, 03 Feb 2009) | 1 line

SVN_SILENT: correct the fix.
------------------------------------------------------------------------
r921298 | rahn | 2009-02-04 18:23:52 +0000 (Wed, 04 Feb 2009) | 7 lines


- First Make-Marble-work-on-Qt4.3-again patch by Matias Kallio.

-This line, and those below, will be ignored--

M    MarbleModel.cpp

------------------------------------------------------------------------
r921867 | hedlund | 2009-02-05 21:04:20 +0000 (Thu, 05 Feb 2009) | 2 lines

Backport of fix for bug 181113.

------------------------------------------------------------------------
r921870 | hedlund | 2009-02-05 21:08:48 +0000 (Thu, 05 Feb 2009) | 2 lines

Backport of fix for bug 176620.

------------------------------------------------------------------------
r921939 | sengels | 2009-02-05 23:45:04 +0000 (Thu, 05 Feb 2009) | 5 lines

fix first MarbleWidget crash in the designer
This has definitely to do with the move of the textureColorizer init.

tackat breaks it,
poor SaroEngels has to fix it
------------------------------------------------------------------------
r921943 | sengels | 2009-02-06 00:13:53 +0000 (Fri, 06 Feb 2009) | 1 line

this doesn't fix anything but it was wrong anyway
------------------------------------------------------------------------
r921960 | sengels | 2009-02-06 01:30:47 +0000 (Fri, 06 Feb 2009) | 2 lines

fix this again.
Maybe it was not such a great idea to simply take over instance() as a function name...
------------------------------------------------------------------------
r921963 | sengels | 2009-02-06 01:52:02 +0000 (Fri, 06 Feb 2009) | 1 line

change this to match the new name
------------------------------------------------------------------------
r922264 | rahn | 2009-02-06 16:19:45 +0000 (Fri, 06 Feb 2009) | 4 lines


- Fixing the examples


------------------------------------------------------------------------
r922267 | rahn | 2009-02-06 16:23:43 +0000 (Fri, 06 Feb 2009) | 4 lines


- Make all Marble Designer Plugins use the same icon


------------------------------------------------------------------------
r922336 | rahn | 2009-02-06 18:33:44 +0000 (Fri, 06 Feb 2009) | 6 lines


- Further patch from Matias Kallio to make Marble SVN compile on Qt 4.3

Now Marble 0.7.x should fully compile even with Qt 4.3


------------------------------------------------------------------------
r922560 | scripty | 2009-02-07 08:50:38 +0000 (Sat, 07 Feb 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r922866 | rahn | 2009-02-07 17:43:19 +0000 (Sat, 07 Feb 2009) | 5 lines


 - Fixing up MarbleWidget::m_popupmenu a bit (needs further work)
 - Fixing functional regression of the CenterSun feature


------------------------------------------------------------------------
r923792 | rahn | 2009-02-09 13:16:41 +0000 (Mon, 09 Feb 2009) | 6 lines


BUG: 183754
- Backport missing line from previous backport SVN commit 922866 
  Should fix the crash for real this time.


------------------------------------------------------------------------
r924117 | sedwards | 2009-02-10 07:01:56 +0000 (Tue, 10 Feb 2009) | 3 lines

Changed license to LGPL 2+ to conform to the marble licensing policy.


------------------------------------------------------------------------
r924430 | aacid | 2009-02-10 19:19:19 +0000 (Tue, 10 Feb 2009) | 4 lines

Backport r924429 | aacid | 2009-02-10 20:18:13 +0100 (Tue, 10 Feb 2009) | 2 lines

fix Limpopo and Mpumalanga are swit

------------------------------------------------------------------------
r925028 | scripty | 2009-02-12 07:44:47 +0000 (Thu, 12 Feb 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r925182 | rahn | 2009-02-12 17:07:19 +0000 (Thu, 12 Feb 2009) | 4 lines


- Protective Crash fix for fully pretiled themes that get deleted by clearing feature.


------------------------------------------------------------------------
r925803 | scripty | 2009-02-14 08:45:56 +0000 (Sat, 14 Feb 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r925929 | nienhueser | 2009-02-14 14:42:20 +0000 (Sat, 14 Feb 2009) | 3 lines

Set the home position and zoom value after the map id. Fixes wrong restoration of the zoom state if it exceeds the default zoom range.
Backport of commit 925256.

------------------------------------------------------------------------
r926108 | sedwards | 2009-02-14 17:18:31 +0000 (Sat, 14 Feb 2009) | 3 lines

Updates.


------------------------------------------------------------------------
r926186 | sedwards | 2009-02-14 20:00:18 +0000 (Sat, 14 Feb 2009) | 3 lines

instance() was renamed pluginInstance() in the C++ class.


------------------------------------------------------------------------
r926194 | rahn | 2009-02-14 20:08:26 +0000 (Sat, 14 Feb 2009) | 4 lines


- Make the Qt version of Marble a lot less jumpy during startup


------------------------------------------------------------------------
r926347 | scripty | 2009-02-15 07:26:15 +0000 (Sun, 15 Feb 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r928965 | apol | 2009-02-20 13:34:16 +0000 (Fri, 20 Feb 2009) | 2 lines

Backport the lexer problem on composited letters.

------------------------------------------------------------------------
r929210 | laidig | 2009-02-20 22:06:30 +0000 (Fri, 20 Feb 2009) | 1 line

backport r929208: don't try to auto save when no document is open
------------------------------------------------------------------------
r929619 | mboquien | 2009-02-21 18:23:12 +0000 (Sat, 21 Feb 2009) | 3 lines

Backport compilation fix when INDI is installed.
CCMAIL:kstars-devel@kde.org

------------------------------------------------------------------------
r930168 | rahn | 2009-02-22 19:22:16 +0000 (Sun, 22 Feb 2009) | 7 lines


- Fix bug reported by jmho which makes Marble crash if the user sets
  the physical cache to zero size.




------------------------------------------------------------------------
r930774 | scripty | 2009-02-24 08:20:37 +0000 (Tue, 24 Feb 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r931151 | rahn | 2009-02-24 21:13:36 +0000 (Tue, 24 Feb 2009) | 4 lines


Version 0.7.1


------------------------------------------------------------------------
r931779 | jakselsen | 2009-02-25 19:36:01 +0000 (Wed, 25 Feb 2009) | 1 line

Replace/updates. and some new. Backported for 4.2.
------------------------------------------------------------------------
