2005-07-20 20:21 +0000 [r437056]  ossi

	* branches/KDE/3.4/kdebase/kdm/kfrontend/kdmshutdown.cpp: backport:
	  prevent the columns from becoming too narrow.

2005-07-20 20:38 +0000 [r437060]  ossi

	* branches/KDE/3.4/kdebase/kdm/README: backport typo fixes

2005-07-20 20:50 +0000 [r437064]  ossi

	* branches/KDE/3.4/kdebase/kdm/kfrontend/kdm_greet.c,
	  branches/KDE/3.4/kdebase/kdm/kfrontend/kdmconfig.cpp,
	  branches/KDE/3.4/kdebase/kdm/config.def: backport: make
	  AntiAliasing work again.

2005-07-21 14:38 +0000 [r437319]  lunakl

	* branches/KDE/3.4/kdebase/kwin/client.cpp: Backport 408336.

2005-07-21 14:41 +0000 [r437322]  lunakl

	* branches/KDE/3.4/kdebase/kwin/layers.cpp: Backport 108020.

2005-07-21 14:46 +0000 [r437324]  lunakl

	* branches/KDE/3.4/kdebase/kcontrol/screensaver/configure.in.in
	  (added), branches/KDE/3.4/kdebase/kdesktop/lock/configure.in.in,
	  branches/KDE/3.4/kdebase/kdesktop/lock/lockprocess.cc,
	  branches/KDE/3.4/kdebase/kcontrol/screensaver/kssmonitor.h,
	  branches/KDE/3.4/kdebase/kcontrol/screensaver/testwin.cpp,
	  branches/KDE/3.4/kdebase/kdesktop/lock/lockprocess.h,
	  branches/KDE/3.4/kdebase/kcontrol/screensaver/kswidget.cpp
	  (added), branches/KDE/3.4/kdebase/kcontrol/screensaver/testwin.h,
	  branches/KDE/3.4/kdebase/kcontrol/screensaver/kswidget.h (added),
	  branches/KDE/3.4/kdebase/kcontrol/screensaver/Makefile.am,
	  branches/KDE/3.4/kdebase/kdesktop/lock/Makefile.am: Backport
	  r73813 (no flickering for opengl).

2005-07-21 14:53 +0000 [r437328]  lunakl

	* branches/KDE/3.4/kdebase/kwin/client.cpp,
	  branches/KDE/3.4/kdebase/kwin/client.h,
	  branches/KDE/3.4/kdebase/kwin/manage.cpp,
	  branches/KDE/3.4/kdebase/kwin/geometry.cpp: Backport functions
	  for postponing geometry updates, needed for shading fixes.

2005-07-21 14:57 +0000 [r437330]  lunakl

	* branches/KDE/3.4/kdebase/kwin/client.cpp,
	  branches/KDE/3.4/kdebase/kwin/client.h,
	  branches/KDE/3.4/kdebase/kwin/rules.cpp,
	  branches/KDE/3.4/kdebase/kwin/geometry.cpp: Backport shading
	  fixes.

2005-07-21 15:19 +0000 [r437342]  pletourn

	* branches/KDE/3.4/kdelibs/kioslave/ftp/ftp.protocol,
	  branches/KDE/3.4/kdebase/kioslave/sftp/sftp.protocol: Allow
	  rename action in konqueror context menu BUG:105563

2005-07-22 12:33 +0000 [r437576]  coolo

	* branches/KDE/3.4/kdebase/kcontrol/screensaver/scrnsave.cpp:
	  backporting the --enable-final fix too

2005-07-28 09:21 +0000 [r439502]  lunakl

	* branches/KDE/3.4/kdebase/kioslave/smb/kio_smb_browse.cpp:
	  Backport better handling of ENOTDIR and EINVAL.

2005-07-31 16:57 +0000 [r441684]  aseigo

	* branches/KDE/3.4/kdebase/kicker/ui/k_mnu.cpp: lets not autodelete
	  either. there was a time i thought i might miss autodelete on qt
	  containers, but now i think they're more hassle than they are
	  worth really

2005-07-31 21:35 +0000 [r441761]  craig

	* branches/KDE/3.4/kdebase/kcontrol/kfontinst/lib/FcEngine.cpp:
	  When parsing a fonts:/ url - re-initialize fontconfig, just in
	  case its a newly installed font.

2005-07-31 21:57 +0000 [r441769]  bero

	* branches/KDE/3.4/kdebase/kioslave/ldap/kio_ldap.h: Compile fix
	  for openldap >= 2.3.x

2005-08-04 00:18 +0000 [r442833]  pletourn

	* branches/KDE/3.4/kdebase/konqueror/listview/konq_listviewwidget.cc,
	  branches/KDE/3.4/kdebase/konqueror/listview/konq_listviewwidget.h:
	  Since KListView::slotMouseButtonClicked() is nonvirtual, rename
	  KonqBaseListViewWidget::slotMouseButtonClicked() because it's not
	  a reimplementation Discussed with David Faure BUG:110097

2005-08-05 08:15 +0000 [r443170]  coolo

	* branches/KDE/3.4/kdebase/kdmlib/kgreet_classic.cpp: give gcc an
	  hint that grid and themer are connected

2005-08-05 19:18 +0000 [r443311]  pletourn

	* branches/KDE/3.4/kdebase/konqueror/konq_mainwindow.cc: If a
	  fileName exits, don't try to detect a name filter BUG:110227
	  BUG:107170

2005-08-05 22:34 +0000 [r443374]  adridg

	* branches/KDE/3.4/kdebase/konqueror/konq_mainwindow.cc: Fix build;
	  can't use KIO::NetAccess::exists without including its header

2005-08-06 10:47 +0000 [r443448]  binner

	* branches/KDE/3.4/kdebase/kicker/share/kickerSettings.kcfg: If you
	  don't watch someone will change a default to the worse for sure

2005-08-09 14:51 +0000 [r444279]  lunakl

	* branches/KDE/3.4/kdebase/kwin/client.cpp: Reverting #105809 here
	  too.

2005-08-10 13:50 +0000 [r444882]  coolo

	* branches/KDE/3.4/kdebase/kioslave/media/kdedmodule/halbackend.cpp:
	  backporting 444456

2005-08-11 09:38 +0000 [r445583]  mlaurent

	* branches/KDE/3.4/kdebase/kcontrol/kthememanager/ktheme.cpp: Fix
	  load default icons theme

2005-08-12 09:01 +0000 [r445964]  mlaurent

	* branches/KDE/3.4/kdebase/kcontrol/kthememanager/ktheme.cpp: Use
	  defaultStyle

2005-08-14 21:00 +0000 [r449255]  ossi

	* branches/KDE/3.4/kdebase/kdmlib/dmctl.cpp,
	  branches/KDE/3.4/kdebase/kdm/backend/ctrl.c: backport: don't
	  request kde 3.4 features from kde 3.3 kdm.

2005-08-17 21:05 +0000 [r450292]  mueller

	* branches/KDE/3.4/kdebase/kcontrol/kfontinst/thumbnail/FontThumbnail.cpp:
	  fix export

2005-08-18 08:48 +0000 [r450441]  mlaurent

	* branches/KDE/3.4/kdebase/kioslave/media/kdedmodule/halbackend.cpp:
	  Fix compile

2005-08-18 22:09 +0000 [r450720]  aseigo

	* branches/KDE/3.4/kdebase/kicker/share/kickertip.cpp,
	  branches/KDE/3.4/kdebase/kicker/share/kickertip.h: backport:
	  watch for the item we are tipping for getting deleted out from
	  underneath us CCBUG:109760

2005-08-22 14:37 +0000 [r452080]  aseigo

	* branches/KDE/3.4/kdebase/kicker/ui/service_mnu.cpp: call close
	  after hitting the event loop again to prevent getting stuck in a
	  nested event loop BUG:111220

2005-08-23 20:45 +0000 [r452598]  dfaure

	* branches/KDE/3.4/kdebase/kcontrol/ebrowsing/plugins/shorturi/kshorturifilter.cpp:
	  Backport name filter code, to fix name filters in konqueror. Leo:
	  thanks for the url with the patch - can you close the bug report?
	  I don't have its number.

2005-08-24 19:57 +0000 [r452960]  binner

	* branches/KDE/3.4/kdebase/kcontrol/kcontrol/searchwidget.cpp: Find
	  more by also searching module name

2005-08-26 16:59 +0000 [r453617]  ossi

	* branches/KDE/3.4/kdebase/kcontrol/kdm/kdm-users.cpp: make user
	  pics readable for 'nobody'.

2005-08-27 20:46 +0000 [r454068]  deller

	* branches/KDE/3.4/kdebase/kcontrol/info/opengl.cpp: backport crash
	  fix: Bug 101154: kinfocenter opengl DRI/GLX crash

2005-09-01 19:55 +0000 [r455981]  tilladam

	* branches/KDE/3.4/kdebase/konqueror/listview/konq_infolistviewitem.cc,
	  branches/KDE/3.4/kdebase/konqueror/listview/konq_listviewitems.cc,
	  branches/KDE/3.4/kdebase/konqueror/listview/konq_listviewitems.h,
	  branches/KDE/3.4/kdebase/konqueror/listview/konq_textviewitem.cc:
	  Show the presence of extended ACL information in the file manager
	  listviews. Do we need something a bit more obvious, and/or
	  something for the icon views? Maybe in 4.0 we can do nice icon
	  decorations ...

2005-09-01 21:20 +0000 [r456013]  tilladam

	* branches/KDE/3.4/kdebase/konqueror/listview/konq_infolistviewitem.cc,
	  branches/KDE/3.4/kdebase/konqueror/listview/konq_listviewitems.cc,
	  branches/KDE/3.4/kdebase/konqueror/listview/konq_listviewitems.h,
	  branches/KDE/3.4/kdebase/konqueror/listview/konq_textviewitem.cc:
	  Revert. Wrong branch. Partial switch. I hate svn. :)

2005-09-05 19:38 +0000 [r457391]  dgp

	* branches/KDE/3.4/kdebase/kdm/configure.in.in,
	  branches/KDE/3.4/kdebase/configure.in.in: Move a couple of
	  KDE_EXPAND_MAKEVAR/AC_DEFINE_UNQUOTED in root configure.in.in as
	  it's used also for kcontrol and not only kdm.

2005-09-05 19:43 +0000 [r457392]  dgp

	* branches/KDE/3.4/kdebase/configure.in.in,
	  branches/KDE/3.4/kdebase/kioslave/thumbnail/configure.in.in: Move
	  libart checks on root configure.in.in as it's used by kdm and
	  kcontrol, too.

2005-09-06 09:28 +0000 [r457647]  mueller

	* branches/KDE/3.4/kdebase/kcheckpass/kcheckpass.c,
	  branches/KDE/3.4/kdebase/kcheckpass/checkpass_pam.c: add
	  kcheckpass fixes

2005-09-06 12:46 +0000 [r457727]  mueller

	* branches/KDE/3.4/kdebase/kioslave/smb/kio_smb_auth.cpp: enable
	  Kerberos authentication (the working version)

2005-09-06 13:32 +0000 [r457755]  binner

	* branches/KDE/3.4/kdebase/kicker/applets/launcher/quicklauncher.cpp:
	  Revert 1.34.2.1, breaks more than it fixes
	  (https://bugzilla.novell.com/show_bug.cgi?id=113879) Usage of
	  QuickURL() was also reintroduced as fix in branches/KDE/3.5/

2005-09-09 15:32 +0000 [r459012]  ossi

	* branches/KDE/3.4/kdebase/kcheckpass/checkpass_pam.c: restore
	  intended behavior: if pam asks for something else than the
	  password, it is not "classic".

2005-09-09 18:19 +0000 [r459062]  mlaurent

	* branches/KDE/3.4/kdebase/kicker/menuext/remote/remotemenu.cpp:
	  Fix signal/slot

2005-09-12 17:03 +0000 [r460003]  ossi

	* branches/KDE/3.4/kdebase/kcheckpass/kcheckpass.c: security: the
	  new locking scheme was inadequate, because renaming of files is
	  not locked. instead, just move the lock file to /var/run - where
	  it should have been from the start. this makes the code close to
	  trivial, again - and this time hopefully secure. remove post-fail
	  delay, as it is pointless (no additional security) and
	  counter-productive (blocks the gui). the pre-auth delay is
	  sufficient. some whitespace fixes i was too lazy to split off.
	  dirk: please port to any branches where applicable.

2005-09-13 08:54 +0000 [r460274]  binner

	* branches/KDE/3.4/kdebase/konqueror/Makefile.am: don't miss
	  remoteencodingplugin in messages target

2005-09-13 09:31 +0000 [r460284-460282]  mueller

	* branches/KDE/3.4/kdebase/libkonq/konq_pixmapprovider.h: unbreak
	  compile

	* branches/KDE/3.4/kdebase/kcontrol/dnssd/kcmdnssd.cpp,
	  branches/KDE/3.4/kdebase/kcontrol/dnssd/kcm_kdnssd.desktop:
	  unbreak kcm_kdnssd

2005-09-14 06:25 +0000 [r460513]  mueller

	* branches/KDE/3.4/kdebase/kdm/kfrontend/sessions/fluxbox.desktop:
	  use startfluxbox

2005-09-14 18:10 +0000 [r460670]  ossi

	* branches/KDE/3.4/kdebase/kdm/backend/dm.c: backport: read
	  display's config before using it ...

2005-09-14 18:17 +0000 [r460673]  ossi

	* branches/KDE/3.4/kdebase/kdm/config.def,
	  branches/KDE/3.4/kdebase/kdm/backend/dm.c: ServerVT=-1 => don't
	  assign VT. strictly speaking it's a feature, but otoh it just
	  restores 3.3's compatibility to weird setups. it's trivial anyway
	  ...

2005-09-14 21:01 +0000 [r460710]  aseigo

	* branches/KDE/3.4/kdebase/kicker/core/main.cpp: put the app on the
	  heap to ensure it's around long enough for clean up to occur

2005-09-15 16:46 +0000 [r460883]  mlaurent

	* branches/KDE/3.4/kdebase/kcontrol/joystick/joystick.desktop: Fix
	  kcmshell joystick

2005-09-15 18:08 +0000 [r460907]  ossi

	* branches/KDE/3.4/kdebase/kdm/config.def: backport typo fix - thx
	  annma.

2005-09-23 07:24 +0000 [r463221]  mueller

	* branches/KDE/3.4/kdebase/kdesktop/minicli.cpp,
	  branches/KDE/3.4/kdebase/kdesktop/minicli_ui.ui: backport Aaron's
	  RMB fix

2005-09-26 13:59 +0000 [r464208]  ossi

	* branches/KDE/3.4/kdebase/kdm/backend/process.c: backport
	  uncritical buffer overflow fix

2005-09-26 23:07 +0000 [r464329]  aseigo

	* branches/KDE/3.4/kdebase/kicker/applets/trash/trashapplet.cpp,
	  branches/KDE/3.4/kdebase/kicker/applets/trash/trashapplet.h:
	  qobjects on the heap, esp if they have a parent

2005-09-29 08:42 +0000 [r465239]  coolo

	* branches/KDE/3.4/kdebase/kioslave/media/configure.in.in: I bet it
	  was a nice exercise finding out where you can add quote signs
	  without making a difference - still they are wrong

2005-09-29 21:37 +0000 [r465476]  craig

	* branches/KDE/3.4/kdebase/kcontrol/kfontinst/viewpart/FontViewPart.h,
	  branches/KDE/3.4/kdebase/kcontrol/kfontinst/viewpart/FontPreview.cpp,
	  branches/KDE/3.4/kdebase/kcontrol/kfontinst/viewpart/FontViewPart.cpp:
	  Bug #111535 Dont try to use KIO::NetAccess::exists(), etc, upon
	  initial app start-up - crashes konqueror. Instead, use a QTimer,
	  and do the NetAccess stuff after the timeout

2005-10-02 13:05 +0000 [r466363]  coolo

	* branches/KDE/3.4/kdebase/khotkeys/kcontrol/ui/condition_list_widget_ui.ui,
	  branches/KDE/3.4/kdebase/khotkeys/kcontrol/ui/windowdef_list_widget_ui.ui,
	  branches/KDE/3.4/kdebase/khotkeys/kcontrol/ui/gestures_settings_tab_ui.ui,
	  branches/KDE/3.4/kdebase/khotkeys/kcontrol/ui/actions_listview_widget_ui.ui,
	  branches/KDE/3.4/kdebase/khotkeys/kcontrol/ui/action_list_widget_ui.ui,
	  branches/KDE/3.4/kdebase/khotkeys/kcontrol/ui/triggers_tab_ui.ui,
	  branches/KDE/3.4/kdebase/khotkeys/kcontrol/ui/keyboard_input_widget_ui.ui:
	  backport 3.3.5 fix

2005-10-03 22:00 +0000 [r466994]  reitelbach

	* branches/KDE/3.4/kdebase/kcontrol/ebrowsing/plugins/ikws/searchproviders/austronaut.desktop,
	  branches/KDE/3.4/kdebase/kcontrol/ebrowsing/plugins/ikws/searchproviders/acronym.desktop,
	  branches/KDE/3.4/kdebase/kcontrol/ebrowsing/plugins/ikws/searchproviders/altavista.desktop,
	  branches/KDE/3.4/kdebase/kcontrol/ebrowsing/plugins/ikws/searchproviders/google_news.desktop,
	  branches/KDE/3.4/kdebase/kcontrol/ebrowsing/plugins/ikws/searchproviders/voila.desktop,
	  branches/KDE/3.4/kdebase/kcontrol/ebrowsing/plugins/ikws/searchproviders/google_groups.desktop,
	  branches/KDE/3.4/kdebase/kcontrol/ebrowsing/plugins/ikws/searchproviders/froogle.desktop,
	  branches/KDE/3.4/kdebase/kcontrol/ebrowsing/plugins/ikws/searchproviders/whatis.desktop,
	  branches/KDE/3.4/kdebase/kcontrol/ebrowsing/plugins/ikws/searchproviders/google_advanced.desktop,
	  branches/KDE/3.4/kdebase/kcontrol/ebrowsing/plugins/ikws/searchproviders/google_images.desktop,
	  branches/KDE/3.4/kdebase/kcontrol/ebrowsing/plugins/ikws/searchproviders/python.desktop:
	  added some german localized search providers and some fixes for
	  broken URLs

2005-10-05 13:42 +0000 [r467506]  coolo

	* branches/KDE/3.4/kdebase/konqueror/version.h,
	  branches/KDE/3.4/kdebase/startkde: 3.4.3

2005-10-05 13:47 +0000 [r467514]  coolo

	* branches/KDE/3.4/kdebase/kdebase.lsm: 3.4.3

