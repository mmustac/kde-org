2005-07-20 22:20 +0000 [r437088]  mueller

	* branches/KDE/3.4/kdenetwork/kopete/protocols/gadu/libgadu/libgadu.h:
	  fix compilation

2005-07-23 16:15 +0000 [r437926]  pino

	* branches/KDE/3.4/kdenetwork/kget/dlgIndividual.cpp: Backport
	  commit 437892 by uwolfer: Single windows resized. Now the windows
	  aren't too wide. Fixing bug #99112 in the 3.4 branch too.

2005-07-26 01:18 +0000 [r438762]  mattr

	* branches/KDE/3.4/kdenetwork/kopete/protocols/oscar/liboscar/ssimodifytask.cpp:
	  disable rename group, it was never implemented

2005-07-27 07:42 +0000 [r439125]  hermier

	* branches/KDE/3.4/kdenetwork/kopete/libkopete/kopetemessage.cpp:
	  BACKPORT: Fix some heuristicContentMatch miss use. BUGS:100148

2005-08-03 15:31 +0000 [r442684]  mlaurent

	* branches/KDE/3.4/kdenetwork/lanbrowsing/kcmlisa/portsettingsbar.cpp:
	  Fix signal/slot

2005-08-07 12:20 +0000 [r443792]  rantala

	* branches/KDE/3.4/kdenetwork/kopete/plugins/nowlistening/nowlisteningguiclient.cpp:
	  CCBUG: 109666 Backport: * Dont crash when selecting menu entry
	  after plugin is unloaded.

2005-08-09 14:09 +0000 [r444260]  rantala

	* branches/KDE/3.4/kdenetwork/kopete/protocols/irc/ui/channellist.cpp,
	  branches/KDE/3.4/kdenetwork/kopete/protocols/irc/ui/channellist.h:
	  backport: Fix two broken signal connections.

2005-08-09 16:35 +0000 [r444308]  rantala

	* branches/KDE/3.4/kdenetwork/kopete/protocols/irc/libkirc/ksslsocket.cpp,
	  branches/KDE/3.4/kdenetwork/kopete/protocols/irc/libkirc/ksslsocket.h,
	  branches/KDE/3.4/kdenetwork/kopete/protocols/irc/libkirc/kircengine.cpp:
	  CCBUG: 104048 Backport: Fix crash when invalid SSL certificate
	  not accepted.

2005-08-10 14:59 +0000 [r445143]  rantala

	* branches/KDE/3.4/kdenetwork/kopete/protocols/irc/ircaccount.h,
	  branches/KDE/3.4/kdenetwork/kopete/protocols/irc/ui/irceditaccountwidget.cpp,
	  branches/KDE/3.4/kdenetwork/kopete/protocols/irc/irccontactmanager.cpp,
	  branches/KDE/3.4/kdenetwork/kopete/protocols/irc/ircaccount.cpp:
	  BUG: 104920 backport revision 410353: Modify IRCContactManager to
	  use account->networkName() instead of
	  account->engine()->currentHost() as the 'nick name' for server
	  contacts. This nick name is eventually used by
	  Kopete::Account::registerContact() and causes problems because
	  currentHost() returns null string before the account is connected
	  to a server. Notes: - I had to add two new parameters to
	  IRCAccount (networkName & nickName), because the contacts are
	  created when the account is constructed. So calling
	  account->setNickName() etc. in editaccountwidget is not enough. -
	  Gets rid of "QGDict::hashKeyString: Invalid null key" when
	  accounts are created at startup, and when they are deleted (that
	  was caused by currentHost()). - Fix crashing after creating a new
	  IRC account and quitting right after that (at least I'm
	  experiencing this with 0.10 & SVN).

2005-08-12 11:44 +0000 [r446066-446064]  wstephens

	* branches/KDE/3.4/kdenetwork/kopete/protocols/groupwise/libgroupwise/client.cpp:
	  correct uninitialised value

	* branches/KDE/3.4/kdenetwork/kopete/protocols/groupwise/libgroupwise/task.cpp:
	  correct uninitialised value

	* branches/KDE/3.4/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/createcontacttask.h,
	  branches/KDE/3.4/kdenetwork/kopete/protocols/groupwise/gwaccount.cpp,
	  branches/KDE/3.4/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/createcontacttask.cpp:
	  Show an error when a contact could not be added on the server

2005-08-12 14:01 +0000 [r446096]  wstephens

	* branches/KDE/3.4/kdenetwork/kopete/protocols/groupwise/libgroupwise/client.cpp:
	  Remove extra whitespace from incoming messages

2005-08-16 16:24 +0000 [r449749]  wstephens

	* branches/KDE/3.4/kdenetwork/kopete/libkopete/kopetepluginmanager.cpp:
	  Fix bug where default plugins are not loaded by default.

2005-08-18 02:15 +0000 [r450389]  mattr

	* branches/KDE/3.4/kdenetwork/kopete/libkopete/kopetepluginmanager.cpp:
	  revert the plugin loader fix because it doesn't fix anything
	  since i can't disable certain plugins now.

2005-08-18 14:52 +0000 [r450573]  wstephens

	* branches/KDE/3.4/kdenetwork/kopete/libkopete/kopetepluginmanager.cpp:
	  Reinstate fix for default plugins not loading. The check for
	  missing config was incorrect, so that it never found any config
	  and always loaded the default plugins.

2005-08-18 16:32 +0000 [r450613]  wstephens

	* branches/KDE/3.4/kdenetwork/kopete/protocols/groupwise/gwaccount.cpp:
	  Fix bug preventing Groupwise from going away automatically

2005-08-19 14:27 +0000 [r450931-450929]  wstephens

	* branches/KDE/3.4/kdenetwork/kopete/protocols/groupwise/gwmessagemanager.cpp:
	  Fake a success to indicate a message failure. Confused? You
	  should be.

	* branches/KDE/3.4/kdenetwork/kopete/protocols/groupwise/gwprotocol.cpp,
	  branches/KDE/3.4/kdenetwork/kopete/protocols/groupwise/icons/cr16-action-groupwise_away.png:
	  Fix GW icons in 3.4

2005-08-23 11:35 +0000 [r452443]  hermier

	* branches/KDE/3.4/kdenetwork/kopete/protocols/irc/ui/networkconfig.ui:
	  Backport of #452435.

2005-08-31 18:19 +0000 [r455518]  mueller

	* branches/KDE/3.4/kdenetwork/kopete/protocols/jabber/libiris/iris/xmpp-im/xmpp_xmlcommon.cpp,
	  branches/KDE/3.4/kdenetwork/kopete/protocols/jabber/libiris/iris/xmpp-core/parser.cpp,
	  branches/KDE/3.4/kdenetwork/kopete/protocols/jabber/jabberprotocol.h,
	  branches/KDE/3.4/kdenetwork/kopete/protocols/jabber/libiris/iris/xmpp-im/xmpp_xmlcommon.h,
	  branches/KDE/3.4/kdenetwork/kopete/protocols/jabber/libiris/iris/include/xmpp.h,
	  branches/KDE/3.4/kdenetwork/kopete/protocols/jabber/libiris/qca/src/qca.h:
	  just another fixkdecompile run

2005-09-12 16:10 +0000 [r459978]  wstephens

	* branches/KDE/3.4/kdenetwork/kopete/protocols/groupwise/ui/gwaddcontactpage.cpp:
	  Backport fix for crash while adding contacts while offline.

2005-09-13 09:43 +0000 [r460290]  mueller

	* branches/KDE/3.4/kdenetwork/kopete/protocols/msn/msnswitchboardsocket.cpp,
	  branches/KDE/3.4/kdenetwork/kopete/protocols/msn/msnfiletransfersocket.cpp:
	  trying to get it to compile before I gave up

2005-09-17 12:03 +0000 [r461352]  gj

	* branches/KDE/3.4/kdenetwork/kopete/protocols/gadu/gadusession.cpp:
	  CCMAIL: kde-packager@kde.org CCMAIL: mueller@kde.org backport the
	  fix to 3.4

2005-09-28 17:32 +0000 [r464967]  gianni

	* branches/KDE/3.4/kdenetwork/kppp/Rules/Italy/Makefile.am,
	  branches/KDE/3.4/kdenetwork/kppp/Rules/Italy/Cheapnet.rst
	  (added): new ISP

2005-10-02 18:57 +0000 [r466473]  coolo

	* branches/KDE/3.4/kdenetwork/kopete/protocols/irc/ui/ircadd.ui,
	  branches/KDE/3.4/kdenetwork/kopete/protocols/oscar/aim/ui/aimeditaccountui.ui,
	  branches/KDE/3.4/kdenetwork/ksirc/KSOpenkSirc/open_ksircData.ui,
	  branches/KDE/3.4/kdenetwork/kopete/protocols/jabber/ui/dlgjabbereditaccountwidget.ui,
	  branches/KDE/3.4/kdenetwork/kopete/protocols/msn/ui/msneditaccountui.ui,
	  branches/KDE/3.4/kdenetwork/kopete/protocols/irc/ui/irceditaccount.ui,
	  branches/KDE/3.4/kdenetwork/kopete/protocols/groupwise/ui/gwaccountpreferences.ui,
	  branches/KDE/3.4/kdenetwork/kopete/protocols/oscar/icq/ui/icqeditaccountui.ui,
	  branches/KDE/3.4/kdenetwork/kopete/protocols/gadu/ui/gadueditaccountui.ui:
	  fix compilation with qt 3.3.5

2005-10-05 13:26 +0000 [r467499]  coolo

	* branches/KDE/3.4/kdenetwork/kopete/kopete/main.cpp: kopete++

2005-10-05 13:48 +0000 [r467521]  coolo

	* branches/KDE/3.4/kdenetwork/kdenetwork.lsm: 3.4.3

