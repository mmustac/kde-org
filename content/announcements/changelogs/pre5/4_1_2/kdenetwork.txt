------------------------------------------------------------------------
r854123 | uwolfer | 2008-08-28 23:11:22 +0200 (Thu, 28 Aug 2008) | 5 lines

Backport:
SVN commit 854121 by uwolfer:

Fix frame buffer size change detection.
Patch by Ben Klopfenstein, thanks.
------------------------------------------------------------------------
r854129 | uwolfer | 2008-08-28 23:23:10 +0200 (Thu, 28 Aug 2008) | 19 lines

Backport:
SVN commit 854023 by gpothier:

Fix concurrency issues. In particular this should fix crashes when a
connection is closed either from krdc or from the server side.
Bug reporters, please check if you can still reproduce the crashes with this
patch (this is for trunk, backporting to 4.1 in a few minutes).

Details:
1- Disconnect signals from the client thread to the vncview when the
latter is deleted. This Fixes a 100% repeatable crash that occurs when
I close a vnc tab that is connected to an ubuntu machine running the
vino vnc server (aka gnome desktop sharing)
2- Remove the buf global variable. I wasn't able to get a
reproducible crash due only to this variable because of the above
item, but getting rid of that variable is the sanest thing to do I
think.

CCMAIL:gpothier@gmail.com
------------------------------------------------------------------------
r854446 | ereslibre | 2008-08-29 15:46:46 +0200 (Fri, 29 Aug 2008) | 2 lines

Backport of rev 854444

------------------------------------------------------------------------
r854508 | uwolfer | 2008-08-29 19:20:05 +0200 (Fri, 29 Aug 2008) | 2 lines

SVN_SILENT
fix compile after r854129
------------------------------------------------------------------------
r854582 | uwolfer | 2008-08-29 22:44:09 +0200 (Fri, 29 Aug 2008) | 5 lines

Backport:
SVN commit 854581 by uwolfer:

* Fix crash which happened when painting while thread deleted framebuffer (for example when VNC server closed connection).
* Replace some returns with asserts.
------------------------------------------------------------------------
r854618 | gpothier | 2008-08-30 00:10:12 +0200 (Sat, 30 Aug 2008) | 5 lines

Backporting from trunk. 
Do not resize krdc window when scaling is enabled in case of
1. Framebuffer resized
2. Random portion of framebuffer starting at (0,0) is updated
CCMAIL:uwolfer@kde.org
------------------------------------------------------------------------
r854901 | lueck | 2008-08-30 17:10:05 +0200 (Sat, 30 Aug 2008) | 1 line

documentation backport fron trunk
------------------------------------------------------------------------
r856424 | uwolfer | 2008-09-02 22:33:48 +0200 (Tue, 02 Sep 2008) | 8 lines

Backport:
SVN commit 855003 by uwolfer:

Change the way how events (mouse, key and wheel) are handled: overwrite QWidget::event. This way we can prevent the wrong (in our case) handling of the tab key. Also simplifies code a lot.
CCBUG:167055

Fixes also new bug:
BUG:170178
------------------------------------------------------------------------
r856462 | rjarosz | 2008-09-02 23:53:29 +0200 (Tue, 02 Sep 2008) | 7 lines

Backport commit 856461.
Fix Gadu-Gadu crashes.
Patches by Kamil Kamiński and Jakub Grandys thanks!

CCBUG: 166465


------------------------------------------------------------------------
r857035 | pino | 2008-09-04 15:38:25 +0200 (Thu, 04 Sep 2008) | 5 lines

Backport SVN commit 857034 by pino:
Ignore empty or whitespace-only formulas.

CCBUG: 170280

------------------------------------------------------------------------
r857736 | pino | 2008-09-06 13:11:36 +0200 (Sat, 06 Sep 2008) | 6 lines

Backport SVN commit 857734 by pino:
Workaround what it seems a Qt bug, and fully update the emoticon preview label when the current emoticon is changed, avoiding glitches from the previous on
e.

CCBUG: 166385

------------------------------------------------------------------------
r858262 | rjarosz | 2008-09-07 18:34:29 +0200 (Sun, 07 Sep 2008) | 5 lines

Backport fix for bug 169235: Changing enconding for icq crashes kopete.
Filter out unavailable encodings.

CCBUG:169235

------------------------------------------------------------------------
r858378 | pino | 2008-09-08 00:18:16 +0200 (Mon, 08 Sep 2008) | 2 lines

simplier (and working) way to layout the widgets

------------------------------------------------------------------------
r859241 | uwolfer | 2008-09-09 22:23:45 +0200 (Tue, 09 Sep 2008) | 1 line

Backport: add text to checkbox which has accidentally been remove some time ago
------------------------------------------------------------------------
r859537 | duffeck | 2008-09-10 17:09:11 +0200 (Wed, 10 Sep 2008) | 3 lines

backport: fix crash when the account is logged in somewhere else
BUG:162090

------------------------------------------------------------------------
r860626 | pino | 2008-09-13 16:58:27 +0200 (Sat, 13 Sep 2008) | 2 lines

do nothing if the user canceled the avatar selection

------------------------------------------------------------------------
r860922 | woebbe | 2008-09-14 18:29:02 +0200 (Sun, 14 Sep 2008) | 1 line

warning: suggest parentheses around comparison in operand of |
------------------------------------------------------------------------
r862404 | rjarosz | 2008-09-18 21:28:14 +0200 (Thu, 18 Sep 2008) | 5 lines

Backport fix for bug 171231: kopete crashes on exit if you performed an incomplete icq account creation

CCBUG: 171231


------------------------------------------------------------------------
r863312 | rjarosz | 2008-09-21 21:27:27 +0200 (Sun, 21 Sep 2008) | 4 lines

Backport commit 863311.
Copy from chatmessagepart only if nothing is selected in textedit. Textedit has higher priority.


------------------------------------------------------------------------
r863634 | rjarosz | 2008-09-22 20:17:30 +0200 (Mon, 22 Sep 2008) | 7 lines

Backport commit 863633.
Don't show notifications if from() is 0.
Fixes bug 162760: Transfer file MSN crash.

CCBUG: 162760


------------------------------------------------------------------------
r863663 | rjarosz | 2008-09-22 21:27:46 +0200 (Mon, 22 Sep 2008) | 3 lines

Bump version - 0.60.2


------------------------------------------------------------------------
