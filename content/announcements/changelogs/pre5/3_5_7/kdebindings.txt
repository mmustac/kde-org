2007-01-24 17:47 +0000 [r626832]  mueller

	* branches/KDE/3.5/kdebindings/python/pykde/sip/kdecore/kcompletion.sip,
	  branches/KDE/3.5/kdebindings/python/pyqt/sip/qt/qdir.sip,
	  branches/KDE/3.5/kdebindings/python/pykde/sip/kdecore/bytearray.sip,
	  branches/KDE/3.5/kdebindings/python/pykde/sip/kdecore/kmacroexpander.sip,
	  branches/KDE/3.5/kdebindings/python/pykde/sip/kmdi/kmdichildfrm.sip,
	  branches/KDE/3.5/kdebindings/python/pykde/sip/kio/global.sip,
	  branches/KDE/3.5/kdebindings/python/pykde/sip/kdeui/kkeydialog.sip,
	  branches/KDE/3.5/kdebindings/python/pykde/sip/kparts/browserextension.sip,
	  branches/KDE/3.5/kdebindings/python/pykde/sip/kdecore/kconfigdata.sip,
	  branches/KDE/3.5/kdebindings/dcoppython/shell/marshaller.cpp,
	  branches/KDE/3.5/kdebindings/python/pyqt/sip/qt/qstringlist.sip,
	  branches/KDE/3.5/kdebindings/python/pykde/sip/kdecore/kurl.sip,
	  branches/KDE/3.5/kdebindings/python/pykde/sip/kdecore/kconfig.sip,
	  branches/KDE/3.5/kdebindings/python/pyqt/sip/qt/qstring.sip,
	  branches/KDE/3.5/kdebindings/python/pykde/sip/kdecore/kaccel.sip,
	  branches/KDE/3.5/kdebindings/python/pykde/sip/kio/kservicetype.sip,
	  branches/KDE/3.5/kdebindings/python/pykde/sip/kio/authinfo.sip:
	  fix build with python 2.4 again BUG:140302
	  CCMAIL:kde-packager@kde.org

2007-02-19 18:17 +0000 [r635286-635285]  rdale

	* branches/KDE/3.5/kdebindings/qtruby/rubylib/qtruby/lib/Qt/qtruby.rb,
	  branches/KDE/3.5/kdebindings/qtruby/ChangeLog: * Fixed a bug
	  where the sort method of Qt::ListView and Qt::ListViewItem was no
	  longer working, as the classes are 'Enumerable' and ruby was
	  calling the ruby sort method instead. Thanks to kelko for
	  reporting it on #kde-ruby. CCMAIL: kde-bindings@kde.org

	* branches/KDE/3.5/kdebindings/korundum/ChangeLog,
	  branches/KDE/3.5/kdebindings/korundum/rubylib/korundum/lib/KDE/korundum.rb:
	  * Fixed a bug where the sort method of KDE::ListView and
	  KDE::ListViewItem was no longer working, as the classes are
	  'Enumerable' and ruby was calling the ruby sort method instead.
	  Thanks to kelko for reporting it on #kde-ruby. CCMAIL:
	  kde-bindings@kde.org

2007-02-19 18:40 +0000 [r635295]  rdale

	* branches/KDE/3.5/kdebindings/qtjava/ChangeLog,
	  branches/KDE/3.5/kdebindings/qtjava/javalib/qtjava/Makefile.am: *
	  The QGL* classes were not being built by default, as a few years
	  ago KDE didn't use OpenGL by default. So enable them by default
	  now 3D is pretty standard. CCBUGS: 75310

2007-03-13 18:57 +0000 [r642250]  rdale

	* branches/KDE/3.5/kdebindings/korundum/rubylib/korundum/lib/KDE/korundum.rb:
	  * Added fix from volty for warnings from ruby 1.8.5 with ruby_w
	  about @functions not being defined - thanks volty.

2007-05-14 07:15 +0000 [r664515]  binner

	* branches/KDE/3.5/kdesdk/kdesdk.lsm,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/kopeteversion.h,
	  branches/arts/1.5/arts/configure.in.in,
	  branches/KDE/3.5/kdelibs/README,
	  branches/KDE/3.5/kdelibs/kdecore/ksycoca.h,
	  branches/KDE/3.5/kdepim/kdepim.lsm,
	  branches/KDE/3.5/kdevelop/kdevelop.lsm,
	  branches/KDE/3.5/kdewebdev/quanta/quanta.lsm,
	  branches/KDE/3.5/kdebase/startkde,
	  branches/KDE/3.5/kdeaccessibility/kdeaccessibility.lsm,
	  branches/arts/1.5/arts/arts.lsm,
	  branches/KDE/3.5/kdeadmin/kdeadmin.lsm,
	  branches/KDE/3.5/kdeaddons/kdeaddons.lsm,
	  branches/KDE/3.5/kdelibs/kdelibs.lsm,
	  branches/KDE/3.5/kdeartwork/kdeartwork.lsm,
	  branches/KDE/3.5/kdenetwork/kdenetwork.lsm,
	  branches/KDE/3.5/kdebase/kdebase.lsm,
	  branches/KDE/3.5/kdemultimedia/kdemultimedia.lsm,
	  branches/KDE/3.5/kdelibs/kdecore/kdeversion.h,
	  branches/KDE/3.5/kdegames/kdegames.lsm,
	  branches/KDE/3.5/kdewebdev/kdewebdev.lsm,
	  branches/KDE/3.5/kdeedu/kdeedu.lsm,
	  branches/KDE/3.5/kdebindings/kdebindings.lsm,
	  branches/KDE/3.5/kdebase/konqueror/version.h,
	  branches/KDE/3.5/kdetoys/kdetoys.lsm,
	  branches/KDE/3.5/kdewebdev/quanta/src/quanta.h,
	  branches/KDE/3.5/kdegraphics/kdegraphics.lsm,
	  branches/KDE/3.5/kdeutils/kdeutils.lsm: bump version numbers for
	  3.5.7

