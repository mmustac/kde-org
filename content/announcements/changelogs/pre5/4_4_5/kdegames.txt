------------------------------------------------------------------------
r1136748 | gpelladi | 2010-06-11 04:33:06 +1200 (Fri, 11 Jun 2010) | 2 lines

BUG: 241256
Fix a crash that occurs if you undo when only a computer mark is on the table.
------------------------------------------------------------------------
r1138033 | scripty | 2010-06-15 14:46:23 +1200 (Tue, 15 Jun 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1140158 | scripty | 2010-06-20 14:08:27 +1200 (Sun, 20 Jun 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1141601 | scripty | 2010-06-23 14:07:23 +1200 (Wed, 23 Jun 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
