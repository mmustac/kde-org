2005-03-04 17:09 +0000 [r394900-394894]  aseigo

	* kicker/applets/clock/clock.cpp: backport: fix for label getting
	  split when spurious heightForWidth or widthForHeight calls are
	  made

	* kicker/applets/systemtray/systemtrayapplet.cpp: backport: don't
	  cut off icons in the hide button that are >16px, even though we
	  explicitly ask the icon loader for a 16px icon. BUG:100693

	* kicker/applets/taskbar/taskbarapplet.cpp: backport: grow the
	  taskbar properly

	* kicker/core/showdesktop.cpp, kicker/share/kickertip.cpp:
	  backport: filter out mouse over effects from show desktop lists

	* kicker/core/extensionmanager.cpp, kicker/core/extensionmanager.h,
	  kicker/core/container_extension.cpp: backport: prevent the menu
	  panel from being moved via drag 'n drop

2005-03-04 19:05 +0000 [r394918]  aseigo

	* kicker/applets/clock/clock.cpp: proper layout on "date first"
	  when switching time zones BUG:100724

2005-03-05 17:11 +0000 [r395087]  waba

	* kmenuedit/treeview.cpp: Don't show menu-item twice after copying
	  it elsewhere

2005-03-05 19:43 +0000 [r395113]  pletourn

	* konqueror/listview/konq_listviewwidget.cc: Obey preview settings
	  BUG:100601

2005-03-05 20:42 +0000 [r395127]  pletourn

	* konqueror/konq_view.cc: Use KURL::fromPathOrURL() BUG:100276

2005-03-05 22:44 +0000 [r395155]  adridg

	* konsole/other/vt420pc.keytab: Revert keytab change so that vt420
	  works again. See also
	  http://www.freebsd.org/cgi/query-pr.cgi?pr=78439 BUGS: 75817

2005-03-05 23:05 +0000 [r395160]  ossi

	* kdm/backend/auth.c, kdm/configure.in.in: handle getifaddrs
	  properly

2005-03-06 00:07 +0000 [r395167]  ossi

	* kdm/backend/auth.c: don't copy stale Xauth entries for our
	  display.

2005-03-06 05:36 +0000 [r395185]  pletourn

	* konqueror/konq_combo.cc: Reinit the pixmap when the item's
	  contents changed BUG:100256

2005-03-06 16:04 +0000 [r395262]  adawit

	* kcontrol/kio/kcookiespolicies.cpp: - Backport fix for bug 51580 &
	  75250 along with a unit test for the fix.

2005-03-06 16:57 +0000 [r395280]  alund

	* kate/app/katefilelist.h, kate/app/katefilelist.cpp: backport fix
	  for 100848 CCBUG: 100848

2005-03-08 01:25 +0000 [r395652]  adawit

	* kdesktop/minicli.cpp: Backport: 2nd part of fix for Bug 100733.
	  Reviewed by dfaure

2005-03-08 13:18 +0000 [r395767-395765]  lunakl

	* kwin/geometry.cpp: Backport #87298.

	* kwin/group.cpp: Backport #95231.

	* konqueror/konq_combo.cc: Backport completion popup fix.

2005-03-08 13:20 +0000 [r395768]  waba

	* kcontrol/kfontinst/kcmfontinst/KCmFontInst.cpp,
	  kcontrol/kfontinst/kio/KioFonts.cpp: Use i18n(KFI_KIO_FONTS_USER)
	  in message boxes but accept KFI_KIO_FONTS_USER in the slave as
	  well.

2005-03-08 14:28 +0000 [r395792]  lunakl

	* ksplashml/wndmain.cpp, ksplashml/wndmain.h: Backport progress
	  state fixes.

2005-03-08 14:36 +0000 [r395797]  waba

	* kcontrol/randr/krandrmodule.cpp: Don't crash. CCMAIL:
	  adrian@suse.de

2005-03-08 14:52 +0000 [r395806]  lunakl

	* klipper/klipperrc.desktop: Backport - more WM_CLASS values for
	  Firefox.

2005-03-09 11:54 +0000 [r396053]  coolo

	* kcontrol/kthememanager/kthemedlg.ui.h: backporting fix for not
	  finding background

2005-03-09 13:01 +0000 [r396060]  lukas

	* kicker/menuext/kdeprint/print_mnu.cpp: do as Waldo said and start
	  something at least

2005-03-09 13:15 +0000 [r396070]  waba

	* kdesktop/krootwm.cc: Use proper menu-id's for module names, fixes
	  KIOSK restrictions.

2005-03-09 13:35 +0000 [r396076]  lunakl

	* kdm/kfrontend/genkdmconf.c: Backport r1.106.

2005-03-09 14:39 +0000 [r396086]  dfaure

	* kioslave/trash/trashimpl.cpp: Sync with HEAD: Fixed the
	  permission test.

2005-03-09 14:40 +0000 [r396087]  waba

	* knetattach/knetattach.ui.h: Show "User:" label for Ftp.

2005-03-09 15:04 +0000 [r396100]  lunakl

	* kwin/layers.cpp: Backport r2.40 (#100762).

2005-03-09 21:00 +0000 [r396204]  craig

	* kcontrol/kfontinst/kio/KioFonts.cpp,
	  kcontrol/kfontinst/lib/FcEngine.cpp: Use QTextStream as apposed
	  to QDataStream when saving/reading url. BUGS:100482

2005-03-10 16:16 +0000 [r396383]  reed

	* kdm/configure.in.in: (backport) utmpx is missing/broken on OSX

2005-03-10 16:29 +0000 [r396388]  lunakl

	* kwin/layers.cpp: Backport #101209.

2005-03-11 10:50 +0000 [r396615]  dfaure

	* kioslave/trash/trashimpl.cpp: Don't mount all automounted
	  directories when starting kio_trash, it can take a VERY long
	  time. (The result is that trashing from such directories will
	  move the file to $HOME, which is OK I think). BUG: 101116

2005-03-11 16:57 +0000 [r396700-396698]  adrian

	* kate/app/katefilelist.cpp: do not return random data

	* kate/app/katefilelist.cpp: revert last change and return null
	  pointer to be in sync with HEAD

2005-03-12 21:26 +0000 [r397066]  lukas

	* kicker/extensions/dockbar/dockbarextension.cpp: show the stuff
	  translated

2005-03-12 22:00 +0000 [r397071]  aseigo

	* kicker/taskbar/taskbar.cpp: backport of fitt's law fix.

2005-03-13 19:55 +0000 [r397354]  thiago

	* kioslave/sftp/sftpfileattr.cpp: (Backport 1.14:1.15) Backporting
	  the bug fix to KDE 3.4.x. CCBUG:66411

2005-03-14 18:24 +0000 [r397586]  mueller

	* kwin/layers.cpp: fix nonreproducible crash (backport revision
	  2.43)

2005-03-14 20:36 +0000 [r397623]  mkoller

	* kcontrol/joystick/joydevice.cpp: added #include <sys/time.h> on
	  request from Leo Savernik

2005-03-15 08:53 +0000 [r397739]  goffioul

	* kdeprint/kdeprintfax/faxctrl.cpp: [Backport] Properly quote
	  anytops arguments.

2005-03-15 12:00 +0000 [r397785]  lunakl

	* kicker/applets/menu/menuapplet.cpp: Backport r1.21 (menu width
	  fix).

2005-03-15 15:34 +0000 [r397828]  staikos

	* nsplugins/viewer/nsplugin.cpp: backport crash fix for 100863

2005-03-15 19:03 +0000 [r397868]  staikos

	* nsplugins/viewer/nsplugin.cpp: backport encoding fix

2005-03-16 18:18 +0000 [r398162]  hindenburg

	* kcontrol/konsole/sessioneditor.cpp: Allow blue/red icons to show
	  up in preview button

2005-03-16 18:27 +0000 [r398164]  jriddell

	* ksplashml/themeengine/redmond/previewredmond.cpp,
	  ksplashml/themeengine/redmond/themeredmond.cpp: Use an icon which
	  exists

2005-03-17 15:29 +0000 [r398403]  pletourn

	* kioslave/tar/tar.cc: Send the data in small pieces
	  (Connection::sendnow() has a 16MB limit) BUG:101486

2005-03-17 15:45 +0000 [r398409]  waba

	* kmenuedit/treeview.cpp: Don't lose recent changes when moving a
	  folder around. CCBUG:93241

2005-03-17 16:31 +0000 [r398418]  waba

	* kmenuedit/treeview.cpp: Deleting the old menu isn't needed and
	  causes problems.

2005-03-17 16:45 +0000 [r398421]  waba

	* kmenuedit/menuinfo.h, kmenuedit/treeview.cpp,
	  kmenuedit/menuinfo.cpp: Fix the following move sequence: /A ->
	  /B/A /B -> /C/B /C/B/A -> /A

2005-03-17 17:04 +0000 [r398425]  pletourn

	* konqueror/listview/konq_listviewwidget.cc: Allow standard
	  tooltips for truncated items in non-executable columns

2005-03-17 17:47 +0000 [r398431]  pletourn

	* konqueror/konq_combo.cc: Reset the background color, not the
	  whole palette

2005-03-18 00:01 +0000 [r398573]  cramblitt

	* konqueror/kttsplugin/khtmlkttsd.rc: Bug Fix. Unable to disable
	  TTS plugin in Konqi Configure Extensions because Name attribute
	  in .rc file did not match X-KDE-PLUGIN-INFO-NAME in .desktop
	  file.

2005-03-18 15:06 +0000 [r398729]  lunakl

	* kcontrol/access/kcmaccess.cpp: Backport r1.45.

2005-03-18 15:40 +0000 [r398739]  lunakl

	* kdesktop/desktop.cc: Backport #100180.

2005-03-18 22:05 +0000 [r398847]  pletourn

	* konqueror/listview/konq_listview.cc: Enable the trash action
	  inside media:/ BUG:101839

2005-03-18 22:42 +0000 [r398857]  pletourn

	* konqueror/listview/konq_infolistviewwidget.cc: Don't clear the
	  old metainfos after adding a new item BUG:101052

2005-03-18 22:52 +0000 [r398859]  pletourn

	* konqueror/listview/konq_infolistview.rc: Add 'view as' action in
	  the menu Remove irrelevant actions

2005-03-19 15:02 +0000 [r398980]  ossi

	* kdm/kfrontend/genkdmconf.c: fix writeout of edited script files

2005-03-20 03:22 +0000 [r399121]  hindenburg

	* konsole/konsole/konsole.cpp: disable close tab button when
	  detaching session leaves only 1 tab

2005-03-21 17:48 +0000 [r399475]  neundorf

	* konqueror/listview/konq_textviewitem.cc: -fix sorting for the
	  textview Alex

2005-03-22 05:32 +0000 [r399613]  staikos

	* knetattach/knetattach.ui.h: bcakport fix for port setting with
	  fish

2005-03-22 10:26 +0000 [r399662]  lukas

	* l10n/tr/entry.desktop: Turkey changed its monetary symbol BUGS:
	  102147

2005-03-22 15:16 +0000 [r399728]  lunakl

	* konqueror/konq_extensionmanager.cc: Backport r1.6.

2005-03-23 13:50 +0000 [r399989]  lunakl

	* kwin/activation.cpp: Backport #96028.

2005-03-23 22:26 +0000 [r400119]  ossi

	* kdm/configure.in.in: fix interpretation of make output

2005-03-24 20:16 +0000 [r400318]  coolo

	* kioslave/media/kdedmodule/halbackend.cpp: try harder not to crash
	  kded

2005-03-24 21:45 +0000 [r400332]  mkoller

	* kcontrol/colors/colorscm.cpp: BUG: 102400 now correctly store the
	  shadeSortColumn setting in the KDE global config file

2005-03-24 22:41 +0000 [r400348]  mkoller

	* kcontrol/colors/colorscm.cpp, kcontrol/colors/colorscm.h: BUG:
	  102400 Invalidate currentScheme when changing shadeSortColumn
	  flag (and therefore now correctly select "current Schema" after
	  applying) Save shadeSortColumn also in the schema file

2005-03-25 12:57 +0000 [r400493]  dfaure

	* konqueror/konq_extensionmanager.cc,
	  konqueror/konq_extensionmanager.h, konqueror/konq_mainwindow.cc:
	  Backport "reload plugins after enabling/disabling them". This has
	  to go together with "show plugins for active part instead of
	  always khtml" since we need the active part for the reloading.
	  Downside is that when looking at an iconview there is no
	  configurable plugin in 3.4-branch...

2005-03-25 16:38 +0000 [r400561]  rich

	* kicker/extensions/kasbar/kasloaditem.cpp,
	  kicker/extensions/kasbar/configure.in.in (added): Patch from
	  Michael Ritzert <kde@ritzert.de> for solaris. CCMAIL:
	  99100@bugs.kde.org BACKPORT

2005-03-25 22:04 +0000 [r400637]  ervin

	* kioslave/media/kdedmodule/linuxcdpolling.h,
	  kioslave/media/kdedmodule/linuxcdpolling.cpp: Backporting fix for
	  #102163 for KDE 3.4.x. CCBUGS:102163

2005-03-26 13:54 +0000 [r400754]  waba

	* kdesktop/krootwm.cc: KIOSK: Get rid of bogus asserts

2005-03-27 14:04 +0000 [r400991]  montanaro

	* kwin/clients/b2/b2client.cpp: Simple fix for the preview
	  rendering bug.

2005-03-28 02:55 +0000 [r401159]  thiago

	* konqueror/shellcmdplugin/kshellcmdexecutor.cpp: Backporting the
	  fix for bug #102440 (committed in r1.8). BUG:102440

2005-03-28 04:02 +0000 [r401173]  staikos

	* knetattach/knetattach.ui.h: backport fix: add / to path in FTP
	  mode if empty

2005-03-28 19:42 +0000 [r401376]  hindenburg

	* konsole/konsole/keytrans.cpp: Fix compile errors on amd64 with
	  gcc4

2005-03-28 20:02 +0000 [r401383]  waba

	* kmenuedit/khotkeys.cpp: Don't crash on exit when kcm_khotkeys.la
	  can't be found.

2005-03-29 15:03 +0000 [r401631]  lunakl

	* ksmserver/shutdown.cpp: Backport #102640.

2005-03-29 18:36 +0000 [r401701]  aseigo

	* kicker/core/panelextension.cpp: backport fix for BR#101695
	  CCBUG:101695

2005-03-30 21:25 +0000 [r402029]  lodewyck

	* kioslave/media/kdedmodule/halbackend.cpp,
	  kioslave/media/configure.in.in: Backport of Coolo's patch
	  completion about halDrive check configure.in.in check for HAL 0.4
	  : KDE 3.4 does not compile with HAL 0.5

2005-03-30 21:42 +0000 [r402031]  waba

	* kcontrol/background/bgsettings.cpp: Try to keep the current
	  wallpaper when making small changes to the pictures in the slide
	  show BUG: 71962

2005-03-31 12:42 +0000 [r402144]  lunakl

	* khotkeys/kcontrol/menuedit.cpp: Backport #91782.

2005-04-01 07:31 +0000 [r402332]  mlaurent

	* kicker/applets/launcher/quicklauncher.cpp: Backport fix dnd when
	  menu name has a space in name

2005-04-01 20:50 +0000 [r402511]  hindenburg

	* konsole/konsole/konsole.cpp,
	  kicker/menuext/konsole/konsole_mnu.cpp: Expand ~ in sessions'
	  Exec= Backport of 102941

2005-04-01 22:03 +0000 [r402533]  hindenburg

	* konsole/konsole/TEmulation.h, konsole/konsole/TEmuVt102.cpp,
	  konsole/konsole/konsole.cpp, konsole/konsole/konsole.h: Allow
	  xterm resize ESC code to work . Backport of 95932

2005-04-02 10:06 +0000 [r402598]  larkang

	* kcontrol/info/main.cpp: Backport: Fix export

2005-04-02 10:47 +0000 [r402608]  aseigo

	* kicker/core/container_applet.cpp, kicker/core/kicker.cpp,
	  kicker/core/containerarealayout.cpp,
	  kicker/core/containerarea.cpp, kicker/core/container_button.cpp,
	  kicker/core/kicker.h, kicker/core/container_extension.cpp,
	  kicker/core/containerarealayout.h, kicker/core/containerarea.h:
	  backport of placement improvement by Waldo

2005-04-03 16:15 +0000 [r402879]  aseigo

	* kcontrol/kicker/positiontab_impl.cpp: Backport: allow setting a
	  panel in the lower left in RTL CCBUG:103117

2005-04-03 21:32 +0000 [r402966]  binner

	* kicker/menuext/prefmenu/prefmenu.cpp, kicker/ui/service_mnu.cpp:
	  Fix sort logic, broken since 1.92

2005-04-04 11:03 +0000 [r403089]  ervin

	* kioslave/media/mediaimpl.cpp: Avoid displaying messagebox while
	  mounting. If mount issues a warning it makes the ioslave crash
	  (since it is a KApplication with nogui) Backporting to 3.4.x
	  branch. BUGS:102086

2005-04-04 11:59 +0000 [r403100]  adrian

	* kcontrol/kfontinst/kio/KioFonts.cpp,
	  kcontrol/kfontinst/kfontinst/Fontmap.cpp,
	  kcontrol/kfontinst/lib/FcEngine.cpp: fix warnings from gcc 4

2005-04-04 17:59 +0000 [r403159]  binner

	* konqueror/konq_viewmgr.cc: Do some calculation to get a always
	  visible "tab loading" color (#70065)

2005-04-04 19:59 +0000 [r403183]  binner

	* kcontrol/background/bgwallpaper.cpp: Bug 102244: SVG files
	  unavail from file selector in slide show

2005-04-04 21:06 +0000 [r403196]  binner

	* kcontrol/background/bgwallpaper.cpp,
	  kcontrol/background/bgdialog.cpp: missing i18n

2005-04-04 22:02 +0000 [r403198]  dfaure

	* kdeprint/slave/kio_print.cpp,
	  kdeprint/kdeprint_part/kdeprint_part.desktop: Backport 2005-03-11
	  commit: use the proper mimetype, and raise initial preference of
	  its viewer, so that print:/manager shows the print manager.

2005-04-04 22:27 +0000 [r403207]  binner

	* libkonq/konq_operations.cc: Bug 100597: SVG images don't have
	  'set as wallpaper' entry in context menu when dragged to desktop

2005-04-05 01:58 +0000 [r403229]  thiago

	* konqueror/konq_main.cc: Backporting the encoding issue to branch.
	  See r1.146.

2005-04-05 03:42 +0000 [r403240]  aseigo

	* kicker/core/panelextension.cpp, kicker/core/container_button.cpp:
	  backport missed insertion point

2005-04-05 15:14 +0000 [r403320]  binner

	* libkonq/konq_popupmenu.cc: If Shift is pressed when menu opens
	  show 'Delete' instead of 'Trash' (#100394)

2005-04-05 15:43 +0000 [r403336]  binner

	* kcontrol/kthememanager/kthememanager.cpp: Don't scale up preview
	  (#100781) BUG:100781

2005-04-05 20:45 +0000 [r403403]  binner

	* kioslave/trash/trash.protocol: fix icon

2005-04-06 15:51 +0000 [r403567]  thiago

	* startkde: Backporting the fix for 'source' from HEAD. (1.158)

2005-04-06 19:37 +0000 [r403593]  thiago

	* startkde: Reversing the last commit: . should be used, not source

2005-04-07 10:20 +0000 [r403731]  adrian

	* kwin/workspace.cpp: fix possible kwin crash on dcop call, if no
	  kompmgr is running

2005-04-07 10:52 +0000 [r403737]  dfaure

	* kdesktop/desktop.cc: Don't crash when configure() is called (via
	  DCOP) before the delayed initialization was done. BUG: 103412

2005-04-07 18:00 +0000 [r403843]  binner

	* kicker/applets/systemtray/systemtrayapplet.cpp: Hidden icons
	  cannot be sorted

2005-04-08 14:31 +0000 [r404070]  lunakl

	* kwin/tabbox.cpp: Backport #103262.

2005-04-09 00:50 +0000 [r404209]  aseigo

	* kicker/core/containerarea.cpp: backport of sane drag and drop
	  placement

2005-04-09 03:50 +0000 [r404226]  aseigo

	* kicker/core/containerarea.cpp: backport of fix for BR#102857
	  CCBUGS:102857

2005-04-09 20:39 +0000 [r404456]  craig

	* kcontrol/kfontinst/kfontinst/Main.cpp: Remove silly fprintf debug
	  that was accidentaly commited! The debug writes to /tmp/kfi -
	  however, if root has run kfontinst, then /tmp/kfi will have been
	  created as root, and when a non-root user runs kfontinst it cant
	  open this file for writing, but no check is made before the
	  fprintf, and hence a segmentation fault! BUGS:103541

2005-04-10 16:41 +0000 [r404587]  binner

	* konqueror/konq_mainwindow.cc: missing icons for Go/ entries

2005-04-10 19:44 +0000 [r404623]  claudiu

	* kcontrol/ebrowsing/plugins/ikws/searchproviders/google.desktop:
	  Google search in Romanian

2005-04-11 10:15 +0000 [r404742]  adrian

	* nsplugins/pluginscan.cpp: add $HOME/.mozilla/plugins path to scan
	  list

2005-04-11 11:43 +0000 [r404768]  lunakl

	* kwin/geometry.cpp: Backport #103586.

2005-04-12 09:54 +0000 [r405014]  dfaure

	* kdm/kfrontend/themer/kdmlabel.cpp: backport no-c-format addition

2005-04-12 14:13 +0000 [r405065]  lukas

	* kioslave/media/kfile-plugin/kfilemediaplugin.cpp: fix
	  untranslated strings

2005-04-12 18:54 +0000 [r405124]  dhaumann

	* kate/app/kateviewspace.cpp: backport Anders' fix: update the
	  modified part of the statusbar when the view changes. CCBUG:
	  103299

2005-04-12 21:06 +0000 [r405158]  dhaumann

	* kate/data/kateui.rc, kate/app/katemainwindow.cpp: backports :o -
	  Cullmann, katemainwindow: don't remove the next/prev actions from
	  document menu - Anders, kateui.rc: remove next/prev from the rmb
	  menu, as agreed on kwrite-devel and #kate.

2005-04-13 01:11 +0000 [r405209]  mueller

	* konqueror/konq_mainwindow.cc: fixing gcc4 konqueror
	  "miscompilation"

2005-04-13 12:51 +0000 [r405298]  lunakl

	* kwin/group.cpp: Backport #94800.

2005-04-13 14:17 +0000 [r405334]  staikos

	* kcontrol/crypto/crypto.cpp, kcontrol/crypto/crypto.h: backport
	  half of fix for 61446

2005-04-13 17:56 +0000 [r405387]  craig

	* kcontrol/kfontinst/kio/KioFonts.cpp: When installing a font,
	  remove any spaces or dashes, and convert the extension to
	  lowercase. BUGS:96622

2005-04-14 09:01 +0000 [r405494]  dfaure

	* konqueror/sidebar/trees/history_module/kcmhistory.cpp: Backport
	  fix for kcontrol crash BUG: 100348

2005-04-14 20:40 +0000 [r405616]  markusb

	* kcontrol/info/info_fbsd.cpp: Backport fix for bug 101686: failure
	  to read 'PCI' data, message that 'it may require root priv...'
	  BUG:101686

2005-04-15 17:17 +0000 [r405760]  mueller

	* kcontrol/krdb/krdb.cpp: set button order (backport revision 1.61)

2005-04-19 20:47 +0000 [r406625]  pletourn

	* konqueror/sidebar/trees/history_module/kcmhistory.cpp: Allow the
	  history to be disabled BUG:103941

2005-04-20 07:21 +0000 [r406682]  binner

	* konqueror/konq_view.cc, konqueror/konq_view.h: Bug 104227:
	  address bar encryption color stays when using back/forward

2005-04-20 08:18 +0000 [r406684]  binner

	* libkonq/konq_popupmenu.cc: Give "Empty Trash Bin" an icon

2005-04-20 19:37 +0000 [r406797]  craig

	* kcontrol/kfontinst/kfontinst/Fontmap.cpp: Better checking of
	  existing and alias entries

2005-04-20 19:46 +0000 [r406799]  craig

	* kcontrol/kfontinst/kio/KioFonts.cpp: Dont allow to install hidden
	  fonts - as fontconfig will ignore these, and therefore fonts:/
	  will not display them, and the user will think that the file has
	  been lost!

2005-04-20 20:20 +0000 [r406807]  craig

	* kcontrol/kfontinst/kio/KioFonts.cpp,
	  kcontrol/kfontinst/kio/KioFonts.h: After performing an operation
	  (specifically deleteing/moving), configure the modified folder -
	  as opposed to just the default folder.

2005-04-21 20:58 +0000 [r407010]  aseigo

	* kicker/core/applethandle.cpp: handle mouse overing applets with
	  out of process items properly

2005-04-21 23:56 +0000 [r407038]  aseigo

	* kicker/taskmanager/taskmanager.h,
	  kicker/extensions/kasbar/kastasker.h,
	  kicker/taskbar/taskcontainer.h,
	  kicker/extensions/kasbar/kasgroupitem.cpp,
	  kicker/taskbar/taskbar.cpp, kicker/taskmanager/tasklmbmenu.cpp,
	  kicker/extensions/kasbar/kasgroupitem.h,
	  kicker/extensions/kasbar/kasstartupitem.cpp,
	  kicker/extensions/kasbar/kastaskitem.cpp,
	  kicker/taskbar/taskbar.h,
	  kicker/extensions/kasbar/kasgrouper.cpp,
	  kicker/taskmanager/taskrmbmenu.cpp,
	  kicker/applets/menu/menuapplet.cpp,
	  kicker/extensions/kasbar/kasstartupitem.h,
	  kicker/extensions/kasbar/kastaskitem.h,
	  kicker/extensions/kasbar/kasgrouper.h,
	  kicker/extensions/kasbar/kastasker.cpp,
	  kicker/taskmanager/taskmanager.cpp,
	  kicker/taskmanager/taskrmbmenu.h,
	  kicker/taskbar/taskcontainer.cpp: the great ksharedptr switcharoo

2005-04-22 17:38 +0000 [r407183]  hindenburg

	* konsole/konsole/schema.cpp: Fix incorrect schema in detached
	  sessions. CCBUG: 98472

2005-04-22 20:24 +0000 [r407223]  craig

	* kcontrol/kfontinst/kio/KioFonts.cpp: Also replace ':' in
	  filenames with '_' (fixes potential problems with X, where faces
	  for a ttc are used as :<face no>: <xlfd>)

2005-04-25 00:45 +0000 [r407651]  aseigo

	* kicker/taskbar/taskbar.cpp: backport fix for 104463 CCBUG:104463

2005-04-25 15:57 +0000 [r407800]  lunakl

	* kcontrol/randr/randr.cpp: Backport - don't fail when switching to
	  a resolution with fewer rates.

2005-04-25 19:46 +0000 [r407845]  dfaure

	* kioslave/fish/fish.cpp: Backport fix for #69333: Fixed mimetype
	  determination for kio_fish: look at extension first, and fallback
	  to what file(1) says only if the extension is unknown. This
	  allows opening kword documents over fish://.

2005-04-26 15:02 +0000 [r407957]  mlaurent

	* kcontrol/background/bgdialog.cpp: Backport : disable action in
	  read-only mode

2005-04-26 22:13 +0000 [r408065]  adawit

	* kioslave/sftp/kio_sftp.cpp, kioslave/sftp/kio_sftp.h,
	  kioslave/sftp/atomicio.cpp, kioslave/sftp/sftpfileattr.cpp,
	  kioslave/sftp/sftpfileattr.h: Backport fixes for BR# 99928,
	  102103, 103729

2005-04-27 11:40 +0000 [r408151]  lunakl

	* kwin/kwinbindings.cpp: Backport r1.50.

2005-04-28 09:57 +0000 [r408340]  lunakl

	* kpager/kpager.cpp: Backport #103301.

2005-04-28 15:08 +0000 [r408417]  adawit

	* kioslave/sftp/sftpfileattr.cpp: - Backport fix for BR# 103729

2005-04-29 14:35 +0000 [r408622]  lunakl

	* kcontrol/konq/desktopbehavior_impl.cpp: Backport #71484.

2005-04-29 15:14 +0000 [r408634]  lunakl

	* kwin/events.cpp: Backport #83378.

2005-05-01 07:25 +0000 [r408952]  binner

	* kcontrol/kcontrol/toplevel.cpp: icons on tabs

2005-05-01 19:33 +0000 [r409097]  dhaumann

	* kate/app/kateapp.cpp: fix mainwindow bug: start kate on another
	  desktop opened a new kate window. The behavior now is: If there
	  is already a kate window on current desktop activate it. If there
	  is none, look for a kate window on another desktop, if found, use
	  it. If there is really no kate mainwindow at all, create a new
	  one. CCMAIL: kwrite-devel@kde.org

2005-05-04 13:29 +0000 [r409260-409258]  lunakl

	* kwin/geometry.cpp: Backport #80545.

	* kwin/tabbox.cpp: Backport #84424.

2005-05-04 16:12 +0000 [r409326]  dfaure

	* kioslave/settings/kio_settings.cc: make clicking on settings:'s
	  items work again (backport) BUG: 103766

2005-05-05 12:44 +0000 [r409715]  lunakl

	* kwin/clients/default/kdedefault.cpp,
	  kwin/clients/modernsystem/modernsys.cpp,
	  kwin/clients/quartz/quartz.cpp,
	  kwin/clients/laptop/laptopclient.cpp,
	  kwin/clients/redmond/redmond.cpp,
	  kwin/clients/kwmtheme/kwmthemeclient.cpp,
	  kwin/clients/b2/b2client.cpp, kwin/clients/keramik/keramik.cpp,
	  kwin/clients/web/Web.cpp: Backport #96079.

2005-05-06 15:48 +0000 [r410066]  adawit

	* kioslave/sftp/kio_sftp.cpp: Backport the upload file permission
	  fixes.

2005-05-06 20:33 +0000 [r410147]  giessl

	* kcontrol/krdb/kcs/Plastik.kcsrc, kcontrol/colors/colorscm.cpp,
	  kcontrol/krdb/themes/Plastik/Plastik.xml: Backport revision
	  401292 (svn doesn't let me view the differences of the .kcsrc
	  file because it thinks it's a binary file. hope everything is ok,
	  though): - Set Plastik's Active/Inactive Title Button color to
	  the grayish color as it was in Keramik. Plastik itself doesn't
	  use the color itself, but other window decorations (or some color
	  shemes inheriting them) use it and work better with the grey
	  color. - Fix the color kcm "KDE Default" sheme to _really_ be
	  Plastik colors, apply the color change as well.
	  http://lists.kde.org/?l=kwin&m=111141057930266&w=2

2005-05-06 22:30 +0000 [r410183]  markusb

	* kcontrol/usbview/usbdevices.cpp: Fix build on FreeBSD 4.9 and
	  below

2005-05-08 08:57 +0000 [r410710]  binner

	* l10n/Makefile.am: fix for .svn/

2005-05-09 10:29 +0000 [r411448]  dfaure

	* kdesktop/kdesktop.kcfg: Backported #105322 fix with svn merge
	  -r411441:411442 $SVNURL/trunk/KDE/kdebase/kdesktop/kdesktop.kcfg
	  (makes it easy to paste from the trunk commit log)

2005-05-09 13:04 +0000 [r411487]  lunakl

	* ksmserver/server.cpp: Backport fix for #49042, comment #19.

2005-05-09 22:02 +0000 [r411688]  craig

	* kcontrol/kfontinst/kio/KioFonts.cpp: "modifyName()" of file
	  before creating afm.

2005-05-10 09:04 +0000 [r411858]  lunakl

	* kwin/activation.cpp: Backport raising modal when activating.

2005-05-10 10:32 +0000 [r411876]  binner

	* kdebase.lsm: update lsm for release

2005-05-10 10:42 +0000 [r411884]  binner

	* konqueror/version.h, startkde: Increasing versions listed in
	  RELEASE-CHECKLIST

2005-05-10 18:06 +0000 [r412078-412076]  adawit

	* kioslave/sftp/templates (removed), kioslave/sftp/kio_sftp.kdevprj
	  (removed): Remove outdated kdevelop project files

	* kioslave/sftp/sftpfileattr.cpp: - Fix regression of BR# 66411.
	  BUGS:66411

2005-05-10 22:12 +0000 [r412199]  craig

	* kcontrol/kfontinst/kfontinst/XConfig.cpp: Preserve any previous
	  ttcap entries.

2005-05-10 22:58 +0000 [r412218]  adawit

	* kcontrol/kio/kmanualproxydlg.cpp: Backport fix for BR# 90624

2005-05-11 16:44 +0000 [r412423]  aseigo

	* kicker/applets/clock/clock.cpp: backport fix for BR86437
	  CCBUGS:86437

2005-05-12 06:42 +0000 [r412612]  binner

	* libkonq/konq_pixmapprovider.h, konqueror/konq_tabs.cc,
	  libkonq/konq_pixmapprovider.cc: No ugly disabled icon when
	  closing a tab

2005-05-13 12:33 +0000 [r413127]  lunakl

	* kwin/events.cpp: Backport #72346.

2005-05-13 13:02 +0000 [r413141]  lunakl

	* drkonqi/main.cpp, startkde: Backport waiting for drkonqi on
	  shutdown.

2005-05-13 13:13 +0000 [r413144]  lunakl

	* kicker/core/showdesktop.cpp, kicker/share/kickertip.cpp: Backport
	  WX11BypassWM for KickerTip.

2005-05-13 15:44 +0000 [r413195]  lunakl

	* konqueror/konq_mainwindow.cc: Backport, don't offer both
	  'kde.org' and 'http://kde.org/' in completion.

2005-05-13 15:57 +0000 [r413201]  lunakl

	* kwin/kcmkwin/kwindecoration/kwindecoration.cpp: Backport #97031.

2005-05-13 23:53 +0000 [r413409]  craig

	* kcontrol/kfontinst/kcmfontinst/KFileFontView.cpp: Disable
	  dragging of items - seems to be getting "stuck" :-(

2005-05-14 11:58 +0000 [r413706]  binner

	* khelpcenter/version.h: Tired of increasing this every time

2005-05-15 00:37 +0000 [r413956-413955]  hindenburg

	* konsole/other/icons/cr16-action-activity.png (added),
	  konsole/other/icons/cr16-action-silence.png (added): KDE3.3
	  crystal idea and ktip icons renamed for use with monitor for
	  activity/silence

	* konsole/konsole/konsole.cpp: Copy KDE 3.3's ktip.png to
	  silence.png and idea.png to activity.png for use with Monitor for
	  Activity/Silence. CCBUG: 103554

2005-05-15 06:55 +0000 [r414031]  binner

	* kcontrol/fonts/fonts.cpp: Bug 101563: tooltips under font preview
	  text areas with '&'

2005-05-15 16:31 +0000 [r414253]  mlaurent

	* kicker/menuext/konq-profiles/konqy_menu.cpp,
	  kicker/menuext/konq-profiles/konqy_menu.h: Backport update
	  profile list

2005-05-15 17:21 +0000 [r414267]  pletourn

	* kioslave/info/kde-info2html: Escape '#' BUG:96485

2005-05-17 08:38 +0000 [r414894]  lunakl

	* kwin/geometry.cpp: Backport fix for fix for #80545.

2005-05-17 09:11 +0000 [r414899]  lunakl

	* kicker/core/extensionmanager.cpp: Backport, global menubar can be
	  turned on only for KDesktop.

2005-05-18 09:03 +0000 [r415298]  lunakl

	* kwin/kompmgr/kompmgr.c: Backport #105757.

2005-05-18 14:00 +0000 [r415391]  lunakl

	* kwin/geometrytip.cpp, kwin/COMPLIANCE, kwin/geometry.cpp:
	  Backport proper usage of PBaseSize and PMinSize.

2005-05-18 14:53 +0000 [r415410]  brade

	* konqueror/listview/konq_treeviewwidget.cc: Backporting fixes for
	  a lot of crashes when deleting items with the treeview. Fixed
	  #105304, #102331.

2005-05-18 16:27 +0000 [r415452]  hindenburg

	* konsole/konsole/konsole.h: Update version

2005-05-20 18:23 +0000 [r416134]  deller

	* kcontrol/info/opengl.cpp: backport: Bug 105963: kcmopengl 3d card
	  is always unkwnown

2005-05-21 18:56 +0000 [r416504]  aseigo

	* kicker/core/menumanager.cpp: we don't need to delete these, since
	  panelservicemenu does so too

2005-05-21 19:01 +0000 [r416506]  binner

	* kcontrol/css/preview.ui: version for unsermake

2005-05-21 20:01 +0000 [r416529]  binner

	* kcontrol/css/preview.ui: Revert, fixed in unsermake already

2005-05-22 07:49 +0000 [r416649]  binner

	* kate/app/katemain.h: Increase version for KDE 3.4.1

2005-05-22 15:58 +0000 [r416950-416948]  binner

	* khelpcenter/Makefile.am, khelpcenter/navigator.cpp,
	  khelpcenter/index.html.in (added): Give Help Center overview
	  pages some look :-)

	* khelpcenter/navigator.cpp: I knew I missed something the moment I
	  committed :-)

2005-05-22 18:14 +0000 [r417044]  aseigo

	* kicker/share/panelbutton.cpp, kicker/buttons/servicebutton.h,
	  kicker/share/panelbutton.h, kicker/share/kickerSettings.kcfg,
	  kicker/buttons/servicebutton.cpp, kicker/buttons/urlbutton.cpp:
	  improve this feature to be not quite so brutal but this is only
	  half the fix. the other half will have to wait until 3.5 because
	  it's too invasive IMO to go into a x.y.(z+1) release. so for
	  3.4.x just disable it by default. CCBUGS:103215

2005-05-22 20:06 +0000 [r417100]  binner

	* konqueror/sidebar/trees/dirtree_module/dirtree_module.cpp,
	  konqueror/sidebar/trees/dirtree_module/dirtree_module.h: Hidden
	  option ShowArchivesAsFolders (#98070)

