2005-03-06 22:15 +0000 [r395352]  bero

	* kjsembed/global.h, kjsembed/plugins/customobject_plugin.h,
	  kjsembed/jsproxy_imp.h, kjsembed/jsobjectproxy.h,
	  kjsembed/customobject_imp.h, kjsembed/jsbindingplugin.h,
	  kjsembed/jsopaqueproxy.h: Fix compile with visibility-enabled gcc

2005-03-13 11:37 +0000 [r397192]  rdale

	* smoke/kde/kde_header_list: * Added klistviewsearchline.h header

2005-03-13 12:39 +0000 [r397222]  rdale

	* qtjava/javalib/qtjava/QImage.cpp, qtjava/ChangeLog,
	  qtjava/javalib/qtjava/JavaSlot.h, qtjava/javalib/qtjava/QImage.h,
	  qtjava/javalib/org/kde/qt/QImage.java: * Fixed compile problems
	  on Solaris

2005-03-21 18:23 +0000 [r399480]  sedwards

	* python/pykde/extensions/dcopexport.py: Small bug fix for
	  returning multiline strings from dcop.

2005-03-24 19:17 +0000 [r400297]  rdale

	* qtruby/rubylib/qtruby/handlers.cpp,
	  qtruby/rubylib/qtruby/lib/Qt/qtruby.rb,
	  qtruby/rubylib/qtruby/lib/Qt/Makefile.am,
	  qtruby/rubylib/qtruby/Qt.cpp, qtruby/INSTALL, qtruby/ChangeLog,
	  qtruby/rubylib/qtruby/lib/Qt.rb, qtruby/README.build (removed),
	  qtruby/rubylib/qtruby/lib/Makefile.am,
	  qtruby/rubylib/qtruby/lib/Qt/Qt.rb (removed): * Promoted some
	  fixes to the release branch

2005-03-31 08:39 +0000 [r402086]  rdale

	* qtruby/rubylib/qtruby/lib/Qt/qtruby.rb,
	  qtruby/rubylib/qtruby/Qt.cpp, korundum/ChangeLog,
	  qtruby/ChangeLog, korundum/rubylib/korundum/lib/KDE/korundum.rb:
	  * Promoted some fixes to the release branch

2005-04-03 21:40 +0000 [r402970-402968]  rdale

	* qtruby/rubylib/qtruby/Qt.cpp, qtruby/ChangeLog: * Upped the
	  version to 1.0.8

	* korundum/rubylib/korundum/Korundum.cpp, korundum/ChangeLog: *
	  Added #ifdefs to build against KDE 3.1.x

2005-04-09 21:03 +0000 [r404460]  rdale

	* qtruby/rubylib/qtruby/Qt.cpp, qtruby/ChangeLog,
	  qtruby/bin/rbqtapi: * Promoted some fixes to the release branch

2005-04-16 10:07 +0000 [r405862]  rdale

	* kalyptus/kalyptusCxxToSmoke.pm: * Promoted some fixes to the
	  release branch

2005-04-18 18:27 +0000 [r406389]  rdale

	* kalyptus/kalyptus, kalyptus/kalyptusCxxToSmoke.pm: * Promoted
	  std::string param fix to the release branch

2005-04-29 18:09 +0000 [r408675]  rdale

	* qtruby/rubylib/qtruby/lib/Qt/qtruby.rb,
	  qtruby/rubylib/qtruby/Qt.cpp, qtruby/ChangeLog: * Promoted some
	  minor changes to the release branch

2005-05-20 14:12 +0000 [r416070]  adrian

	* python/pykde/sip/kdecore/kurl.sip: fix build with gcc4 (patch
	  from Rudi) CCMAIL: ro@suse.de

2005-05-22 14:10 +0000 [r416887]  jriddell

	* kjsembed/jseventmapper.cpp, kjsembed/jsfactory.cpp: Fix compile
	  on 64 bit with GCC 4

2005-05-23 07:19 +0000 [r417243]  rdale

	* qtruby/rubylib/qtruby/lib/Qt/qtruby.rb, kalyptus/kalyptus,
	  kalyptus/kalyptusCxxToSmoke.pm, qtruby/rubylib/qtruby/Qt.cpp,
	  korundum/ChangeLog, qtruby/ChangeLog,
	  korundum/rubylib/korundum/lib/KDE/korundum.rb,
	  qtruby/bin/rbqtapi, kalyptus/kalyptusDataDict.pm, smoke/smoke.h:
	  * Promoted fixes for improved ruby Smoke lib introspection to the
	  release branch

