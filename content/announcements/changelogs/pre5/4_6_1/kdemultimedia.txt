------------------------------------------------------------------------
r1217597 | sitter | 2011-01-28 13:26:39 +1300 (Fri, 28 Jan 2011) | 6 lines

Also supress screen power management when watching a film.
Please note that this currently does not seem to work on 4.6 as powerdevil does not seem to actually trigger the appropriate action.

CCBUG: 243035
CCMAIL: drf@kde.org

------------------------------------------------------------------------
r1217761 | scripty | 2011-01-29 02:05:12 +1300 (Sat, 29 Jan 2011) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1218474 | scripty | 2011-02-03 02:03:41 +1300 (Thu, 03 Feb 2011) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1218832 | vrusu | 2011-02-05 10:46:37 +1300 (Sat, 05 Feb 2011) | 1 line

Fixed bug 265435
------------------------------------------------------------------------
r1218836 | vrusu | 2011-02-05 11:03:04 +1300 (Sat, 05 Feb 2011) | 1 line

Previous commit undo waiting i18n team ok
------------------------------------------------------------------------
