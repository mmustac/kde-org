2007-05-18 20:57 +0000 [r666124]  ossi

	* branches/KDE/3.5/kdeartwork/kscreensaver/kdesavers/slideshow.cpp:
	  backport: fix div by 0 BUG: 138161

2007-05-21 17:50 +0000 [r667049]  ilic

	* branches/KDE/3.5/kdeartwork/kscreensaver/kxsconfig/kxsconfig.cpp:
	  Pass normalized screensaver descriptions through i18n call.

2007-06-19 18:10 +0000 [r677684]  tyrerj

	* branches/KDE/3.5/kdeartwork/IconThemes/kdeclassic/32x32/apps/dolphin.png
	  (added),
	  branches/KDE/3.5/kdeartwork/IconThemes/kdeclassic/16x16/apps/dolphin.png
	  (added),
	  branches/KDE/3.5/kdeartwork/IconThemes/kdeclassic/index.theme,
	  branches/KDE/3.5/kdeartwork/IconThemes/kdeclassic/64x64/apps
	  (added),
	  branches/KDE/3.5/kdeartwork/IconThemes/kdeclassic/64x64/apps/dolphin.png
	  (added),
	  branches/KDE/3.5/kdeartwork/IconThemes/kdeclassic/22x22/apps/dolphin.png
	  (added),
	  branches/KDE/3.5/kdeartwork/IconThemes/kdeclassic/48x48/apps/dolphin.png
	  (added): Adding Dolphin KDEClassic Icons

2007-06-26 05:57 +0000 [r680410]  tyrerj

	* branches/KDE/3.5/kdeartwork/IconThemes/kdeclassic/32x32/actions/kde.png:
	  seems to be the wrong size

2007-08-10 11:53 +0000 [r698584]  lunakl

	* branches/KDE/3.5/kdeartwork/kscreensaver/kdesavers/slideshow.h,
	  branches/KDE/3.5/kdeartwork/kscreensaver/kdesavers/slideshow.cpp:
	  Paint per-screen on Xinerama. Patch by Arno <arno@disconnect.de>.

2007-10-08 11:05 +0000 [r722961]  coolo

	* branches/KDE/3.5/kdeartwork/kdeartwork.lsm: updating lsm

