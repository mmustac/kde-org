------------------------------------------------------------------------
r1004602 | rdieter | 2009-07-30 15:23:20 +0000 (Thu, 30 Jul 2009) | 4 lines

backport: better (and working!) pykdeuic4 cmake fix

BUG: 198632

------------------------------------------------------------------------
r1006386 | rdale | 2009-08-03 16:36:51 +0000 (Mon, 03 Aug 2009) | 6 lines

* Added Ruby versions of the i18n(), i18nc(), i18np() and i18ncp() methods
  with variable numbers of arguments that replace the '%1, '%2' place
  holders in the main strings. Thanks to Robert Riemann for reporting them
  missing.


------------------------------------------------------------------------
r1007217 | rdale | 2009-08-05 10:17:00 +0000 (Wed, 05 Aug 2009) | 4 lines

* Fix building the Nepomuk extension with Ruby 1.9.x. Thanks to oneforall
  for the bug report


------------------------------------------------------------------------
r1008554 | rdale | 2009-08-07 20:14:16 +0000 (Fri, 07 Aug 2009) | 5 lines

* Enable KDE::ConfigGroup.read_entry() and write_entry() methods to
  automatically convert args to Qt::Variants as well as the camel case
  versions. Thanks to David Palacio for the bug report.


------------------------------------------------------------------------
