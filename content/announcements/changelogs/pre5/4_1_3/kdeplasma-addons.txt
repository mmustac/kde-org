2008-10-03 06:15 +0000 [r867215]  scripty <scripty@localhost>:

	* branches/KDE/4.1/kdeplasma-addons/applets/calculator/plasma-applet-calculator.desktop,
	  branches/KDE/4.1/kdeplasma-addons/applets/fifteenPuzzle/plasma-applet-fifteenPuzzle.desktop,
	  branches/KDE/4.1/kdeplasma-addons/applets/konqprofiles/plasma-applet-konqprofiles.desktop,
	  branches/KDE/4.1/kdeplasma-addons/applets/konsoleprofiles/plasma-applet-konsoleprofiles.desktop,
	  branches/KDE/4.1/kdeplasma-addons/applets/binary-clock/plasma-applet-binaryclock.desktop,
	  branches/KDE/4.1/kdeplasma-addons/applets/fuzzy-clock/plasma-clock-fuzzy.desktop,
	  branches/KDE/4.1/kdeplasma-addons/applets/luna/plasma-applet-luna.desktop,
	  branches/KDE/4.1/kdeplasma-addons/applets/notes/plasma-notes-default.desktop:
	  SVN_SILENT made messages (.desktop file)

2008-10-20 19:46 +0000 [r874109]  bettio <bettio@localhost>:

	* branches/KDE/4.1/kdeplasma-addons/desktopthemes/heron/license
	  (removed): removed as requested by SaroEngels.

2008-10-20 21:28 +0000 [r874168]  sengels <sengels@localhost>:

	* branches/KDE/4.1/kdeplasma-addons/dataengines/comic/plasma_comic_export.h:
	  fix export

2008-10-20 22:01 +0000 [r874177]  sengels <sengels@localhost>:

	* branches/KDE/4.1/kdeplasma-addons/applets/CMakeLists.txt: disable
	  showdesktop again as it breaks CCMAIL:brandon.ml@gmail.com

