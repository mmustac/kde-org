2008-10-23 10:56 +0000 [r875105]  rdale <rdale@localhost>:

	* branches/KDE/4.1/kdebindings/ruby/plasma/ChangeLog,
	  branches/KDE/4.1/kdebindings/ruby/plasma/examples/applets/plasma_applet_ruby_webapplet/contents
	  (added),
	  branches/KDE/4.1/kdebindings/ruby/plasma/examples/applets/webapplet
	  (removed),
	  branches/KDE/4.1/kdebindings/ruby/plasma/examples/applets/plasma_applet_ruby_webapplet/contents/code/main.rb
	  (added),
	  branches/KDE/4.1/kdebindings/ruby/plasma/examples/applets/plasma_applet_ruby_webapplet
	  (added),
	  branches/KDE/4.1/kdebindings/ruby/plasma/examples/applets/plasma_applet_ruby_webapplet/contents/code
	  (added),
	  branches/KDE/4.1/kdebindings/ruby/plasma/examples/applets/CMakeLists.txt,
	  branches/KDE/4.1/kdebindings/ruby/plasma/examples/applets/plasma_applet_ruby_webapplet/metadata.desktop
	  (added): * Converted the webapplet to use the script engine api,
	  and renamed it plasma_applet_ruby_webapplet

2008-10-29 19:49 +0000 [r877559]  sedwards <sedwards@localhost>:

	* branches/KDE/4.1/kdebindings/python/pykde4/sip/kdecore/klocalizedstring.sip:
	  Fixed a crash when a QString is used as a argument for i18n().

