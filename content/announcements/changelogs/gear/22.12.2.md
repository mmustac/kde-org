---
aliases:
- ../../fulllog_releases-22.12.2
title: KDE Gear 22.12.2 Full Log Page
type: fulllog
gear: true
hidden: true
---
{{< details id="akonadi-calendar" title="akonadi-calendar" link="https://commits.kde.org/akonadi-calendar" >}}
+ Convert event start time to user's local timezone. [Commit.](http://commits.kde.org/akonadi-calendar/fdbd7e42b6707cf99db48f62d43ca104de632159)Fixes bug [#453805](https://bugs.kde.org/453805)
{{< /details >}}
{{< details id="dolphin" title="dolphin" link="https://commits.kde.org/dolphin" >}}
+ Adapt autotest to new expected "Space" key behaviour. [Commit.](http://commits.kde.org/dolphin/66f6221a176156a3fc393123cfe5320f8f513f4d)
+ Make space shortcut for selection mode view-local instead of global. [Commit.](http://commits.kde.org/dolphin/aac61acf41fb3ccd6f9a807984336d9be07917a4)Fixes bug [#458282](https://bugs.kde.org/458282). Fixes bug [#458281](https://bugs.kde.org/458281). See bug [#463048](https://bugs.kde.org/463048)
+ Remove unnecessary method parameter. [Commit.](http://commits.kde.org/dolphin/ee919ea329ace516fb67468fa2d3c98bf46a66d2)
+ Fix size of directories if a subdir fails to open. [Commit.](http://commits.kde.org/dolphin/a6490755ca7d00c964a1349443ba9c06dbb33e50)
+ Don't recurse into symlinks when counting directory contents. [Commit.](http://commits.kde.org/dolphin/491068a4405f93ce66d4f49fa5ba5dee29e9546b)Fixes bug [#434125](https://bugs.kde.org/434125)
{{< /details >}}
{{< details id="elisa" title="elisa" link="https://commits.kde.org/elisa" >}}
+ Adapt APK build to Qt 5.15.8. [Commit.](http://commits.kde.org/elisa/ef148f041d72dc9934c18518cb8c5a75f784b734)
+ Display high DPI album art when using scaling. [Commit.](http://commits.kde.org/elisa/8e2510a2390ab56aba72545ae4f9f60220a49d1a)Fixes bug [#464342](https://bugs.kde.org/464342)
+ Fix Recently and Frequently Played sort buttons. [Commit.](http://commits.kde.org/elisa/1f5b57e8e1c3019c4ab00d6d1eba5ddd6b3feb8b)Fixes bug [#442176](https://bugs.kde.org/442176)
{{< /details >}}
{{< details id="gwenview" title="gwenview" link="https://commits.kde.org/gwenview" >}}
+ Fixed coexistence between Qt Raw plugin and KDcraw. [Commit.](http://commits.kde.org/gwenview/c419ec4a052b39eae7c17caf2a89d39d76b43777)Fixes bug [#463132](https://bugs.kde.org/463132)
{{< /details >}}
{{< details id="itinerary" title="itinerary" link="https://commits.kde.org/itinerary" >}}
+ Hide separator if there are no back fields in an event ticket pkpass. [Commit.](http://commits.kde.org/itinerary/f36202064045988353b6a5304d33b17d547cc5ae)
+ Gradle 7 / Qt 5.15.8 compatibility. [Commit.](http://commits.kde.org/itinerary/f33acb276683264bee0b71547c0c43c7849eae99)
+ Use higher-quality down-scaling for event pass logos. [Commit.](http://commits.kde.org/itinerary/7fe40475748c1b2509cd95b96555e5fecb264de7)
+ Deal with overly wide event pass background images. [Commit.](http://commits.kde.org/itinerary/36ae3ee0e8dbf106f63abfdeb86cdaadf5667761)
+ Fix too aggressive merging of generic Apple Wallet files. [Commit.](http://commits.kde.org/itinerary/56c049890427c7a8f3d197311c5ae3e957207010)
+ Ensure TransferManager has a LiveDataManager before scanning. [Commit.](http://commits.kde.org/itinerary/616f941864f6d925f4f780f2595c5b22b5e622b1)
{{< /details >}}
{{< details id="k3b" title="k3b" link="https://commits.kde.org/k3b" >}}
+ Fix adding files from a m3u playlist. [Commit.](http://commits.kde.org/k3b/c001a2d0600956646f5c16114bd71af7a61cde5d)
+ Videodvd KIO worker: add missing QApp instance for dispatchLoop to work. [Commit.](http://commits.kde.org/k3b/46640b16cbf976b5d9a8ecb2fc46597728944dae)
{{< /details >}}
{{< details id="kalendar" title="kalendar" link="https://commits.kde.org/kalendar" >}}
+ Fix crash on startup when m_changer is null. [Commit.](http://commits.kde.org/kalendar/0e0371fdcc0de00e478d441c1389c1950b7286c4)Fixes bug [#464274](https://bugs.kde.org/464274)
{{< /details >}}
{{< details id="kalgebra" title="kalgebra" link="https://commits.kde.org/kalgebra" >}}
+ Export main activity as required by Android 12. [Commit.](http://commits.kde.org/kalgebra/154c9d702dab264d9b022286d4999088e04a7a43)
{{< /details >}}
{{< details id="kate" title="kate" link="https://commits.kde.org/kate" >}}
+ Build-plugin: Set focus the widget of the build tab that is clicked. [Commit.](http://commits.kde.org/kate/53215f807a04a65b3b42136e2146ca1fa1204466)
+ Fix settings saving regression in CTags plugin. [Commit.](http://commits.kde.org/kate/526d04db82d4b65c61b4b0f607c4b2ffa8a02f3d)Fixes bug [#463993](https://bugs.kde.org/463993)
{{< /details >}}
{{< details id="kdeconnect-kde" title="kdeconnect-kde" link="https://commits.kde.org/kdeconnect-kde" >}}
+ [SMS App] Fix loading more messages. [Commit.](http://commits.kde.org/kdeconnect-kde/66a355076c39170047735a5a5bdb45bd25bf70ce)Fixes bug [#452946](https://bugs.kde.org/452946)
+ Fix ssh authentication using pubkey on recent openssh versions. [Commit.](http://commits.kde.org/kdeconnect-kde/a7c17468f0bf16e4ed87c093dac0b77749c61d50)Fixes bug [#443155](https://bugs.kde.org/443155)
+ [plugins/virtualmonitor] Fix crash when krfb-virtualmonitor fails all retries. [Commit.](http://commits.kde.org/kdeconnect-kde/87686bf5be98671e0398d779af022b9445bef646)Fixes bug [#464241](https://bugs.kde.org/464241)
+ Fix "Pair a Device..." button in plasmoid. [Commit.](http://commits.kde.org/kdeconnect-kde/23098c85444dbe135384460589c30e5b72706f77)
{{< /details >}}
{{< details id="kdenlive" title="kdenlive" link="https://commits.kde.org/kdenlive" >}}
+ Fix crash deleting a subtitle clip. [Commit.](http://commits.kde.org/kdenlive/a9ff26d06812e574d7e98040e4bb8be2cb53726c)
+ Fix scene split job does not save sub clips. [Commit.](http://commits.kde.org/kdenlive/80f5c87c9e4ecc4d80003e2f254dd3f2d614a17a)
+ Fix monitor qml overlay painting corruption with Qt 5.15.8. [Commit.](http://commits.kde.org/kdenlive/2c6eeac036f8243363df6da9314f3fa0b8ee0834)See bug [#464027](https://bugs.kde.org/464027)
+ Don't unnecessarily double check track duration on clip move. [Commit.](http://commits.kde.org/kdenlive/2f8715c7668be63ccd2f7eef4621d345661a4b97)
{{< /details >}}
{{< details id="kdepim-addons" title="kdepim-addons" link="https://commits.kde.org/kdepim-addons" >}}
+ Fix kdepim-addons: support Spamassassin 4.0.0 with Kmail. [Commit.](http://commits.kde.org/kdepim-addons/e9eda87eaf8838d5ee29860129817e5b90afd78e)Fixes bug [#464973](https://bugs.kde.org/464973)
{{< /details >}}
{{< details id="kdev-python" title="kdev-python" link="https://commits.kde.org/kdev-python" >}}
+ Build: fix supported Python version in error string. [Commit.](http://commits.kde.org/kdev-python/ab0350f87d2d3aea923ba73a5241960320fd2d5e)
+ Initial support for python 3.11. [Commit.](http://commits.kde.org/kdev-python/8d8e8947c9f0ca78417c8ecb499589fbb8c804d7)
{{< /details >}}
{{< details id="kdialog" title="kdialog" link="https://commits.kde.org/kdialog" >}}
+ Fix: disable progressdialog's autoreset. [Commit.](http://commits.kde.org/kdialog/1a808ac2b8a007c6cfff777a69d04f15b99b9f4c)Fixes bug [#450015](https://bugs.kde.org/450015)
{{< /details >}}
{{< details id="kget" title="kget" link="https://commits.kde.org/kget" >}}
+ Fix translations function misuse. [Commit.](http://commits.kde.org/kget/208ca160514eb6a3d92420beeb8d945b8bab9763)
{{< /details >}}
{{< details id="kio-extras" title="kio-extras" link="https://commits.kde.org/kio-extras" >}}
+ Fix int overflow with images larger than 4 Giga-Pixel. [Commit.](http://commits.kde.org/kio-extras/0c7c60cf15e7f9bf2ef75d3a83a8fb02ba888fae)
{{< /details >}}
{{< details id="kitinerary" title="kitinerary" link="https://commits.kde.org/kitinerary" >}}
+ Also handle Indico registration updates. [Commit.](http://commits.kde.org/kitinerary/760c2333736c61b53993708d09a4f16df3ec6d67)
+ Check floating point values more strictly when reading. [Commit.](http://commits.kde.org/kitinerary/2357d39be407dd135d2ccdb391a38c495cce1b3a)
+ Fix date format pattern for reading ticket validity time ranges. [Commit.](http://commits.kde.org/kitinerary/774215c43a8b5e8aedc95f93ee5b7dbc8bd95bc0)
+ Find booking.com modification URLs independent of the document language. [Commit.](http://commits.kde.org/kitinerary/da874e840abe7db3c2aa688aff8730d5bb2ceaf0)
+ Drop UTC offsets from booking.com checkin/out times. [Commit.](http://commits.kde.org/kitinerary/0c0b7635fc03cc98aa3bc633e90df78e8866868d)
+ Switch the static build job to CentOS 7. [Commit.](http://commits.kde.org/kitinerary/52edfebfa2d1d8b99e2106560a740acbc3437009)
+ Link statically against zlib. [Commit.](http://commits.kde.org/kitinerary/b8d3b15de247067fa4d07904c41ac31d6dded66d)
+ Adapt unit test to KCalendarCore changes. [Commit.](http://commits.kde.org/kitinerary/203bedde9b3d45b95718152e7d5684f1bf91cabd)
+ Set XDG_DATA_DIRS when building the static standalone extractor. [Commit.](http://commits.kde.org/kitinerary/4cbb37418ec90dd0e97d0cce56d27e3074022a9f)
+ Add fully static extractor executable builds on Gitlab. [Commit.](http://commits.kde.org/kitinerary/f96ff6cec80f132cdce4ef5fa3285ae5b6dd0d01)
+ Decrease lower aspect ratio bound for 1D barcode detection. [Commit.](http://commits.kde.org/kitinerary/de5e94adab2a2d6ee2a3d9210b57f18160787a6b)
+ Also remove vdv-cert-extractor from build when flag is disabled. [Commit.](http://commits.kde.org/kitinerary/e857e27a6eff7a757e787ee225e659c8d108327a)
+ Fix inverted PdfImageRef::isNull logic. [Commit.](http://commits.kde.org/kitinerary/c7f1172cbdc70dd40045352fa2d3814b75140bcc)
+ Be slightly more tolerant on arrival time variations when comparing trips. [Commit.](http://commits.kde.org/kitinerary/b74fdee413f2f9b60eb7e468c15eaefe417b0d2b)
+ Do the same string normalization for city-level location comparison. [Commit.](http://commits.kde.org/kitinerary/5ef51868915ee01a58a944a1d3a63f04cacba525)
+ Fix onepagebooking extractor if the number of children field is empty. [Commit.](http://commits.kde.org/kitinerary/af6ca20443923d271cb3b00194078945cbcd085b)
+ Support full day events in Pretix pkpass files. [Commit.](http://commits.kde.org/kitinerary/cafdf4c2442da89fd6a06b4610e17e6f8007b509)
+ Improve extraction from international MAV RCT2 tickets. [Commit.](http://commits.kde.org/kitinerary/97dae77ffd1c1894adffda4c33b5d34aac2f8886)
+ Add unit test data changes forgotten in the previous commit. [Commit.](http://commits.kde.org/kitinerary/0bac87264505e2c96240890792cc35046d6d530a)
+ Correctly determine year from context for year-less RCT2 data. [Commit.](http://commits.kde.org/kitinerary/28830ffb64801e5d3ff2ece40f5f0f528663c9d1)
+ Deduplicate parsing of the two MAV barcode variants. [Commit.](http://commits.kde.org/kitinerary/676cc9afcd3f052f8d45e1cad89ae500f8d179a3)
{{< /details >}}
{{< details id="kmail" title="kmail" link="https://commits.kde.org/kmail" >}}
+ Remove unused variable. [Commit.](http://commits.kde.org/kmail/d78478b2d828e2e8879b8189c0e2b476bcec247e)
+ Fix bug 463935: Crash on attempting to replay a message with an user template. [Commit.](http://commits.kde.org/kmail/94e28559c96b71721b02393a23cbfeefa93a607b)Fixes bug [#463935](https://bugs.kde.org/463935)
+ Remove unused method. [Commit.](http://commits.kde.org/kmail/a18189898a0a9c900f7e256dbd81126b67a6c03f)
{{< /details >}}
{{< details id="kolourpaint" title="kolourpaint" link="https://commits.kde.org/kolourpaint" >}}
+ Add more image formats to the list of ones supporting variable quality. [Commit.](http://commits.kde.org/kolourpaint/0c880aae02411c97241c85bc16410c83e0d06168)Fixes bug [#463212](https://bugs.kde.org/463212)
{{< /details >}}
{{< details id="konqueror" title="konqueror" link="https://commits.kde.org/konqueror" >}}
+ Always look in the kf5/parts directory for part offers. [Commit.](http://commits.kde.org/konqueror/0bbb030523b4a565f8644924769f7cf9f162cce8)Fixes bug [#463729](https://bugs.kde.org/463729)
+ Avoid a crash when QClipboard::mimeData returns nullptr. [Commit.](http://commits.kde.org/konqueror/d628bbfcf618e9cd62c7789d592990014642e9c4)Fixes bug [#463674](https://bugs.kde.org/463674)
+ Check that the current view is valid. [Commit.](http://commits.kde.org/konqueror/42f8f5a48f2d1ae9f44e443320dce08cdfd23912)Fixes bug [#463358](https://bugs.kde.org/463358)
+ Make handling of WebEnginePart dictionaries more flexible. [Commit.](http://commits.kde.org/konqueror/2b944c5adb5429627941aea376a49c09cdb27654)
{{< /details >}}
{{< details id="konsole" title="konsole" link="https://commits.kde.org/konsole" >}}
+ Update blinking cursor in its correct visual position. [Commit.](http://commits.kde.org/konsole/6a7b9bf944d96eb68a915bc797c9a5b9422015e1)
+ If paragraph direction maybe RtL, don't omit spaces at end of line. [Commit.](http://commits.kde.org/konsole/a68d89dd070a41d1caee88dc7ea87b93f37219ce)
+ When the cursor is blinking and off, do not reverse text color. [Commit.](http://commits.kde.org/konsole/8dadf204770a528fc9950a319e5b87d5ba4cdf7f)Fixes bug [#463911](https://bugs.kde.org/463911)
+ When pasting from clipboard, check data is non null before dereferencing. [Commit.](http://commits.kde.org/konsole/a0316a39ee67c70cfe617b87b2aef4e25bf27387)Fixes bug [#463683](https://bugs.kde.org/463683)
{{< /details >}}
{{< details id="kontrast" title="kontrast" link="https://commits.kde.org/kontrast" >}}
+ Appstream: Add default property to th only screenshot. [Commit.](http://commits.kde.org/kontrast/4f8b2a3ff09a07b69aa499908f1693c3865ae647)
{{< /details >}}
{{< details id="krdc" title="krdc" link="https://commits.kde.org/krdc" >}}
+ Fix libvncclient log handler. [Commit.](http://commits.kde.org/krdc/131f2b2ade06318b85b442d86c3874e286321299)
{{< /details >}}
{{< details id="ktouch" title="ktouch" link="https://commits.kde.org/ktouch" >}}
+ Fix typo in de.neo.xml, lesson "Die Umlaute: ä und ö". [Commit.](http://commits.kde.org/ktouch/6481de867d73cf5d46bd72eb106c2d17fc24cb92)
{{< /details >}}
{{< details id="ktp-common-internals" title="ktp-common-internals" link="https://commits.kde.org/ktp-common-internals" >}}
+ Adjust to move of kaccounts KCM. [Commit.](http://commits.kde.org/ktp-common-internals/c0e93c796b3f9779f36f115853fa4b93961a4c99)Fixes bug [#463923](https://bugs.kde.org/463923)
{{< /details >}}
{{< details id="libkdegames" title="libkdegames" link="https://commits.kde.org/libkdegames" >}}
+ Fix finding knewstuff themes. [Commit.](http://commits.kde.org/libkdegames/fae6ef9b8785fdbdc8518000a5865d88026c7148)Fixes bug [#464288](https://bugs.kde.org/464288)
{{< /details >}}
{{< details id="okular" title="okular" link="https://commits.kde.org/okular" >}}
+ Export main activity as required by Android 12. [Commit.](http://commits.kde.org/okular/463b5d223e50e3b5eea35e0034303490b1751fe4)
{{< /details >}}
{{< details id="pimcommon" title="pimcommon" link="https://commits.kde.org/pimcommon" >}}
+ Fix KAddressBook crash on LDAP import/search menu configuration button. [Commit.](http://commits.kde.org/pimcommon/b677ea51aef7d831cdcb3dd5d2b7ac162a01947a)Fixes bug [#464514](https://bugs.kde.org/464514)
{{< /details >}}
{{< details id="spectacle" title="spectacle" link="https://commits.kde.org/spectacle" >}}
+ Make "Until Spectacle is closed" the default setting for remembering area. [Commit.](http://commits.kde.org/spectacle/e52fd311a7e074dd14a795e99478630ae1bd10bb)Fixes bug [#463417](https://bugs.kde.org/463417)
{{< /details >}}
