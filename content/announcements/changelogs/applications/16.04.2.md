---
aliases:
- ../../fulllog_applications-16.04.2
hidden: true
title: KDE Applications 16.04.2 Full Log Page
type: fulllog
version: 16.04.2
---

<h3><a name='akonadi' href='https://cgit.kde.org/akonadi.git'>akonadi</a> <a href='#akonadi' onclick='toggle("ulakonadi", this)'>[Show]</a></h3>
<ul id='ulakonadi' style='display: none'>
<li>Fix response handling in TagModifyJob. <a href='http://commits.kde.org/akonadi/cfbba91329ac1833e34d353638a58bbc6db7292d'>Commit.</a> </li>
<li>Swap assert and qWarning() from 3d35aaf to make debuggin easier. <a href='http://commits.kde.org/akonadi/212fe5dfce51d99fca0fbb7f797318ee78593700'>Commit.</a> </li>
<li>Fix Bug 358696 - KF5's kaddressbook crashes when adding a contact that has a birthday. <a href='http://commits.kde.org/akonadi/c51da6e79efa54850a676b0e6367f8cfda407351'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/358696'>#358696</a></li>
<li>Jobs: make sure all subjobs are finished before emitting result(). <a href='http://commits.kde.org/akonadi/3d35aaf11c92d2941cbe84af59c8fb95906e2d15'>Commit.</a> </li>
<li>Fix CollectionCopyJob expecting wrong type of response. <a href='http://commits.kde.org/akonadi/04a1db61a223f3aeeb6fb4a5efc254858cc783bb'>Commit.</a> </li>
<li>Remove double margin. <a href='http://commits.kde.org/akonadi/eef4408d3fa133e5e97d334e217dfc01002e4afd'>Commit.</a> </li>
<li>Fix port to qCWarning, too many quotes and spaces and endl. <a href='http://commits.kde.org/akonadi/d2394df287b5ccab40fdd51a443ee7a1bd18a097'>Commit.</a> </li>
<li>Fix infinite loop if socket was unconnected while in readCommand(). <a href='http://commits.kde.org/akonadi/75d33d900647307ae4faf55775c7a44907c760f5'>Commit.</a> </li>
<li>Fix truncation of the unread count drawn above the favorite folders icon. <a href='http://commits.kde.org/akonadi/a8b6382b2407b9e4ee87796eb4586b2ac33ef200'>Commit.</a> </li>
<li>Fix support for newer PostgreSQL versions, make the lookup generic. <a href='http://commits.kde.org/akonadi/a4ae7597a5c02e7e69b5e17739cd3ab8c2db85a9'>Commit.</a> </li>
<li>Fix MySQL 5.7 support. <a href='http://commits.kde.org/akonadi/213e7f9d97e4f072615555563abfebfe932e39a1'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/361485'>#361485</a></li>
<li>Fix MySQL initialization with NO_ZERO_DATE mode. <a href='http://commits.kde.org/akonadi/02555d1695a5b77d2ebead2a982481e1706016ef'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/362580'>#362580</a></li>
<li>Don't call qgetenv(USER) when not needed. <a href='http://commits.kde.org/akonadi/c61d36c109c3749c66d1a3fe1486066c2d6b1875'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/362652'>#362652</a></li>
</ul>
<h3><a name='ark' href='https://cgit.kde.org/ark.git'>ark</a> <a href='#ark' onclick='toggle("ulark", this)'>[Show]</a></h3>
<ul id='ulark' style='display: none'>
<li>Fix crash with blocking Open dialog. <a href='http://commits.kde.org/ark/0d16e7d028a06c75e43389726a668a1803e9496b'>Commit.</a> </li>
<li>Fix opening of files whose mimetype inherits from a supported mimetype. <a href='http://commits.kde.org/ark/8b9cd300c354cc1a8b14d5cd1ae75c25ad424927'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/363717'>#363717</a></li>
<li>Don't tell kioexec to autodelete our temp files. <a href='http://commits.kde.org/ark/4318ac1b94adab726cbe8fccc801da3dcfe32e98'>Commit.</a> </li>
<li>Infopanel: make text selectable by mouse. <a href='http://commits.kde.org/ark/e7b1f55f5e64f5831d04623554874139436e5f87'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/363718'>#363718</a></li>
<li>CliInterface: Return after reaching a CorruptArchivePattern. <a href='http://commits.kde.org/ark/3792fb96b45c7409ae4b5ff7834b2bd1f4e7f1d6'>Commit.</a> </li>
</ul>
<h3><a name='artikulate' href='https://cgit.kde.org/artikulate.git'>artikulate</a> <a href='#artikulate' onclick='toggle("ulartikulate", this)'>[Show]</a></h3>
<ul id='ulartikulate' style='display: none'>
<li>Correctly initialize/delete QtMultimedia backends. <a href='http://commits.kde.org/artikulate/f250175f483873bc622deaa33fe371e15e57ebd2'>Commit.</a> </li>
<li>Use system temp path for temp files. <a href='http://commits.kde.org/artikulate/fcfc27f67a1fda350ce2639e34ca5bd73f0bcb48'>Commit.</a> </li>
<li>Bump minor version. <a href='http://commits.kde.org/artikulate/270ea3d056c83b71b13721b62ced3af1e4b97726'>Commit.</a> </li>
<li>Load welcome screen when no course is available. <a href='http://commits.kde.org/artikulate/543d2b5bab2e114631b4c095bcb0a77bc1e918cf'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/363250'>#363250</a></li>
<li>Use correct goal's language id. <a href='http://commits.kde.org/artikulate/e232c9c3c9a6f1d5b4c7f4fd4b8e85fc8417b3ff'>Commit.</a> </li>
<li>Register learning goals on demand. <a href='http://commits.kde.org/artikulate/d7e09b93ad36fe406fd86b50588246a72d118ebc'>Commit.</a> </li>
<li>Fix crash on first course install. <a href='http://commits.kde.org/artikulate/bfa52d01b8d0611a98835ee0e2a4d2c53c0d2323'>Commit.</a> </li>
<li>Fix layout minors. <a href='http://commits.kde.org/artikulate/ddeef917cc099c7a360da1f7f132e884b6e95206'>Commit.</a> </li>
<li>Fix binding loop with Qt 5.6. <a href='http://commits.kde.org/artikulate/e17a70a639f6f769f891eb8c32bdb7f94ba8a846'>Commit.</a> </li>
</ul>
<h3><a name='cantor' href='https://cgit.kde.org/cantor.git'>cantor</a> <a href='#cantor' onclick='toggle("ulcantor", this)'>[Show]</a></h3>
<ul id='ulcantor' style='display: none'>
<li>Fix compilation error "qobject.h:300:9: error: static assertion failed: Signal and slot arguments are not compatible.". <a href='http://commits.kde.org/cantor/683418b3663ddc588c44063cc11abb20970e8e08'>Commit.</a> </li>
<li>Added support to sage 7.2 backend. <a href='http://commits.kde.org/cantor/8f1424f1b0e40c21c2d77f31afa9220a2360a177'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/363560'>#363560</a></li>
</ul>
<h3><a name='dolphin' href='https://cgit.kde.org/dolphin.git'>dolphin</a> <a href='#dolphin' onclick='toggle("uldolphin", this)'>[Show]</a></h3>
<ul id='uldolphin' style='display: none'>
<li>Fix crash when closing split view with ownCloud plugin loaded. <a href='http://commits.kde.org/dolphin/bed16191b5e9253d8658c0dac0d336b3dab5e0e3'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/357479'>#357479</a>. Code review <a href='https://git.reviewboard.kde.org/r/127930'>#127930</a></li>
<li>Don't allow opening the terminal if shell_access Kiosk mode is set. <a href='http://commits.kde.org/dolphin/3b95644fa1495785f617165f4e8035528ec714e8'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/127951'>#127951</a></li>
</ul>
<h3><a name='ffmpegthumbs' href='https://cgit.kde.org/ffmpegthumbs.git'>ffmpegthumbs</a> <a href='#ffmpegthumbs' onclick='toggle("ulffmpegthumbs", this)'>[Show]</a></h3>
<ul id='ulffmpegthumbs' style='display: none'>
<li>Silence CMake policy CMP0063 warning. <a href='http://commits.kde.org/ffmpegthumbs/46724a7ee5ef469bedbe399c643214f949d9e668'>Commit.</a> </li>
</ul>
<h3><a name='incidenceeditor' href='https://cgit.kde.org/incidenceeditor.git'>incidenceeditor</a> <a href='#incidenceeditor' onclick='toggle("ulincidenceeditor", this)'>[Show]</a></h3>
<ul id='ulincidenceeditor' style='display: none'>
<li>Remove double margin here too. <a href='http://commits.kde.org/incidenceeditor/35f5e1e12ffe700ba7d427716a9cfffe7b57240d'>Commit.</a> </li>
<li>Remove double margin. <a href='http://commits.kde.org/incidenceeditor/7425a61b6040a8bb4e514609d1a203d803d91672'>Commit.</a> </li>
</ul>
<h3><a name='kapptemplate' href='https://cgit.kde.org/kapptemplate.git'>kapptemplate</a> <a href='#kapptemplate' onclick='toggle("ulkapptemplate", this)'>[Show]</a></h3>
<ul id='ulkapptemplate' style='display: none'>
<li>Load translations + use ki18n_wrap_ui. <a href='http://commits.kde.org/kapptemplate/76f63a67b443962caf6992742cc79e598fe66b50'>Commit.</a> </li>
</ul>
<h3><a name='kate' href='https://cgit.kde.org/kate.git'>kate</a> <a href='#kate' onclick='toggle("ulkate", this)'>[Show]</a></h3>
<ul id='ulkate' style='display: none'>
<li>Add i18n calls to make the strings translatable. <a href='http://commits.kde.org/kate/74321f5de36157dc47b6244b8ac5b6a07b7dcb6f'>Commit.</a> </li>
</ul>
<h3><a name='kde-dev-scripts' href='https://cgit.kde.org/kde-dev-scripts.git'>kde-dev-scripts</a> <a href='#kde-dev-scripts' onclick='toggle("ulkde-dev-scripts", this)'>[Show]</a></h3>
<ul id='ulkde-dev-scripts' style='display: none'>
<li>Install grantlee_strings_extractor.py. <a href='http://commits.kde.org/kde-dev-scripts/e23cf5b92a521cdd4cae642e73aede8206c1509d'>Commit.</a> </li>
</ul>
<h3><a name='kdelibs' href='https://cgit.kde.org/kdelibs.git'>kdelibs</a> <a href='#kdelibs' onclick='toggle("ulkdelibs", this)'>[Show]</a></h3>
<ul id='ulkdelibs' style='display: none'>
<li>Implement background-size parsing in shorthand. <a href='http://commits.kde.org/kdelibs/476a96538b1e4f69156f476ac069eb8452d22ee6'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/364154'>#364154</a></li>
<li>Mark properties as set when repeating patterns. <a href='http://commits.kde.org/kdelibs/bc728fcb9a570e12c45fdf1d59d66be43e61c03c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/364052'>#364052</a></li>
<li>Fix background properties inheritance. <a href='http://commits.kde.org/kdelibs/2f8f9756bd8d66f3d8e235a706ffacf1698322a4'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/364051'>#364051</a></li>
<li>Fix compilation. <a href='http://commits.kde.org/kdelibs/0794a22c73ed9f3b98c626264e4d32c1dee91235'>Commit.</a> </li>
<li>Fix applying Initial and Inherit for background-size property. <a href='http://commits.kde.org/kdelibs/dee3cce6f343c298fc23a1fdd77b880aacf3c354'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/363677'>#363677</a></li>
<li>Backport e4658f1 - dom: Add tag priorities for 'comment' tag. <a href='http://commits.kde.org/kdelibs/bb61aed40e06f5390ea0a879ad5a042191fcde57'>Commit.</a> </li>
<li>Backport 84e1c2b - Fix potential memory leak reported by Coverity and simplify the code. <a href='http://commits.kde.org/kdelibs/922e4e6fbad029271d8e09b6f0e1110e2b8c3ca2'>Commit.</a> </li>
<li>Backport 3b49dc8 - htmlediting: Fix memleaks. <a href='http://commits.kde.org/kdelibs/28e5431e598f15044cdd89062e40bc9998d7ad9a'>Commit.</a> </li>
<li>Backport 1d667da - Correctly use QCache::insert. <a href='http://commits.kde.org/kdelibs/77c4f833d7b1c4cf05797b387ad37dd9c6f06475'>Commit.</a> </li>
<li>Backport 02c3192 - Patch some memory leaks. <a href='http://commits.kde.org/kdelibs/947ffe60af957643c858f1142aaf011bfac95926'>Commit.</a> </li>
<li>Backport bd427c4 - Sanity check CSS web font parsing, avoid potential mem leak. <a href='http://commits.kde.org/kdelibs/3becddeaffb60a78d421e8c811009ed1216b1aba'>Commit.</a> </li>
<li>Backport b52325e - Fix potential mem leak in CSS parsing. <a href='http://commits.kde.org/kdelibs/1fb6f0ed9f67ae6652d51c1ffd0f7fc0cf6b7c3e'>Commit.</a> </li>
<li>Backport 1c85155 - css: Fix potential memleak parsing CSS style values. <a href='http://commits.kde.org/kdelibs/d3d4d4818df1abe3e027cd556eda6796cacd3f02'>Commit.</a> </li>
<li>The number of layers is determined by the number of comma-separated values in the ‘background-image’ property. <a href='http://commits.kde.org/kdelibs/fa29fce8736e79e0ad45a74225092329453b58f1'>Commit.</a> </li>
<li>Fix parsing background-position in shorthand declaration. <a href='http://commits.kde.org/kdelibs/96bb3aca276144cf1eb9bae74ac9c47973b8d69b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/363378'>#363378</a></li>
<li>Do not create new fontFace if there is no valid source. <a href='http://commits.kde.org/kdelibs/362c56e76073c73ae2c8775926068011c3a7daff'>Commit.</a> </li>
<li>Warn about KDateTimeParser::parseDateUnicode not being implemented. <a href='http://commits.kde.org/kdelibs/19f1a0869d09a2bad006632b2a2857536f6d3a5b'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/127924'>#127924</a></li>
</ul>
<h3><a name='kdenetwork-filesharing' href='https://cgit.kde.org/kdenetwork-filesharing.git'>kdenetwork-filesharing</a> <a href='#kdenetwork-filesharing' onclick='toggle("ulkdenetwork-filesharing", this)'>[Show]</a></h3>
<ul id='ulkdenetwork-filesharing' style='display: none'>
<li>Sync smbd detection with kio. <a href='http://commits.kde.org/kdenetwork-filesharing/fb48a2e997eb76ff2eeff1e69c2db35478c2f0c0'>Commit.</a> </li>
</ul>
<h3><a name='kdenlive' href='https://cgit.kde.org/kdenlive.git'>kdenlive</a> <a href='#kdenlive' onclick='toggle("ulkdenlive", this)'>[Show]</a></h3>
<ul id='ulkdenlive' style='display: none'>
<li>Fix editing title file. <a href='http://commits.kde.org/kdenlive/46e20dcceb8c6fdb586f014a0c687ed0802d8a86'>Commit.</a> </li>
<li>Backport fix audio mixing and possible corruption on add track. <a href='http://commits.kde.org/kdenlive/994606ee86e92dfd0af59dff698c2943d29b0645'>Commit.</a> </li>
<li>Add GIF to default image sequence render profiles. <a href='http://commits.kde.org/kdenlive/4671d97590ee607c1da395542957068497a94993'>Commit.</a> See bug <a href='https://bugs.kde.org/357614'>#357614</a></li>
<li>Save bin sort order. <a href='http://commits.kde.org/kdenlive/2163dc2295cb09cd5b2f4d82acb838483f478f44'>Commit.</a> See bug <a href='https://bugs.kde.org/363989'>#363989</a></li>
<li>More movit fixes. <a href='http://commits.kde.org/kdenlive/c6280892da25ef125cfe6030df57366e978c5d05'>Commit.</a> See bug <a href='https://bugs.kde.org/361086'>#361086</a></li>
<li>Backport some Shotcut's movit specific code. <a href='http://commits.kde.org/kdenlive/e4a5919bf1a4ce4adef566e35a9a0768f212af9c'>Commit.</a> See bug <a href='https://bugs.kde.org/361086'>#361086</a></li>
<li>Check audio mix on track add/remove. <a href='http://commits.kde.org/kdenlive/85e9f4f1c611289431fa6bb9f23bf6c5d360b735'>Commit.</a> See bug <a href='https://bugs.kde.org/353251'>#353251</a></li>
<li>Use natural sort order in Bin. <a href='http://commits.kde.org/kdenlive/b2c498f6039001383d8c68a0507f6aa20ac51f3d'>Commit.</a> See bug <a href='https://bugs.kde.org/363604'>#363604</a></li>
<li>Fix monitor context menu losing options after configuring toolbars. <a href='http://commits.kde.org/kdenlive/2d96ef4d4ce62a58ee6db920af1ff10fc4e59c2b'>Commit.</a> </li>
<li>Fix crash on loading title in Title Editor. <a href='http://commits.kde.org/kdenlive/358ff8ff96801caaf7ecae5e04760120333c68d3'>Commit.</a> See bug <a href='https://bugs.kde.org/363404'>#363404</a></li>
<li>Minor improvement for forward/rewind playback. <a href='http://commits.kde.org/kdenlive/c0bd021a463fd2643d4913cfa4e17860214e37cc'>Commit.</a> </li>
<li>Fix K shortcut delay for pausing. <a href='http://commits.kde.org/kdenlive/97c5359a48ecd4004b921602ac7252ea88246f18'>Commit.</a> See bug <a href='https://bugs.kde.org/363055'>#363055</a></li>
<li>Fix generators. <a href='http://commits.kde.org/kdenlive/93992a0dc197d6c83152b420a8f16b2a8a45e303'>Commit.</a> </li>
<li>Properly focus track name on double click. <a href='http://commits.kde.org/kdenlive/11358e1afe44f31be9c9c8b58596d8bb23875e8a'>Commit.</a> See bug <a href='https://bugs.kde.org/363033'>#363033</a></li>
<li>Fix green background image in titler. <a href='http://commits.kde.org/kdenlive/22432e9cedd74414ecbc638ee411b630cd19f644'>Commit.</a> See bug <a href='https://bugs.kde.org/358185'>#358185</a></li>
<li>No more timeout on audio thumbs creation, display ffmpeg's progress info. <a href='http://commits.kde.org/kdenlive/1968877dd6fc36b9ea0e6025ee29bf84a5ce3a47'>Commit.</a> See bug <a href='https://bugs.kde.org/362925'>#362925</a></li>
<li>If FFmpeg audio thumbnails fail, switch to MLT audio thumbnail (slower but safer). <a href='http://commits.kde.org/kdenlive/6b26253fe6b7f5fc8cb58b20a6f6cdb35fe38c51'>Commit.</a> See bug <a href='https://bugs.kde.org/362925'>#362925</a></li>
<li>Fix incorrect composite transitions after insert/remove track. <a href='http://commits.kde.org/kdenlive/4053b68169290ba003b73e6f4d808f84b7d83d75'>Commit.</a> See bug <a href='https://bugs.kde.org/362253'>#362253</a></li>
<li>Incude math.h . <a href='http://commits.kde.org/kdenlive/15c9262880a220274fce06e0ee009c66f46368c7'>Commit.</a> </li>
<li>Add check for other stabilization filter names. <a href='http://commits.kde.org/kdenlive/96d863093774b4ec0ddaac69dfb0fd6c2a11732a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/360440'>#360440</a></li>
<li>Fix clip keyframes unreachable on high zoom. <a href='http://commits.kde.org/kdenlive/a5e9518dadc7b09025be7a43f1ae14fbe8a681bf'>Commit.</a> See bug <a href='https://bugs.kde.org/363905'>#363905</a></li>
<li>Fix possible crash if invalid custom profile was saved. <a href='http://commits.kde.org/kdenlive/19dc8f41fd4af56fa7b0463a4786990bb0934079'>Commit.</a> See bug <a href='https://bugs.kde.org/363407'>#363407</a></li>
<li>Fix text outline width incorrectly loaded on title editing. <a href='http://commits.kde.org/kdenlive/d36a5e0dd81472f41f3545e35462bb33d4e50ad6'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/363340'>#363340</a></li>
<li>Correctly add/remove last video track compositing on add/remove track. <a href='http://commits.kde.org/kdenlive/66e9596b1d5ac67c472c565ca9b8f3086138c2d3'>Commit.</a> See bug <a href='https://bugs.kde.org/362253'>#362253</a></li>
<li>Add support for Krita images. <a href='http://commits.kde.org/kdenlive/e907349c58e94a151c9018cf1ade5839b9c8d914'>Commit.</a> See bug <a href='https://bugs.kde.org/362654'>#362654</a></li>
</ul>
<h3><a name='kdepim' href='https://cgit.kde.org/kdepim.git'>kdepim</a> <a href='#kdepim' onclick='toggle("ulkdepim", this)'>[Show]</a></h3>
<ul id='ulkdepim' style='display: none'>
<li>Use same icon as pimsettingexport. <a href='http://commits.kde.org/kdepim/ee6ff1a53b5df427bdbcf62da8f5b323d8277e77'>Commit.</a> </li>
<li>FIX Bug 363498 - Missing icon on pimsettingexporter.desktop file provided by kdepim-core (or kdepim4-core). <a href='http://commits.kde.org/kdepim/dffbb98dd60c37505bcc0f15b3616d28e34f3cdb'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/363498'>#363498</a></li>
<li>Fix Bug 363027 - KMail 16.04.0: Per folder HTML settings are not working. <a href='http://commits.kde.org/kdepim/7e508a64bd5c0042279c5731d3ae1b7800ab52fd'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/363027'>#363027</a></li>
<li>Fix default shortcut (next/preview message found by david). <a href='http://commits.kde.org/kdepim/ba9cecc07c169fcae674ce09ec726bf2d0471e3f'>Commit.</a> </li>
<li>Increase default minimum size. <a href='http://commits.kde.org/kdepim/89873171329f0f935102ab7bfa9841a0b855b81e'>Commit.</a> </li>
<li>Remove duplicate margin. <a href='http://commits.kde.org/kdepim/e9791b1196030adea7715ea9ebc8ce2f88175f09'>Commit.</a> </li>
<li>Hide messagelabel when not necessary. <a href='http://commits.kde.org/kdepim/a9cef8e291b06e24bc10bdee891f2355158b8dd3'>Commit.</a> </li>
<li>Remove margin. Fix layout. <a href='http://commits.kde.org/kdepim/c0fd0809e3887cbe6bfcd778567cca22e049c231'>Commit.</a> </li>
<li>Fix crash if --nofork option is used. <a href='http://commits.kde.org/kdepim/cba541a600f8b2edd9ed215bd39b1256c618d326'>Commit.</a> </li>
</ul>
<h3><a name='kdepim-addons' href='https://cgit.kde.org/kdepim-addons.git'>kdepim-addons</a> <a href='#kdepim-addons' onclick='toggle("ulkdepim-addons", this)'>[Show]</a></h3>
<ul id='ulkdepim-addons' style='display: none'>
<li>Relicense adblock code as GPLv2+ to remove license conflict. <a href='http://commits.kde.org/kdepim-addons/e4bcb6ce1a430842dbfe139f7548c073fdccc39c'>Commit.</a> </li>
<li>Remove double margin. <a href='http://commits.kde.org/kdepim-addons/95f2f5b3f33e1729eaf46af7d9a1fe1aa2158282'>Commit.</a> </li>
</ul>
<h3><a name='kdepim-runtime' href='https://cgit.kde.org/kdepim-runtime.git'>kdepim-runtime</a> <a href='#kdepim-runtime' onclick='toggle("ulkdepim-runtime", this)'>[Show]</a></h3>
<ul id='ulkdepim-runtime' style='display: none'>
<li>Remove double margin. <a href='http://commits.kde.org/kdepim-runtime/0639f3f3bfb3eb3f7e5fd4ee95c51533ac460d2c'>Commit.</a> </li>
<li>Remove double margin. <a href='http://commits.kde.org/kdepim-runtime/ac456720d8982382c0390744de9c8b9395cbfbf8'>Commit.</a> </li>
<li>Remove duplicate margin. <a href='http://commits.kde.org/kdepim-runtime/b1e16f8d19376c5b3b37cf5dc0c7e1a4399a3a51'>Commit.</a> </li>
</ul>
<h3><a name='kdepimlibs' href='https://cgit.kde.org/kdepimlibs.git'>kdepimlibs</a> <a href='#kdepimlibs' onclick='toggle("ulkdepimlibs", this)'>[Show]</a></h3>
<ul id='ulkdepimlibs' style='display: none'>
<li>Fix double margin. <a href='http://commits.kde.org/kdepimlibs/11f93191f446878f12429c62bfcdf207330fd6c2'>Commit.</a> </li>
</ul>
<h3><a name='kgpg' href='https://cgit.kde.org/kgpg.git'>kgpg</a> <a href='#kgpg' onclick='toggle("ulkgpg", this)'>[Show]</a></h3>
<ul id='ulkgpg' style='display: none'>
<li>Remove unneeded include. <a href='http://commits.kde.org/kgpg/c7aa52784f97b24f334f307afe61fff1f6e0f119'>Commit.</a> </li>
<li>Use QScopedPointer to manage a the temporary search result objects. <a href='http://commits.kde.org/kgpg/9a059f38748cbdf15471c92f0cc8fef3acaa514b'>Commit.</a> </li>
<li>Rewrite KGpgSearchResultModel::slotAddKey() to not modify the list of lines. <a href='http://commits.kde.org/kgpg/f9ff100f4c07a9e3892539d09dc80ebba2d11486'>Commit.</a> </li>
<li>Include <QStringList> instead of fwd declarations. <a href='http://commits.kde.org/kgpg/3349d8a48ad1b957859a16643c912c4febec73b0'>Commit.</a> </li>
</ul>
<h3><a name='kidentitymanagement' href='https://cgit.kde.org/kidentitymanagement.git'>kidentitymanagement</a> <a href='#kidentitymanagement' onclick='toggle("ulkidentitymanagement", this)'>[Show]</a></h3>
<ul id='ulkidentitymanagement' style='display: none'>
<li>Only try to migrate config files once, not in every IdentityManager instance being created. <a href='http://commits.kde.org/kidentitymanagement/c9775d29fb173d2d4c4357b0a661d9f6b3e889da'>Commit.</a> </li>
</ul>
<h3><a name='killbots' href='https://cgit.kde.org/killbots.git'>killbots</a> <a href='#killbots' onclick='toggle("ulkillbots", this)'>[Show]</a></h3>
<ul id='ulkillbots' style='display: none'>
<li>Fix adding high scored. <a href='http://commits.kde.org/killbots/6e8797c26327dc7eb50246a3d9356a42db540ba8'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/363005'>#363005</a></li>
</ul>
<h3><a name='kio-extras' href='https://cgit.kde.org/kio-extras.git'>kio-extras</a> <a href='#kio-extras' onclick='toggle("ulkio-extras", this)'>[Show]</a></h3>
<ul id='ulkio-extras' style='display: none'>
<li>Fish: Fix bug with directories having a non-latin1 name. <a href='http://commits.kde.org/kio-extras/1876df2e3d59c3d0fdee3d3084a07175fc9a00ed'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/127941'>#127941</a>. Fixes bug <a href='https://bugs.kde.org/357870'>#357870</a></li>
</ul>
<h3><a name='kiten' href='https://cgit.kde.org/kiten.git'>kiten</a> <a href='#kiten' onclick='toggle("ulkiten", this)'>[Show]</a></h3>
<ul id='ulkiten' style='display: none'>
<li>Remove empty Extra toolbar. <a href='http://commits.kde.org/kiten/a31420752b3116d374f2e0a72ee451a3841f23a2'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/127861'>#127861</a></li>
</ul>
<h3><a name='kmplot' href='https://cgit.kde.org/kmplot.git'>kmplot</a> <a href='#kmplot' onclick='toggle("ulkmplot", this)'>[Show]</a></h3>
<ul id='ulkmplot' style='display: none'>
<li>Add missing dbus service to kmplot. <a href='http://commits.kde.org/kmplot/d75eb33b078c967ce7d613e838b70ce6051717a0'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128028'>#128028</a></li>
</ul>
<h3><a name='konsole' href='https://cgit.kde.org/konsole.git'>konsole</a> <a href='#konsole' onclick='toggle("ulkonsole", this)'>[Show]</a></h3>
<ul id='ulkonsole' style='display: none'>
<li>Fix issue when edits current profile when using the builtin/fallback. <a href='http://commits.kde.org/konsole/7b9340e4bd2abc70cd2a10f6b2dadfa21fbd255f'>Commit.</a> </li>
<li>Fill in the "Initial directory" widget w/ the Profile's property. <a href='http://commits.kde.org/konsole/288990d492f8cc9fe4f345979045dd2a10f92ca4'>Commit.</a> </li>
<li>Fix the dbus autotest. <a href='http://commits.kde.org/konsole/b7f17d9d856e700ad3bec4b98827f6c596e52f60'>Commit.</a> </li>
</ul>
<h3><a name='kopete' href='https://cgit.kde.org/kopete.git'>kopete</a> <a href='#kopete' onclick='toggle("ulkopete", this)'>[Show]</a></h3>
<ul id='ulkopete' style='display: none'>
<li>Fix compilation with GCC 6. <a href='http://commits.kde.org/kopete/9f994ba6950117cbbeefc6027fa0a52ce74932e2'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128006'>#128006</a>. Fixes bug <a href='https://bugs.kde.org/363053'>#363053</a></li>
<li>Prepare for 16.04.2. <a href='http://commits.kde.org/kopete/0e24d2647af7f24af0da017050b07027442acf14'>Commit.</a> </li>
<li>Relicence to GNU LGPL2+. <a href='http://commits.kde.org/kopete/d7fd4e5b6772e1527215459aff8abc46f49b70e9'>Commit.</a> </li>
</ul>
<h3><a name='kpimtextedit' href='https://cgit.kde.org/kpimtextedit.git'>kpimtextedit</a> <a href='#kpimtextedit' onclick='toggle("ulkpimtextedit", this)'>[Show]</a></h3>
<ul id='ulkpimtextedit' style='display: none'>
<li>Other double margin. <a href='http://commits.kde.org/kpimtextedit/02c6581a12add7feccfccf77711c664e2b19eada'>Commit.</a> </li>
<li>Remove double margin here too. <a href='http://commits.kde.org/kpimtextedit/c60e96e570fca863d53920e1b9a9474cbbda38f1'>Commit.</a> </li>
<li>Remove double margin. <a href='http://commits.kde.org/kpimtextedit/623da56066a2584d948451b0482b99286b4954ca'>Commit.</a> </li>
</ul>
<h3><a name='kshisen' href='https://cgit.kde.org/kshisen.git'>kshisen</a> <a href='#kshisen' onclick='toggle("ulkshisen", this)'>[Show]</a></h3>
<ul id='ulkshisen' style='display: none'>
<li>Initialize vectors with correct size. <a href='http://commits.kde.org/kshisen/0ab163a847db3b37b983f9341cccaf243acfd95c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/363038'>#363038</a></li>
</ul>
<h3><a name='ktp-desktop-applets' href='https://cgit.kde.org/ktp-desktop-applets.git'>ktp-desktop-applets</a> <a href='#ktp-desktop-applets' onclick='toggle("ulktp-desktop-applets", this)'>[Show]</a></h3>
<ul id='ulktp-desktop-applets' style='display: none'>
<li>[Quick Chat] Close chat window when pressing Escape. <a href='http://commits.kde.org/ktp-desktop-applets/75b2836db07562b52070c225ffe382eedeebcd84'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128001'>#128001</a></li>
<li>[Quick Chat] Use onAccepted rather than Keys.onReturnPressed. <a href='http://commits.kde.org/ktp-desktop-applets/2ae2d22ae92f77c5bb80bf3d8818deb89c0c909a'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128000'>#128000</a></li>
</ul>
<h3><a name='ktuberling' href='https://cgit.kde.org/ktuberling.git'>ktuberling</a> <a href='#ktuberling' onclick='toggle("ulktuberling", this)'>[Show]</a></h3>
<ul id='ulktuberling' style='display: none'>
<li>Removed text + make description translated in the about dialog, ack'ed by aacid. <a href='http://commits.kde.org/ktuberling/ab0dbadc144700f56a56470c019a876f6ed0884e'>Commit.</a> </li>
</ul>
<h3><a name='libksieve' href='https://cgit.kde.org/libksieve.git'>libksieve</a> <a href='#libksieve' onclick='toggle("ullibksieve", this)'>[Show]</a></h3>
<ul id='ullibksieve' style='display: none'>
<li>Fix mem leak. <a href='http://commits.kde.org/libksieve/52ca40572c52950ebd4d1cd227469ca4d7f97dd9'>Commit.</a> </li>
</ul>
<h3><a name='mailcommon' href='https://cgit.kde.org/mailcommon.git'>mailcommon</a> <a href='#mailcommon' onclick='toggle("ulmailcommon", this)'>[Show]</a></h3>
<ul id='ulmailcommon' style='display: none'>
<li>Fix Bug 363027 - KMail 16.04.0: Per folder HTML settings are not working. <a href='http://commits.kde.org/mailcommon/23ffe422fb8240c712e53d6e9f6fc853a164fdf4'>Commit.</a> See bug <a href='https://bugs.kde.org/363027'>#363027</a></li>
<li>Sort case insensitive. Bug reported by david. <a href='http://commits.kde.org/mailcommon/21c46ecc4a2de5ba07054967b72d5fa6e03196c7'>Commit.</a> </li>
</ul>
<h3><a name='mailimporter' href='https://cgit.kde.org/mailimporter.git'>mailimporter</a> <a href='#mailimporter' onclick='toggle("ulmailimporter", this)'>[Show]</a></h3>
<ul id='ulmailimporter' style='display: none'>
<li>Fix version. <a href='http://commits.kde.org/mailimporter/8f946b9095db736e56b4da992484cbad153b0c12'>Commit.</a> </li>
</ul>
<h3><a name='messagelib' href='https://cgit.kde.org/messagelib.git'>messagelib</a> <a href='#messagelib' onclick='toggle("ulmessagelib", this)'>[Show]</a></h3>
<ul id='ulmessagelib' style='display: none'>
<li>Add missing "/" as reported by David. <a href='http://commits.kde.org/messagelib/832f62921d3c1e849132b77cbe0b33188d48b877'>Commit.</a> </li>
<li>Save attachment dir location. <a href='http://commits.kde.org/messagelib/2b0d0f05b010df5cac5dea1bf15f5ae9c60e5bd8'>Commit.</a> </li>
<li>Add NoFocus so color will not changed. <a href='http://commits.kde.org/messagelib/c626f4cbbfa816d1f206b44a0b06b7cf0e6eaf0b'>Commit.</a> </li>
<li>Make statusbar html as show by default. <a href='http://commits.kde.org/messagelib/5598049c6821d1f138ef9ffa56569d0e6c421d8f'>Commit.</a> </li>
<li>Don't show "Me" when we have several identity. <a href='http://commits.kde.org/messagelib/bb8aa0e75f50caaff4a9a7b137d3a663b2e467ac'>Commit.</a> </li>
</ul>
<h3><a name='minuet' href='https://cgit.kde.org/minuet.git'>minuet</a> <a href='#minuet' onclick='toggle("ulminuet", this)'>[Show]</a></h3>
<ul id='ulminuet' style='display: none'>
<li>Keep tempo factor settings across exercises playing. <a href='http://commits.kde.org/minuet/9d6a7821581bb74be49d7fb3077b71de33bf1cb7'>Commit.</a> </li>
<li>Silence CMake policy CMP0063 warning. <a href='http://commits.kde.org/minuet/db8050879fd49534135eae5aaaa6ab0971637584'>Commit.</a> </li>
<li>Add .arcconfig in .gitignore. <a href='http://commits.kde.org/minuet/b67ea28dfb1385b8a6aca12cefa9c67c0221c6ab'>Commit.</a> </li>
<li>Fix cmath include error when building with gcc6. <a href='http://commits.kde.org/minuet/29be98835452d2bfcd6679f56c449832728be359'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/363303'>#363303</a></li>
<li>Increase version number. <a href='http://commits.kde.org/minuet/c2eba0efafb1583100b48a53822fe31dfebf5479'>Commit.</a> </li>
</ul>
<h3><a name='okteta' href='https://cgit.kde.org/okteta.git'>okteta</a> <a href='#okteta' onclick='toggle("ulokteta", this)'>[Show]</a></h3>
<ul id='ulokteta' style='display: none'>
<li>Merge project_license tags into one in appdata, using SPDX OR operator. <a href='http://commits.kde.org/okteta/d4eab3c16f2c33a57d8a99ce286144e962184f4d'>Commit.</a> </li>
<li>Change <updatecontact> -> <update_contact> in appdata. <a href='http://commits.kde.org/okteta/f7b9085a44636fc806cdbb50886178486c89a50c'>Commit.</a> </li>
<li>Add donation link to appdata. <a href='http://commits.kde.org/okteta/a5112d88f559488c4070892fa4403632dc02d757'>Commit.</a> </li>
<li>Add QT_STRICT_ITERATORS, QT_NO_URL_CAST_FROM_STRING, QT_NO_SIGNALS_SLOTS_KEYWORDS. <a href='http://commits.kde.org/okteta/74aeea1f8c6e83726f6f79674e6842e52a07b02b'>Commit.</a> </li>
<li>Printing: for file location use normal path with local files, no file://. <a href='http://commits.kde.org/okteta/5fee84b1c39c3031731ded35d85fed7ead9f63e1'>Commit.</a> </li>
<li>Printing: print full url in footer again, not just filename/title. <a href='http://commits.kde.org/okteta/478a4c6b16752e58cf0de08356fa2cd2fa8ac3ac'>Commit.</a> </li>
</ul>
<h3><a name='spectacle' href='https://cgit.kde.org/spectacle.git'>spectacle</a> <a href='#spectacle' onclick='toggle("ulspectacle", this)'>[Show]</a></h3>
<ul id='ulspectacle' style='display: none'>
<li>Fix invalid KHotkeys file version. <a href='http://commits.kde.org/spectacle/df84ed53f92c844ca98ef3b0a8bc9b4308eee816'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/362257'>#362257</a></li>
<li>When clicking a link in the kmessagewidget open the url. <a href='http://commits.kde.org/spectacle/469791c929d69a8c74e8edd3ea44664fc315b601'>Commit.</a> </li>
<li>Don't list mimetype field it produces false positive on linting. <a href='http://commits.kde.org/spectacle/ebb654b6ff1bc0c7ccab5e212a42c1ed41286d71'>Commit.</a> </li>
<li>Fix high dpi region selection. <a href='http://commits.kde.org/spectacle/38804556588e977aaaea5f50115363d9f4dc379d'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/127814'>#127814</a>. See bug <a href='https://bugs.kde.org/357022'>#357022</a></li>
</ul>
<h3><a name='umbrello' href='https://cgit.kde.org/umbrello.git'>umbrello</a> <a href='#umbrello' onclick='toggle("ulumbrello", this)'>[Show]</a></h3>
<ul id='ulumbrello' style='display: none'>
<li>Fix 'Reversed sequence numbers on stack trace import'. <a href='http://commits.kde.org/umbrello/3dd3d99df813776598e50c2012ade8ce309a0620'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/363807'>#363807</a></li>
<li>Fix coverity CID 161660: (FORWARD_NULL). <a href='http://commits.kde.org/umbrello/da9dcc1afb1a1c260b2d262ef60f79fdb5f1e333'>Commit.</a> See bug <a href='https://bugs.kde.org/340646'>#340646</a></li>
<li>Fix coverity check CID 161662: Control flow issues  (MISSING_BREAK). <a href='http://commits.kde.org/umbrello/455365a5bec9cc47020ca6e8e178a23160db65ff'>Commit.</a> See bug <a href='https://bugs.kde.org/340646'>#340646</a></li>
<li>MSVC 2015 compile fix. <a href='http://commits.kde.org/umbrello/a4c5b0ce7723deda56cb86d369729d5e78eab513'>Commit.</a> </li>
<li>Fix 'Crash on clicking item in ER diagram'. <a href='http://commits.kde.org/umbrello/00f94155ef4ea57991ee56c5a3a83a44fe0e0139'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/362895'>#362895</a>. Code review <a href='https://git.reviewboard.kde.org/r/127987'>#127987</a></li>
<li>Fix 'inversion of force documentation and sections' in code generator settings. <a href='http://commits.kde.org/umbrello/e4954692f7ef39c0feaec6354d456f48d9cc5440'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/174194'>#174194</a></li>
</ul>