---
aliases:
- ../../fulllog_applications-18.08.1
hidden: true
title: KDE Applications 18.08.1 Full Log Page
type: fulllog
version: 18.08.1
---

h3><a name='akonadi' href='https://cgit.kde.org/akonadi.git'>akonadi</a> <a href='#akonadi' onclick='toggle("ulakonadi", this)'>[Hide]</a></h3>
<ul id='ulakonadi' style='display: block'>
<li>Attempt to mitigate trx deadlocks with SELECT ... FOR UPDATE. <a href='http://commits.kde.org/akonadi/8ad55b356ef08a65bb3459b382db1979c55e1e99'>Commit.</a> </li>
<li>Fix crash when NotificationCollector does not have Connection. <a href='http://commits.kde.org/akonadi/0297248b0e768516236bdfeb55d04e8d4f4b1c80'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/397239'>#397239</a></li>
<li>Akonadi: forget about jobtracker if akonadiconsole went away. <a href='http://commits.kde.org/akonadi/f7ccb98bc6a499edebbf24a3fc4c69fc75399061'>Commit.</a> </li>
<li>Akonadi protocol: an invalid response is still a response. <a href='http://commits.kde.org/akonadi/4aca38836ab37e797ea737648125e46aa1a81422'>Commit.</a> </li>
<li>Use nullptr to fix gcc7 warning. <a href='http://commits.kde.org/akonadi/d7b4ef028c176c858dfd90eb326842079f1b5690'>Commit.</a> </li>
<li>JobTracker: publish all jobs in the session when first connecting. <a href='http://commits.kde.org/akonadi/2be7318b1e9529d9194cb2c02881c15939e04ac0'>Commit.</a> </li>
<li>Debug output: show which collection we're syncing. <a href='http://commits.kde.org/akonadi/7e948e3b5701986d6acb9cab7749684bf10cb30c'>Commit.</a> </li>
<li>Fsck output: print out collection ID as well, for items with no RID. <a href='http://commits.kde.org/akonadi/c3858fe9490d63f356b25004fba0453a7971405f'>Commit.</a> </li>
<li>Disable remote logging for inside Akonadi Console process. <a href='http://commits.kde.org/akonadi/3447c7a7b77cfb3482f058c43a8cccd54a43f625'>Commit.</a> </li>
<li>Split out search-related debug output into own category. <a href='http://commits.kde.org/akonadi/f8dea5f4f21f844f14e241bb6830cd7125f0673a'>Commit.</a> </li>
</ul>
<h3><a name='akonadi-contacts' href='https://cgit.kde.org/akonadi-contacts.git'>akonadi-contacts</a> <a href='#akonadi-contacts' onclick='toggle("ulakonadi-contacts", this)'>[Hide]</a></h3>
<ul id='ulakonadi-contacts' style='display: block'>
<li>Don't install it. <a href='http://commits.kde.org/akonadi-contacts/0b03cbcb9bf59ab6cd1b45bc5bb4a73e1a0ad98a'>Commit.</a> </li>
<li>Remvoe it. <a href='http://commits.kde.org/akonadi-contacts/7d3af1de39e940d3c55cddbe8ddc98990ad4ddb5'>Commit.</a> </li>
</ul>
<h3><a name='akonadi-search' href='https://cgit.kde.org/akonadi-search.git'>akonadi-search</a> <a href='#akonadi-search' onclick='toggle("ulakonadi-search", this)'>[Hide]</a></h3>
<ul id='ulakonadi-search' style='display: block'>
<li>Fix categories description. <a href='http://commits.kde.org/akonadi-search/9a94aff27e7fc9e6d7d4363daac7bc6e28ddb6c5'>Commit.</a> </li>
</ul>
<h3><a name='akonadiconsole' href='https://cgit.kde.org/akonadiconsole.git'>akonadiconsole</a> <a href='#akonadiconsole' onclick='toggle("ulakonadiconsole", this)'>[Hide]</a></h3>
<ul id='ulakonadiconsole' style='display: block'>
<li>Browser tab: show GID in tableview and below the remote ID. <a href='http://commits.kde.org/akonadiconsole/808360e3145b087ddccea810898dcda26a737013'>Commit.</a> </li>
<li>Implement "Save To File" for the Notifications Monitor. <a href='http://commits.kde.org/akonadiconsole/15e5de417bfaaaec66540da51f84c46adc77fab7'>Commit.</a> </li>
<li>OK, enough tinkering with sizes, give good default and move on. <a href='http://commits.kde.org/akonadiconsole/079b8ca0961e8528fc6f8ad9e58b0620df5b69eb'>Commit.</a> </li>
<li>Implement filtering of notification types using KCheckComboBox. <a href='http://commits.kde.org/akonadiconsole/2793d34f8393f38b3a15324aded40c2f7423ce4d'>Commit.</a> </li>
<li>Add milliseconds to timestamps. <a href='http://commits.kde.org/akonadiconsole/f50d85eb5bb1c0b5b55e743608136e152562b79d'>Commit.</a> </li>
</ul>
<h3><a name='akregator' href='https://cgit.kde.org/akregator.git'>akregator</a> <a href='#akregator' onclick='toggle("ulakregator", this)'>[Hide]</a></h3>
<ul id='ulakregator' style='display: block'>
<li>Fix Bug 398162 - Current blog scaling not applied on next/previous blog. <a href='http://commits.kde.org/akregator/c94377f61bd4c6a5de61d21bac5b1728c0ec955a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/398162'>#398162</a></li>
</ul>
<h3><a name='ark' href='https://cgit.kde.org/ark.git'>ark</a> <a href='#ark' onclick='toggle("ulark", this)'>[Hide]</a></h3>
<ul id='ulark' style='display: block'>
<li>Fix bzip main page. <a href='http://commits.kde.org/ark/317620e2a3349cf6ebb3b43ff22d2c5d73555bc6'>Commit.</a> </li>
</ul>
<h3><a name='cantor' href='https://cgit.kde.org/cantor.git'>cantor</a> <a href='#cantor' onclick='toggle("ulcantor", this)'>[Hide]</a></h3>
<ul id='ulcantor' style='display: block'>
<li>Fix error with wrong highlighting in Python backend. <a href='http://commits.kde.org/cantor/4040b6ac6f2c0940c255bae704e2272a3533a6d6'>Commit.</a> </li>
<li>Fix error with highlighting single line comment in julia backend. <a href='http://commits.kde.org/cantor/b0e2570ab6371bbf467fbfd66d855c4cc87b184f'>Commit.</a> </li>
<li>Fix code completition for Julia >= 0.7.0 in Julia backend. <a href='http://commits.kde.org/cantor/d341ae1c3e27c1e2f9dab21a58e9ad84141c1f8d'>Commit.</a> </li>
<li>Fix deprecationg warning for Julia >= 0.7.0. <a href='http://commits.kde.org/cantor/5b18deaa8c6d88d50453f0811c7d7cba5a38f44a'>Commit.</a> </li>
<li>Fix build with julia 1.0. <a href='http://commits.kde.org/cantor/7020a7461515ae0fda83798f53f55bf64fe58aae'>Commit.</a> </li>
<li>Fix sagemath backend with sagemath 8.3. <a href='http://commits.kde.org/cantor/bd9a2d12b3f02f71414fc5054257624263a5e8b4'>Commit.</a> </li>
<li>Support julia>=0.7. <a href='http://commits.kde.org/cantor/78bfe202c6cd5390a605313d0621b02b27956926'>Commit.</a> </li>
<li>Fix build error with julia with version greater, that 5. <a href='http://commits.kde.org/cantor/e03e3d91ee8739021998f5cd817ba49924400ce3'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/397548'>#397548</a></li>
</ul>
<h3><a name='dolphin' href='https://cgit.kde.org/dolphin.git'>dolphin</a> <a href='#dolphin' onclick='toggle("uldolphin", this)'>[Hide]</a></h3>
<ul id='uldolphin' style='display: block'>
<li>Pretty-print "creationtime" role. <a href='http://commits.kde.org/dolphin/f75b8a4287f920aa9367aa62efa24fb11b9e5409'>Commit.</a> </li>
</ul>
<h3><a name='gwenview' href='https://cgit.kde.org/gwenview.git'>gwenview</a> <a href='#gwenview' onclick='toggle("ulgwenview", this)'>[Hide]</a></h3>
<ul id='ulgwenview' style='display: block'>
<li>Fix Crop tool Aspect ratio sometimes randomly preselected. <a href='http://commits.kde.org/gwenview/d280cc6052a224d185d0400460b3eb30420bb4ec'>Commit.</a> </li>
<li>Improve keyboard handling for focused buttons in Crop and Reduce Red Eye tools. <a href='http://commits.kde.org/gwenview/df8cb1abf5bae9649b15fbcc2f93fc0b85ba0dd4'>Commit.</a> </li>
<li>Fix Advanced settings crop toolbar sometimes cut off. <a href='http://commits.kde.org/gwenview/8e69f60ffa923a083cfd1c77900a75d96ad73aeb'>Commit.</a> </li>
<li>Keep the width of the crop spinboxes fixed to avoid wiggling. <a href='http://commits.kde.org/gwenview/74acf5fbe5e4125780d5bd6a4fb58fa9bfa5036b'>Commit.</a> </li>
</ul>
<h3><a name='juk' href='https://cgit.kde.org/juk.git'>juk</a> <a href='#juk' onclick='toggle("uljuk", this)'>[Hide]</a></h3>
<ul id='uljuk' style='display: block'>
<li>Set the tick interval, because the default value is 0. <a href='http://commits.kde.org/juk/665a629e914d5d08bcf0dacd553fa4a382c798f2'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/389907'>#389907</a></li>
</ul>
<h3><a name='kalarm' href='https://cgit.kde.org/kalarm.git'>kalarm</a> <a href='#kalarm' onclick='toggle("ulkalarm", this)'>[Hide]</a></h3>
<ul id='ulkalarm' style='display: block'>
<li>Update version number. <a href='http://commits.kde.org/kalarm/fc4987f4034333a488bc119e9980211f29e2ad4f'>Commit.</a> </li>
<li>Remove seconds values from Time column (erroneously added in 2.12.0). <a href='http://commits.kde.org/kalarm/5ee34146fe6736d4bbfa6ad62816bcd0bc775174'>Commit.</a> </li>
<li>Bug 397130: align and right adjust 'Time to' column values. <a href='http://commits.kde.org/kalarm/56f3d3e4042a19dd6263edfb75613ecf2ece2515'>Commit.</a> </li>
</ul>
<h3><a name='kate' href='https://cgit.kde.org/kate.git'>kate</a> <a href='#kate' onclick='toggle("ulkate", this)'>[Hide]</a></h3>
<ul id='ulkate' style='display: block'>
<li>Explicitly include <QWidgetAction>. <a href='http://commits.kde.org/kate/28ca88f0221fc371e185ba8ee5815106e8d1942f'>Commit.</a> </li>
</ul>
<h3><a name='kdav' href='https://cgit.kde.org/kdav.git'>kdav</a> <a href='#kdav' onclick='toggle("ulkdav", this)'>[Hide]</a></h3>
<ul id='ulkdav' style='display: block'>
<li>Need for static_cast to fix the test. <a href='http://commits.kde.org/kdav/68946de0c12f4b409f8c2f222d3d11b852c7d2b3'>Commit.</a> </li>
<li>Fix noMatchingMime test. <a href='http://commits.kde.org/kdav/a66f412163c709fd6dcb491e4ef1cb5ccbb3eb38'>Commit.</a> </li>
</ul>
<h3><a name='kdebugsettings' href='https://cgit.kde.org/kdebugsettings.git'>kdebugsettings</a> <a href='#kdebugsettings' onclick='toggle("ulkdebugsettings", this)'>[Hide]</a></h3>
<ul id='ulkdebugsettings' style='display: block'>
<li>Add gitignore. <a href='http://commits.kde.org/kdebugsettings/a69071cac87651e4daef7debc54d4d25423d52ba'>Commit.</a> </li>
<li>Fix backport. <a href='http://commits.kde.org/kdebugsettings/1f3a2984c6b414bd07344358281996bee7c5e91e'>Commit.</a> </li>
<li>Force forcus to lineedit. <a href='http://commits.kde.org/kdebugsettings/ced10107e66b47bbfd995c2725208f7b9e299e89'>Commit.</a> </li>
</ul>
<h3><a name='kdenlive' href='https://cgit.kde.org/kdenlive.git'>kdenlive</a> <a href='#kdenlive' onclick='toggle("ulkdenlive", this)'>[Hide]</a></h3>
<ul id='ulkdenlive' style='display: block'>
<li>Fix proxy creation not restarting when changing a property while creating proxy. <a href='http://commits.kde.org/kdenlive/68556dbd6a9865b16b876dc559d0f38c2356d9fa'>Commit.</a> </li>
<li>Prepare AppData for next release. <a href='http://commits.kde.org/kdenlive/5cd17e333a08d1c47b7cfb4c339b3560be85c1ed'>Commit.</a> </li>
<li>Fix clip proxy on autorotate. <a href='http://commits.kde.org/kdenlive/7e511e09b20597b2bc65bae00d25e132945c828a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/397762'>#397762</a></li>
<li>Fix effects parameters parsing. <a href='http://commits.kde.org/kdenlive/cf4980fc0e16b90b5de45d4a6317daca8a61eb73'>Commit.</a> </li>
<li>Give priority to local effects over system ones. <a href='http://commits.kde.org/kdenlive/01797e3b94809708d26491384ee35cb533b1039c'>Commit.</a> </li>
<li>Access to MLT debug info (command line option). <a href='http://commits.kde.org/kdenlive/a9e06e0a9108049eaa33a7f10ab7366284360829'>Commit.</a> </li>
<li>Fix OffScreenSurface creation in GUI thread. <a href='http://commits.kde.org/kdenlive/946842ac8794109d305208b20a7091c5523f7f4c'>Commit.</a> </li>
<li>Increase max unzoom level, uniform zoom factor. <a href='http://commits.kde.org/kdenlive/727f86bc4b028ff8f3d4c3d45e1fcb1551ee1097'>Commit.</a> </li>
</ul>
<h3><a name='kdepim-addons' href='https://cgit.kde.org/kdepim-addons.git'>kdepim-addons</a> <a href='#kdepim-addons' onclick='toggle("ulkdepim-addons", this)'>[Hide]</a></h3>
<ul id='ulkdepim-addons' style='display: block'>
<li>Add support for UTC. <a href='http://commits.kde.org/kdepim-addons/2e29cc37e4e3d90c5ae4078004766370cf40c7b2'>Commit.</a> </li>
</ul>
<h3><a name='kdepim-apps-libs' href='https://cgit.kde.org/kdepim-apps-libs.git'>kdepim-apps-libs</a> <a href='#kdepim-apps-libs' onclick='toggle("ulkdepim-apps-libs", this)'>[Hide]</a></h3>
<ul id='ulkdepim-apps-libs' style='display: block'>
<li>Add FollowUpReminder::FollowUpReminderInfo debug method. <a href='http://commits.kde.org/kdepim-apps-libs/04bbf87be3586ac64cda52c80ab74ab3e0ca4bf4'>Commit.</a> </li>
</ul>
<h3><a name='kdepim-runtime' href='https://cgit.kde.org/kdepim-runtime.git'>kdepim-runtime</a> <a href='#kdepim-runtime' onclick='toggle("ulkdepim-runtime", this)'>[Hide]</a></h3>
<ul id='ulkdepim-runtime' style='display: block'>
<li>Fix crash when we didn't select a agent. <a href='http://commits.kde.org/kdepim-runtime/7f6629ec24b58e1c40559a355dd35f3039f985c7'>Commit.</a> </li>
<li>USe nullptr. <a href='http://commits.kde.org/kdepim-runtime/3f2a8ce4990c039666a66da36502f637b64954b6'>Commit.</a> </li>
<li>Fix gmail auth dialog popping up over and over again. <a href='http://commits.kde.org/kdepim-runtime/6752a16970995996a2a79a4eedae3457d36437c8'>Commit.</a> </li>
<li>Fix gmail auth dialog popping up over and over again. <a href='http://commits.kde.org/kdepim-runtime/8e7dfb8696c8eb40c20ce46c532b2fcd10edf483'>Commit.</a> </li>
<li>Remove these ones. <a href='http://commits.kde.org/kdepim-runtime/3e5cc405455aebae98415096fe1a21cd4dcfb8f2'>Commit.</a> </li>
<li>Process IMAP Quota roots with associated quotas only. <a href='http://commits.kde.org/kdepim-runtime/e523a60e8c73d6b4fa8c04216d7bd703d877a418'>Commit.</a> </li>
<li>Fix array access bounds in IMAP resource. <a href='http://commits.kde.org/kdepim-runtime/c9254f3497e0cd37039333c17085b064e3b323a2'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/396980'>#396980</a></li>
</ul>
<h3><a name='kio-extras' href='https://cgit.kde.org/kio-extras.git'>kio-extras</a> <a href='#kio-extras' onclick='toggle("ulkio-extras", this)'>[Hide]</a></h3>
<ul id='ulkio-extras' style='display: block'>
<li>Add license text of GPL 2.0, LGPL 2.0 and LGPL 2.1. <a href='http://commits.kde.org/kio-extras/bfbef9f61a1700391fe203a007af8b33ca96f157'>Commit.</a> </li>
<li>Avoid a kio-mtp crash when trying to add a blocked device to the device cache. <a href='http://commits.kde.org/kio-extras/6010d09233e5bb7a00606c44a2fbe5898ecc36e0'>Commit.</a> See bug <a href='https://bugs.kde.org/396527'>#396527</a></li>
</ul>
<h3><a name='kitinerary' href='https://cgit.kde.org/kitinerary.git'>kitinerary</a> <a href='#kitinerary' onclick='toggle("ulkitinerary", this)'>[Hide]</a></h3>
<ul id='ulkitinerary' style='display: block'>
<li>Run the generic IATA BCBP extractor also for Vueling. <a href='http://commits.kde.org/kitinerary/d6a8a24def8882b57cac7d6ebe0a0b1d08b7f7fe'>Commit.</a> </li>
<li>Make the pdfdocumenttest pass without Poppler. <a href='http://commits.kde.org/kitinerary/7af8526819ecbf57c74490b10114e315f144f8ba'>Commit.</a> </li>
</ul>
<h3><a name='kleopatra' href='https://cgit.kde.org/kleopatra.git'>kleopatra</a> <a href='#kleopatra' onclick='toggle("ulkleopatra", this)'>[Hide]</a></h3>
<ul id='ulkleopatra' style='display: block'>
<li>Fix i18n. <a href='http://commits.kde.org/kleopatra/878502e60adb5ce8bc71d0ff6825f6702b8c84e0'>Commit.</a> </li>
<li>Ensure importcertificatefromfilescommand finishes. <a href='http://commits.kde.org/kleopatra/093714ef3057d70cd2df86cc66e82b1e35f506e4'>Commit.</a> </li>
<li>Fix refresh in smartcard view not finishing. <a href='http://commits.kde.org/kleopatra/fa579755fde3310281e08ef929a285c163d13a44'>Commit.</a> </li>
<li>Implement protocol accessors for all tasks. <a href='http://commits.kde.org/kleopatra/ff18ce7f80004e0b03030ea8e953c6a0b493d3ba'>Commit.</a> </li>
</ul>
<h3><a name='kmail' href='https://cgit.kde.org/kmail.git'>kmail</a> <a href='#kmail' onclick='toggle("ulkmail", this)'>[Hide]</a></h3>
<ul id='ulkmail' style='display: block'>
<li>Squeeze title menu. <a href='http://commits.kde.org/kmail/dd7a18e2026b19cf379f7764ec6989a786bf21ad'>Commit.</a> </li>
<li>Remove unused action. <a href='http://commits.kde.org/kmail/571296a1346010dfc8994a2b3ac6d98287e898da'>Commit.</a> </li>
<li>Add missing action + use zoom_menu directly. <a href='http://commits.kde.org/kmail/4313762d297ccad571be8860f9c387fc02ac6814'>Commit.</a> </li>
<li>Remove unused action. <a href='http://commits.kde.org/kmail/e7cd0afb2bdc4113281b35333e8f61af86780564'>Commit.</a> </li>
<li>Fix Bug 397717 - "View Source" option is on 2 menus i.e. "View" and "Message". <a href='http://commits.kde.org/kmail/aff57d146a64e3542cc444c8a5ef1718d3fd9cca'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/397717'>#397717</a></li>
<li>Backport setting for delete-without-confirmation from master, without GUI. <a href='http://commits.kde.org/kmail/8316c7a83c068cf102985166a7de5144a4e23fbf'>Commit.</a> </li>
<li>Fix Quota progress bar for quota limit of 2GB and more. <a href='http://commits.kde.org/kmail/c7d0d125140a31aac00cdfd25b48823113df39dc'>Commit.</a> </li>
</ul>
<h3><a name='kmailtransport' href='https://cgit.kde.org/kmailtransport.git'>kmailtransport</a> <a href='#kmailtransport' onclick='toggle("ulkmailtransport", this)'>[Hide]</a></h3>
<ul id='ulkmailtransport' style='display: block'>
<li>Fix error. <a href='http://commits.kde.org/kmailtransport/6656ae1449c8743243391d15dca41a912e2aecd3'>Commit.</a> </li>
<li>Fix Bug 398036 - Password for sending mails not used when specified via password prompt. <a href='http://commits.kde.org/kmailtransport/6c0464d203066cddadc9383867d6d585b0e613f4'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/398036'>#398036</a></li>
<li>Disable password lineedit when we don't store password. <a href='http://commits.kde.org/kmailtransport/325d0e03480ee756713eccdcffe19a09c44daff2'>Commit.</a> See bug <a href='https://bugs.kde.org/398036'>#398036</a></li>
<li>Make sure to remove password stored. <a href='http://commits.kde.org/kmailtransport/cea86933e00a6ac9ea21ff36ad6e54331ef96ff6'>Commit.</a> </li>
<li>Only recommend XOAUTH2 method on Gmail. <a href='http://commits.kde.org/kmailtransport/c61d6e7873b6e57a36f50f6789c0ebf0c1528064'>Commit.</a> </li>
</ul>
<h3><a name='konsole' href='https://cgit.kde.org/konsole.git'>konsole</a> <a href='#konsole' onclick='toggle("ulkonsole", this)'>[Hide]</a></h3>
<ul id='ulkonsole' style='display: block'>
<li>Scope the glib hack to only < Qt5.11.2 versions. <a href='http://commits.kde.org/konsole/2c9cbdd1d0ac9690e6b6b55192709b5e46fd2faa'>Commit.</a> </li>
</ul>
<h3><a name='kpat' href='https://cgit.kde.org/kpat.git'>kpat</a> <a href='#kpat' onclick='toggle("ulkpat", this)'>[Hide]</a></h3>
<ul id='ulkpat' style='display: block'>
<li>FreeCell: Restore cards autoplaying to foundation. <a href='http://commits.kde.org/kpat/fac28f110c0a9c976677bee2d9d2126f93a711fd'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/397770'>#397770</a></li>
</ul>
<h3><a name='libkdcraw' href='https://cgit.kde.org/libkdcraw.git'>libkdcraw</a> <a href='#libkdcraw' onclick='toggle("ullibkdcraw", this)'>[Hide]</a></h3>
<ul id='ullibkdcraw' style='display: block'>
<li>Fix build with libraw 0.19. <a href='http://commits.kde.org/libkdcraw/f40ebe30dca1cfadc8171364ac325532e6c3f229'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/397345'>#397345</a></li>
</ul>
<h3><a name='libkdepim' href='https://cgit.kde.org/libkdepim.git'>libkdepim</a> <a href='#libkdepim' onclick='toggle("ullibkdepim", this)'>[Hide]</a></h3>
<ul id='ullibkdepim' style='display: block'>
<li>Remove None mode, it was unused. <a href='http://commits.kde.org/libkdepim/fd30041fa65afc52c1a7b54f0f449e40d371f193'>Commit.</a> </li>
<li>Followup fix: only set mWasLastShown if we're actually toggling. <a href='http://commits.kde.org/libkdepim/0a2da16d214d9483cc52ba218c022351350b4aba'>Commit.</a> </li>
<li>Fix inconsistent state of the button to show/hide progress information. <a href='http://commits.kde.org/libkdepim/273a5c3c68b28b67f88e9986feb720648d52502d'>Commit.</a> </li>
</ul>
<h3><a name='libkgapi' href='https://cgit.kde.org/libkgapi.git'>libkgapi</a> <a href='#libkgapi' onclick='toggle("ullibkgapi", this)'>[Hide]</a></h3>
<ul id='ullibkgapi' style='display: block'>
<li>Gmail auth: return proper HTTP response to avoid qwebengine error. <a href='http://commits.kde.org/libkgapi/2b138b37971c53e84b68e01e190ee9624dd89140'>Commit.</a> </li>
<li>Fix typos. <a href='http://commits.kde.org/libkgapi/12d61077135f8f5e218498a7cd9b1f779f747ba0'>Commit.</a> </li>
</ul>
<h3><a name='libksieve' href='https://cgit.kde.org/libksieve.git'>libksieve</a> <a href='#libksieve' onclick='toggle("ullibksieve", this)'>[Hide]</a></h3>
<ul id='ullibksieve' style='display: block'>
<li>Fix i18n. <a href='http://commits.kde.org/libksieve/a01ae07d75701661e8490b1197e84245bd1080a8'>Commit.</a> </li>
</ul>
<h3><a name='mailcommon' href='https://cgit.kde.org/mailcommon.git'>mailcommon</a> <a href='#mailcommon' onclick='toggle("ulmailcommon", this)'>[Hide]</a></h3>
<ul id='ulmailcommon' style='display: block'>
<li>Fix Bug 398149 - Execute command does not work (argument badly transformed). <a href='http://commits.kde.org/mailcommon/9cb198999e0e494c53aa22c2fbad86eb4c5ce31f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/398149'>#398149</a></li>
<li>Const'ify. <a href='http://commits.kde.org/mailcommon/762f37842a659145cafe284a465b1220f195aab4'>Commit.</a> </li>
</ul>
<h3><a name='messagelib' href='https://cgit.kde.org/messagelib.git'>messagelib</a> <a href='#messagelib' onclick='toggle("ulmessagelib", this)'>[Hide]</a></h3>
<ul id='ulmessagelib' style='display: block'>
<li>Make sure that we can create a followupreminder. <a href='http://commits.kde.org/messagelib/8f24d754c9cd73ebe4f4a193905c0eb20cc7eec4'>Commit.</a> </li>
<li>Show info debug. <a href='http://commits.kde.org/messagelib/d8cfc073f9d6e038a9e3cae0f3138f9901aa109d'>Commit.</a> </li>
<li>Fix Bug 397011 - Add Followup Reminder doesn't create reminder correctly. <a href='http://commits.kde.org/messagelib/ac4f4f0fe38132aa7f839af4214c28210298eb3f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/397011'>#397011</a></li>
<li>Run UrlHandlerManagerTest inside the Akonad isolated environment. <a href='http://commits.kde.org/messagelib/feef577c9dc5a192dd0b34d824961cb18ed76068'>Commit.</a> </li>
<li>Fix block tracking url. <a href='http://commits.kde.org/messagelib/d28e3314d6ec12de7c8568d3b527a17a2757363f'>Commit.</a> See bug <a href='https://bugs.kde.org/397464'>#397464</a></li>
<li>Allow to close dialog. <a href='http://commits.kde.org/messagelib/b197ba901c1e6bd213f06a8503aded1aeae884a1'>Commit.</a> </li>
<li>Make CI happy. <a href='http://commits.kde.org/messagelib/5272b5dc0934d7e849920bb62d6d8c403b391dea'>Commit.</a> </li>
<li>Warning--. <a href='http://commits.kde.org/messagelib/7c83328319329a9a63bdafef2ddc5cb2950396b7'>Commit.</a> </li>
<li>Avoid warning about "device not open". <a href='http://commits.kde.org/messagelib/5f604b6eee7d233178db8eb951ea3182791336a8'>Commit.</a> </li>
<li>Fix autotest. <a href='http://commits.kde.org/messagelib/e0748e3b1d039b78226db24814bc7443b7468614'>Commit.</a> </li>
<li>Fix autotest. <a href='http://commits.kde.org/messagelib/06e1683709e11965d67695c487662b5a395d9adc'>Commit.</a> </li>
<li>Fix false positive. <a href='http://commits.kde.org/messagelib/80633c5e2d7d8791009e910fcf03aeb34ca28f6f'>Commit.</a> </li>
</ul>
<h3><a name='okular' href='https://cgit.kde.org/okular.git'>okular</a> <a href='#okular' onclick='toggle("ulokular", this)'>[Hide]</a></h3>
<ul id='ulokular' style='display: block'>
<li>Fix path traversal issue when extracting an .okular file. <a href='http://commits.kde.org/okular/8ff7abc14d41906ad978b6bc67e69693863b9d47'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/398096'>#398096</a></li>
<li>Fix links being "lost" on save. <a href='http://commits.kde.org/okular/0a8d2f7f85750d3852f256ebc8f0e8c9bad9fd84'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/397373'>#397373</a></li>
<li>Make sure sorting in the configure backends is always the same. <a href='http://commits.kde.org/okular/9ba8dd2cd7838c626af79a4edcd8b8437205cc02'>Commit.</a> </li>
<li>Fix problem of saving pdf switches from thumbnail view in sidebar to contents view. <a href='http://commits.kde.org/okular/5e622484c874a81718faaac3d948d355d4e03164'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/389668'>#389668</a></li>
</ul>
<h3><a name='spectacle' href='https://cgit.kde.org/spectacle.git'>spectacle</a> <a href='#spectacle' onclick='toggle("ulspectacle", this)'>[Hide]</a></h3>
<ul id='ulspectacle' style='display: block'>
<li>Fix missing notification title found by -Wswitch-enum. <a href='http://commits.kde.org/spectacle/6eebb57e6e1ff1d753943bd02b77fdef50fbecdc'>Commit.</a> </li>
</ul>
<h3><a name='umbrello' href='https://cgit.kde.org/umbrello.git'>umbrello</a> <a href='#umbrello' onclick='toggle("ulumbrello", this)'>[Hide]</a></h3>
<ul id='ulumbrello' style='display: block'>
<li>Adding a C++ test case with multiple template parameters. <a href='http://commits.kde.org/umbrello/2d3369b2252b1f6255f9a4afc0d607a5c067c858'>Commit.</a> See bug <a href='https://bugs.kde.org/397664'>#397664</a></li>
<li>More robust determination of the name of the base class with template parameters using the abstract syntax tree. <a href='http://commits.kde.org/umbrello/fc7dd9e1578d289a322de12cc33eabf91405b4a2'>Commit.</a> See bug <a href='https://bugs.kde.org/397664'>#397664</a></li>
<li>Fix 'C++ importer does not correctly detect generalization in template classes'. <a href='http://commits.kde.org/umbrello/f7c4d22cf3663d134656055226c7207c88bbef97'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/397664'>#397664</a></li>
</ul>