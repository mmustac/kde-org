---
qtversion: 5.15.2
date: 2024-01-13
layout: framework
libCount: 83
---


### Baloo

* [IndexCleaner] Remove no-op recursion over includedFolders
* [ExtractorProcess] Remove unused members
* [TimeEstimator] Cleanup, remove QObject depency
* Use forward declaration for Baloo::Database
* Remove inacurate mimetype check in xattrindexer and indexcleaner

### Extra CMake Modules

* Fixes to FindLibGit2.cmake (bug 478669)

### KActivities

* Drop unused KF5WindowSystem from cli

### KCodecs

* KEmailAddress: Only trim surrounding whitespace between E-Mail addresses instead of also replacing all whitespace within E-Mail address names with a single ASCII space

### KCoreAddons

* Fix license text loading on Android

### KHolidays #

* Introduce holidays observed in Kenya

### KImageFormats

* avif: new quality settings
* Update CI template
* HEIF plug-in extended to support HEJ2 format

### KIO

* kpropertiesdialog: don't trip over malformed Exec (bug 465290)
* WidgetsAskUserActionHandler: fix backport (bug 448532)
* WidgetsAskUserActionHandler: Use QPointer to check the validity of parent widgets (bug 448532)

### Kirigami

* Make drawer actions accessible

### KJobWidgets

* KUiServerV2JobTracker: prevent potenial use-after-free (bug 471531)

### KRunner

* DBusRunner: Use /runner as default for X-Plasma-DBusRunner-Path property

### Plasma Framework

* [CI] Fix pipeline include

### Purpose

* Adapt to KAccounts API change

### Security information

The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure <faure@kde.org>
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
