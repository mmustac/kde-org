---
qtversion: 5.15.2
date: 2022-06-12
layout: framework
libCount: 83
---


### Baloo

* tag:/ better handle uds name field (bug 449126)

### Extra CMake Modules

* fix linking on OpenBSD
* ECMQtDeclareLoggingCategory: drop support for Qt < 5.4
* ECMAddQtDesignerPlugin: drop support for Qt < 5.9
* ECMAddQtDesignerPlugin: fix support for Qt6
* ECMAddQtDesignerPlugin: fix missing FeatureSummary include
* KDEGitCommitHooks: Fix quoting of variables

### KArchive

* KArchive::addLocalDirectory: preserve empty directories
* Fix zstd KCompressionDevice not compressing as much as it could
* Always delete device if we created it (bug 432726)

### KCalendarCore

* Make the Calendar::accessMode property actually accessible from QML
* Don't heap allocate ICalFormatImpl in ICalFormat::Private
* Don't remove the hasGeo property for Qt6, just make it read-only
* Change Calendar::sortX() methods to work in-place
* Initialize all fields in a default constructed Attendee object
* Add Duration <-> iCal string conversion methods to ICalFormat
* Deprecate largely unused rawEventsForDate overload

### KCMUtils

* KPluginDelegate: reserve space for subtitle
* KPluginSelector: animate placeholder appearance
* KPluginSelector: Don't show placeholder message immediately upon load
* KPluginSelector: fix placeholder location
* AboutPlugin.qml: Do not use translation function when we only have one value
* [kcmultidialog] Fix crash in clear()

### KConfig

* Change "Actual Size" shortcut's text to "Zoom to Actual Size"

### KConfigWidgets

* Change "Actual Size" Action's text to "Zoom to Actual Size"
* KRecentFilesAction: remove the corresponding element in m_recentActions when calling removeAction(action)
* KRecentFilesAction: refactor some code related to removing an action
* KRecentFilesAction: do not reuse removed actions and adding an URL that is already in the menu
* Allow specifying a Qt::ConnectionType in KStandardAction::create

### KContacts

* Re-enable vCard CATEGORIES export

### KCoreAddons

* New: KMemoryInfo class
* KPluginFactory: Create un-deprecated overload to register CreateInstanceWithMetaDataFunction
* KSignalHandler: add error warnings when reading or writing signal
* KPluginFactory: Provide better context in error message if instance could not be created
* KJob: add method to check if job was started with exec()
* New util ksandbox
* KDirWatch: handle IN_Q_OVERFLOW events (bug 387663)
* KListOpenFilesJob: check lsof executable exist in PATH before starting it with QProcess
* KAboutData: improve the API docs for LicenseKey enum

### KDeclarative

* qmlcontrols: bump `QtQuick` import version to 2.15 for GridViewInternal
* QmlObject: Use std::shared_ptr to properly track the lifetime of QQmlEngine (bug 451790)
* Move type registrations to the plugin initialization
* calendareventsplugin: Add label property
* calendareventsplugin: Add support for alternate dates and sub-labels (bug 429882)

### KDE WebKit

* This is going to be gone in KF6

### KGlobalAccel

* Encapsulate duplicate code in a local function
* Properly create lists of items
* Use KWindowSystem to request activation tokens if necessary (bug 453748)

### KDE GUI Addons

* WaylandClipboard: DataControlSource: delete m_mimeData in dtor (bug 454590)
* keysequence: Fix race between recording and currentKeySequence
* keysequence: Fix warning message about sequence size to be more precise
* keysequence: Replace magic number 4 with enum constant

### KHolidays #

* Work around qmlplugindump crashing on gadget singletons

### KHTML

* Remove support for Java applets

### KI18n

* Directly load catalogs from Android assets
* Port to ECMQmlModule

### KImageFormats

* psd: Fix segfault on architectures where char is unsigned (like ARM)

### KInit

* KInit is going to be deprecated in KF6

### KIO

* KCoreUrlNavigator: add urlSelectionRequested signal (bug 453289)
* kio_file: fix data race on static user cache (bug 454619)
* file_unix: use thread id rather than pid for filehelper socket
* [KDirOperator] Add option to show hidden files last
* [KFileItemDelegate] Add a semi-transparent effect to the labels of hidden files
* PreviewJob: Resolve parent mime types before checking wildcards (bug 453480)
* [kemailclientlauncherjob] Allow setting BCC
* TrashSizeCache: fix parsing of directory cachesize file and improve code
* DirectorySizeJob: use targetUrl as url
* KFileItem: GetStatusBarInfo: Improve display for urls
* KDirModel: don't nuke query and fragment in URLs
* [KFilePlacesView] Show inline eject button only for removable drives (bug 453890)
* [KFilePlacesView] Don't show capacity bar for network shares
* kpasswdserver: fix mistake
* rfc search provider: Fix URL
* Skip generation of KCM symlinks on windows
* KCMs: Define plugin id to match the desktop file name
* Embed json metadata in KCMs, port to new systemsettings namespaces
* quickfix crash if element not found, we want to use the range erase here
* Add env var KIO_ENABLE_WORKER_THREADS in case of trouble
* Implement running KIO workers in-process using a thread
* [StatJob] Set total amount to 1 Item
* KSambashare: handle "Weak crypto is allowed" error message
* dropjob: don't create PasteJob when `m_hasArkFormat` is true
* KFileWidget: initialize KFilePlaceModel before KUrlNavigator
* [previewjob] Deduplicate legacy plugins based on desktop file name instead of pluginId

### Kirigami

* Add tools-report-bug to kirigami_package_breeze_icons (bug 454735)
* Wrap license text in AboutItem (bug 454738)
* OverlaySheet: Always restart the open animation if we abort due to size changes
* InlineMessage: Better multiline calculation
* Show tooltip for collapsed "Open Sidebar" button (bug 453771)
* [ColumnView] Don't leak SeparatorItems (bug 453201)
* controls/private: Allow customizing `heightMode` in `ToolBarPageHeader`
* controls: Add `SelectableLabel` component to enable text selection in label
* Handle NavigationTabButton without icon

### KNewStuff

* Don't use menu as transientParent (bug 454895)
* Engine: Provide getter for the pageSize property
* Adjust KMoreTools headers installation
* Relayout header installation location
* EntryDetails: Make summary text selectable
* Fix QtQuickDialogWrapper dialog not being usable in konsole (bug 452593)
* Fix crash if entry gets deleted (bug 454010)
* Add missing translation domain (bug 453827)
* Reuse instances of QFileSystemWatcher for KNSCore::Cache

### KRunner

* RunnerContext: Call addMatch(QList<QueryMatch) internally when addMatch(QueryMatch) is called
* RunnerContext::addMatch: Fix broken handling of relevance for often launched entries
* DBusRunner: Avoid double lookup of match properties
* DBusRunner: Add multiline value to property map of RemoteMatch

### KService

* kbuildsycoca: Ignore last modified time when set to UNIX Epoch (bug 442011)

### KTextEditor

* Don't add empty dictionary to context menu
* Fix whitespace slider & group text related options
* Ensure to keep special dictionary setting on replaced word
* Observe changes on textChanged for QSpinBox (bug 453978)
* Export multi cursors to JS
* Add multi cursor API
* Add suggestion on top of context menu
* Avoid calling slightly expensive contextMenu() twice
* Ensure next right click works properly if there was a selection
* Protect replaceWordBySuggestion against segfault
* Support to change dictionary on words and selections
* Oops! Fix permanent active misspelled range after word selection
* Add misspelled word to spell menu action
* Properly support to fix selected words by spell check
* Move spellcheck entry in context menu on top
* Don't track mouse move for spellcheking
* Add an action to remove cursors from empty lines
* Add methods to set/get cursors and selections

### KUnitConversion

* Add "sq [thing]" as activation trigger for various common area measurements (bug 448868)
* Match singular form in user input for tea/tablespoon (bug 451278)

### KWayland

* Add AppletPopup window type to PlasmaShellSurface (bug 332512)
* Raise minimum plasma-wayland-proto version to 1.7.0
* Fix include dir in the generated pri file
* client: Bump plasmashell version to 7 (bug 453578)

### KWidgetsAddons

* Update kcharselect-data to Unicode 14.0
* KMessageDialog: handle the dialog getting closed without using the buttonBox

### KWindowSystem

* Add a new window type named AppletPopup (bug 332512)

### KXMLGUI

* KShortcutsDialog: hide Global columns when there are no Global shortcuts (bug 427129)
* Silence UBSan false positive in KActionCollection

### Plasma Framework

* Dialog should use the Dock type unless requested otherwise (bug 454635)
* Do not send all windows below the dialog when window type is AppletPopup
* Use QT_FEATURE_foo to detect opengl* support, and TARGET for glesv2
* extras/Representation: Bump QML imports
* extras/Representation: Fix indentation
* Disable spacing around ActionTextFields action-row when it is empty
* Disable undo operation for PlasmaExtras PasswordField
* breeze/widgets: Make analog clock follow accent color
* KDeclarative::ConfigPropertyMap has been deprecated since 5.89
* extras/PasswordField: Fix imports and docs
* PC3 ScrollView: Move contentItem clipping hack into a Binding component
* PC3 ScrollView: Drop support for nullable ScrollBars
* PC3 ScrollView: Remove id from a scrollbar
* PC3 ScrollView: Fix ScrollBar.active property
* Dialog: Expose QQuickItem instead and cast to AppletQuickItem in C++
* Implement resize capability for Plasma dialog (bug 332512)
* Use size_t for qHash return values
* Protect against missing KService lib when building without deprecations
* Plasma:PluginLoader: Add missing fwd of KPluginInfo
* Plasma::Theme: Add accessor for metadata
* ExpandableListItem: Refactor JavaScript expression to use newer features
* [Calendar] Wheel year on year view and decade on decade view
* Use QSFPM::setFilterRegularExpression everywhere
* Remove author data, name and description from package metadata files
* plasmaquick: fix a memory leak caused by `qmlRegisterType`
* containmentinterface: Change wallpaper plugin in dropjob action
* Revert "IconItem: Allow specifying a custom loader"

### Prison

* Fix decimal/hexadecimal mix-up in the binary detection heuristic
* Simplify headers installation location
* cmake: add purpose property to 3d-party libraries
* Handle video frames with a non-standard row stride correctly as well

### QQC2StyleBridge

* Buttons: Press F to pay respects
* DelayButton: Fix invalid id references in transition/animation
* ProgressBar,Slider: Draw position smoothly with greater precision
* ProgressBar: Fix rendering of visual position when `from` is non-zero
* ProgressBar: Replace hardcoded redraw interval with frame-based animations
* RoundButton: Fix expression block may return nothing (undefined)
* ScrollView: Stop passing down `enabled` property to the ScrollBars explicitly
* ScrollView: Drop support for nullable ScrollBars
* ComboBox: Use qualified ListView.view attached property in delegate
* ComboBox: Remove workaround for `editable` property from Qt 5.7 times
* ComboBox: Use rect type for rect property
* Fix overlapping scrollbar (bug 453958)
* Relayout the scrollbar when non interactive
* Remove code that was only used with Qt <5.8

### Sonnet

* Try to load en_US for LANG=C (bug 410113)
* Don't warn, just debug about not loadable plugin
* Unload skipped/already loaded plugin

### Syntax Highlighting

* Avoid spell checking in diffs
* Avoid unprefixed CamelCase headers generated directly in build dir (bug 453759)
* Bash: fix comments in double braces (bug 450878)
* systemd unit: update to systemd v251
* debchangelog: add Kinetic Kudu

### Security information

The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure <faure@kde.org>
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
