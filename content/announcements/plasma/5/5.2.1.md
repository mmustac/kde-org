---
aliases:
- ../../plasma-5.2.1
changelog: 5.2.0-5.2.1
date: '2015-02-24'
layout: plasma
---

{{<figure src="/announcements/plasma/5/5.2.0/full-screen.png" alt="Dual monitor setup" class="text-center" width="600px">}}

{{< i18n_date >}}

{{< i18n "annc-plasma-bugfix-intro" "5" "5.2.1" >}}

{{% i18n "annc-plasma-bugfix-minor-release-1" "5.2" "/announcements/plasma/5/5.2.0" "2015" %}}

{{< i18n "annc-plasma-bugfix-worth-5" >}}

{{< i18n "annc-plasma-bugfix-last" >}}

- Don't turn off the screen or suspend the computer when watching videos in a web browser
- Fix Powerdevil from using full CPU
- Show the correct prompt for a fingerprint reader swipe
- Show correct connection name in Plasma Network Manager
- Remove kdelibs4support code in many modules
- Fix crash when switching to/from Breeze widget style
- In KScreen fix crash when multiple EDID requests for the same output are enqueued
- In KScreen fix visual representation of output rotation
- In Oxygen style improved rendering of checkbox menu item's contrast pixel, especially when selected using Strong highlight.
- In Plasma Desktop improve rubber band feel and consistency with Dolphin.
- In Plasma Desktop use smooth transformation for scaling down the user picture
- When setting color scheme information for KDElibs 4, don't read from KF5 kdeglobals
- Baloo KCM: Show proper icons (porting bug)
