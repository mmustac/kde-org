---
aliases:
- ../../plasma-5.9.3
changelog: 5.9.2-5.9.3
date: 2017-02-28
layout: plasma
youtube: lm0sqqVcotA
figure:
  src: /announcements/plasma/5/5.9.0/plasma-5.9.png
  class: text-center mt-4
asBugfix: true
---

- KSSHAskPass: Fix git ssh password detection. <a href='https://commits.kde.org/ksshaskpass/408c284f6a35694f7e6be76c3416b80f3ef7fdba'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/376228'>#376228</a>. Phabricator Code review <a href='https://phabricator.kde.org/D4540'>D4540</a>
- Fix default fixed font in fonts kcm. <a href='https://commits.kde.org/plasma-desktop/d02de0db36a35cc2e66ff91b8f5796962fa4e04e'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4794'>D4794</a>
- Fix crash when switching activities. <a href='https://commits.kde.org/plasma-workspace/05826bd5ba25f6ed7be94ff8d760d6c8372f148c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/376055'>#376055</a>. Phabricator Code review <a href='https://phabricator.kde.org/D4631'>D4631</a>
