---
aliases:
- ../../plasma-5.1.95
changelog: 5.1.2-5.1.95
date: '2015-01-13'
layout: plasma
---

Tuesday, 13 January 2015. Today KDE releases a beta for Plasma 5.2. This release adds a number of new components and improves the existing desktop. We welcome all testers to find and help fix the bugs before our stable release in two weeks' time.

{{<figure src="/announcements/plasma/5/5.2.0/full-screen.png" alt="Dual monitor setup" class="text-center" width="600px">}}

## New Components

{{<figure src="/announcements/plasma/5/5.2.0/kscreen.png" alt="Plasma 5.2" class="text-center" width="600px" caption="KScreen dual monitor setup">}}

{{< hg_stop >}}

This release of Plasma comes with some new components to make your desktop even more complete:

<strong>BlueDevil</strong>: a range of desktop components to manage Bluetooth devices. It'll set up your mouse, keyboard, send &amp; receive files and you can browse for devices.

<strong>KSSHAskPass</strong>: if you access computers with ssh keys but those keys have passwords this module will give you a graphical UI to enter those passwords.

<strong>Muon</strong>: install and manage software and other addons for your computer.

<strong>Login theme configuration (SDDM)</strong>: SDDM is now the login manager of choice for Plasma and this new System Settings module allows you to configure the theme.

<strong>KScreen</strong>: getting its first release for Plasma 5 is the System Settings module to set up multiple monitor support.

<strong>GTK Application Style</strong>: this new module lets you configure themeing of applications from Gnome.

<strong>KDecoration</strong>: this new library makes it easier and more reliable to make themes for KWin, Plasma's window manager.

## Work in Progress

These modules have problem which makes it unlikely they will be in the stable release but we welcome testing.

<strong>Touchpad settings</strong>: feeling left handed? You can use this new System Settings module to set up your touchpad however you like.

<strong>User Manager</strong>: a module for System Settings to create and administer user accounts.

## Other highlights

Undo changes to Plasma desktop layout

{{<figure src="/announcements/plasma/5/5.2.0/output.gif" alt="Undo desktop changes" class="text-center" width="600px" caption="Undo changes to desktop layout">}}

Smarter sorting of results in KRunner, press Alt-space to easily search through your computer

{{<figure src="/announcements/plasma/5/5.2.0/krunner.png" alt="Smart sorting in KRunner" class="text-center" width="600px" caption="Smart sorting in KRunner">}}

Breeze window decoration theme adds a new look to your desktop and is now used by default

{{<figure src="/announcements/plasma/5/5.2.0/window_decoration.png" alt="New Breeze Window Decoration" class="text-center" width="600px" caption="New Breeze Window Decoration">}}

The artists in the visual design group have been hard at work on many new Breeze icons

They are have added a new white mouse cursor theme for Breeze.

New plasma widgets: 15 puzzle, web browser, show desktop

{{<figure src="/announcements/plasma/5/5.2.0/new_plasmoid.png" alt="Web browser plasmoid" class="text-center" width="600px" caption="Web browser plasmoid">}}

Audio Player controls in KRunner, press Alt-Space and type next to change music track

The Kicker alternative application menu can install applications from the menu and adds menu editing features.

Over 300 bugs fixed throughout Plasma modules.
