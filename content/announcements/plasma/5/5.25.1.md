---
date: 2022-06-21
changelog: 5.25.0-5.25.1
layout: plasma
video: false
asBugfix: true
draft: false
---

+ Kcms/fonts: Fix font hinting preview. [Commit.](http://commits.kde.org/plasma-workspace/e560ea67f0cda92c82774ba776b8c0a72ce4dc43) Fixes bug [#413673](https://bugs.kde.org/413673)
+ Upower: Prevent integer overflow during new brightness computation. [Commit.](http://commits.kde.org/powerdevil/dd74cdbdd3849fbd86e6613ef7ecab6c7857cb89) Fixes bug [#454161](https://bugs.kde.org/454161)
+ Fix dragging especially by touch. [Commit.](http://commits.kde.org/kwin/2eb0c67b0bc7ee62e37cb510fafeec66a012b53f) Fixes bug [#455268](https://bugs.kde.org/455268)
