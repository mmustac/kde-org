---
aliases:
- ../../plasma-5.8.2
changelog: 5.8.1-5.8.2
date: 2016-10-18
layout: plasma
youtube: LgH1Clgr-uE
figure:
  src: /announcements/plasma/5/5.8.0/plasma-5.8.png
  class: text-center mt-4
asBugfix: true
---

- Fix 'Default' color scheme. <a href="http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=613194c293b63004af5fc43762b92bd421ddf5b6">Commit.</a>
- Restore all panel properties. <a href="http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=aea33cddb547cc2ba98be5dd45dc7562b32b4b9a">Commit.</a> Fixes bug <a href="https://bugs.kde.org/368074">#368074</a>. Fixes bug <a href="https://bugs.kde.org/367918">#367918</a>
