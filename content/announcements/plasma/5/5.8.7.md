---
aliases:
- ../../plasma-5.8.7
changelog: 5.8.6-5.8.7
date: 2017-05-23
layout: plasma
youtube: LgH1Clgr-uE
figure:
  src: /announcements/plasma/5/5.8.0/plasma-5.8.png
  class: text-center mt-4
asBugfix: true
---

- User Manager: Make sure the new avatar is always saved. <a href="https://commits.kde.org/user-manager/826e41429917f6c1534e84e8b7821b8b53675910">Commit.</a> Fixes bug <a href="https://bugs.kde.org/350836">#350836</a>. Phabricator Code review <a href="https://phabricator.kde.org/D5779">D5779</a>
- [Logout Screen] Show suspend button only if supported. <a href="https://commits.kde.org/plasma-workspace/8bc32846a5a41fa67c106045c43bb8c4af7e7e6f">Commit.</a> Fixes bug <a href="https://bugs.kde.org/376601">#376601</a>
- [Weather] Fix term used for thunderstorm in bbcukmet data db. <a href="https://commits.kde.org/plasma-workspace/12a82fcba5672ee6b4473dfc6d0a84280a2bfbbb">Commit.</a>
