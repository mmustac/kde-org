---
aliases:
- ../../plasma5.0
date: '2014-07-15'
description: KDE Ships Plasma 5.0.
title: New Plasma brings a cleaner interface on top of a new graphics stack
layout: plasma
customOutro: true
---

{{<figure src="/announcements/plasma/5/5.0/plasma-5-banner.png" class="text-center" >}}

{{<figure src="/announcements/plasma/5/5.0/screenshots/desktop.png" class="text-center float-right ml-4" caption="Plasma 5.0" width="400px" >}}

July 15, 2014. KDE proudly announces the immediate availability of Plasma 5.0, providing a visually updated core desktop experience that is easy to use and familiar to the user. Plasma 5.0 introduces a new major version of KDE&#x27;s workspace offering. The new Breeze artwork concept introduces cleaner visuals and improved readability. Central work-flows have been streamlined, while well-known overarching interaction patterns are left intact. Plasma 5.0 improves support for high-DPI displays and ships a converged shell, able to switch between user experiences for different target devices. Changes under the hood include the migration to a new, fully hardware-accelerated graphics stack centered around an OpenGL(ES) scenegraph. Plasma is built using Qt 5 and Frameworks 5.

Major changes in this new version include:

- <strong>An updated and modernized, cleaner visual and interactive user experience</strong><br /> The new Breeze theme is a high-contrast, flat theme for the workspace. It is available in light and dark variants. Simpler and more monochromatic graphics assets and typography-centered layouts offer a clean and visually clear user experience.

* <strong>Smoother graphics performance thanks to an updated graphics stack</strong> <br /> Plasma&#x27;s user interfaces are rendered on top of an OpenGL or OpenGL ES scenegraph, offloading many of the computational-intensive rendering tasks. This allows for higher framerates and smoother graphics display while freeing up resources of the main system processor.

{{<youtube id="c8JYt_xkJuY">}}

<br>Other user-visible changes are:<br>

- <strong>Converged shell</strong><br />
  The &quot;converged Plasma shell&quot; that loads up the desktop in Plasma 5.0 can be extended with other user experiences. This lays the base for a converged user experience bringing up a suitable UI for a given target device. User experiences can be switched dynamically at runtime, allowing, based on hardware events such as plugging in a keyboard and a mouse.

- <strong>Modernized launchers</strong><br />
  The application launchers&#x27; user interfaces have been reworked. Among the changes are a visually redesigned Kickoff application launcher, a newly included, more menu-like launcher, called Kicker and a new, QtQuick-based interface for KRunner.<br>

- <strong>Workflow improvements in the notification area</strong><br />
  The notification area has been cleaned up, and sports a more integrated look now. Less popup windows and quicker transitions between for example power management and networks settings lead to a more distraction-free interaction pattern and greater visual coherence.

- <strong>Better support for high-density (high-DPI) displays</strong><br />
  Support for high-density displays has been improved. Many parts of the UI now take the physical size of the display into account. This leads to better usability and display on screens with very small pixels, such as Retina displays.

The <a href='http://youtu.be/4n2dthDSGkc'>Plasma 5.0 Visual Feature Guide</a> provides a tour around the updated desktop.<br />

{{<youtube id="4n2dthDSGkc">}}

## Breeze Artwork Improves Visual clarity

{{<figure src="/announcements/plasma/5/5.0/screenshots/osd-brightness.png" class="text-center float-right ml-4" caption="Brightness Settings in Plasma 5" width="400px" >}}

The new Breeze theme, which is still in its infancy, welcomes the user with a <strong>cleaner, modernized user interface</strong>, which improves contrast and reduces visual clutter throughout the workspace. Stronger reliance on typography eases the recognition of UI elements. These changes go together with flatter default theming and improved contrast to improve visual clarity further. Breeze being a new artwork concept, is only starting to show its face. A theme for the workspace components is already available, theming of traditional widgets is under way, and the work on a new icon theme has commenced. The migration to a fully Breeze-themed workspace will be a gradual one, with its first signs showing up in Plasma 5.0.<br /> Plasma 5 brings a <strong>greater level of flexibility and consistency</strong> to core components of the desktop. The widget explorer, window and activity switcher now share a common interaction scheme. More reliance on vertical instead of horizontal lists provides better usability. Moving the window switcher to the side of the screen shifts the user&#x27;s focus towards the applications and documents, clearing the stage for the task at hand.

## Converging User Experience

{{<figure src="/announcements/plasma/5/5.0/screenshots/wallpaper-config.png" class="text-center float-right ml-4" caption="Wallpaper Settings" width="400px" >}}

The new-in-Plasma-5 &quot;Look and Feel&quot; mechanism allows swapping out parts like the task and activity switchers, lock and login screen and the &quot;Add Widgets&quot; dialog. This allows for greater consistency across central workflows, improves clarity within similar interaction patterns and changing related interaction patterns across the workspace at once.<br>On top of that, the Plasma 5 shell is able to load and switch between user experiences for a given target device, introducing a truly convergent workspace shell. The workspace demonstrated in this release is Plasma Desktop. It represents an evolution of known desktop and laptop paradigms. A tablet-centric and mediacenter user experience are under development as alternatives. While Plasma 5.0 will feel familiar, users will notice a more modern and consistent, cleaner workspace experience.

## Fully Hardware-Accelerated Graphics Stack

{{<figure src="/announcements/plasma/5/5.0/screenshots/krunner-kde.png" class="text-center float-right ml-4" caption="Search in Plasma" width="400px" >}}

Plasma 5 completes the migration of the Plasma workspace to QtQuick. Qt 5&#x27;s QtQuick 2 uses a hardware-accelerated OpenGL(ES) scenegraph to compose and render graphics on the screen. This allows offloading computationally expensive graphics rendering tasks onto the GPU which frees up resources on the system&#x27;s main processing unit, is faster and more power-efficient.<br /> Internal changes in the graphics compositor and underlying Frameworks prepare support for running on Wayland, which is planned for an upcoming release.

<br/>

## Suitability and Updates

{{<figure src="/announcements/plasma/5/5.0/screenshots/lockscreen.png" class="text-center float-right ml-4" caption="New Lock Screen" width="400px" >}}

Plasma 5.0 provides a core desktop with a feature set that will suffice for many users. The development team has concentrated on tools that make up the central workflows. As such, not all features from the Plasma 4.x series are available yet, many of them planned to return with a subsequent release. As with any software release of this size, there will be bugs that make a migration to Plasma 5 hard, if not impossible for some users. The development team would like to hear about such issues, so they can be addressed and fixed. We have compiled a list of <a href='https://community.kde.org/Plasma/5.0_Errata'>known issues</a>. Users can expect monthly bugfix updates, and a release bringing new features and more old ones back in the autumn 2014.<br /> With a substantial new toolkit stack below some exciting new crashes and problems that need time to be shaken out are to be expected in a first stable release. Especially graphics performance is heavily dependent on specific hardware and software configurations and usage patterns. While it has great potential, it takes time to wrangle this out of it. The underlying stack may not be entirely ready for this either. In many scenarios, Plasma 5.0 will display the buttery smooth performance it is capable of - while at other times, it may be hampered by various shortcomings. These can and will be addressed, however, much is dependent on components like Qt, Mesa and hardware drivers lower in the stack.

## Installing and providing feedback

Before installation, it is recommended to read the list of <a href='https://community.kde.org/Plasma/5.0_Errata'>known issues</a>.

{{<figure src="/announcements/plasma/5/5.0/screenshots/shutdown.png" class="text-center float-right ml-4" caption="Ending the Plasma Session" width="400px" >}}

The easiest way to try it out is the <a href='http://files.kde.org/snapshots/neon5-latest.iso.mirrorlist'>Neon 5 ISO</a>, a live OS image updated with the latest builds straight from source.

Some distributions have created, or are in the process of creating, packages; for an overview of 5.0 packages, see <a href='http://community.kde.org/Plasma/Packages'>our distribution packages wiki page</a>

<a href='/info/plasma-5.0.0'>Source download</a>. You can install Plasma 5.0 directly from source. KDE's community wiki has <a href='http://community.kde.org/Frameworks/Building'>instructions for compiling it</a>. Note that Plasma 5 does not co-install with Plasma 4.x, you will need to uninstall older versions or install into a separate prefix.

You can provide feedback either via the <a href='irc://#plasma@freenode.net'>#Plasma IRC channel</a>, <a href='https://mail.kde.org/mailman/listinfo/plasma-devel'>Plasma-devel mailing list</a> or report issues via <a href='https://bugs.kde.org/enter_bug.cgi?product=plasmashell&format=guided'>bugzilla</a>. Plasma 5 is also <a href='http://forum.kde.org/viewforum.php?f=289'>discussed on the KDE Forums</a>. Your feedback is greatly appreciated. If you like what the team is doing, please let them know!
