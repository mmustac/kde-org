---
aliases:
- ../../plasma-5.14.1
changelog: 5.14.0-5.14.1
date: 2018-10-16
layout: plasma
figure:
  src: /announcements/plasma/5/5.14.0/plasma-5.14.png
asBugfix: true
---

- Keyboard works again for desktop icons. Focus handling fixes. <a href="https://commits.kde.org/plasma-desktop/480962c3ff93f9d3839a426b9ac053399d03e38d">Commit.</a> Fixes bug <a href="https://bugs.kde.org/399566">#399566</a>. Phabricator Code review <a href="https://phabricator.kde.org/D16106">D16106</a>
- Snap: no need to have a notifier. <a href="https://commits.kde.org/discover/252558c13d57e3530a3dac5f3b2b700c61ba059d">Commit.</a>
- [effects/wobblywindows] Fix visual artifacts caused by maximize effect. <a href="https://commits.kde.org/kwin/c2ffcfdc218c94fc93e1960a8760883eef4e4495">Commit.</a> Fixes bug <a href="https://bugs.kde.org/370612">#370612</a>. Phabricator Code review <a href="https://phabricator.kde.org/D15961">D15961</a>
- [KonsoleProfiles applet] Fix navigating with the keyboard. <a href="https://commits.kde.org/kdeplasma-addons/91ee9cc72a490ccaf931c49229a9b8d2303b8e65">Commit.</a> Phabricator Code review <a href="https://phabricator.kde.org/D15877">D15877</a>
