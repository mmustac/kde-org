---
custom_about: true
custom_contact: true
title: ZD Innovation of the Year 1998/1999
date: "1999-03-19"
description: (Hanover, Germany) The K Desktop Environment(KDE), an advanced and user-friendly desktop for the increasingly popular GNU/Linux - Unix operating system, was awarded top honors at CeBIT, the worlds largest computer trade fair, as "Innovation of the Year 1998/99" in the category "Software".
---

FOR IMMEDIATE RELEASE

(Hanover, Germany) [The K Desktop Environment(KDE)](http://www.kde.org),
an advanced and user-friendly desktop for
the increasingly popular GNU/Linux - Unix operating system, was awarded
top honors at CeBIT, the worlds largest computer trade fair, as
"Innovation of the Year 1998/99" in the category "Software".

According to an [article](http://www.zdnet.de/news/artikel/1999/03/19011-wf.htm)
published by Ziff-Davis, sponsor of the award, the criterion
for granting this award of technical excellence was not commerical success
but
creativity behind the product's design, an exceptional solution for
a specific problem or a completely new concept. The other two
finalists for the award were Lotus eSuite and Microtest Virtual CD.

KDE developers were very pleased by the announcement. In announcing the
award to the KDE community, Kalle Dalheimer, a KDE developer,
beamed, "This award is a great achievement for the whole KDE team!
Congratulations to all of you!"

The availability of the high quality and mature desktop is considered a
key software for enterprise and home use of GNU/Linux. Several large
software vendors have announced support of KDE in recent weeks,
including Red Hat Software, Inc. and Corel Corporation.

#### ABOUT KDE

KDE released version 1.1 of its desktop on February 19, 1999.
KDE runs on GNU/Linux, FreeBSD, Solaris, HP-UX and other Unix variants. It is
available in all major Linux and BSD distributions, and is also downloadable
free of charge from
[KDE's web site](ftp://ftp.kde.org/pub/kde).

KDE's major contributions to Unix are related to
the ease of installation, configuration and use. KDE provides users
with an attractive, functional desktop, applications that
provide a consistent look-and-feel as well as internationalization.
KDE offers also a consistent user interface across all Unix systems
and numerous hardware platforms, from PCs to
powerful Internet servers, thereby permitting organizations to
freely switch hardware without incurring the costs associated with
switching operating systems.

The KDE project was launched in October 1996 by a small group of
developers. The project immediately adopted the open source model
and grew quickly. Today, it is one of the largest open source development
projects, with several hundred contributing developers, 1.2 million lines of
source code, hundreds of translators who translate KDE into 32 different
languages and thousands of interested users assisting
in testing and debugging.
