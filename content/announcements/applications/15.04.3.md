---
aliases:
- ../announce-applications-15.04.3
changelog: true
date: 2015-07-01
description: KDE Ships KDE Applications 15.04.3
layout: application
title: KDE Ships KDE Applications 15.04.3
version: 15.04.3
---

July 1, 2015. Today KDE released the third stability update for <a href='../15.04.0'>KDE Applications 15.04</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

More than 20 recorded bugfixes include improvements to kdenlive, kdepim, kopete, ktp-contact-list, marble, okteta and umbrello.

This release also includes Long Term Support versions of Plasma Workspaces 4.11.21, KDE Development Platform 4.14.10 and the Kontact Suite 4.14.10.