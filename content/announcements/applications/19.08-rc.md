---
aliases:
- ../announce-applications-19.08-rc
date: 2019-08-02
description: KDE Ships Applications 19.08 Release Candidate.
layout: application
release: applications-19.07.90
title: KDE Ships Release Candidate of KDE Applications 19.08
version_number: 19.07.90
version_text: 19.08 Release Candidate
---

August 02, 2019. Today KDE released the Release Candidate of the new versions of KDE Applications. With dependency and feature freezes in place, the KDE team's focus is now on fixing bugs and further polishing.

Check the <a href='https://community.kde.org/Applications/19.08_Release_Notes'>community release notes</a> for information on tarballs and known issues. A more complete announcement will be available for the final release.

The KDE Applications 19.08 releases need a thorough testing in order to maintain and improve the quality and user experience. Actual users are critical to maintaining high KDE quality, because developers simply cannot test every possible configuration. We're counting on you to help find bugs early so they can be squashed before the final release. Please consider joining the team by installing the Release Candidate <a href='https://bugs.kde.org/'>and reporting any bugs</a>.