---
aliases:
- ../announce-applications-15.08.0
changelog: true
date: 2015-08-19
description: KDE Ships Applications 15.08.0.
layout: application
release: applications-15.08.0
title: KDE Ships KDE Applications 15.08.0
version: 15.08.0
---

{{<figure src="https://dot.kde.org/sites/dot.kde.org/files/dolphin15.08_0.png" alt="Dolphin in the new look - now KDE Frameworks 5 based" class="text-center" width="600px" caption="Dolphin in the new look - now KDE Frameworks 5 based">}}

August 19, 2015. Today KDE released KDE Applications 15.08.

With this release a total of 107 applications have been ported to <a href='https://dot.kde.org/2013/09/25/frameworks-5'>KDE Frameworks 5</a>. The team is striving to bring the best quality to your desktop and these applications. So we're counting on you to send your feedback.

With this release there are several new additions to the KDE Frameworks 5-based applications list, including <a href='https://www.kde.org/applications/system/dolphin/'>Dolphin</a>, <a href='https://www.kde.org/applications/office/kontact/'>the Kontact Suite</a>, <a href='https://www.kde.org/applications/utilities/ark/'>Ark</a> <a href='https://games.kde.org/game.php?game=picmi'>Picmi</a>, etc.

### Kontact Suite technical preview

Over the past several months the KDE PIM team did put a lot of effort into porting Kontact to Qt 5 and KDE Frameworks 5. Additionally the data access performance got improved considerably by an optimized communication layer. The KDE PIM team is working hard on further polishing the Kontact Suite and is looking forward to your feedback. For more and detailed information about what changed in KDE PIM see <a href='http://www.aegiap.eu/kdeblog/'>Laurent Montels blog</a>.

### Kdenlive and Okular

This release of Kdenlive includes lots of fixes in the DVD wizard, along with a large number of bug-fixes and other features which includes the integration of some bigger refactorings. More information about Kdenlive's changes can be seen in its <a href='https://kdenlive.org/discover/15.08.0'>extensive changelog</a>. And Okular now supports Fade transition in the presentation mode.

{{<figure src="https://dot.kde.org/sites/dot.kde.org/files/ksudoku_small_0.png" alt="KSudoku with a character puzzle" class="text-center" width="600px" caption="KSudoku with a character puzzle">}}

### Dolphin, Edu and Games

Dolphin was as well ported to KDE Frameworks 5. Marble got improved <a href='https://en.wikipedia.org/wiki/Universal_Transverse_Mercator_coordinate_system'>UTM</a> support as well as better support for annotations, editing and KML overlays.

Ark has had an astonishing number of commits including many small fixes. Kstars received a large number of commits, including improving the flat ADU algorithm and checking for out of bound values, saving Meridian Flip, Guide Deviation, and Autofocus HFR limit in the sequence file, and adding telescope rate and unpark support. KSudoku just got better. Commits include: add GUI and engine for entering in Mathdoku and Killer Sudoku puzzles, and add a new solver based on Donald Knuth's Dancing Links (DLX) algorithm.

### Other Releases

Along with this release Plasma 4 will be published in its LTS version for the last time as version 4.11.22.