---
aliases:
- ../announce-4.14-rc
date: '2014-07-31'
description: KDE Ships Release Candidate of Applications and Platform 4.14.
title: KDE Ships Release Candidate of Applications and Platform 4.14
---

July 31, 2014. Today KDE released the Release Candidate of the new versions of Applications and Development Platform. With API, dependency and feature freezes in place, the KDE team's focus is now on fixing bugs and further polishing.

With the large number of changes, the 4.14 releases need a thorough testing in order to maintain and improve the quality and user experience. Actual users are critical to maintaining high KDE quality, because developers simply cannot test every possible configuration. We're counting on you to help find bugs early so they can be squashed before the final release. Please consider joining the 4.14 team by installing the release candidate <a href='https://bugs.kde.org/'>and reporting any bugs</a>.

### KDE Software Compilation 4.14 Release Candidate

The KDE Software Compilation, including all its libraries and its applications, is available for free under Open Source licenses. KDE's software can be obtained in source and various binary formats from <a href='http://download.kde.org/unstable/4.13.97/'>download.kde.org</a> or with any of the <a href='/distributions'>major GNU/Linux and UNIX systems</a> shipping today.

#### Installing 4.14 Release Candidate Binary Packages

<em>Packages</em>.
Some Linux/UNIX OS vendors have kindly provided binary packages of 4.14 Release Candidate (internally 4.13.97) for some versions of their distribution, and in other cases community volunteers have done so. Additional binary packages, as well as updates to the packages now available, may become available over the coming weeks.

<em>Package Locations</em>.
For a current list of available binary packages of which the KDE Project has been informed, please visit the <a href='http://community.kde.org/KDE_SC/Binary_Packages#KDE_SC_4.14_RC_.284.13.97.29'>Community Wiki</a>.

#### Compiling 4.14 Release Candidate

The complete source code for 4.14 Release Candidate may be <a href='http://download.kde.org/unstable/4.13.97/src/'>freely downloaded</a>. Instructions on compiling and installing 4.13.97 are available from the <a href="/info/4/4.13.97">4.13.97 Info Page</a>.

#### Supporting KDE

KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Supporting KDE page</a> for further information or become a KDE e.V. supporting member through our new <a href='https://kde.org/community/donations/'>Join the Game</a> initiative.


