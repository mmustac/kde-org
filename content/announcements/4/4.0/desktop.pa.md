---
custom_about: true
custom_contact: true
hidden: true
title: KDE 4.0 ਡੈਸਕਟਾਪ
---

<h2>ਪਲਾਜ਼ਮਾ</h2>

<div class="text-center">
<a href="/announcements/4/4.0/desktop.png">
<img src="/announcements/4/4.0/desktop_thumb.png" class="img-fluid">
</a> <br/>
<em>KDE 4.0 ਡੈਸਕਟਾਪ</em>
</div>
<br/>

<p>
ਪਲਾਜ਼ਮਾ KDE ਦੀ ਨਵੀਂ ਡੈਸਕਟਾਪ ਸ਼ੈੱਲ ਹੈ, ਜੋ ਕਿ ਐਪਲੀਕੇਸ਼ਨ ਸ਼ੁਰੂ ਕਰਨ ਲਈ ਨਵੇਂ ਟੂਲ ਦਿੰਦੀ ਹੈ, ਮੇਨ KDE ਯੂਜ਼ਰ ਇੰਟਰਫੇਸ ਦਿੰਦੀ ਹੈ ਅਤੇ 
ਤੁਹਾਡੇ ਆਪਣੇ ਡੈਸਕਟਾਪ ਨਾਲ ਸਾਂਝ ਪਾਉਣ ਲਈ ਕਈ ਨਵੇ ਢੰਗ ਵੀ ਉਪਲੱਬਧ ਕਰਵਾਉਦੀ ਹੈ।
</p>
<p>
ਪਲਾਜ਼ਮਾ ਦੇ ਡੈਸ਼ਬੋਰਡ ਝਲਕ ਨੇ ਪੁਰਾਣੇ "ਡੈਸਕਟਾਪ ਵੇਖੋ" ਫੀਚਰ ਨੂੰ ਖਤਮ ਕਰ ਦਿੱਤਾ ਹੈ। ਡੈਸ਼ਬੋਰਡ ਯੋਗ ਕਰਨ ਨਾਲ
ਸਭ ਵਿੰਡੋਜ਼ ਨੂੰ ਓਹਲੇ ਕਰ ਦਿੱਤਾ ਜਾਂਦਾ ਹੈ ਅਤੇ ਵਿਦਜੈੱਟਾਂ ਨੂੰ ਉਹਨਾਂ ਦੇ ਅੱਗੇ ਰੱਖ ਦਿੱਤਾ ਜਾਂਦਾ ਹੈ। ਡੈਸ਼ਬੋਰਡ ਝਲਕ
ਵੇਖਣ ਲਈ CONTOL+F12 ਦੱਬੋ, ਜਿਸ ਨਾਲ ਤੁਸੀਂ ਆਪਣੇ ਪਲਾਜ਼ਮੋਡ ਦੀ ਝਲਕ ਵੇਖ ਸਕੋ, ਡੈਸਕਟਾਪ ਨੋਟ ਪੜ੍ਹ
ਸਕੋ, RSS ਫੀਡ ਚੈੱਕ ਕਰ ਸਕੋ, ਮੌਜੂਦਾ ਮੌਸਮ ਹਾਲਤ ਵੇਖ ਸਕੋ, ਇਹ ਸਭ ਪਲਾਜ਼ਮਾ ਰਾਹੀਂ ਕੀਤਾ ਜਾ ਸਕਦਾ ਹੈ।
</p>

<div class="text-center">
<a href="/announcements/4/4.0/dashboard.png">
<img src="/announcements/4/4.0/dashboard_thumb.png" class="img-fluid">
</a> <br/>
<em>ਪਲਾਜ਼ਮਾ ਦਾ ਡੈਸ਼ਬੋਰਡ</em>
</div>
<br/>

<h3>ਕੇ-ਰਨਰ ਨਾਲ ਐਪਲੀਕੇਸ਼ਨਾਂ ਚਲਾਓ, ਖੋਜੋ ਅਤੇ ਵੈੱਬ ਪੇਜ਼ ਖੋਲ੍ਹੋ</h3>
<p>
<strong>ਕੇ-ਰਨਰ</strong> ਤੁਹਾਨੂੰ ਤੁਰੰਤ ਐਪਲੀਕੇਸ਼ਨ ਚਲਾਉਣ ਲਈ ਸਹਾਇਕ ਹੈ। ALT+F2 ਦੱਬਣ
ਨਾਲ ਕੇਰਨਰ ਡਾਈਲਾਗ ਵੇਖੋ। ਤੁਸੀਂ ਤਦ ਲਿਖਣ ਸ਼ੁਰੂ ਕਰ ਸਕਦੇ ਹੋ। ਜਦੋਂ ਲਿਖ ਰਹੇ ਹੋਵੋ ਤਾਂ ਕੇਰਨਰ ਤੁਹਾਡੇ ਟਾਈਪ
ਕੀਤੇ ਮੁਤਾਬਕ ਮਿਲਦੀਆਂ ਚੋਣਾਂ ਵੇਖਾਉਦਾ ਹੈ:

<div class="text-center">
<a href="/announcements/4/4.0/krunner-desktop.png">
<img src="/announcements/4/4.0/krunner-desktop_thumb.png" class="img-fluid">
</a> <br/>
<em>ਕੇ-ਰਨਰ ਨਾਲ ਐਪਲੀਕੇਸ਼ਨਾਂ ਸ਼ੁਰੂ ਕਰਨੀਆਂ</em>
</div>
<br/>

<ul>
  <li>
    ਇੱਕ ਐਪਲੀਕੇਸ਼ਨ ਦਾ ਨਾਂ ਲਿਖੋ, ਕੇ-ਰਨਰ ਤੁਹਾਡੀ ਲੋੜ ਦੇ ਮੁਤਾਬਕ ਐਪਲੀਕੇਸਨ ਖੋਜੇਗਾ।
ਟੈਕਸਟ ਇੰਪੁੱਟ ਲਾਈਨ ਦੇ ਹੇਠਾਂ ਦਿੱਤੇ ਲਿਸਟ ਤੋਂ ਐਪਲੀਕੇਸ਼ਨ ਚੁਣੋ, ਸਭ ਤੋਂ ਉਤਲੀ ਐਪਲੀਕੇਸ਼ਨ, ਜਿਸ ਨੂੰ
"ਡਿਫਾਲਟ" ਬਣਾਇਆ ਗਿਆ ਹੋਵੇਗਾ, ਚਲਾਉਣ ਲਈ ਐਂਟਰ ਦੱਬੋ। ਉਸ ਸਮੇਂ ਐਕਟਿਵ ਪਰੋਸੈਸ ਤੁਰੰਤ ਵੇਖਣ
ਵਾਸਤੇ, "ਸਿਸਟਮ ਐਕਟਵਿਟੀ ਵੇਖੋ" ਬਟਨ ਨੂੰ ਦੱਬੋ ਜਾਂ ALT+S ਦੱਬੋ। ਗਰਾਫਿਕਲ ਝਲਕ ਲਈ
ਕੇ-ਸਿਸਟਮ-ਗਾਰਡ ਚਲਾਓ"
  </li>
  <li>
    ਕੇ-ਰਨਰ ਇੱਕ ਛੋਟਾ ਜੇਬੀ ਕੈਲਕੂਲੇਟਰ ਵੀ ਹੈ।  "=1337*2" ਦੇ ਰੂਪ ਵਿੱਚ ਇੱਕ ਛੋਟੀ ਸਮੀਕਰਨ ਦਿਓ ਅਤੇ ਕੇ-ਰਨਰ
 ਨਤੀਜੇ ਤੁਰੰਤੇ ਵੇਖਾਏਗਾ।

<div class="text-center">
<a href="/announcements/4/4.0/krunner-calculator.png">
<img src="/announcements/4/4.0/krunner-calculator.png" class="img-fluid">
</a> <br/>
<em>ਕੇ-ਰਨਰ ਇੱਕ ਕੈਲਕੂਲੇਟਰ ਵਾਂਗ</em>
</div>
<br/>

  </li>
  <li>
    ਆਪਣੇ ਬੁੱਕਮਾਰਕ ਇੱਕ ਵੈੱਬ ਬਰਾਊਜਰ ਵਿੱਚ ਖੋਲ੍ਹੋ। ਬੁੱਕਮਾਰਕਾਂ ਨੂੰ ਸ਼ਾਰਟਕੱਟ ਰਾਹੀਂ ਇਸਤੇਮਾਲ
ਕੀਤਾ ਜਾ ਸਕਦਾ ਹੈ। "kde4 visul guide" ਨੂੰ ਗੂਗਲ ਵਿੱਚ ਲੱਭਣ ਵਾਸਤੇ "gg:kde4 visual guide"
ਲਿਖੋ।  KDE ਬਾਰੇ ਵਿਕਿਪੀਡਿਆ ਵਿੱਚ ਲੱਭਣ ਵਾਸਤੇ "wp:kde" ਲਿਖੋ। ਤੁਸੀਂ ਵੈੱਬ ਦੀ ਸੌਖੀ ਵਰਤੋਂ ਲਈ ਪਹਿਲਾਂ
ਸੰਰਚਿਤ ਕੀਤੇ ਸ਼ਾਰਟਕੱਟ ਵੀ ਲੱਭ ਸਕੋਗੇ। ਕੇਰਨਰ ਤੋਂ ਵਰਤੋਂਯੋਗ ਵੈੱਬ-ਸ਼ਾਰਟਕੱਟਾਂ ਦੀ ਲੰਮ ਲਿਸਟ ਨੂੰ ਕੋਨਕਿਉਰੋਰ ਦੀ ਸੈਟਿੰਗ
ਵਿੱਚੋਂ ਵੇਖਿਆ ਜਾ ਸਕਦਾ ਹੈ (ਬੇਸ਼ੱਕ ਇਸ ਨੂੰ ਕੋਨਕਿਉਰੋਰ, KDE ਦੇ ਵੈੱਬ ਬਰਾਊਜ਼ਰ, ਰਾਹੀਂ ਵੀ ਵਰਤਿਆ ਜਾ ਸਕਦਾ ਹੈ)।
ਕੋਨਕਿਊਰੋਰ ਖੋਲ੍ਹੋ, ਮੇਨੂ ਤੋਂ "ਸੈਟਿੰਗ" ਚੁਣੋ। "ਕੋਨਕਿਊਰੋਰ ਸੰਰਚਨਾ" ਸੈਟਿੰਗ ਡਾਈਲਾਗ ਖੋਲ੍ਹੇਗਾ, ਵੈੱਬ-ਸ਼ਾਰਟਕੱਟਾਂ ਨੂੰ ਇਸ
ਡਾਈਲਾਗ ਦੇ ਖੱਬੇ ਪਾਸੇ ਬਾਰ ਵਿੱਚ ਲੱਭਿਆ ਜਾ ਸਕਦਾ ਹੈ।
  </li>
</ul>
</p>

<h3>ਕਿੱਕਆਫ਼</h3>
<p>
ਕਿੱਕਆਫ਼ ਨਵਾਂ KDE ਐਪਲੀਕੇਸ਼ਨ ਲਾਂਚਰ ਜਾਂ "ਸਟਾਰਟ ਮੇਨੂ" ਹੈ।". 
ਆਪਣੀ ਸਕਰੀਨ ਦੇ ਸੱਜੇ ਪਾਸੇ ਤਲ ਉੱਤੇ ਮੌਜੂਦਾ KDE  ਲੋਗੋ ਉੱਤੇ ਕਲਿੱਕ ਕਰੋ। ਕਿੱਕਆਫ ਮੇਨੂ ਖੁੱਲ੍ਹੇਗਾ, ਜੋ ਕਿ ਇੰਸਟਾਲ ਹੋਈਆਂ
ਐਪਲੀਕੇਸ਼ਨਾਂ, ਤਾਜ਼ਾ ਵਰਤੀਆਂ ਫਾਇਲਾਂ ਅਤੇ ਐਪਲੀਕੇਸ਼ਨਾਂ ਲਈ ਸੌਖੀ ਪਹੁੰਚ ਦਿੰਦਾ ਹੈ। ਛੱਡੋ ਟੈਬ ਰਾਹੀਂ ਤੁਸੀਂ
ਆਪਣੇ ਕੰਪਿਊਟਰ ਨੂੰ ਲਾਗ-ਆਉਟ, ਬੰਦ ਅਤੇ ਸਸਪੈਂਡ ਵੀ ਕਰ ਸਕਦੇ ਹੋ।

<div class="text-center">
<a href="/announcements/4/4.0/kickoff-favorites.png">
<img src="/announcements/4/4.0/kickoff-favorites_thumb.png" class="img-fluid">
</a> <br/>
<em>ਕਿੱਕਆਫ਼ ਨਾਲ ਆਪਣੀ ਪਸੰਦ ਦੀ ਐਪਲੀਕੇਸ਼ਨ ਸੌਖੀ ਤਰ੍ਹਾਂ ਚਲਾਓ</em>
</div>
<br/>

<ul>
  <li>
    ਤੁਹਾਡੀ <strong>ਪਸੰਦ</strong> This is a number of applications or 
	documents you need to access very often, therefore it is shown first. Yo can 
	easily add or remove items from your Favorites menu by right clicking on the, 
	and then either choosing "Add to Favorites" or "Remove from Favorites".
  </li>
  <li>
    <strong>ਐਪਲੀਕੇਸ਼ਨ</strong> ਟੈਬ tab shows a categorized list of 
	applications. Move your mouse over the buttons at KickOff's bottom, within the 
	blink of an eye, KickOff switches to the Applications view. Browse through th 
	menu to find out what tools are installed on your system. You can always get ne 
	level to the left by clicking the large button on the side. Hint: You can just 
	ove the mouse cursor the the left until you hit the screen edge. This makes 
	itmuch easier to hit.
  </li>
  <li>
	<strong>ਕੰਪਿਊਟਰ</strong> ਟੈਬ ਤੁਹਾਨੂੰ ਤੁਹਾਡੇ ਸਟੋਰੇਜ਼ ਮੀਡਿਆ, ਜਿਵੇਂ ਕਿ ਹਾਰਡ ਡਰਾਇਵਾਂ, ਜਾਂ ਹਟਾਉਣ ਯੋਗ
  ਮੀਡਿਆ ਜਿਵੇਂ ਕਿ USB ਸਟਿੱਕਾਂ ਲਈ ਪਹੁੰਚ (ਅਸੈੱਸ) ਦਿੰਦੀ ਹੈ। ਇਹ ਤੁਹਾਡੇ ਪਸੰਦੀਦਾ ਥਾਵਾਂ ਬਾਰੇ ਵੀ ਜਾਣਦਾ ਹੈ ਅਤੇ ਸਿਸਟਮ
  ਸੈਟਿੰਗ ਖੋਲ੍ਹਣ ਲਈ ਵੀ ਇੱਕ ਬਟਨ ਹੈ।
  </li>
  <li>
	<strong>ਤਾਜ਼ਾ ਵਰਤੇ</strong> ਟੈਬ ਐਪਲੀਕੇਸ਼ਨਾਂ ਅਤੇ ਡੌਕੂਮੈਂਟ ਵੇਖਾਉਦੀ ਹੈ, ਜੋ ਕਿ ਪਹਿਲਾਂ ਵਰਤੇ
        ਹਨ। ਇੱਥੋਂ ਤੁਹਾਡੇ ਵਲੋਂ ਪਹਿਲਾਂ ਵਰਤੇ ਡੌਕੂਮੈਂਟ ਅਤੇ ਐਪਲੀਕੇਸ਼ਾਂ ਨੂੰ ਲੱਭਣਾ ਸੌਖਾ ਹੋ ਜਾਂਦਾ ਹੈ।
  </li>
  <li>
	<strong>ਛੱਡੋ</strong> ਟੈਬ ਤੁਹਾਨੂੰ ਤੁਹਾਡਾ ਕੰਪਿਊਤਰ ਬੰਦ ਕਰਨ ਅਤੇ ਲਾਗ ਆਉਟ ਕਰਨ ਦੀ ਸਹੂਲਤ
        ਦਿੰਦੀ ਹੈ। ਜੇ ਤੁਸੀਂ ਆਪਣੀ ਮਸ਼ੀਨ ਨੂੰ ਸਸਪੈਂਡ ਜਾਂ ਹਾਈਬਰਨੇਟ ਕਰਨਾ ਚਾਹੁੰਦੇ ਹੋ ਤਾਂ ਬੰਦ ਕਰੋ ਦੱਬੋ ਅਤੇ ਲਾਗ-ਆਉਟ
ਮੇਨੂ ਵਿੱਚੋਂ "ਬੰਦ ਕਰੋ" ਬਟਨ ਰੱਖੋ। ਤਦ ਤੁਹਾਨੂੰ ਸਸਪੈਂਡ ਅਤੇ ਹਾਈਬਰਨੇਟ ਚੋਣਾਂ ਮਿਲਣਗੀਆਂ, ਜੇ ਸਿਸਟਮ ਇਨ੍ਹਾਂ ਲਈ
ਸਹਿਯੋਗੀ ਹੋਇਆ।
  </li>
</ul>
</p>

<h3>ਪੈਨਲ</h3>

<div class="text-center">
<a href="/announcements/4/4.0/panel.png">
<img src="/announcements/4/4.0/panel_thumb.png" class="img-fluid">
</a> <br/>
<em>ਪੈਨਲ ਵਿੱਚ ਕਿੱਕ-ਆਫ਼, ਟਾਸਕਬਾਰ, ਪੇਜ਼ਰ, ਘੜੀ, ਸਿਸ-ਟਰੇ ਅਤੇ ਹੋਰ ਐਪਲਿਟ</em>
</div>
<br/>

<p>
If you are looking for a specific tool, click on the K Button in the lower left
corner to open Kickoff type right away. 
Typing filters the list of applications and attempts to find those that mach 
your criteria. So entering "cd" find applications to play an audio CD, burn  CD 
or DVD, or encode an audio CD. Likewise, typing "viewer" gives you a list of 
aplications that can be used to open and view all sorts of fileformats.
</p>
<p>
ਪਲਾਜ਼ਮਾ <strong>ਪੈਨਲ</strong> ਮੇਨੂ, ਸਿਸਟਮ-ਟਰੇ ਅਤੇ ਟਾਸਕਾਂ ਦੀ ਲਿਸਟ ਰੱਖਦਾ ਹੈ।
ਟਾਸਕਬਾਰ, ਪੈਨਲ ਵਿੱਚ ਮੌਜੂਦ ਹੈ, ਵਿੰਡੋ ਦਾ ਲਾਈਵ ਥੰਮਨੇਲ ਵੇਖਾਉਦੀ ਹੈ, ਜੋ ਕਿ ਉਸ ਵੇਲੇ ਲੁਕਵੀ ਟਾਸਕਬਾਰ
ਥੰਮਨੇਲ ਹੈ।
<br />
</p>
<p>
ਪੈਨਲ ਉੱਤੇ, ਤੁਸੀਂ ਪੇਜ਼ਰ ਐਪਲਿਟ ਲੱਭ ਸਕਦੇ ਹੋ। ਇਸ ਐਪਲਿਟ ਨੂੰ ਆਪਣੇ ਵਰਕਸਪੇਸ ਵਿੱਚ ਨੇਵੀਗੇਟ ਕਰਨ ਲਈ
ਵਰਤੋਂ, ਜਿਸ ਨੂੰ "ਵੁਰਚੁਅਲ ਡੈਸਕਟਾਪ" ਕਹਿੰਦੇ ਹਨ। ਪੇਜ਼ਰ ਉੱਤੇ ਸੱਜਾ ਕਲਿੱਕ ਕਰਕੇ, ਤੁਸੀਂ
ਉਨ੍ਹਾਂ ਵਰਕਸਪੇਸ ਦੀ ਗਿਣਤੀ ਅਤੇ ਸਥਿਤੀ ਦੀ ਸੰਰਚਨਾ ਕਰ ਸਕਦੇ ਹੋ। ਜੇ ਤੁਹਾਡੇ ਕੋਲ ਡੈਸਕਟਾਪ ਪਰਭਾਵ
ਯੋਗ ਹਨ ਤਾਂ, ਆਪਣੇ ਵਰਕਸਪੇਸਾਂ ਦੀ ਪੂਰੀ ਸਕਰੀਨ ਝਲਕ ਵੇਖਣ ਲਈ  CTRL+F8 ਦੱਬੋ।
</p>

<p>
ਇਸ਼ਾਰਾ: To open KickOff, you can 'blindly' move the mouse cursor to the 
bottom-left corner and hit the left mouse button. As KickOff's button also covers 
the screen edges, it is extremely easy to access.
</p>
<p>
ਜੇ ਤੁਸੀਂ ਪਲਾਜ਼ਮਾ ਬਾਰੇ ਹੋਰ ਜਾਣਨਾ ਚਾਹੁੰਦੇ ਹੋ ਤਾਂ 
<a href="http://techbase.kde.org/Projects/Plasma/FAQ">ਪਲਾਜ਼ਮਾ ਸਵਾਲ ਜਵਾਬ</a> ਉੱਤੇ ਵੇਖੋ।
</p>

<h2>KWin - KDE ਵਿੰਡੋ ਮੈਨੇਜਰ</h2>
<p>
ਕੇ-ਵਿਨ, KDE ਦਾ ਇਸਤੇਮਾਲ ਕੀਤਾ ਸਥਿਰ ਵਿੰਡੋ ਮੈਨੇਜਰ ਹੈ, ਨੂੰ ਸੁਧਾਰਿਆ ਗਿਆ ਹੈ ਤਾਂ ਕਿ ਮਾਰਡਨ
ਗਰਾਫਿਕਸ ਹਾਰਡਵੇਅਰ ਦੀ ਸਮਰੱਥਾ ਨਾਲ ਸੌਖਾ ਤਰ੍ਹਾਂ ਸੰਚਾਰ ਕਰ ਸਕੇ। ਇੱਕ ਵਿੰਡੋਜ਼ ਦੇ ਉੱਤੇ ਬਾਰ ਨਾਲ
ਸੱਜਾ ਕਲਿੱਕ ਕਰਨ ਨਾਲ ਤੁਸੀਂ ਵਿੰਡੋ ਮੈਨੇਜਰ ਸੈਟਿੰਗ ਲਈ "ਵਿੰਡੋ ਰਵੱਈਆ ਸੰਰਚਨਾ" ਲਵੋ।
ਇੱਥੇ ਤੁਸੀਂ ਕੇ-ਵਿਨ ਦੇ ਤਕਨੀਕੀ ਫੀਚਰ ਸਿੱਖ ਅਤੇ ਸੰਰਚਿਤ ਕਰ ਸਕਦੇ ਹੋ। ਐਕਸ਼ਨ
ਸੰਰਚਨਾ ਸ਼ੀਟ ਵਿੱਚ, ਤੁਸੀਂ ਵਿੰਡੋ ਸਜਾਵਟ ਉੱਤੇ ਡਬਲ-ਕਲਿੱਕ ਕਰਕੇ ਵਿੰਡੋ ਨੂੰ ਵੱਧੋ-ਵੱਧ
ਕਰਨ ਲਈ ਚੁਣ ਸਕਦੇ ਹੋ।
</p>
<p>
Hint: KWin lets you easily move windows by pressing the ALT button. You can then 
just click on a window's content. While you hold the left mouse button pressed, 
windows will move. Hold ALT pressed and click on the right mouse button to easily
resize a window. No need to aim precisely, just keep ALT pressed and manipulate your 
windows easily!
</p>

<h3>ਡੈਸਕਟਾਪ ਪਰਭਾਵ</h3>
<p>
ਡੈਸਕਟਾਪ ਪਰਭਾਵ ਯੋਗ ਕਰਨ ਨਾਲ  KWin ਤੁਹਾਨੂੰ ਤੁਹਾਡੇ ਵਿੰਡੋਜ਼ ਨਾਲ ਸੰਚਾਰ ਕਰਨ ਦਾ ਨਵੇਂ ਢੰਗ
ਦਿੰਦਾ ਹੈ। ਵਿੰਡੋ ਸਜਾਵਟ ਉੱਤੇ ਸੱਜਾ-ਕਲਿੱਕ ਕਰੋ, "ਵਿੰਡੋ ਰਵੱਈਆ ਸੰਰਚਾਨ" ਚੁਣੋ ਅਤੇ "ਡੈਸਕਟਾਪ ਪਰਭਾਵ"
ਸ਼ੀਟ ਉੱਤੇ ਜਾਓ। "ਡੈਸਕਟਾਪ ਪਰਭਾਵ ਯੋਗ ਕਰੋ" ਚੁਣੋ ਅਤੇ "ਠੀਕ ਹੈ", "ਲਾਗੂ ਕਰੋ" ਕਲਿੱਕ ਕਰਕੇ ਨਵੀਂ ਸੈਟਿੰਗ ਮਨਜ਼ੂਰ ਕਰੋ
ਜਾਂ ਕੇਵਲ ਐਂਟਰ ਸਵਿੱਚ ਦੱਬੋ। ਹੁਣ ਕੇ-ਵਿਨ ਦੀ ਤਕਨੀਕੀ ਵਿੰਡੋ ਹੈਂਡਲਿੰਗ ਯੋਗ ਹੋ ਜਾਵੇਗੀ।
</p>

<div class="text-center">
<a href="/announcements/4/4.0/kwin-presentwindows.png">
<img src="/announcements/4/4.0/kwin-presentwindows_thumb.png" class="img-fluid">
</a> <br/>
<em>ਪ੍ਰੀ-ਸੈੱਟ ਵਿੰਡੋ ਪਰਭਾਵ ਦੀ ਵਰਤੋਂ ਕਰਕੇ ਐਪਲੀਕੇਸ਼ਨ ਬਦਲੋ</em>
</div>
<br/>

<p>
    <strong>ਮੌਜੂਦਾ ਵਿੰਡੋ</strong> ਤੁਹਾਨੂੰ ਆਪਣੇ ਖੁੱਲ੍ਹੇ ਵਿੰਡੋ ਦੀ ਝਲਕ ਦਿੰਦਾ ਹੈ।
ਮਾਊਸ ਕਰਸਰ ਨੂੰ ਸਕਰੀਨ ਦੇ ਉੱਤੇ-ਸੱਜੇ ਕੋਨੇ ਵਿੱਚ ਧੱਕੋ ਜਾਂ ਆਪਣੀਆਂ ਵਿੰਡੋ ਨਾਲ ਨਾਲ ਰੱਖੋ। ਤੁਸੀਂ
ਇੱਕ ਵਿੰਡੋ ਉੱਤੇ ਕਲਿੱਕ ਕਰਕੇ ਇਸ ਨੂੰ ਫਰੰਟ-ਆਫ਼ ਲਈ ਫੋਕਸ ਕਰ ਸਕਦੇ ਹੋ।  ਹਰੇਕ ਵਿੰਡੋ ਉੱਤੇ ਲੇਬਲ
ਤੋਂ ਸ਼ਬਦ ਲਿਖਣਾ ਸ਼ੁਰੂ ਕਰੋ ਅਤੇ ਰਲਦੀ ਐਪਲੀਕੇਸ਼ਨ ਨੂੰ ਫਿਲਟਰ ਕੀਤਾ ਜਾਵੇਗਾ। ਚੁਣੀ ਐਪਲੀਕੇਸ਼ਨ ਲਈ
ਸਵਿੱਚ ਕਰਨ ਵਾਸਤੇ ਐਂਟਰ ਦੱਬੋ। ਤੁਸੀਂ ਮੌਜੂਦਾ ਵਿੰਡੋਜ਼ ਪਰਭਾਵ ਨੂੰ ਕੀਬੋਰਡ ਦੇ ਰਾਹੀਂ CONTROL+F9 ਨਾਲ ਜਾਂ
CONTROL+F10 ਰਾਹੀਂ ਵਿੰਡੋਜ਼ ਨੂੰ ਸਭ ਵੁਰਚੁਅਲ ਡੈਸਕਟਾਪ ਉੱਤੇ ਵੇਖਾਉਣ ਲਈ ਐਕਟੀਵੇਟ ਕਰ ਸਕਦੇ ਹੋ।
</p>
<p>
Hint: With the Present Windows effect enabled, just start typing a word from the title
of the application you want to choose and the grid of applications gets filtered. Just hit
enter once you've picked the application you want and it will be zoomed in.
</p>
<p>
<strong>ਡੈਸਕਟਾਪ ਗਰਿੱਡ</strong> ਤੁਹਾਡੇ ਡੈਸਕਟਾਪ ਨੂੰ ਜ਼ੂਮ ਆਉਟ ਕਰਦਾ ਹੈ, ਜਿਸ ਨਾਲ ਤੁਹਾਡੇ ਵੁਰਚੁਅਲ ਡੈਸਕਟਾਪਾਂ
ਜਾਂ ਵਰਕਸਪੇਸਾਂ ਨੂੰ ਇੱਕ ਗਰਿੱਡ ਦੇ ਰੂਪ ਵਿੱਚ ਵੇਖਾਇਆ ਜਾ ਸਕੇ। ਵਿੰਡੋਜ਼ ਨੂੰ ਵੁਰਚੁਅਲ ਡੈਸਕਟਾਪ ਵਿੱਚ ਚੁੱਕੋ ਅਤੇ ਸੁੱਟੋ ਤਾਂ ਕਿ ਉਨ੍ਹਾਂ ਨੂੰ
ਆਪਣੇ ਵਰਕਸਪੇਸਾਂ ਵਿੱਚ ਭੇਜੋ। ਵੁਰਚੁਅਲ ਡੈਸਕਟਾਪਾਂ ਵਿੱਚੋਂ ਇੱਕ ਉੱਤੇ ਕਲਿੱਕ ਕਰੋ ਤਾਂ ਕਿ ਉਸ ਵਰਕਸਪੇਸ ਵਿੱਚ ਜ਼ੂਮ ਇਨ ਕੀਤਾ ਜਾ ਸਕੇ।
</p>
<p>
ਇਸ਼ਾਰਾ: ਤੁਸੀਂ ਇੱਕ ਵੁਰਚੁਅਲ ਡੈਸਕਟਾਪ ਦੇ ਨੰਬਰ ਨੂੰ ਦੱਬ ਕੇ ਉਸ ਵਰਕਸਪੇਸ ਵਿੱਚ ਜਾ ਸਕਦੇ ਹੋ।
<em>CONTROL+F8</em> ਨਾਲ ਡੈਸਕਟਾਪ ਗਰਿੱਡ ਯੋਗ ਕਰੋ। <br />
ਇੱਕ ਵਿੰਡੋ ਉੱਤੇ ਮਾਊਂਸ ਵੀਲ ਨਾਲ ਕਲਿੱਕ ਕਰਕੇ ਸਭ ਡੈਸਕਟਾਪਾਂ ਉੱਤੇ ਉਹ ਵਿੰਡੋ ਵੇਖੋ।
</p>

<div class="text-center">
<a href="/announcements/4/4.0/desktopgrid.png">
<img src="/announcements/4/4.0/desktopgrid_thumb.png" class="img-fluid">
</a> <br/>
<em>ਡੈਸਕਟਾਪ ਪਰਭਾਵ ਪੇਜ਼ਰ ਵਾਂਗ ਹੀ ਸਹੂਲਤਾਂ ਹੀ ਦਿੰਦਾ ਹੈ।</em>
</div>
<br/>

<p> 
The Panel holds an applet that provides similar functionality and is also 
available when Desktop Effects have been disabled. Right click on the "Pager"in 
the panel to configure the number and arrangement of your virtual desktops. Drag 
the pager applet from the appletbrowser to the panel or to the desktop. The 
screenshots illustrates this nicely.
</p>

<div class="text-center">
<a href="/announcements/4/4.0/pager.png">
<img src="/announcements/4/4.0/pager.png" class="img-fluid">
</a> <br/>
<em>The Pager allows you to switch between your workspaces</em>
</div>
<br/>

<p>
    <strong>ਟਾਸਕਬਾਰ ਥੰਮਨੇਲ</strong> ਪਰਭਾਵ ਵਿੰਡੋਜ਼ ਦੀ ਝਲਕ ਹੈ, ਜਦੋਂ ਕਿ ਤੁਹਾਡਾ ਮਾਊਂਸ
ਟਾਸਕਬਾਰ ਵਿੱਚ ਉਹਨਾਂ ਦੀ ਐਂਟਰੀ ਉੱਤੇ ਜਾਂਦਾ ਹੈ। ਇਸ ਨਾਲ ਲੁਕਵੀਆਂ ਵਿੰਡੋ ਅਤੇ ਐਪਲੀਕੇਸ਼ਨ ਐਕਟਵਿਟੀ
ਵਿੱਚ ਸੌਖੀ ਨਿਗਰਾਨੀ ਲਈ ਸਹਾਇਕ ਹੈ।  ਟਾਸਕਬਾਰ ਥੰਮਨੇਲ ਇਸ਼ਾਰੇ ਵੀ ਵੇਖਾਉਦੇ ਹਨ, ਜਿਸ ਨਾਲ ਤੁਹਾਡੇ
ਵਲੋਂ ਬਦਲਣ ਲਈ ਐਪਲੀਕੇਸ਼ਨਾਂ ਲੱਭਣੀਆਂ ਸੌਖੀਆਂ ਹੋ ਜਾਂਦੀਆਂ ਹਨ।

<div class="text-center">
<a href="/announcements/4/4.0/kwin-taskbarthumbnails.png">
<img src="/announcements/4/4.0/kwin-taskbarthumbnails_thumb.png" class="img-fluid">
</a> <br/>
<em>Task-switching enhanced with live thumbnails</em>
</div>
<br/>

</p>
<p>
ਕੇ-ਵਿਨ ਦੇ ਡੈਸਕਟਾਪ ਪਰਭਾਵ ਜਾਂ ਹੋਰ ਤਕਨੀਕੀ ਕੇ-ਵਿਨ ਦੇ ਕੰਪੋਜ਼ੀਸ਼ਨ ਫੀਚਰ ਕਈ ਐਪਲੀਕੇਸ਼ਨਾਂ ਵਿੱਚ
 <strong>ਟਰਾਂਸਪਰੇਸੀ ਪਰਭਾਵ</strong> ਯੋਗ ਕਰਦਾ ਹੈ। ਇੱਕ ਉਦਾਹਰਨ ਵਜੋਂ, 
ਕਨਸੋਲ, KDE ਦਾ ਟਰਮੀਨਲ ਈਮੂਲੇਟਰ, ਇੱਕ ਟਰਾਂਸਪਰੇਂਟ ਬੈਕਗਰਾਊਂਡ ਵਰਤ ਸਕਦੀ ਹੈ, ਇਸਕਰਕੇ ਇਸ 
ਪਿੱਛੇ ਮੌਜੂਦਾ ਐਪਲੀਕੇਸ਼ਨਾਂ ਨੂੰ ਹਾਲੇ ਵੀ ਵੇਖਿਆ ਜਾ ਸਕਦਾ ਹੈ। ਇਹ ਕੰਮ ਦੌਰਾਨ ਹੋਰ ਵੀ ਆਨੰਦ ਲਈ
ਵਧੀਆ ਦਿੱਖ ਪਰਭਾਵ ਵੇਖ ਸਕਦੇ ਹੋ। ਤੁਸੀਂ ਇੱਕ ਵਿੰਡੋ ਦੀ ਧੁੰਦਲਤਾ ਨੂੰ ਵਿੰਡੋ ਸਜਾਵਟ ਉੱਤੇ ਸੱਜਾ
ਕਲਿੱਕ ਕਰਕੇ ਧੁੰਦਲਾਪਨ ਲੈਵਲ ਚੁਣ ਕਰੇ ਬਦਲ ਸਕਦੇ ਹੋ।

<div class="text-center">
<a href="/announcements/4/4.0/kwin-transparency.png">
<img src="/announcements/4/4.0/kwin-transparency_thumb.png" class="img-fluid">
</a> <br/>
<em>Change the opacity of individual windows and applications</em>
</div>
<br/>

</p>
<p>
"ਸਭ ਪਰਭਾਵ" ਸ਼ੀਟ ਵਿੱਚ, ਤੁਹਾਨੂੰ ਪਰਭਾਵਾਂ ਉੱਤੇ ਪੂਰਾ ਵੇਰਵੇ ਸਮੇਤ ਕੰਟਰੋਲ ਦਿੱਤਾ ਜਾ ਸਕਦਾ ਹੈ।
ਅਕਸਰ, ਤੁਸੀਂ ਉਹ ਪਰਭਾਵਾਂ ਨੂੰ ਸੰਰਚਨਾਯੋਗ ਲੱਭ ਸਕਦੇ ਹੋ ਤਾਂ ਕਿ ਉਹ ਤੁਹਾਡੇ ਨਿੱਜੀ ਟੇਸਟ ਲਈ
ਕੰਮ ਕਰਨ ਵਾਸਤੇ ਫਿੱਟ ਆ ਸਕਣ। ਕੇ-ਵਿਨ ਪਰਭਾਵ ਆਟੋਮੈਟਿਕ ਹੀ ਯੋਗ ਕਰਨ ਦੀ ਕੋਸ਼ਿਸ਼ ਕਰਦੀ ਹੈ,
ਜੋ ਕਿ ਤੁਹਾਡੇ ਗਰਾਫਿਕਸ ਕਾਰਡ ਦੀ ਸਮਰੱਥਾ ਮੁਤਾਬਕ ਹੈ, ਇਸਕਰਕੇ ਇਸਕਰਕ ਹੋ ਸਕਦਾ ਹੈ ਕਿ ਤੁਹਾਡੇ
ਸਿਸਟਮ ਉੱਤੇ ਪਰਭਾਵ ਪਹਿਲਾਂ ਹੀ ਯੋਗ ਹੋਣ।<br /><br />
ਜੇ ਤੁਸੀਂ ਕੇ-ਵਿਨ ਬਾਰੇ ਹੋਰ ਜਾਣਨਾ ਚਾਹੁੰਦੇ ਹੋ ਅਤੇ ਖਾਸ ਤੌਰ ਉੱਤੇ ਇਸ ਦੇ ਕੰਪੋਜ਼ੀਸ਼ਨ ਫੀਚਰਾਂ ਬਾਰੇ ਤਾਂ 
<a href="http://websvn.kde.org/*checkout*/trunk/KDE/kdebase/workspace/kwin/NOTES_4_0">KWin ਦੇ ਰੀਲਿਜ਼ ਨੋਟਿਸ</a> or <a href="http://techbase.kde.org/Projects/KWin">TechBase</a> ਵੇਖੋ।
</p>

<h2>Sweet Spots</h2>
<p>
KDE makes extensive use of the 'sweet spots' of the screen -- edges and corners, 
which are easier to aim at make reaching the buttons of the window decorations 
more convenient. Slap the mouse to the top-right corner, click, and the window is being 
closed. The "Close Window" button has some space left of it, so you don't hit it 
accidentally when maximizing a window. The window button ordering can easily 
be changed to comfort your style of working. Right click on the decoration, the 
top frame of the window, choose "Configure Window Behavior".
<br />
Hint: To grab a scrollbar, the easiest way is to move the mouse to the edge of 
the screen, this way aiming and grabbing the scrollbar can be done quickly and 
conveniently.
</p>
<p>
<a href="../applications.pa">ਅੱਗੇ ਪੇਜ਼: KDE 4.0 ਦੇ ਬੇਸਿਕ ਐਪਲੀਕੇਸ਼ਨ</a>
</p>