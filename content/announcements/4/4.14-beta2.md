---
aliases:
- ../announce-4.14-beta2
date: '2014-07-17'
description: KDE Ships Second Beta of Applications and Platform 4.14.
title: KDE Ships Second Beta of Applications and Platform 4.14
---

July 17, 2014. Today KDE released the second beta of the new versions of Applications and Development Platform. With API, dependency and feature freezes in place, the KDE team's focus is now on fixing bugs and further polishing.

With the large number of changes, the 4.14 releases need a thorough testing in order to maintain and improve the quality and user experience. Actual users are critical to maintaining high KDE quality, because developers simply cannot test every possible configuration. We're counting on you to help find bugs early so they can be squashed before the final release. Please consider joining the 4.14 team by installing the beta <a href='https://bugs.kde.org/'>and reporting any bugs</a>.

### KDE Software Compilation 4.14 Beta2

The KDE Software Compilation, including all its libraries and its applications, is available for free under Open Source licenses. KDE's software can be obtained in source and various binary formats from <a href='http://download.kde.org/unstable/4.13.90/'>download.kde.org</a> or with any of the <a href='/distributions'>major GNU/Linux and UNIX systems</a> shipping today.

#### Installing 4.14 Beta2 Binary Packages

<em>Packages</em>.
Some Linux/UNIX OS vendors have kindly provided binary packages of 4.14 Beta2 (internally 4.13.90) for some versions of their distribution, and in other cases community volunteers have done so. Additional binary packages, as well as updates to the packages now available, may become available over the coming weeks.

<em>Package Locations</em>.
For a current list of available binary packages of which the KDE Project has been informed, please visit the <a href='http://community.kde.org/KDE_SC/Binary_Packages#KDE_SC_4.14_Beta_2_.284.13.90.29'>Community Wiki</a>.

#### Compiling 4.14 Beta2

The complete source code for 4.14 Beta2 may be <a href='http://download.kde.org/unstable/4.13.90/src/'>freely downloaded</a>. Instructions on compiling and installing 4.13.90 are available from the <a href="/info/4/4.13.90">4.13.90 Info Page</a>.

#### Supporting KDE

KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Supporting KDE page</a> for further information or become a KDE e.V. supporting member through our new <a href='https://kde.org/community/donations/'>Join the Game</a> initiative.


