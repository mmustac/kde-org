---
title: KDE Plasma Workspaces Improve User Experience
hidden: true
---

<h2>La Comunidad de KDE publica Plasma para escritorio y netbooks 4.5.0</h2>

<div class="text-center">
	<a href="/announcements/4/4.5.0/plasma-netbook-sal.png">
	<img src="/announcements/4/4.5.0/thumbs/plasma-netbook-sal.png" class="img-fluid" >
	</a> <br/>
	<em>La interfaz de b&uacute;squeda y lanzamiento de Plasma para netbooks muestra aplicaciones, contactos y b&uacute;squeda en el escritorio</em>
</div>
<br/>

<p>
KDE, una comunidad de Software Libre internacional, se complace en anunciar la disponibilidad inmediata de los espacios de trabajo de Plasma para escritorio y netbooks en su versi&oacute;n 4.5. Los espacios de trabajo de Plasma se han refinado e innovado en su orientaci&oacute;n hacia flujos de trabajo sem&aacute;nticos y dirigidos por tareas. Adem&aacute;s de los miles de errores corregidos para hacer m&aacute;s completa, usable y atractiva la experiencia de usuario, en esta versi&oacute;n se han a&ntilde;adido algunos cambios interesantes:
</p>
<ul>
    <li>El <b>&aacute;rea de notificaciones</b> del panel se ha &quot;limpiado&quot; a nivel visual. Los iconos monocrom&aacute;ticos otorgan una mayor claridad visual, y una interacciones con el usuario m&aacute;s consistentes mejoran la usabilidad. El seguimiento de las descargas y otros indicadores de tareas de larga duraci&oacute;n, centralizadas en el &aacute;rea de notificaciones, ahora son controlados por un indicador visual de progreso en el propio componente gr&aacute;fico. De manera similar, la visualizaci&oacute;n, clasificaci&oacute;n y encolado de las notificaciones de aplicaciones se han redise&ntilde;ado. Las notificaciones de componentes Plasma compartidos con sistemas remotos ahora tambi&eacute;n se muestran como eventos locales.
    </li>

<div class="text-center">
	<a href="/announcements/4/4.5.0/plasma-notification.png">
	<img src="/announcements/4/4.5.0/thumbs/plasma-notification.png" class="img-fluid" >
	</a> <br/>
	<em>Se han depurado la gesti&oacute;n, visualizaci&oacute;n y apariencia de las notificaciones del Escritorio Plasma</em>
</div>
<br/>

<br />
    <li><b>KWin</b>, el gestor de ventanas del espacio de trabajo de Plasma, permite disponer las ventanas en la pantalla sin que se superpongan mediante algoritmos de mosaico. Siguiendo un conjunto definido de reglas, KWin se hace cargo de ubicarlas por el usuario. Tambi&eacute;n es posible <b>moverlas f&aacute;cilmente arranstrando un &aacute;rea vac&iacute;a</b> del interior de la ventana. Esta nueva funci&oacute;n mejora la ergonom&iacute;a al permitir usar un espacio vac&iacute;o como manejador para el arrastre. Esta caracter&iacute;stica solo funciona en aplicaciones basadas en Qt, pues requiere cierta integraci&oacute;n con ello para funcionar correctamente. Ahora puede a&ntilde;adir o eliminar escritorios directamente desde el paginador y el efecto de cuadr&iacute;cula del escritorio. Adem&aacute;s de estos cambios, muchos sistemas podr&aacute;n disfrutar de un mejor rendimiento en los efectos de ventanas.
    </li>

<div class="text-center">
	<a href="/announcements/4/4.5.0/plasma-desktopgrid.png">
	<img src="/announcements/4/4.5.0/thumbs/plasma-desktopgrid.png" class="img-fluid" >
	</a> <br/>
	<em>Ahora puede a&ntilde;adir y eliminar escritorios virtuales desde la cuadr&iacute;cula del escritorio</em>
</div>
    <li><b>Actividades</b> del espacio de trabajo de Plasma: la Interfaz de Usuario para Zoom (ZUI) de anteriores versiones del Escritorio Plasma se ha reemplazado por un "Gestor de actividades", similar al di&aacute;logo para a&ntilde;adir componentes presentado en la versi&oacute;n 4.4. El nuevo Gestor de actividades permitir a&ntilde;adir, eliminar, guardar y restaurar actividades, as&iacute; como cambiar entre ellas. Tambi&eacute;n se ha expandido el concepto visual de una Actividad. Ahora son m&aacute;s f&aacute;ciles de gestionar y pueden ayudar a los usuarios en su uso diario del equipo, proporcion&aacute;ndoles una separaci&oacute;n m&aacute;s clara entre distintas tareas. El nuevo Gestor de actividades es la primera pieza visible de la <em>importancia del contexto</em> introducida en Plasma a trav&eacute;s de las caracter&iacute;sticas del <em>Escritorio sem&aacute;ntico</em> de Nepomuk.
    </li>

<div class="text-center">
	<a href="/announcements/4/4.5.0/plasma-netbook-pageone.png">
	<img src="/announcements/4/4.5.0/thumbs/plasma-netbook-pageone.png" class="img-fluid" >
	</a> <br/>
	<em>Page One de Plasma para netbooks est&aacute; dise&ntilde;ado para mantener pesta&ntilde;as de la web social, puede contener cualquier componente Plasma</em>
</div>
<br/>
    <li><b>Plasma Netbook</b>, el espacio de trabajo de KDE para miniport&aacute;tiles y netbooks, se ha mejorado sustancialmente en esta versi&oacute;n. Muchos de estos cambios no son inmediatamente visibles, pero proporcionan una experiencia mucho m&aacute;s agradable, un mejor rendimiento y mejores capacidades de pantalla t&aacute;ctil.
    </li>
    <li>La nueva herramienta "oxygen-settings" permite a los usuarios avanzados configurar a su gusto el estilo por defecto de Oxygen. Finalmente, el motor de temas Aurorae se ha mejorado mucho y se ha reintroducido el efecto borroso, pero puede estar desactivado dependiendo de las capacidades de su controlador gr&aacute;fico.</li>
    <li>En un movimiento de apoyo al Software Libre en todo el mundo, ahora todas las aplicaciones de KDE pueden trabajar con los calendarios tailand&eacute;s, taiwan&eacute;s y japon&eacute;s, y se aceptan m&aacute;s conjuntos de d&iacute;gitos. En el cuadro emergente del calendario se muestran las fiestas locales.</li>
</ul>

<div class="text-center">
	<a href="/announcements/4/4.5.0/plasma-calendar.png">
	<img src="/announcements/4/4.5.0/thumbs/plasma-calendar.png" class="img-fluid" >
	</a> <br/>
	<em>Al hacer clic sobre el reloj se muestra el calendario, que muestra las fiestas de su regi&oacute;n</em>
</div>
<br/>

<p align="justify">
Otras mejoras son los cambios en la visualizaci&oacute;n de resultados de KRunner, el panel de configuraci&oacute;n de tama&ntilde;os de iconos, la configuraci&oacute;n sencilla de paneles mediante plantillas JavaScript y sombras, la mejora de arrastrar-y-soltar en el componente de lanzamiento r&aacute;pido, que ahora permite organizar las aplicaciones favoritas en varias filas y columnas, y las mejoras en Dolphin, Konsole, y muchos m&aacute;s. Tambi&eacute;n se permite iniciar miniaplicaciones Plasma de manera independiente a trav&eacute;s de la aplicaci&oacute;n <em>plasma-windowed</em>. Los errores en el Notificador de dispositivos extra&iacute;bles ahora se muestran de manera m&aacute;s clara.
</p>

<h4>Restructuraci&oacute;n de la configuraci&oacute;n del espacio de trabajo</h4>
<p align="justify">
La configuraci&oacute;n del espacio de trabajo se ha restructurado y ahora se muestra en una categor&iacute;a de las Preferencias del sistema. La nueva secci&oacute;n &laquo;Espacio de trabajo&raquo; permite controlar la apariencia de su espacio de trabajo de Plasma. En ella puede configurar los temas a su gusto, activar o desactivar el Escritorio Sem&aacute;ntico y la B&uacute;squeda de Escritorio, y activar muchas caracter&iacute;sticas que pueden convertir su espacio de trabajo como quiera, ya sea con esquinas pegajosas, ayudas visuales para la gesti&oacute;n de vestanas, el nuevo mosaico de ventanas, y mucho m&aacute;s.
</p>

<div class="text-center">
	<a href="/announcements/4/4.5.0/plasma-systemsettings.png">
	<img src="/announcements/4/4.5.0/thumbs/plasma-systemsettings.png" class="img-fluid" >
	</a> <br/>
	<em>La secci&oacute;n &laquo;Espacio de trabajo&raquo; de las Preferencias del sistema le da control absoluto sobre la apariencia de su espacio de trabajo.</em>
</div>
<br/>

<p align="justify">
Los efectos del espacio de trabajo mejoran la experiencia de usuario en muchos aspectos, incluyendo el uso de previsualizaciones de ventanas en la barra de tareas mediante OpenGL, y un efecto de cuadr&iacute;cula para ampliar o reducir la vista de los escritorios virtuales. Si su hardware o controlador gr&aacute;fico no permiten estos efectos gr&aacute;ficos, Plasma utilizar&aacute; sin ning&uacute;n problema el t&iacute;pico renderizado 2D. Le recomendamos que utilice la &uacute;ltima versi&oacute;n de sus controladores gr&aacute;ficos y de la capa XOrg subyancente para obtener el mejor rendimiento. En 4.5 los administradores y distribuidores pueden tener m&aacute;s control sobre el hardware gr&aacute;fico y los controladores que utiliza a trav&eacute;s de <a href="http://blog.martin-graesslin.com/blog/2010/07/blacklisting-drivers-for-some-kwin-effects/">listas negras por efecto</a>, que le facilitan activar y desactivar ciertas caracter&iacute;sticas seg&uacute;n su disponibilidad y estabilidad en ciertos sistemas. Tambi&eacute;n facilita que muchos usuarios obtengan una experiencia m&aacute;s agradable sin necesidad de configuraci&oacute;n.
</p>

<h3>M&aacute;s capturas de pantalla...</h3>
<p align="justify">

<div class="text-center">
	<a href="/announcements/4/4.5.0/plasma-desktopsettings.png">
	<img src="/announcements/4/4.5.0/thumbs/plasma-desktopsettings.png" class="img-fluid" >
	</a> <br/>
	<em>Se ha mejorado la interfaz de usuario de las preferencias del escritorio</em>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/4/4.5.0/plasma-jobs.png">
	<img src="/announcements/4/4.5.0/thumbs/plasma-jobs.png" class="img-fluid" >
	</a> <br/>
	<em>Los trabajos en ejecuci&oacute;n son visualizados por Plasma...</em>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/4/4.5.0/plasma-filecopyjobs.png">
	<img src="/announcements/4/4.5.0/thumbs/plasma-filecopyjobs.png" class="img-fluid" >
	</a> <br/>
	<em>...los trabajos se agrupan y las notificaciones se recuerdan durante un tiempo.</em>
</div>
<br/>

</p>

<h3>Planes y trabajo en proceso</h3>
<p align="justify">
El equipo de Plasma est&aacute; trabajando en mejorar el rendimiento de la gesti&oacute;n de ventanas. Tambi&eacute;n se planea expandir el concepto de las <b>Actividades</b>, la agrupaci&oacute;n intencional y autom&aacute;tica de ventanas relacionadas y el soporte a decoraciones de escritorio, para que el espacio de trabajo y las aplicaciones sean m&aacute;s conscientes del contexto, incluyendo el uso de geolocalizaci&oacute;n. Para mejorar la ergonom&iacute;a conservando la buena apariencia, y hacer el texto y las interfas m&aacute;s legibles, se utilizan efectos como el difuminado del fondo de ventanas transparentes.<br/>
Los nuevos espacios de trabajo de Plasma para <b>Media Center</b> y tel&eacute;fonos m&oacute;viles tambi&eacute;n est&aacute;n en desarrollo. Durante las recientes conferencias Akademy se hizo la primera llamada telef&oacute;nica mediante la nueva shell de <b>Plasma Mobile</b>.
</p>

<h4>Instalaci&oacute;n de Plasma</h4>
<p align="justify">
El software de KDE, incluyendo todas sus bibliotecas y aplicaciones, est&aacute; disponible bajo licencias de c&oacute;digo abierto. El software de KDE se puede ejecutar sobre varias configuraciones de hardware, sistemas operativos y cualquier gestor de ventanas o entorno de escritorio. Adem&aacute;s de Linux y otros sistemas operativos basados en UNIX, en el sitio web del <a href="http://windows.kde.org">KDE para Windows</a> dispone de versiones para Microsoft Windows de la mayor&iacute;a de aplicaciones, y versiones para Mac OS X en el sitio de <a href="http://mac.kde.org/">KDE on Mac</a>. Tambi&eacute;n puede encontrar compilaciones experimentales de las aplicaciones de KDE para plataformas m&oacute;viles como MeeGo, MS Windows Mobile y Symbian en la web, pero no est&aacute;n soportadas. 
KDE se puede obtener en c&oacute;digo fuente y varios formatos binarios en <a
href="http://download.kde.org/stable/4.5.0/">download.kde.org</a> y tambi&eacute;n se puede obtener en <a href="/download">CD-ROM</a> o con cualquiera de las <a href="/distributions">principales distribuciones de GNU/Linux y sistemas UNIX</a> actuales.
</p>
<p align="justify">
  <a id="packages"><em>Paquetes</em></a>.
  Algunos proveedores de sistemas operativos Linux/UNIX han tenido la amabilidad de proporcionar paquetes binarios de 4.5.0 para algunas versiones de su distribuci&oacute;n, y en otros casos lo han hecho voluntarios de la comunidad. <br />
  Algunos de estos paquetes binarios est&aacute;n disponibles para su descarga gratuita desde <a
href="http://download.kde.org/binarydownload.html?url=/stable/4.5.0/">download.kde.org</a>.
  En las pr&oacute;ximas semanas estar&aacute;n disponibles paquetes binarios adicionales, as&iacute; como actualizaciones de los ya disponibles.
</p>

<p align="justify">
  <a id="package_locations"><em>Ubicaciones de los paquetes</em></a>.
  Visite la <a href="/info/4/4.5.0">p&aacute;gina de informaci&oacute;n de 4.5</a> para obtener una lista actualizada de los paquetes binarios disponibles de los que ha sido informado el Equipo de Publicaci&oacute;n de KDE.
</p>

<h4>
  Compilaci&oacute;n de 4.5.0
</h4>
<p align="justify">
  <a id="source_code"></a>
  El c&oacute;digo fuente completo de 4.5.0 se puede <a
href="http://download.kde.org/stable/4.5.0/src/">descargar libremente</a>. Las instrucciones para compilar e instalar KDE 4.5.0 est&aacute;n disponibles en la <a href="/info/4/4.5.0">p&aacute;gina de informaci&oacute;n de 4.5</a>.
</p>

<h4>
Requisitos del sistema
</h4>
<p align="justify">
Para obtener lo m&aacute;ximo posible de nuestras publicaciones, le recomendamos encarecidamente el uso de la &uacute;ltima versi&oacute;n de Qt, que a d&iacute;a de hoy es la 4.6.3. Es necesaria para asegurarle una experiencia estable, as&iacute; como algunas mejoras del software KDE que se basan en el framework Qt subyacente.<br />
Algunos controladores gr&aacute;ficos pueden, bajo ciertas condiciones, utilizar XRender en lugar de OpenGL para la composici&oacute;n. Si experimenta una notable lentitud en cuanto al rendimiento gr&aacute;fico, puede que le ayude desactivar los efectos de escritorio, dependiendo del controlador gr&aacute;fico y de la configuraci&oacute;n utilizados. Para poder aprovechar todas las posibilidades del software KDE, le recomendamos que utilice los &uacute;ltimos controladores disponibles para su sistema, puesto que pueden mejorar su experiencia notablemente, tanto en cuesti&oacute;n de funciones opcionales como en rendimiento y estabilidad.
</p>




<h2>Tambi&eacute;n publicados hoy:</h2>

<a href="../platform">
<img src="/announcements/4/4.5.0/images/platform.png" class="app-icon float-left m-3" alt="La plataforma de desarrollo de KDE 4.5.0"/>
</a>

<h2>La plataforma de desarrollo de KDE 4.5.0 mejora su rendimiento, estabilidad, introduce una nueva cach&eacute; de alta velocidad y el uso de WebKit</h2>
<p align="justify">
 Hoy, KDE publica la plataforma de desarrollo de KDE 4.5.0. Esta versi&oacute;n incluye muchas mejoras de rendimiento y estabilidad. La nueva <b>KSharedDataCache</b> est&aacute; optimizada para un acceso r&aacute;pido a los recursos almacenados en disco, como los iconos. La nueva biblioteca <b>KDE WebKit</b> proporciona integraci&oacute;n con las preferencias de red, almacenamiento de contrase&ntilde;as y muchas otras caracter&iacute;sticas de Konqueror. <a href="../platform"><b>Leer el anuncio completo</b></a>
</p>

<a href="../applications">
<img src="/announcements/4/4.5.0/images/applications.png" class="app-icon float-left m-3" alt="Las aplicaciones de KDE 4.5.0"/>
</a>

<h2>Las aplicaciones de KDE 4.5.0 mejoran su usabilidad e incluyen rutas de mapas</h2>
<p align="justify">
Hoy, el equipo de KDE publica una nueva versi&oacute;n de las aplicaciones de KDE. Se han mejorado muchos t&iacute;tulos educativos, herramientas, juegos y utilidades gr&aacute;ficas a nivel funcional y de usabilidad. Marble, el globo virtual, incluye rutas bas&aacute;ndose en OpenRouteService. Konqueror, el navegador web de KDE se puede configurar para utilizar WebKit mediante el componente KWebKit disponible en el repositorio Extragear. <a href="../applications"><b>Leer el anuncio completo</b></a>
</p>
