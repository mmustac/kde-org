---
aliases:
- ../announce-4.11.2
date: 2013-10-01
description: KDE Ships Plasma Workspaces, Applications and Platform 4.11.2.
title: KDE Ships October Updates to Plasma Workspaces, Applications and Platform
---

October 1, 2013. Today KDE released updates for its Workspaces, Applications and Development Platform. These updates are the second in a series of monthly stabilization updates to the 4.11 series. As was announced on the release, the workspaces will continue to receive updates for the next two years. This release only contains bugfixes and translation updates and will be a safe and pleasant update for everyone.

More than 70 recorded bugfixes include improvements to the Window Manager KWin, the file manager Dolphin, the personal information management suite Kontact, and others. There are many stability fixes and the usual additions of translations.

A more complete <a href='https://bugs.kde.org/buglist.cgi?query_format=advanced&amp;short_desc_type=allwordssubstr&amp;short_desc=&amp;long_desc_type=substring&amp;long_desc=&amp;bug_file_loc_type=allwordssubstr&amp;bug_file_loc=&amp;keywords_type=allwords&amp;keywords=&amp;bug_status=RESOLVED&amp;bug_status=VERIFIED&amp;bug_status=CLOSED&amp;emailtype1=substring&amp;email1=&amp;emailassigned_to2=1&amp;emailreporter2=1&amp;emailcc2=1&amp;emailtype2=substring&amp;email2=&amp;bugidtype=include&amp;bug_id=&amp;votes=&amp;chfieldfrom=2013-06-01&amp;chfieldto=Now&amp;chfield=cf_versionfixedin&amp;chfieldvalue=4.11.2&amp;cmdtype=doit&amp;order=Bug+Number&amp;field0-0-0=noop&amp;type0-0-0=noop&amp;value0-0-0='>list</a> of changes can be found in KDE's issue tracker. For a detailed list of changes that went into 4.11.2, you can also browse the Git logs.

To download source code or packages to install go to the <a href='/info/4/4.11.2'>4.11.2 Info Page</a>. If you want to find out more about the 4.11 versions of KDE Workspaces, Applications and Development Platform, please refer to the <a href='/announcements/4.11/'>4.11 release notes</a>.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/send-later.png" caption="The new send-later work flow in Kontact" width="600px">}}

KDE software, including all libraries and applications, is available for free under Open Source licenses. KDE's software can be obtained as source code and various binary formats from <a
href='http://download.kde.org/stable/4.11.2/'>download.kde.org</a> or from any of the <a href='/distributions'>major GNU/Linux and UNIX systems</a> shipping today.
