---
aliases:
- ../announce-4.8.3
date: '2012-05-04'
description: KDE Ships 4.8.3 Workspaces, Applications and Platform.
title: KDE Ships May Updates to Plasma Workspaces, Applications and Platform
---

<p align="justify">
Today KDE released updates for its Workspaces, Applications, and Development Platform.
These updates are the third in a series of monthly stabilization updates to the 4.8 series. 4.8.3 updates bring many bugfixes and translation updates on top of the latest edition in the 4.8 series and are recommended updates for everyone running 4.8.2 or earlier versions. As the release only contains bugfixes and translation updates, it will be a safe and pleasant update for everyone. KDE’s software is already translated into more than 55 languages, with more to come.
<br /><br />
Significant bugfixes include improvements to the Kontact Suite, bugfixes in Dolphin and many more corrections and performance improvements all over the place.
The <a href="/announcements/changelogs/changelog4_8_2to4_8_3">changelog</a> and <a href="https://bugs.kde.org/buglist.cgi?query_format=advanced&short_desc_type=allwordssubstr&short_desc=&long_desc_type=substring&long_desc=&bug_file_loc_type=allwordssubstr&bug_file_loc=&keywords_type=allwords&keywords=&bug_status=RESOLVED&bug_status=VERIFIED&bug_status=CLOSED&emailtype1=substring&email1=&emailassigned_to2=1&emailreporter2=1&emailcc2=1&emailtype2=substring&email2=&bugidtype=include&bug_id=&votes=&chfieldfrom=2011-06-01&chfieldto=Now&chfield=cf_versionfixedin&chfieldvalue=4.8.3&cmdtype=doit&order=Bug+Number&field0-0-0=noop&type0-0-0=noop&value0-0-0=">Bugzilla</a> list more, but not all improvements since 4.8.2.
Note that these changelogs are incomplete. For a complete list of changes that went into 4.8.3, you can browse the Subversion and Git logs. 4.8.3 also ships a more complete set of translations for many of the 55+ supported languages.
To  download source code or packages to install go to the <a href="/info/4/4.8.3">4.8.3 Info Page</a>. If you would like to find out more about the KDE Workspaces and Applications 4.8, please refer to the <a href="/announcements/4.8/">4.8 release notes</a> and its earlier versions.
</p>

<div class="text-center">
	<a href="/announcements/4/4.8.0/plasma-desktop-4.8.png">
	<img src="/announcements/4/4.8.0/thumbs/plasma-desktop-4.8.png" class="img-fluid" alt="Plasma Desktop with the Dolphin file manager">
	</a> <br/>
	<em>Plasma Desktop with the Dolphin file manager</em>
</div>
<br/>

<p align="justify">
The KDE Software Compilation, including all its libraries and its applications, is available for free
under Open Source licenses. KDE's software can be obtained in source and various binary
formats from <a
href="http://download.kde.org/stable/4.8.3/">download.kde.org</a>
or with any of the <a href="/distributions">major
GNU/Linux and UNIX systems</a> shipping today.
</p>

<h4>
  Installing 4.8.3 Binary Packages
</h4>
<p align="justify">
  <em>Packages</em>.
  Some Linux/UNIX OS vendors have kindly provided binary packages of 4.8.3
for some versions of their distribution, and in other cases community volunteers
have done so.
  Additional binary packages, as well as updates to the packages now available,
may become available over the coming weeks.
</p>

<p align="justify">
  <a id="package_locations"></a><em>Package Locations</em>.
  For a current list of available binary packages of which the KDE Project has
been informed, please visit the <a href="/info/4/4.8.3#binary">4.8.3 Info
Page</a>.
</p>

<h4>
  Compiling 4.8.3
</h4>
<p align="justify">
  <a id="source_code"></a>
  The complete source code for 4.8.3 may be <a
href="http://download.kde.org/stable/4.8.3/src/">freely downloaded</a>.
Instructions on compiling and installing 4.8.3
  are available from the <a href="/info/4/4.8.3">4.8.3 Info
Page</a>.
</p>

<h4>
  Supporting KDE
</h4>

<p align="justify">
 KDE is a <a href="http://www.gnu.org/philosophy/free-sw.html">Free Software</a>
community that exists and grows only because of the help of many volunteers that
donate their time and effort. KDE is always looking for new volunteers and
contributions, whether it is help with coding, bug fixing or reporting, writing
documentation, translations, promotion, money, etc. All contributions are
gratefully appreciated and eagerly accepted. Please read through the <a
href="/community/donations/">Supporting KDE page</a> for further information or 
become a KDE e.V. supporting member through our new 
<a href="http://jointhegame.kde.org/">Join the Game</a> initiative. </p>


