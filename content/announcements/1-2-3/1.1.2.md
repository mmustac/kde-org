---
aliases:
- ../announce-1.1.2
custom_about: true
custom_contact: true
date: '1999-09-13'
title: KDE 1.1.2 Release Announcement
---

#### New Release of Award-Winning Linux Desktop GUI Features

Look and Feel Improvements and Bugfixes

The  <a href="/">K  Desktop  Environment  (KDE)  Team</a>
today announced the shipment of  KDE  1.1.2,  the
latest release of the Linux de facto standard desktop.

"The last few KDE releases stressed stability  and  power",  explained
Kurt Granroth, a member  of  the  KDE  core  team.   "With  the  1.1.2
release, we  have  also  made  major  improvements  to  the  aesthetic
aspects of the user experience".

KDE 1.1.2 includes the KDE Theme Manager, a new tool enabling users to
change  their  desktop  theme  --  ranging  from  colors  and   window
decorations to icons and sounds -- with a  few  simple  mouse  clicks.
This release includes hundreds of new  high  quality  application  and
tool bar icons, created by the very active  KDE  Artists  Team,  which
dramatically enhances the appearance of the desktop,  the  panel,  and
various applications.

Further changes include:

*  Significant enhancements and bugfixes to the KDE mail client
    (kmail);

*  New translations -- KDE now supports up to 35 languages;

*  Improved stability and HTML compatability in the KDE  web  browser
    and file manager (kfm);

*  Numerous bugfixes in many other packages.

KDE 1.1.2 is available for free download from  numerous  <a href="/mirrors">mirrors</a>
and  also  from  KDE's  primary  <a
href="ftp://ftp.kde.org/pub/kde/stable/1.1.2/distribution/">ftp
server</a>.
Practically  all  Linux  and  *BSD  distributions  are   expected   to
incorporate KDE 1.1.2 in future releases.

Technical support for KDE is provided by Linux vendors who include KDE
in their distributions.  Informal support is also  available  from  an
international group of KDE  enthusiasts  by  joining  the  <a href="news://comp.windows.x.kde">KDE  Usenet
discussion group</a> or  one  of  the  KDE
<a href="http://www.kde.org/mailinglists/">mailing lists</a>

#### ABOUT KDE

KDE is a collaborative Open Source project by hundreds  of  developers
worldwide to create a sophisticated, customizable and  stable  desktop
environment employing a network-transparent, intuitive user interface.
KDE  includes   applications   for   email,   web   browsing,   system
administration,  dial-up   networking,   scheduling,   Palm   Pilot(r)
synchronization, and many other tasks.  KDE is working  proof  of  the
power of the open source software development model.

The elegance and usefulness of the KDE environment has been recognized
through multiple awards, including Linux  World  magazine's  "Editor's
Choice" award in the Desktop  Environment  category,  and  Ziff-Davis'
"Innovation of the year 1998/1999" award in the Software category.

For more information about KDE, please visit
<a href="/whatiskde">http://www.kde.org/whatiskde/</a>.