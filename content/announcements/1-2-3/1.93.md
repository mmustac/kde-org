---
aliases:
- ../announce-1.93
custom_about: true
custom_contact: true
date: 2000-08-23
title: KDE 1.93 Release Announcement
---

<STRONG>Fourth Beta Preview of Leading Desktop for Linux<SUP>&reg;</SUP> and Other UNIXes<SUP>&reg;</SUP></STRONG>
August 23, 2000 (The INTERNET).  The <a href="/">KDE
Team</A> today announced the release of KDE 1.93, the fourth beta preview
of Kopernicus, KDE's next-generation, powerful, modular desktop.  Following
on the heels of the release of KDE 1.92 on July 27, 2000, the release,
code-named "Kooldown", is based on a beta of
<a href="http://www.trolltech.com/">Trolltech's</A><SUP>tm</SUP>
upcoming Qt<SUP>&reg;</SUP> 2.2.0 and will include the core libraries,
the core desktop environment, the KOffice suite, as well
as the over 100 applications from the other standard base KDE packages: 
Administration, Games,
Graphics, Multimedia, Network, Personal Information Management (PIM),
Toys and Utilities. 
Kooldown is targeted at users who would like to help the
KDE team make usability and feature enhancements and fix the remaining
set of bugs before the release of KDE 2.0 ("Kopernicus"), scheduled
for early-fourth quarter 2000.

The major changes since the beta 3 release ("Korner") last month include:

- The addition of Qt Designer, an excellent visual user interface design
  tool.
- Fixed Konqueror's HTTP- and FTP-proxy support.
- Great speed improvements in rendering by the HTML widget.
- Integrated support for GTK themes.
- Improvements and many fixes to the JavaScript<SUP>TM</SUP> library.
- Improvements to KWord's WinWord filter -- it now imports even MS Office 2000<SUP>TM</SUP> documents quite well.
- Greater stability and many bug-fixes to virtually all packages.

"It is really great to work together with all of the dedicated developers,
translators, artists, documentation writers and beta users. I'm very proud of
the results that we have achieved with this release and I hope that
our users enjoy it just as much," said core developer Waldo Bastian.
"Given the massive amount of fixes and improvements in
usability as well as appearance, this beta preview could also have been
labeled a pre-release candidate for Kopernicus," added core developer Lars Knoll.

Kooldown offers a usable desktop for a non-critical production environment.
The principal benefits lie in the cutting-edge technologies provided by
<a href="http://konqueror.kde.org/">Konqueror</A> and
the <a href="http://koffice.kde.org/">KOffice suite</A>, in
KDE's enhanced customizability and in KDE's continued improvements in
ease of use.

- <a id="Konqueror">Konqueror</A> reigns as the next-generation
  web browser, file manager and
  document viewer for KDE 2.0. Widely acclaimed as a technological
  break-through for the Linux desktop, Konqueror has a component-based
  architecture which combines the features and functionality of Internet
  Explorer<SUP>&reg;</SUP>/Netscape Communicator<SUP>&reg;</SUP> and
  Windows Explorer<SUP>&reg;</SUP>. Konqueror will support
  the full gamut of current Internet technologies, including
  JavaScript, Java<SUP>&reg;</SUP>, HTML 4.0, CSS-2
  (Cascading Style Sheets), SSL (Secure Socket Layer for secure communications)
  and Netscape Communicator<SUP>&reg;</SUP> plug-ins (for
  playing Flash<SUP>TM</SUP>,
  RealAudio<SUP>TM</SUP>, RealVideo<SUP>TM</SUP> and similar technologies).
  In addition,
  Konqueror's network transparency offers seamless support for browsing
  Linux<SUP>&reg;</SUP> NFS shares, Windows<SUP>&reg;</SUP> SMB shares,
  HTTP pages, FTP directories as well as any other protocol for which
  a KIO plug-in is available.

<BR> <BR>

- <a id="KOffice">The KOffice suite</A> is one of the most-anticipated
  Open Source projects. The suite consists
  of a spreadsheet application (KSpread), a vector drawing application
  (KIllustrator), a bitmap drawing application (KImageShop), a frame-based
  word-processing application (KWord), a presentation program (KPresenter),
  and a chart and diagram application
  (KChart). Native file formats are XML-based, and work on
  filters for proprietary binary file formats is progressing.
  Combined with a powerful scripting language and the
  ability to embed individuals components within each other using KDE's
  KParts technology, the KOffice
  suite will provide all the necessary functionality to all but the most
  demanding power users, at an unbeatable price -- free.

<BR> <BR>

- KDE's customizability touches every aspect of this next-generation
  desktop. <a id="Style engine">Kooldown benefits from Qt's
  style engine, which permits developers and artists to create their
  own widget designs down to the precise appearance of a scrollbar,
  a button, a menu and more, combined with development tools which will
  largely automate the creation of these widget sets. In addition, KDE
  offers excellent support for GTK themes. Just to
  mention a few of the legion configuration options,
  users can choose among: numerous types of menu effects; a menu
  bar atop the display (Macintosh<SUP>&reg;</SUP>-style) or atop each individual
  window (Windows-style); icon styles; system sounds; key bindings;
  languages; toolbar and menu composition; and much much more.

<BR> <BR>

#### Downloading and Compiling Beta 4

The source packages for Kooldown are available for free download at
<a href="ftp://ftp.kde.org/pub/kde/unstable/distribution/2.0Beta4/tar/src/">ftp://ftp.kde.org/pub/kde/unstable/distribution/2.0Beta4/tar/src/</A> or in the
equivalent directory at one of the many KDE ftp server
<a href="/mirrors">mirrors</A>. Kooldown requires
qt-2.2.0-beta2, which is available from the above locations under the name
<a href="ftp://ftp.kde.org/pub/kde/unstable/distribution/2.0Beta4/tar/src/qt-x11-2.2.0-beta2.tar.gz">qt-x11-2.2.0-beta2.tar.gz</A>.
Please be advised that Kooldown will <STRONG>not</STRONG> work with any
older versions of Qt. Qt is not part of KDE's beta testing.

For further instructions on compiling and installing Kooldown, please consult
the <a href="http://developer.kde.org/build/index.html">installation
instructions</A> and, if you encounter problems, the
<a href="http://developer.kde.org/build/index.html">compilation FAQ</A>.

#### Installing Binary Packages

The binary packages for Kooldown will be available for free download under
<a href="ftp://ftp.kde.org/pub/kde/unstable/distribution/2.0Beta4/">ftp://ftp.kde.org/pub/kde/unstable/distribution/2.0Beta4/</A>
or under the equivalent directory at one of the many KDE ftp server
<a href="/mirrors">mirrors</A>. Kooldown requires
qt2.2.0-beta2, which is available from the above locations under the name
qt-x11-2.2.0-beta2.
Please be advised that Kooldown will <STRONG>not</STRONG> work with any
older versions of Qt. Qt is not part of KDE's beta testing.

At the time of this release, pre-compiled packages are available for:

- <a href="ftp://ftp.kde.org/pub/kde/unstable/distribution/2.0Beta4/rpm/COL-2.4/">Caldera OpenLinux 2.4</A>
- <a href="ftp://ftp.kde.org/pub/kde/unstable/distribution/2.0Beta4/rpm/SuSE/6.4-i386/">SuSE Linux 6.4</A>
- <a href="ftp://ftp.kde.org/pub/kde/unstable/distribution/2.0Beta4/rpm/LTP/">Linux Technology Preview</A>

Check the ftp servers periodically for pre-compiled packages for other
distributions. More binary packages will become available over the
coming days.

#### About KDE
KDE is an independent, collaborative project by hundreds of developers
worldwide to create a sophisticated, customizable and stable desktop environment
employing a component-based, network-transparent architecture.
Currently development is focused on KDE 2, which will for the first time
offer a free, Open Source, fully-featured office suite and which promises to
make the Linux desktop as easy to use as Windows<SUP>&reg;</SUP> and
the Macintosh<SUP>&reg;</SUP>
while remaining loyal to open standards and empowering developers and users
with Open Source software.  KDE is working proof of how the Open Source
software development model can create first-rate technologies on par with
and superior to even the most complex commercial software.

For more information about KDE, please visit KDE's <a href="/whatiskde">web site</A>.
<BR>

<HR NOSHADE SIZE=1 WIDTH="90%" ALIGN="center">
<TABLE BORDER=0 CELLPADDING=8 CELLSPACING=0>
<TR><TH COLSPAN=2 ALIGN="left">
Press Contacts:
</TH></TR>
<TR VALIGN="top"><TD ALIGN="right" NOWRAP>
United&nbsp;States:
</TD><TD NOWRAP>
Kurt Granroth<BR>
g&#x72;anro&#00116;h&#64;kde.&#111;rg<BR>
(1) 480 732 1752<BR>&nbsp;<BR>
Andreas Pour<BR>
&#x70;&#x6f;ur&#x40;k&#x64;e.or&#00103;<BR>
(1) 718-456-1165
</TD></TR>
<TR VALIGN="top"><TD ALIGN="right" NOWRAP>
Europe (English and German):
</TD><TD NOWRAP>
Martin Konold<BR>
&#0107;&#111;&#110;ol&#0100;&#064;&#x6b;d&#101;&#x2e;or&#x67;<BR>
(49) 179 2252249
</TD></TR>
</TABLE>