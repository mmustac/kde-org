---
aliases:
- ../announce-3.5.6
- ../announce-3.5.6.sl
custom_about: true
custom_contact: true
date: '2007-01-25'
title: KDE 3.5.6 - najava izdaje
---

<h3 align="center">
   Projekt KDE je izdal četrto servisno različico vodilnega prostega namizja,
   ki prinaša tudi posodobljene prevode
</h3>

<p align="justify">
  <strong>
    KDE 3.5.6 prinaša prevode v 65 jezikih, Izboljšave pogona
    za prikaz HTML-ja (KHTML) in izboljšave v ostalih programih.
  </strong>
</p>

<p align="justify">
  Pri <a href="/">Projektu
  KDE</a> so danes najavili takojšnjo dostopnost KDE 3.5.6, vzdrževalne
  izdaje najnovejše generacije najnaprednejšega in najzmoglivejšega
  <em>prostega</em> namizja za GNU/Linux in ostale UNIX-e. KDE sedaj
  podpira 65 jezikov, s čimer je v materinem jeziku na voljo večim ljudem
  kot večina komercialnih programov. Skupine, ki želijo
  prispevati k odprtokodnemu projektu lahko na preprost način dodajo podporo za ostale
  jezike. Če se želite pridružiti slovenski ekipi prevajalcev obiščite spletno stran
  <a href="http://www.lugos.si/projekti/KDE/osnove">Uvod v slovenjenje KDE</a>.
</p>

<p align="justify">
  Ta izdaja vsebuje vrsto popravkov za KHTML, <a href="http://kate-editor.org">Kate</a>,
  Kicker, KSysGuard in mnoge ostale programe. Med pomembnejšimi izboljšavami so dodana podpora za okenski upravljalnik Compiz v Kickerju,
  upravljanje sej za zavihke v <a href="http://akregator.kde.org">Akregator</a>-ju,
  predloge za sporočila v <a href="kmail.kde.org">KMail</a>-u in novi meniji s povzetki v
  <a href="http://kontact.kde.org">Kontact</a>-u, kar vam olajša delo s sestanki in opravili. Prav tako se nadaljuje delo na prevodih. Količina prevodov v galicijski jezik se je skoraj podvojila. KDE 3.5.6 je tudi prva različica, ki prinaša polno podporo za uvedbo Evra, kot državne valute v Sloveniji.
</p>

<p align="justify">
  Za podrobnejši seznam izboljšav glede na
  <a href="/announcements/announce-3.5.5">izdajo KDE 3.5.5</a>
  11. oktobra 2006 si oglejte
  <a href="/announcements/changelogs/changelog3_5_5to3_5_6">seznam sprememb v KDE 3.5.6</a>.
</p>

<p align="justify">
  KDE 3.5.6 vsebuje osnovno namizje in petnajst ostalih paketov (upravljanje
  z osebnimi podatki, administracija, omrežje, izobraževanje, pripomočki, večpredstavnost,
  igre, spletni razvoj, programiranje...). KDE-jeva orodja in programi so prejeli več
  nagrad in so na voljo v <strong>65 jezikih</strong>. Med njimi je tudi slovenščina.
</p>

<h4>
  Distribucije, ki vsebujejo KDE
</h4>
<p align="justify">
  Večina distribucij operacijskih sistemov Linux in UNIX ne vključi novih izdaj
  KDE-ja takoj. Pakete, ki sestavljajo KDE 3.5.6, bodo vključili v prihodnje izdaje
  svojih distribucij. Oglejte si <a href="/distributions">seznam
  distribucij, ki vsebujejo KDE</a>.
</p>

<h4>
  Nameščanje paketov za KDE 3.5.6
</h4>
<p align="justify">
  <em>Ustvarjalci paketov</em>.
  Nekateri ponudniki operacijskih sistemov so sami pripravili pakete namizja
  KDE 3.5.6 za nekatere različice svojih distribucij. V določenih primerih pa so
  to storili prostovoljci iz skupnosti uporabnikov.
  Nekateri od teh paketov so za prost prenos na voljo na KDE-jevem strežniku
  <a href="http://download.kde.org/binarydownload.html?url=/stable/3.5.6/">download.kde.org</a>.
  Dodatni ali posodobljeni paketi so lahko na voljo v prihajajočih tednih.
</p>

<p align="justify">
  <a id="package_locations"><em>Lokacije paketov</em></a>
  Za trenuten seznam razpoložljivih paketov, o katerih je bil obveščen
  Projekt KDE, obiščite angleško stran
  <a href="/info/1-2-3/3.5.6">KDE 3.5.6 Info Page</a>.
</p>

<h4>
  Prevajanje izvorne kode KDE 3.5.6
</h4>
<p align="justify">
  <a id="source_code"></a><em>Izvorna koda</em>
  Celotna izvorna koda za KDE 3.5.6 je na voljo za
  <a href="http://download.kde.org/stable/3.5.6/src/">prost
  prenos</a>. Navodila za prevajanje in nameščanje KDE 3.5.6
  so na voljo na angleški strani <a href="/info/1-2-3/3.5.6">KDE
  3.5.6 Info Page</a>.
</p>

<h4>
  Kako podpreti KDE
</h4>
<p align="justify">
KDE je projekt <a href="http://www.gnu.org/philosophy/free-sw.html">prostega programja</a>,
ki obstaja in raste le zaradi pomoči mnogih prostovoljcev, ki vanj vložijo svoj čas in trud.
KDE vedno išče nove prostovoljce in prispevkarje. Pomagate lahko s programiranjem,
odpravljanjem napak ali testiranjem in poročanjem o napakah. Pišete lahko dokumentacijo,
prevajate, promovirate KDE ali pa nam pomagate denarno. Vsi prispevki so izredno
cenjeni in nestrpno pričakovani. Za dodatne informacije si oglejte angleško stran
<a href="/community/donations/">Supporting KDE</a>.</p>

<p align="justify">
Veseli bomo, če boste stopili v stik z nami!
</p>

<h4>
  O projektu KDE
</h4>
<p align="justify">
  KDE, ki je prejel <a href="/community/awards/">več nagrad</a>, je neodvisen projekt <a href="/people/">več sto</a>
  programerjev, prevajalcev, umetnikov in ostalih profesionalcev z vsega sveta, ki sodelujejo prek interneta.
  Njihov cilj je ustvariti in prosto razširjati sofisticirano, prilagodljivo in stabilno
  namizje in pisarniško okolje. Pri tem uporabljajo fleksibilno arhitekturo, ki temelji na komponentah
  in deluje tudi preko omrežja. Vse to omogoča izvrstno platformo za razvoj.</p>

<p align="justify">
  KDE ponuja stabilno namizje, ki vključuje modern spletni brskalnik
  (<a href="http://konqueror.kde.org/">Konqueror</a>), orodja za upravljanje
  z osebnimi podatki (<a href="http://kontact.org/">Kontact</a>), celoten
  pisarniški paket (<a href="http://www.koffice.org/">KOffice</a>), obsežen
  nabor omrežnih programov in orodij ter učinkovito in intuitivno okolje za razvoj
  in programiranje <a href="http://www.kdevelop.org/">KDevelop</a>.</p>

<p align="justify">
  KDE je delujoč dokaz, da odprtokoden razvoj programja v slogu bazarja
  lahko privede do najboljših tehnologij, ki so enakovredne najbolj
  zapletenim komercialnim programom, oziroma jih celo prekašajo.
</p>

<hr />

<p align="justify">
  <font size="2">
  <em>Opombe o zaščitenih znamkah</em>
  Logotipa KDE<sup>&#174;</sup> in K Desktop Environment<sup>&#174;</sup> sta
  zaščiteni znamki organizacije KDE e.V.

  Linux je zaščitena znamka, ki si jo lasti Linus Torvalds.

  UNIX je v ZDA in v drugih državah zaščitena znamka konzorcija The Open Group.

  Vse ostale zaščitene znamke in avtorske pravice, ki so omenjene v tej najavi,
  pripadajo posameznim lastnikom.
  </font>
</p>

<hr />

<h4>Stiki za novinarje</h4>
<table cellpadding="10" align="center"><tr valign="top">
<td>

<b>Afrika</b><br />
Uwe Thiem<br />
P.P.Box 30955<br />
Windhoek<br />
Namibia<br />
Phone: +264 - 61 - 24 92 49<br />
<a href="&#109;a&#105;l&#116;o:&#105;&#110;fo-&#0097;&#0102;r&#105;&#99;a&#x40;k&#100;e.&#111;&#x72;g">info-africa kde.org</a><br />
</td>

<td>
<b>Azija in Indija</b><br />
     Pradeepto Bhattacharya<br/>
     A-4 Sonal Coop. Hsg. Society<br/>
     Plot-4, Sector-3,<br/>
     New Panvel,<br/>
     Maharashtra.<br/>
     India 410206<br/>
     Phone : +91-9821033168<br/>
<a href="ma&#0105;&#108;to&#00058;inf&#00111;-&#97;&#115;&#x69;a&#x40;kde.or&#x67;">info-asia kde.org</a>
</td>

</tr>
<tr valign="top">

<td>
<b>Evropa</b><br />
Matthias Kalle Dalheimer<br />
Rysktorp<br />
S-683 92 Hagfors<br />
Sweden<br />
Phone: +46-563-540023<br />
Fax: +46-563-540028<br />
<a href="m&#x61;il&#0116;o&#x3a;i&#x6e;fo-&#00101;&#00117;rope&#64;k&#x64;&#x65;&#00046;o&#x72;&#00103;">info-europe kde.org</a>
</td>

<td>
<b>Severna Amerika</b><br />
George Staikos <br />
889 Bay St. #205 <br />
Toronto, ON, M5S 3K5 <br />
Canada<br />
Phone: (416)-925-4030 <br />
<a href="&#109;ai&#x6c;&#x74;&#x6f;&#0058;i&#x6e;&#0102;o&#0045;no&#0114;t&#104;&#0097;m&#x65;&#x72;i&#x63;&#x61;&#x40;k&#x64;e&#46;&#0111;&#x72;&#x67;">info-northamerica kde.org</a><br />
</td>

</tr>

<tr>
<td>
<b>Oceanija</b><br />
Hamish Rodda<br />
11 Eucalyptus Road<br />
Eltham VIC 3095<br />
Australia<br />
Phone: (+61)402 346684<br />
<a href="&#109;&#x61;&#x69;&#x6c;&#x74;o:&#105;&#x6e;fo&#45;&#x6f;c&#101;&#x61;&#110;ia&#064;kde&#00046;org">info-oceania kde.org</a><br />
</td>

<td>
<b>Južna Amerika</b><br />
Helio Chissini de Castro<br />
R. Jos&eacute; de Alencar 120, apto 1906<br />
Curitiba, PR 80050-240<br />
Brazil<br />
Phone: +55(41)262-0782 / +55(41)360-2670<br />
<a href="ma&#105;&#x6c;&#116;&#x6f;&#x3a;&#0105;&#110;&#102;&#x6f;-&#00115;&#111;ut&#104;&#97;&#x6d;e&#0114;&#x69;ca&#0064;&#107;d&#x65;.&#111;r&#x67;">info-southamerica kde.org</a><br />
</td>

</tr></table>
