---
date: 2023-03-02
appCount: 120
image: true
layout: gear
---
+ ark: Properly check if there is sufficient free space available before extracting ([Commit](http://commits.kde.org/ark/6cbcbb4a7972c0887ab8e8be4c7ebd7ff5f27e00), fixes bug [#459418](https://bugs.kde.org/459418))
+ kate: Activate the view of viewspace which made the request ([Commit](http://commits.kde.org/kate/6a28dfe19247a7a02a5e08e9bfa245ec4c10280e), fixes bug [#465811](https://bugs.kde.org/465811))
+ kdenlive: Fix crash and offset when moving a group with subtitle ([Commit](http://commits.kde.org/kdenlive/998ddfa5e0471b7dba34ebb16909931cae3d7922))
