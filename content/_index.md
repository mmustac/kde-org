---
images:
- /thumbnail.png

highlight_apps:
- name: krita
  home: https://krita.org
  screen: https://krita.org/wp-content/uploads/2019/08/krita-ui-40.png
  screenShadow: true

- name: kdenlive
  home: https://kdenlive.org
  screen: https://cdn.kde.org/screenshots/kdenlive/k1.png
  screenShadow: true

- name: kontact
  home: https://kontact.kde.org
  screen: https://cdn.kde.org/screenshots/kmail/kmail.png
  screenShadow: false

- name: kdevelop
  home: https://kdevelop.org
  screen: https://cdn.kde.org/screenshots/kdevelop/kdevelop.png
  screenShadow: false

- name: gcompris
  home: https://gcompris.net
  screen: https://cdn.kde.org/screenshots/gcompris/gcompris.png
  screenShadow: true

- name: labplot2
  home: https://labplot.kde.org/
  screen: https://cdn.kde.org/screenshots/labplot2/labplot.png
  screenShadow: true

community:
  - img: /content/people/ubuntu-2022.jpg
    name: Ubuntu Summit 2022
    city: Prague
    link: https://planet.kde.org/jonathan-esk-riddell-2022-11-14-ubuntu-summit-2022-prague/
  - img: /content/people/state-of-open.jpeg
    name: State of Open Con
    city: London - England
    link: https://floss.social/@kde/111885640712359926
  - img: /content/people/plasma-2023.webp
    name: Plasma Sprint
    city: Augsburg - Germany
    link: https://pointieststick.com/2023/05/11/plasma-6-better-defaults/
  - img: /content/people/latinoware.jpeg
    name: Latinoware
    city: Brazil
    link: https://twitter.com/kdecommunity/status/1587808735594004484
  - img: /content/people/fosdem-2024.jpeg
    name: Fosdem
    city: Brussels - Belgium
    link: https://floss.social/@kde/111866927455521568
  - img: /content/people/GSoCx2.jpg
    name: Google Summer of Code Students
    link: https://community.kde.org/GSoC
  - img: /content/people/goals-2022.jpg
    name: KDE Goals
    city: Thessaloniki - Greece
    link: https://dot.kde.org/2022/11/16/kde%E2%80%99s-new-goals-join-kick-meeting
  - img: /content/people/akademy2022-groupphoto.jpg
    name: Akademy 2022
    city: Barcelona
    link: https://dot.kde.org/2022/10/16/akademy-2022-weekend-kde-talks-panels-and-presentations
  - img: /content/people/cki2024.jpg
    name: KDE Conf India 2024
    city: Pune, Maharashtra
    link: https://conf.kde.in/
---
