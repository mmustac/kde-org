<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top">
  <th align="left">Location</th>
  <th align="left">Size</th>
  <th align="left">MD5&nbsp;Sum</th>
</tr>
<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.8/src/arts-1.5.8.tar.bz2">arts-1.5.8</a></td>
   <td align="right">948kB</td>
   <td><tt>061ce49351d970a81f4c0a1b0339fb34</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.8/src/kdeaccessibility-3.5.8.tar.bz2">kdeaccessibility-3.5.8</a></td>
   <td align="right">8.3MB</td>
   <td><tt>0ede2d48df626aa436dbe6c741d575f1</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.8/src/kdeaddons-3.5.8.tar.bz2">kdeaddons-3.5.8</a></td>
   <td align="right">1.6MB</td>
   <td><tt>4a338f14210ad920bb54624cd330dd89</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.8/src/kdeadmin-3.5.8.tar.bz2">kdeadmin-3.5.8</a></td>
   <td align="right">2.0MB</td>
   <td><tt>324a44d854a92177e71954f9264c98a8</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.8/src/kdeartwork-3.5.8.tar.bz2">kdeartwork-3.5.8</a></td>
   <td align="right">15MB</td>
   <td><tt>03becf82a233e6007e9372ffa5756d0b</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.8/src/kdebase-3.5.8.tar.bz2">kdebase-3.5.8</a></td>
   <td align="right">23MB</td>
   <td><tt>9990c669229daaaa8fca4c5e354441fd</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.8/src/kdebindings-3.5.8.tar.bz2">kdebindings-3.5.8</a></td>
   <td align="right">5.4MB</td>
   <td><tt>4325d22ac70d3945609bd952c19e793b</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.8/src/kdeedu-3.5.8.tar.bz2">kdeedu-3.5.8</a></td>
   <td align="right">28MB</td>
   <td><tt>aaae4c6fe82c806eb20942178cadad9e</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.8/src/kdegames-3.5.8.tar.bz2">kdegames-3.5.8</a></td>
   <td align="right">10MB</td>
   <td><tt>786ee4e47820d92aef7db73424b9604c</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.8/src/kdegraphics-3.5.8.tar.bz2">kdegraphics-3.5.8</a></td>
   <td align="right">7.0MB</td>
   <td><tt>a3a31fc0e5b791ef330dd0627095d90f</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.8/src/kdelibs-3.5.8.tar.bz2">kdelibs-3.5.8</a></td>
   <td align="right">14MB</td>
   <td><tt>acaa37e79e840d10dca326277a20863c</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.8/src/kdemultimedia-3.5.8.tar.bz2">kdemultimedia-3.5.8</a></td>
   <td align="right">6.0MB</td>
   <td><tt>9f3c95231ea265b09f3010adb954ae30</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.8/src/kdenetwork-3.5.8.tar.bz2">kdenetwork-3.5.8</a></td>
   <td align="right">8.9MB</td>
   <td><tt>0e79374d1109d937b0c9bdd3a75e7476</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.8/src/kdepim-3.5.8.tar.bz2">kdepim-3.5.8</a></td>
   <td align="right">13MB</td>
   <td><tt>a1ffff553f1d6739c7791891028b176b</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.8/src/kdesdk-3.5.8.tar.bz2">kdesdk-3.5.8</a></td>
   <td align="right">4.9MB</td>
   <td><tt>c809c15eb8c09a7eb2d070395202910b</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.8/src/kdetoys-3.5.8.tar.bz2">kdetoys-3.5.8</a></td>
   <td align="right">3.1MB</td>
   <td><tt>b42c1f08e5c4ac93a04aadb75679139f</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.8/src/kdeutils-3.5.8.tar.bz2">kdeutils-3.5.8</a></td>
   <td align="right">2.9MB</td>
   <td><tt>d1a0fcc83f35428a76cf7523a04ba19c</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.8/src/kdevelop-3.5.0.tar.bz2">kdevelop-3.5.0</a></td>
   <td align="right">8.9MB</td>
   <td><tt>1101077b3a0164da463f60cad4f13e25</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.8/src/kdewebdev-3.5.8.tar.bz2">kdewebdev-3.5.8</a></td>
   <td align="right">5.7MB</td>
   <td><tt>6c17c4b71a4d306da4b81a0cfd3116e1</tt></td>
</tr>

</table>
