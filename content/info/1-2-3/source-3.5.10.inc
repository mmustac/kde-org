<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top">
  <th align="left">Location</th>
  <th align="left">Size</th>
  <th align="left">MD5&nbsp;Sum</th>
</tr>
<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.10/src/arts-1.5.9.tar.bz2">arts-1.5.10</a></td>
   <td align="right">950kB</td>
   <td><tt>6da172aab2a4a44929b5fdfc30fa3efc</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.10/src/kdeaccessibility-3.5.10.tar.bz2">kdeaccessibility-3.5.10</a></td>
   <td align="right">8.3MB</td>
   <td><tt>feb1582b9acc573cef8cd357d8a7bc1d</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.10/src/kdeaddons-3.5.10.tar.bz2">kdeaddons-3.5.10</a></td>
   <td align="right">1.6MB</td>
   <td><tt>c69d082407b2c1bb46d078f8ac5d2bea</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.10/src/kdeadmin-3.5.10.tar.bz2">kdeadmin-3.5.10</a></td>
   <td align="right">2.0MB</td>
   <td><tt>eb23c52c945f31a48f2c9df4330a1262</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.10/src/kdeartwork-3.5.10.tar.bz2">kdeartwork-3.5.10</a></td>
   <td align="right">15MB</td>
   <td><tt>6e6f089dc0f1dabb0f138641600d0b59</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.10/src/kdebase-3.5.10.tar.bz2">kdebase-3.5.10</a></td>
   <td align="right">23MB</td>
   <td><tt>88237188271fbf1e6bcd40180a75d953</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.10/src/kdebindings-3.5.10.tar.bz2">kdebindings-3.5.10</a></td>
   <td align="right">5.4MB</td>
   <td><tt>7d5119160ac3688ac1a63954d03ab4a8</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.10/src/kdeedu-3.5.10.tar.bz2">kdeedu-3.5.10</a></td>
   <td align="right">28MB</td>
   <td><tt>1b1466bf4cb0a59b1abd8613a2588142</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.10/src/kdegames-3.5.10.tar.bz2">kdegames-3.5.10</a></td>
   <td align="right">10MB</td>
   <td><tt>5533b3886cbb74180933fe3f3d209031</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.10/src/kdegraphics-3.5.10.tar.bz2">kdegraphics-3.5.10</a></td>
   <td align="right">7.0MB</td>
   <td><tt>a09094b5357d8cd5c49d45b5d291dcfe</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.10/src/kdelibs-3.5.10.tar.bz2">kdelibs-3.5.10</a></td>
   <td align="right">14MB</td>
   <td><tt>43cd55ed15f63b5738d620ef9f9fd568</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.10/src/kdemultimedia-3.5.10.tar.bz2">kdemultimedia-3.5.10</a></td>
   <td align="right">6.0MB</td>
   <td><tt>8e8cd7f41d37f7da8bd239048abf3516</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.10/src/kdenetwork-3.5.10.tar.bz2">kdenetwork-3.5.10</a></td>
   <td align="right">8.9MB</td>
   <td><tt>634b2e914661faebae79d95dcc6c5bfa</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.10/src/kdepim-3.5.10.tar.bz2">kdepim-3.5.10</a></td>
   <td align="right">13MB</td>
   <td><tt>fc93e458a8eec8131ede56cff30c28b2</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.10/src/kdesdk-3.5.10.tar.bz2">kdesdk-3.5.10</a></td>
   <td align="right">5.0MB</td>
   <td><tt>ad711d1ce09242bd13b73a9a005f3143</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.10/src/kdetoys-3.5.10.tar.bz2">kdetoys-3.5.10</a></td>
   <td align="right">3.1MB</td>
   <td><tt>1da4383e2d520dfd572edb33b708822d</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.10/src/kdeutils-3.5.10.tar.bz2">kdeutils-3.5.10</a></td>
   <td align="right">2.9MB</td>
   <td><tt>038f94275f42df3cae89735506ffbc0b</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.10/src/kdevelop-3.5.3.tar.bz2">kdevelop-3.5.3</a></td>
   <td align="right">9.0MB</td>
   <td><tt>a2cdb5f71952386798175f8ce5a3e196</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.10/src/kdewebdev-3.5.10.tar.bz2">kdewebdev-3.5.10</a></td>
   <td align="right">5.9MB</td>
   <td><tt>7188f351158ca5a7613c3de4a6854b37</tt></td>
</tr>

</table>
