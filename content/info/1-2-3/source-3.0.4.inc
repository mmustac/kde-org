    <table border="0" cellpadding="4" cellspacing="0">
      <tr valign="top">
        <th align="left">Location</th>

        <th align="left">Size</th>

        <th align="left">MD5&nbsp;Sum</th>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.0.4/src/arts-1.0.4.tar.bz2">arts-1.0.4</a></td>

        <td align="right">1001KB</td>

        <td><tt>a88c1d7f2eb8d702045400c37b6d582e</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.0.4/src/kde-i18n-3.0.4.tar.bz2">kde-i18n-3.0.4</a></td>

        <td align="right">93MB</td>

        <td><tt>14cd99b51ea8fa55c4b78498484dc781</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.0.4/src/kdeaddons-3.0.4.tar.bz2">kdeaddons-3.0.4</a></td>

        <td align="right">921KB</td>

        <td><tt>6ced17e59e7fd36e606623e226c47718</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.0.4/src/kdeadmin-3.0.4.tar.bz2">kdeadmin-3.0.4</a></td>

        <td align="right">1.3MB</td>

        <td><tt>6e93fd31067deab4e08460e84174dd19</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.0.4/src/kdeartwork-3.0.4.tar.bz2">kdeartwork-3.0.4</a></td>

        <td align="right">11MB</td>

        <td><tt>f7a7e75b66620a33680107874fa5a941</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.0.4/src/kdebase-3.0.4.tar.bz2">kdebase-3.0.4</a></td>

        <td align="right">13MB</td>

        <td><tt>d82ddae716912a3195139c8755e5c668</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.0.4/src/kdebindings-3.0.4.tar.bz2">kdebindings-3.0.4</a></td>

        <td align="right">4.9MB</td>

        <td><tt>a21da18ab557d6b26161e4ec883e96b1</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.0.4/src/kdeedu-3.0.4.tar.bz2">kdeedu-3.0.4</a></td>

        <td align="right">8.7MB</td>

        <td><tt>141cea8c0186d6c75fdb4f7a069a48d9</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.0.4/src/kdegames-3.0.4.tar.bz2">kdegames-3.0.4</a></td>

        <td align="right">7.0MB</td>

        <td><tt>5f7ea33c54e68fd673c5c48b49e0c1e3</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.0.4/src/kdegraphics-3.0.4.tar.bz2">kdegraphics-3.0.4</a></td>

        <td align="right">2.6MB</td>

        <td><tt>6065219c825102c843ba582c4a520cac</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.0.4/src/kdelibs-3.0.4.tar.bz2">kdelibs-3.0.4</a></td>

        <td align="right">7.3MB</td>

        <td><tt>0b81f457a78c07978d2c973e0d7d7d49</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.0.4/src/kdemultimedia-3.0.4.tar.bz2">kdemultimedia-3.0.4</a></td>

        <td align="right">5.6MB</td>

        <td><tt>ebb28282a85bd3e06a4d9cd9c7df6537</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.0.4/src/kdenetwork-3.0.4.tar.bz2">kdenetwork-3.0.4</a></td>

        <td align="right">3.8MB</td>

        <td><tt>9f64e76cc6b922e1bf105099e3bf8205</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.0.4/src/kdepim-3.0.4.tar.bz2">kdepim-3.0.4</a></td>

        <td align="right">3.1MB</td>

        <td><tt>0455afeec058386049a46e7ea5ec9363</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.0.4/src/kdesdk-3.0.4.tar.bz2">kdesdk-3.0.4</a></td>

        <td align="right">1.8MB</td>

        <td><tt>13c54f973533f4816069e82f7f375c34</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.0.4/src/kdetoys-3.0.4.tar.bz2">kdetoys-3.0.4</a></td>

        <td align="right">1.4MB</td>

        <td><tt>d39101459a5c5a3e5b241e57882e0c20</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.0.4/src/kdeutils-3.0.4.tar.bz2">kdeutils-3.0.4</a></td>

        <td align="right">1.5MB</td>

        <td><tt>e34ccb00b25f04f0a27474fa17d68e77</tt></td>
      </tr>
    </table>

