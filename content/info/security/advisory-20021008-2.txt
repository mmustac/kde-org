-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1


KDE Security Advisory: kpf Directory traversal
Original Release Date: 2002-10-08
URL: http://www.kde.org/info/security/advisory-20021008-2.txt

0. References


1. Systems affected:

        kpf of any KDE release between KDE 3.0.1 and KDE 3.0.3a. 

2. Overview:
            
        kpf is a file sharing utility that can be docked into the 
        KDE kicker bar. It uses a subset of the HTTP protocol internally
        and acts much similiar to a webserver. 

        A feature added in KDE 3.0.1 accidently allowed retrieving any
        file, not limited to the configured shared directory, if it is
        readable by the user kpf runs under. 

3. Impact:
        
        Files not stored in the shared directory were remotely 
        retrievable. 
   
4. Solution:
        
        The vulnerable feature has been removed. 
         
        Apply the patch listed in section 5 to kdenetwork/kpf, or update
        to KDE 3.0.4. 

        kdenetwork-3.0.4 can be downloaded from

        http://download.kde.org/stable/3.0.4 :
        9f64e76cc6b922e1bf105099e3bf8205  kdenetwork-3.0.4.tar.bz2

5. Patch:

        A patch for KDE 3.0.3 is available from
        
        ftp://ftp.kde.org/pub/kde/security_patches :
        2e8ddbb0d75cd63fd534ec001bb5a415  post-3.0.3-kdenetwork-kpf.diff
  
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.0.7 (GNU/Linux)

iD8DBQE9pD4NvsXr+iuy1UoRAsX7AKChfW49EmYsvodQ1LIvxuQoNsCpDACfale1
iC3TCzTlXxYWZIUdlSPC3tc=
=D/Lf
-----END PGP SIGNATURE-----
