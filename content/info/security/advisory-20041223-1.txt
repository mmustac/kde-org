-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1


KDE Security Advisory: kpdf Buffer Overflow Vulnerability
Original Release Date: 2004-12-23
URL: http://www.kde.org/info/security/advisory-20041223-1.txt

0. References

        http://cve.mitre.org/cgi-bin/cvename.cgi?name=CAN-2004-1125
        http://www.idefense.com/application/poi/display?id=172&type=vulnerabilities


1. Systems affected:

        KDE 3.2 up to including KDE 3.2.3.
        KDE 3.3 up to including KDE 3.3.2.


2. Overview:

        kpdf, the KDE pdf viewer, shares code with xpdf. xpdf contains
        a buffer overflow that can be triggered by a specially 
        crafted PDF file.


3. Impact:

        Remotely supplied pdf files can be used to execute arbitrary
        code on the client machine.


4. Solution:

        Source code patches have been made available which fix these
        vulnerabilities. Contact your OS vendor / binary package provider
        for information about how to obtain updated binary packages.


5. Patch:

        Patch for KDE 3.2.3 is available from 
        ftp://ftp.kde.org/pub/kde/security_patches :

        6f345c4b89f0bc27522f5d62bfd941cd  post-3.2.3-kdegraphics-2.diff

        Patch for KDE 3.3.2 is available from 
        ftp://ftp.kde.org/pub/kde/security_patches :

        0ac92868d3b84284e54877e32cde521f  post-3.3.2-kdegraphics.diff


6. Time line and credits:

        21/12/2004 KDE Security Team alerted by Matthias Geerdsen
        22/12/2004 Patch from xpdf 3.00pl2 applied to KDE CVS and patches
                   prepared.
        23/12/2004 Public disclosure.


-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.2.5 (GNU/Linux)

iD8DBQFBymz5vsXr+iuy1UoRAgtzAJ9XJZax9tSD29d2ax2kfZ7AOUVNVgCg1GmS
1KHQE843oYavbPBPXVNPJFM=
=BiPb
-----END PGP SIGNATURE-----
