---
title: "KDE Applications 17.08.1 Info Page"
announcement: /announcements/announce-applications-17.08.1
layout: applications
signer: Christoph Feck
signing_fingerprint: F23275E4BF10AFC1DF6914A6DBD2CE893E2D1C87
---
