---
title: "KDE Applications 15.11.80 Info Page"
announcement: /announcements/announce-applications-15.12-beta
layout: applications
build_instructions: https://techbase.kde.org/Getting_Started/Build/Historic/KDE4
---
