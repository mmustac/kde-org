---
title: "KDE Applications 14.12.1 Info Page"
announcement: /announcements/announce-applications-14.12.1
layout: applications
build_instructions: https://techbase.kde.org/Getting_Started/Build/Historic/KDE4
---
