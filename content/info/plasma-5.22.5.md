---
version: "5.22.5"
title: "KDE Plasma 5.22.5, bugfix Release"
type: info/plasma5
signer: Jonathan Riddell
signing_fingerprint: EC94D18F7F05997E
draft: false
---

This is a bugfix release of KDE Plasma, featuring Plasma Desktop and
other essential software for your computer. Details in the
[Plasma 5.22.5 announcement](/announcements/plasma/5/5.22.5).
