---
aliases:
- /info/releases-19.12.0
title: "19.12.0 Releases Source Info Page"
announcement: /announcements/releases/19.12
layout: releases
signer: Christoph Feck
signing_fingerprint: F23275E4BF10AFC1DF6914A6DBD2CE893E2D1C87
---
