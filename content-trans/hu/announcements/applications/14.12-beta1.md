---
aliases:
- ../announce-applications-14.12-beta1
custom_spread_install: true
date: '2014-11-06'
description: A KDE elérhetővé teszi az Applications 14.12 Béta 1-et.
layout: application
title: A KDE elérhetővé teszi a KDE Applications 14.12 első bétaverzióját
---
November 6, 2014. Today KDE released the beta of the new versions of KDE Applications. With dependency and feature freezes in place, the KDE team's focus is now on fixing bugs and further polishing.

With the various applications being based on KDE Frameworks 5, the KDE Applications 14.12 releases need a thorough testing in order to maintain and improve the quality and user experience. Actual users are critical to maintaining high KDE quality, because developers simply cannot test every possible configuration. We're counting on you to help find bugs early so they can be squashed before the final release. Please consider joining the team by installing the beta <a href='https://bugs.kde.org/'>and reporting any bugs</a>.

#### A KDE Applications 14.12 Béta 1 bináris csomagjainak telepítése

<em>Csomagok</em>. Néhány Linux/UNIX gyártó biztosít KDE Applications 14.12 Béta 1(belső verziószám: 14.11.80) bináris csomagokat a disztribúciója néhány verziójához, más esetekben a közösség készíthetett. További bináris csomagok és a most elérhető csomagok frissítései válhatnak elérhetővé a következő hetekben.

<em>Package Locations</em>. For a current list of available binary packages of which the KDE Project has been informed, please visit the <a href='http://community.kde.org/KDE_SC/Binary_Packages'>Community Wiki</a>.

#### Compiling KDE Applications 14.12 Beta1

The complete source code for KDE Applications 14.12 Beta1 may be <a href='http://download.kde.org/unstable/applications/14.11.80/src/'>freely downloaded</a>. Instructions on compiling and installing are available from the <a href='/info/applications/applications-14.11.80'>KDE Applications Beta 1 Info Page</a>.
