---
aliases:
- ../announce-applications-15.08.1
changelog: true
date: 2015-09-15
description: KDE, KDE Uygulamaları 15.08.1'i Gönderdi
layout: application
title: KDE, KDE Uygulamaları 15.08.1'i Gönderdi
version: 15.08.1
---
15 Eylül 2015. Bugün KDE, <a href='../15.08.0'>KDE Uygulamaları</a> 15.08 için ilk kararlılık güncellemesini yayınladı. Bu sürüm yalnızca hata düzeltmeleri ve çeviri güncellemelerini içerir, herkes için güvenli ve hoş bir güncelleme sağlar.

More than 40 recorded bugfixes include improvements to kdelibs, kdepim, kdenlive, dolphin, marble, kompare, konsole, ark and umbrello.

This release also includes Long Term Support version of KDE Development Platform 4.14.12.
