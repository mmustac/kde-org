---
aliases:
- ../announce-applications-17.04.3
changelog: true
date: 2017-07-13
description: KDE, KDE Uygulamaları 17.04.3'ü Gönderdi
layout: application
title: KDE, KDE Uygulamaları 17.04.3'ü Gönderdi
version: 17.04.3
---
13 Temmuz 2017. Bugün KDE, <a href='../17.04.0'>KDE Uygulamaları 17.04</a> için üçüncü kararlılık güncellemesini yayınladı. Bu sürüm yalnızca hata düzeltmeleri ve çeviri güncellemelerini içerir, herkes için güvenli ve hoş bir güncelleme sağlar.

More than 25 recorded bugfixes include improvements to kdepim, dolphin, dragonplayer, kdenlive, umbrello, among others.

This release also includes Long Term Support version of KDE Development Platform 4.14.34.
