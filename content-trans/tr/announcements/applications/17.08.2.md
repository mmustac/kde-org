---
aliases:
- ../announce-applications-17.08.2
changelog: true
date: 2017-10-12
description: KDE, KDE Uygulamaları 17.08.2'yi Gönderdi
layout: application
title: KDE, KDE Uygulamaları 17.08.2'yi Gönderdi
version: 17.08.2
---
October 12, 2017. Today KDE released the second stability update for <a href='../17.08.0'>KDE Applications 17.08</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

More than 25 recorded bugfixes include improvements to Kontact, Dolphin, Gwenview, Kdenlive, Marble, Okular, among others.

This release also includes Long Term Support version of KDE Development Platform 4.14.37.

İyileştirmeler şunları içerir:

- Plasma olayları eklenti yapılandırmasında bir bellek sızıntısı ve çökme düzeltildi
- Read messages are no longer removed immediately from Unread filter in Akregator
- Gwenview Importer artık EXIF tarih/saatini kullanıyor
