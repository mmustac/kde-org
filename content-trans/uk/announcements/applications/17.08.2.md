---
aliases:
- ../announce-applications-17.08.2
changelog: true
date: 2017-10-12
description: KDE випущено збірку програм KDE 17.08.2
layout: application
title: KDE випущено збірку програм KDE 17.08.2
version: 17.08.2
---
12 жовтня 2015 року. Сьогодні KDE випущено друге стабільне оновлення <a href='../17.08.0'>набору програм KDE 17.08</a>. До цього випуску включено лише виправлення вад та оновлення перекладів. Його встановлення є безпечним і корисним для усіх користувачів.

Серед принаймні 25 зареєстрованих виправлень вад поліпшення у роботі Kontact, Dolphin, Gwenview, Kdenlive, Marble, Okular та інших програм.

До цього випуску також включено версію з подовженою підтримкою платформи для розробки KDE 4.14.37.

Серед удосконалень:

- Виправлено виток пам'яті та код, який призводив до аварійного завершення роботи, у налаштуваннях додатка подій Плазми
- У новій версії прочитані повідомлення більше не зникають негайно з фільтра непрочитаних у Akregator
- Тепер у засобі імпортування даних Gwenview використовується дата і час з EXIF
