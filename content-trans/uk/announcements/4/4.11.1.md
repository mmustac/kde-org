---
aliases:
- ../announce-4.11.1
date: 2013-09-03
description: KDE випущено робочі простори Плазми, програм і платформи 4.11.1.
title: KDE випущено вересневе оновлення робочих просторів Плазми, програм і платформи
---
3 вересня 2013 року. Сьогодні KDE випущено оновлення робочих просторів, програм та платформи для розробки. Це перше оновлення з послідовності щомісячних оновлень випуску 4.11. Як було оголошено під час випуску, підтримка цієї версії оновленням буде здійснюватися протягом двох років. У новій версії ви зможете скористатися багатьма виправленнями вад та оновленими перекладами щодо випуску 4.11. Оновлення є безпечним і корисним для усіх користувачів.

Серед понад 70 зареєстрованих виправлень вад, зокрема поліпшення у роботі програми для керування вікнами KWin, програми для керування файлами Dolphin та інших програм. Система стільниці Плазми у новій версії запускатиметься швидше, гортання списків у Dolphin буде плавнішим, програми споживатимуть менше пам'яті. Серед поліпшень повернення можливості перетягування зі скиданням з панелі задач на пейджер, виправлення у підсвічуванні і розфарбовуванні у Kate та багато малих виправлень у грі Kmahjongg. Також виправлено багато проблем із стабільністю та розширено покриття перекладами випуску.

З повнішим <a href='https://bugs.kde.org/buglist.cgi?query_format=advanced&amp;short_desc_type=allwordssubstr&amp;short_desc=&amp;long_desc_type=substring&amp;long_desc=&amp;bug_file_loc_type=allwordssubstr&amp;bug_file_loc=&amp;keywords_type=allwords&amp;keywords=&amp;bug_status=RESOLVED&amp;bug_status=VERIFIED&amp;bug_status=CLOSED&amp;emailtype1=substring&amp;email1=&amp;emailassigned_to2=1&amp;emailreporter2=1&amp;emailcc2=1&amp;emailtype2=substring&amp;email2=&amp;bugidtype=include&amp;bug_id=&amp;votes=&amp;chfieldfrom=2011-06-01&amp;chfieldto=Now&amp;chfield=cf_versionfixedin&amp;chfieldvalue=4.11.1&amp;cmdtype=doit&amp;order=Bug+Number&amp;field0-0-0=noop&amp;type0-0-0=noop&amp;value0-0-0='>списком</a> змін можна ознайомитися за допомогою системи стеження за вадами KDE. Крім того, докладний список змін, які було внесено до 4.11.1 можна отримати з журналів відповідної гілки сховища Git.

Посилання на пакунки з початковими кодами та зібраними програмами для встановлення можна знайти на <a href='/info/4/4.11.1'>інформаційній сторінці 4.11.1</a>. Якщо вам хочеться дізнатися більше про версію 4.11 робочих просторів KDE, програм та платформи для розробки, будь ласка, зверніться до <a href='/announcements/4.11/'>нотаток щодо випуску 4.11</a>.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/send-later.png" caption=`Нові можливості з відкладеного надсилання повідомлень у Kontact` width="600px">}}

Програмне забезпечення KDE, зокрема бібліотеки та всі програми, розповсюджується за умов дотримання ліцензійних угод з відкритим доступом до початкового коду. Програмне забезпечення KDE можна отримати у форматі початкових кодів та різноманітних бінарних форматах за посиланнями на сайті <a href='http://download.kde.org/stable/4.11.1/'>download.kde.org</a> або разом з будь-якою з сучасних <a href='/distributions'>поширених систем GNU/Linux та UNIX</a>.
