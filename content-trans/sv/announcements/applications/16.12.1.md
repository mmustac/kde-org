---
aliases:
- ../announce-applications-16.12.1
changelog: true
date: 2017-01-12
description: KDE levererar KDE-program 16.12.1
layout: application
title: KDE levererar KDE-program 16.12.1
version: 16.12.1
---
12:e januari, 2017. Idag ger KDE ut den första stabilitetsuppdateringen av <a href='../16.12.0'>KDE-program 16.12</a>. Utgåvan innehåller bara felrättningar och översättningsuppdateringar, och är därmed en säker och behaglig uppdatering för alla.

Den här utgåvan rättar ett fel som orsakade DATAFÖRLUST i iCal-resursen, som misslyckades skapa std.ics om den inte fanns.

Mer än 40 registrerade felrättningar omfattar förbättringar av bland annat kdepim, ark, gwenview, kajongg, okular, kate och kdenlive.

Utgåvan inkluderar också versioner för långtidsunderhåll av KDE:s utvecklingsplattform 4.14.28.
