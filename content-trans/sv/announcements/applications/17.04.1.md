---
aliases:
- ../announce-applications-17.04.1
changelog: true
date: 2017-05-11
description: KDE levererar KDE-program 17.04.1
layout: application
title: KDE levererar KDE-program 17.04.1
version: 17.04.1
---
11:e maj, 2017. Idag ger KDE ut den första stabilitetsuppdateringen av <a href='../17.04.0'>KDE-program 17.04</a>. Utgåvan innehåller bara felrättningar och översättningsuppdateringar, och är därmed en säker och behaglig uppdatering för alla.

Mer än 20 registrerade felrättningar omfattar förbättringar av bland annat kdepim, dolphin, gwenview, kate, kdenlive.

Utgåvan inkluderar också versioner för långtidsunderhåll av KDE:s utvecklingsplattform 4.14.32.
