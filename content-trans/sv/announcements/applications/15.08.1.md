---
aliases:
- ../announce-applications-15.08.1
changelog: true
date: 2015-09-15
description: KDE levererar KDE-program 15.08.1
layout: application
title: KDE levererar KDE-program 15.08.1
version: 15.08.1
---
15:e september, 2015. Idag ger KDE ut den första stabilitetsuppdateringen av <a href='../15.08.0'>KDE-program 15.08</a>. Utgåvan innehåller bara felrättningar och översättningsuppdateringar, och är därmed en säker och behaglig uppdatering för alla.

Mer än 40 registrerade felrättningar omfattar förbättringar av kdelibs, kdepim, kdenlive, dolphin, marble, kompare, konsole, ark och umbrello.

Utgåvan inkluderar också versioner för långtidsunderhåll av KDE:s utvecklingsplattform 4.14.12.
