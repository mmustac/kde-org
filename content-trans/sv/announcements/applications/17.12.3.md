---
aliases:
- ../announce-applications-17.12.3
changelog: true
date: 2018-03-08
description: KDE levererar KDE-program 17.12.3
layout: application
title: KDE levererar KDE-program 17.12.3
version: 17.12.3
---
8:e mars, 2018. Idag ger KDE ut den tredje stabilitetsuppdateringen av <a href='../17.12.0'>KDE-program 17.12</a>. Utgåvan innehåller bara felrättningar och översättningsuppdateringar, och är därmed en säker och behaglig uppdatering för alla.

Omkring 25 registrerade felrättningar omfattar förbättringar av bland annat Kontact, Dolphin, Gwenview, JuK, KGet, Okular och Umbrello.

Förbättringar omfattar:

- Akregator raderar inte längre kanallistan feeds.opml efter ett fel
- Gwenviews fullskärmsläge hanterar nu rätt filnamn efter namnbyte
- Flera ovanliga krascher i Okular har identifierats och rättats
