---
aliases:
- ../announce-applications-17.12.1
changelog: true
date: 2018-01-11
description: KDE levererar KDE-program 17.12.1
layout: application
title: KDE levererar KDE-program 17.12.1
version: 17.12.1
---
11:e januari, 2018. Idag ger KDE ut den första stabilitetsuppdateringen av <a href='../17.12.0'>KDE-program 17.12</a>. Utgåvan innehåller bara felrättningar och översättningsuppdateringar, och är därmed en säker och behaglig uppdatering för alla.

Omkring 20 registrerade felrättningar omfattar förbättringar av bland annat Kontact, Dolphin, Filelight, Gwenview, KGet, Okteta och Umbrello.

Förbättringar omfattar:

- Att skicka brev i Kontact har rättats för vissa SMTP-servrar
- Gwenviews tidslinje- och etikettsökningar har förbättrats
- Java-import har rättats i UML-diagramverktyget Umbrello
