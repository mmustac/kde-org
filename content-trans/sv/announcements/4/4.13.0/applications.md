---
date: '2014-04-16'
description: KDE levererar Program och Plattform 4.13.
hidden: true
title: KDE-program i 4.13 drar nytta av den nya semantiska sökfunktionen, och introducerar
  nya funktioner
---
KDE-gemenskapen är stolt över att tillkännage den senaste större uppdateringen av KDE:s program, som levererar nya funktioner och felrättningar. Kontact (den personliga informationshanteraren) har utsatts för intensiv aktivitet, drar nytta av förbättringarna av KDE:s teknologi för semantisk sökning, och innehåller nya funktioner. Dokumentvisaren Okular och den avancerade texteditorn Kate har erhållit funktions- och gränssnittsrelaterade förbättringar. Inom utbildnings- och spelområdet introducerar vi det nya programmet Artikulate för uttalsövningar på främmande språk, skrivbordsjordgloben Marble har fått stöd för solen, månen, planeter, cykelvägar och sjömil. Pusselprogrammet Palapeli har tagit språnget till nya dimensioner och möjligheter utan motstycke.

{{<figure src="/announcements/4/4.13.0/screenshots/applications.png" class="text-center" width="600px">}}

## KDE:s Kontact introducerar nya funktioner och mer fart

KDE:s Kontact-svit introducerar en mängd funktioner i dess olika komponenter. Kmail introducerar lagring i molnet och förbättrar Sieve-stöd för filtrering på serversidan. Knotes kan nu skapa alarm och introducerar sökmöjligheter, och många förbättringar har gjorts av datacache-lagret i Kontact, som snabbar upp nästan alla åtgärder.

### Stöd för lagring i molnet

Kmail introducerar stöd för lagringstjänster, så stora bilagor kan lagras med molntjänster, och inkluderas som länkar i brev. Lagringstjänster som stöds omfattar Dropbox, Box, KolabServer, YouSendIt, UbuntuOne, Hubic och det finns ett generellt WebDav-alternativ. Verktyget <em>storageservicemanager</em> hjälper till att hantera filer hos dessa tjänster.

{{<figure src="/announcements/4/4.13.0/screenshots/CloudStorageSupport.png" class="text-center" width="600px">}}

### Stora förbättringar i stödet för Sieve

Sieve-filter, en teknologi som låter Kmail hantera filter på servern, kan nu hantera semesterstöd för flera servrar. Verktyget Ksieveeditor låter användare redigera Sieve-filter utan att behöva lägga till servern i Kontact.

### Övriga ändringar

Snabbfiltreringsraden har en liten förbättring av användargränssnittet och drar stor nytta av de förbättrade sökmöjligheterna som introduceras i utgåva 4.13 av KDE:s utvecklingsplattform. Sökning har blivit väsentligt snabbare, och mer tillförlitlig. Brevfönstret introducerar avkortning av webbadresser, vilket hjälper befintliga verktyg för översättning och textsnuttar.

Etiketter och kommentarer för personlig informationshanteringsdata lagras nu i Akonadi. I framtida versioner kommer de också att lagras på servrar (med IMAP eller Kolab), vilket gör det möjligt att dela etiketter på flera datorer. I Akonadi har stöd för Google Drive programmeringsgränssnittet lagts till. Det finns stöd för att söka med insticksprogram från tredje part (vilket betyder att resultatet kan erhållas mycket snabbt) och sökning på servern (sökbegrepp indexeras inte av en lokal indexeringstjänst).

### Anteckningar och adressboken

Betydande arbete har utförts i Knotes, för att rätta en mängd fel och mindre irritationsmoment. Möjlighet att ange alarm för anteckningar är ny, liksom att söka igenom anteckningar. Läs mer <a href='http://www.aegiap.eu/kdeblog/2014/03/whats-new-in-kdepim-4-13-knotes/'>här</a>. Adressboken har utökats med stöd för utskrift: mer detaljerad information finns <a href='http://www.aegiap.eu/kdeblog/2013/11/new-in-kdepim-4-12-kaddressbook/'>här</a>.

### Prestandaförbättringar

Kontacts prestanda är märkbart förbättrad i den här versionen. Några förbättringar beror på integrering med den nya versionen av KDE:s infrastruktur för <a href='http://dot.kde.org/2014/02/24/kdes-next-generation-semantic-search'>semantisk sökning</a>, och betydande arbete har också utförts för datacache-lagret och inläsning av data i själva Kmail. Märkbart arbete har skett för att förbättra stödet för databasen PostgreSQL. Mer information och detaljer om prestandarelaterade ändringar kan hittas med dessa länkar:

- <li>Lagringsoptimeringar: <a href='http://www.progdan.cz/2013/11/kde-pim-sprint-report/'>arbetsrapport</a>
- Hastighetsförbättring och storleksreduktion för databas: <a href='http://lists.kde.org/?l=kde-pim&amp;m=138496023016075&amp;w=2'>sändlista</a>
- Optimering vid åtkomst av korgar: <a href='https://git.reviewboard.kde.org/r/113918/'>granskningstavla</a>

### Anteckningar och adressboken

Betydande arbete har utförts i Knotes, för att rätta en mängd fel och mindre irritationsmoment. Möjlighet att ange alarm för anteckningar är ny, liksom att söka igenom anteckningar. Läs mer <a href='http://www.aegiap.eu/kdeblog/2014/03/whats-new-in-kdepim-4-13-knotes/'>här</a>. Adressboken har utökats med stöd för utskrift: mer detaljerad information finns <a href='http://www.aegiap.eu/kdeblog/2013/11/new-in-kdepim-4-12-kaddressbook/'>här</a>.

## Okular förfinar användargränssnittet

Den här utgåvan av dokumentläsaren Okular innehåller ett antal förbättringar. Man kan nu öppna flera PDF-filer i en instans av Okular tack vare stöd för flikar. Det finns ett nytt förstoringsläge för musen och den aktuella bildskärmens punkter per tum används för återgivning av PDF, vilket förbättrar dokumentens utseende. En ny uppspelningsknapp är inkluderad i presentationsläget och det har skett förbättringar av åtgärderna för att söka, ångra och göra om.

{{<figure src="/announcements/4/4.13.0/screenshots/okular.png" class="text-center" width="600px">}}

## Kate introducerar en förbättrad statusrad, animerad matchning av parenteser och förbättrade insticksprogram

Den senaste versionen av den avancerade texteditorn Kate introducerar <a href='http://kate-editor.org/2013/11/06/animated-bracket-matching-in-kate-part/'>animerad matchning av parenteser</a>, ändringar för att få <a href='http://dot.kde.org/2014/01/20/kde-commit-digest-5th-january-2014'>tangentbord med Alt Gr att fungera i vim-läge</a> och en serie förbättringar av insticksprogram för Kate, i synnerhet när det gäller stöd för Python och <a href='http://kate-editor.org/2014/03/16/coming-in-4-13-improvements-in-the-build-plugin/'>bygginsticksprogrammet</a>. Det finns en ny, mycket <a href='http://kate-editor.org/2014/01/23/katekdevelop-sprint-status-bar-take-2/'>förbättrad statusrad</a> som gör direkta ändringar möjliga, såsom att ändra indenteringsinställningar, kodning och färgläggning, en ny flikrad i varje vy, kodkompletteringsstöd för <a href='http://kate-editor.org/2014/02/20/lumen-a-code-completion-plugin-for-the-d-programming-language/'>programspråket D</a> och <a href='http://kate-editor.org/2014/02/02/katekdevelop-sprint-wrap-up/'>mycket annat</a>. Teamet har <a href='http://kate-editor.org/2014/03/18/kate-whats-cool-and-what-should-be-improved/'>bett om åsikter om vad som ska förbättras i Kate</a> och skiftar en del av sin uppmärksamhet mot en anpassning till Ramverk 5.

## Diverse funktioner överallt

Terminal ger viss ytterligare flexibilitet genom att tillåta egna stilmallar för att kontrollera flikrader. Profiler kan nu lagra önskad kolumn- och radstorlek. Mer information finns <a href='http://blogs.kde.org/2014/03/16/konsole-new-features-213'>här</a>.

Umbrello gör det möjligt att duplicera diagram och introducerar intelligenta sammanhangsberoende menyer, som anpassar sitt innehåll enligt de markerade komponenterna. Stödet för att ångra, och visuella egenskaper har dessutom förbättrats. Gwenview <a href='http://agateau.com/2013/12/12/whats-new-in-gwenview-4.12/'>introducerar förhandsgranskning av obehandlade bilder</a>.

{{<figure src="/announcements/4/4.13.0/screenshots/marble.png" class="text-center" width="600px">}}

Ljudmixern Kmix introducerar fjärrkontroll via DBUS-protokollet för kommunikation mellan processer (<a href='http://kmix5.wordpress.com/2013/12/28/kmix-dbus-remote-control/'>detaljerad information</a>), tillägg i ljudmenyn och en ny inställningsdialogruta (<a href='http://kmix5.wordpress.com/2013/12/23/352/'>detaljerad information</a>), samt en mängd felrättningar och mindre förbättringar.

Dolphins sökgränssnitt har ändrats för att dra nytta av den nya sökinfrastrukturen och fått ytterligare prestandaförbättringar. För att få detaljerad information, läs följande <a href='http://freininghaus.wordpress.com/2013/12/12/a-brief-history-of-dolphins-performance-and-memory-usage'>översikt över optimeringsarbete under det senaste året</a>.

Hjälpcentralen lägger till alfabetisk sortering av moduler och omorganisering av kategorier för att göra den lättare att använda.

## Spel- och utbildningsprogram

KDE:s spel- och utbildningsprogram har fått många uppdateringar i den här utgåvan. KDE:s pusselspel, Palapeli, har erhållit <a href='http://techbase.kde.org/Schedules/KDE4/4.13_Feature_Plan#kdegames'>käcka nya funktioner</a> som gör det mycket enklare att lägga stora pussel (upp till 10 000 bitar), för de som klarar av utmaningen. Sänka fartyg visar fiendefartygens positioner efter att spelet är slut, så att man kan se vad som gick fel.

{{<figure src="/announcements/4/4.13.0/screenshots/palapeli.png" class="text-center" width="600px">}}

KDE:s utbildningsprogram har fått nya funktioner. Kstars har erhållit ett skriptgränssnitt via D-Bus och kan använda programmeringsgränssnittet hos webbtjänsten astrometry.net för att optimera minnesanvändning. Cantor har erhållit syntaxfärgläggning i sin skripteditor och dess Scilab- och Python-bakgrundsprogram stöds nu i editorn. Utbildningsverktyget Marble för kartvisning och navigering inkluderar nu positionerna för <a href='http://kovalevskyy.tumblr.com/post/71835769570/news-from-marble-introducing-sun-and-the-moon'>solen, månen</a> och <a href='http://kovalevskyy.tumblr.com/post/72073986685/news-from-marble-planets'>planeterna</a>, samt möjliggör <a href='http://ematirov.blogspot.ch/2014/01/tours-and-movie-capture-in-marble.html'>inspelning av filmer under virtuella turer</a>. Cykelfärdvägar är förbättrade med tillägg av stöd för cyclestreets.net. Nautiska mil stöds nu, och att klicka på en <a href='http://en.wikipedia.org/wiki/Geo_URI'>geografisk adress</a> öppnar nu Marble.

#### Installera KDE-program

KDE:s programvara, inklusive alla dess bibliotek och program, är fritt tillgänglig med licenser för öppen källkod. KDE:s programvara kör på diverse hårdvarukonfigurationer och processorarkitekturer, såsom ARM och x86, olika operativsystem och fungerar med alla sorters fönsterhanterare och skrivbordsmiljöer. Förutom Linux och andra UNIX-baserade operativsystem finns det versioner av de flesta KDE-program för Microsoft Windows på <a href='http://windows.kde.org'>KDE:s programvara för Windows</a>, och versioner för Apple Mac OS X på <a href='http://mac.kde.org/'>KDE:s programvara för Mac</a>. Experimentella byggen av KDE-program för diverse mobila plattformar som MeeGo, MS Windows Mobile och Symbian finns på webben, men stöds för närvarande inte. <a href='http://plasma-active.org'>Plasma Aktiv</a> erbjuder användningsmöjlighet på en större omfattning apparater, som läsplattor och annan mobil hårdvara. <br />

KDE:s programvara kan erhållas som källkod och som diverse binärformat från <a href='http://download.kde.org/stable/4.13.0'>download.kde.org</a> och kan också erhållas på <a href='/download'>Cd-rom</a> eller med något av de <a href='/distributions'>större GNU/Linux- och UNIX-systemen</a> som levereras idag.

##### Paket

Vissa Linux- och UNIX-operativsystemleverantörer har vänligen tillhandahållit binärpaket av 4.13.0 för vissa versioner av sina distributioner, och i andra fall har volontärer i gemenskapen gjort det.

##### Paketplatser

Besök <a href='http://community.kde.org/KDE_SC/Binary_Packages#KDE_SC_4.13.0'>Gemenskapens Wiki</a> för en aktuell lista med tillgängliga binärpaket som har kommit till KDE-projektets kännedom.

Fullständig källkod för 4.13.0 kan <a href='/info/4/4.13.0'>laddas ner fritt</a>. Instruktioner om hur man kompilerar och installerar KDE:s programvara 4.13.0 är tillgängliga på <a href='/info/4/4.13.0#binary'>informationssidan om 4.13.0</a>.

#### Systemkrav

För att få ut så mycket som möjligt av dessa utgåvor, rekommenderar vi att använda en aktuell version av Qt, såsom 4.8.4. Det är nödvändigt för att försäkra sig om en stabil och högpresterande upplevelse, eftersom vissa förbättringar som har gjorts av KDE:s programvara har i själva verket skett i det underliggande Qt-ramverket.

För att helt dra nytta av kapaciteten i KDE:s programvara, rekommenderar vi också att använda de senaste grafikdrivrutinerna för systemet, eftersom det avsevärt kan förbättra användarupplevelsen både när det gäller tillvalsfunktioner, och övergripande prestanda och stabilitet.
