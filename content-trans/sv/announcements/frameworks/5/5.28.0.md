---
aliases:
- ../../kde-frameworks-5.28.0
date: 2016-11-15
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Nytt ramverk: syntax-highlighting

Gränssnitt för syntaxfärgläggning för Kate syntaxdefinitioner

Det är en fristående implementering av gränssnittet för syntaxfärgläggning i Kate. Det är avsett som ett byggblock för texteditorer samt för enkel återgivning av text med färgläggning (t.ex. som HTML), både med stöd för integrering med en egen editor och för en delklass,  QSyntaxHighlighter, redo att använda.

### Breeze-ikoner

- Uppdatera åtgärdsikoner för kstars (fel 364981)
- Breeze Mörk listas som Breeze i systeminställningarnas felaktiga .themes-fil (fel 370213)

### Extra CMake-moduler

- Gör så att KDECMakeSettings fungerar med KDE_INSTALL_DIRS_NO_DEPRECATED
- Kräv inte beroende av Python-bindningar för ECM
- Lägg till modulen PythonModuleGeneration

### KActivitiesStats

- Ignorera länkstatus vid sortering av modellerna UsedResources och LinkedResources

### KDE Doxygen-verktyg

- [CSS] återställ ändringarna gjorda av doxygen 1.8.12
- Lägg till filen doxygenlayout
- Uppdatera sättet att definiera gruppnamn

### KAuth

- Säkerställ att vi kan göra mer än en begäran
- Säkerställ att vi får reda på förloppet genom att läsa programmets utmatning

### KConfig

- Säkerställ att vi inte förstör kompilering med tidigare felaktiga enheter
- Inget allvarligt fel när fältet File inte tolkas riktigt

### KCoreAddons

- Visa felaktig webbadress
- Läs in användaravatarer från AccountsServicePath om den finns (fel 370362)

### KDeclarative

- [QtQuickRendererSettings] Rätta standardvärde så att det är tomt istället för "false"

### Stöd för KDELibs 4

- Gör så att Frankrikes flagga använder hela bildpunktsavbildningen

### KDocTools

- Rätta 'checkXML5 skriver ut genererad HTML i arbetskatalog för giltiga docbooks' (fel 371987)

### KIconThemes

- Stöd för andra skalfaktorer än heltal i kiconengine (fel 366451)

### KIdleTime

- Inaktivera att terminalutmatningen fylls med 'väntar på'-meddelanden

### KImageFormats

- imageformats/kra.h: Överskrider KraPlugin capabilities() och create()

### KIO

- Rätta HTTP-datumformat som skickas av kio_http att alltid använda C-landsinställningar (fel 372005)
- KACL: Rätta minnesläckor detekterade av ASAN
- Rätta minnesläckor i KIO::Scheduler, detekterade av ASAN
- Ta bort duplicerad rensningsknapp (fel 369377)
- Rätta redigering av poster för automatisk start när /usr/local/share/applications inte finns (fel 371194)
- [KOpenWithDialog] Dölj trädvyns rubrik
- Sanera storleken på namnbufferten för symboliska länkar (fel 369275)
- Avsluta Dropjobs på ett riktigt sätt när triggered inte skickas (fel 363936)
- ClipboardUpdater: Rätta en annan krasch på Wayland (fel 359883)
- ClipboardUpdater: Rätta krasch på Wayland (fel 370520)
- Stöd skalfaktorer som inte är heltal i KFileDelegate (fel 366451)
- kntlm: Skilj på NULL och tom domän
- Visa inte överskrivningsdialogrutan om filnamnet är tomt
- kioexec: Använd vänliga filnamn
- Rätta ägarskap av fokus om webbadressen ändras innan den grafiska komponenten visas
- Större prestandaförbättring när förhandsgranskningar stängs av i fildialogrutan (fel 346403)

### KItemModels

- Lägg till Python-bindningar

### KJS

- Exportera FunctionObjectImp, använd av avlusaren i KHTML

### KNewStuff

- Skilj på sorteringsroller och filter
- Gör det möjligt att fråga efter installerade poster

### KNotification

- Avreferera inte ett objekt som vi inte har refererat när underrättelsen inte har någon åtgärd
- KNotification kraschar inte längre vid användning i en QGuiApplication och ingen tjänst för underrättelser kör (fel 370667)
- Rätta krascher i NotifyByAudio

### Ramverket KPackage

- Säkerställ att vi både tittat på json- och skrivbordsmetadata
- Skydda mot att Q_GLOBAL_STATIC förstörs när program avslutas
- Rätta åtkomst av felaktig pekare i KPackageJob (fel 369935)
- Ta bort upptäckt associerad med en nyckel när en definition tas bort
- Skapa ikonen in i appstream-filen

### KPty

- Använd ulog-helper på FreeBSD istället för utempter
- Sök grundligare efter utempter också med användning av grundläggande cmake-prefix
- Provisorisk lösning för fel i find_program ( utempter ...)
- Använd ECM-sökväg för att hitta binärfilen utempter, mer tillförlitlig än enkelt CMake-prefix

### Kör program

- i18n: Hantera strängar i kdevtemplate-filer

### KTextEditor

- Breeze mörk: Gör bakgrunden för aktuell rad mörkare för bättre läsbarhet (fel 371042)
- Sorterade Dockerfile-instruktioner
- Breeze (mörk): Gör kommentarer något ljusare för bättre läsbarhet (fel 371042)
- Rätta indentering för CStyle och C++/boost när automatiska parenteser är aktiverade (fel 370715)
- Lägg till modeline 'auto-brackets'
- Rätta infogning av text efter filslut (ovanligt fall)
- Rätta ogiltiga XML-färgläggningsfiler
- Maxima: Ta bort hårdkodade färger, rätta beteckning för itemData
- Lägg till syntaxdefinitioner för OBJ, PLY och STL
- Lägg till stöd för syntaxfärgläggning av Praat

### KUnitConversion

- Nya termiska och elektriska enheter och enhetskonverteringsfunktioner

### Ramverket KWallet

- Om Gpgmepp inte hittas, försök använda KF5Gpgmepp
- Använd Gpgmepp från GpgME-1.7.0

### Kwayland

- Förbättrad relokeringsbarhet för CMake-export
- [tools] Rätta generering av wayland_pointer_p.h
- [tools] Skapa bara eventQueue metoder för globala klasser
- [server] Rätta krasch vid uppdatering av fokuserad tangentbordsyta
- [server] Rätta möjlig krasch när DataDevice skapas
- [server] Säkerställ att det finns en DataSource för DataDevice i setSelection
- [tools/generator] Förbättra resursdestruktion på serversidan
- Lägg till begäran att ha fokus i PlasmaShellSurface för panelen Role
- Lägg till stöd för att automatiskt dölja panel i gränssnittet PlasmaShellSurface
- Stör att skicka en generell QIcon via gränssnittet PlasmaWindow
- [server] Implementera den generella fönsteregenskapen i QtSurfaceExtension
- [client] Lägg till metoder för att hämta ShellSurface från ett QWindow
- [server] Skicka pekarhändelser till alla wl_pointer resurser i en klient
- [server] Anropa inte wl_data_source_send_send om DataSource är obunden
- [server] Använd deleteLater när en ClientConnection destrueras (fel 370232)
- Implementera stöd för det relativa pekarprotokollet
- [server] Avbryt tidigare markering från SeatInterface::setSelection
- [server] Skicka tangenthändelser till alla wl_keyboard resurser i en klient

### KWidgetsAddons

- Flytta kcharselect-generate-datafile.py till underkatalogen src
- Importera kcharselect-generate-datafile.py skriptet med historik
- Ta bort föråldrad sektion
- Lägg till Unicode copyright- och rättighetsdeklaration
- Rätta varning: Saknar överskrid
- Lägg till symbol SMP-block
- Rätta referenser till "Se också"
- Lägg till saknade Unicode-block, förbättra ordning (fel 298010)
- Lägg till teckenkategorier i datafilen
- Uppdatera Unicode-kategorierna i datafilgenereringsskriptet
- Justera datafilgenereringsfilen för att kunna tolka unicode 5.2.0 datafiler
- Konvertera rättningen för att skapa översättningar framåt
- Låt skriptet för att skapa datafilen för kcharselect också skriva en översättningsmall
- Lägg till skriptet för att skapa datafilen för KCharSelect
- Ny KCharSelect program (som nu använder den grafiska komponenten kcharselect från kdelibs)

### KWindowSystem

- Förbättrad relokeringsbarhet för CMake-export
- Lägg till stöd av desktopFileName i NETWinInfo

### KXMLGUI

- Tillåt användning av ny typ av anslutning i KActionCollection::add<a href="">Action</a>

### ModemManagerQt

- Rätta deklarationskatalog i pri-fil

### NetworkManagerQt

- Rätta deklarationskatalog i pri-fil
- Rätta moc-fel på grund av att Q_ENUMS används i en namnrymd, med Qt-grenen 5.8

### Plasma ramverk

- Säkerställ att skärmvisning inte har flaggan Dialog (fel 370433)
- Ställ in sammanhangsegenskaper innan QML läses in igen (fel 371763)
- Tolka inte metadatafilen igen om den redan har lästs in
- Rätta krasch i qmlplugindump när igen QApplication är tillgänglig
- Visa inte normalt menyn "Alternativ"
- Ny bool för att använda aktiverad signal för att byta värde på expanderad (fel 367685)
- Rättningar för byggning av plasma-framework med Qt 5.5
- [PluginLoader] Använd operator&lt;&lt; för finalArgs istället för en initieringslista
- Använd kwayland för skuggor och positionering av dialogrutor
- Återstående ikoner som saknas och nätverksförbättringar
- Flytta availableScreenRect/Region upp till AppletInterface
- Ladda inte omgivningsåtgärder för inbäddade omgivningar (systembrickor)
- Uppdatera synlighet för miniprogrammens menyvärde Alternativ på begäran

### Solid

- Rätta instabil ordning av frågeresultat återigen
- Lägg till en CMake-alternativ för att byta mellan HAL- och UDisks-hanterare på FreeBSD
- Gör så att UDisks2 gränssnittet kompilerar på FreeBSD (och, möjligtvis, andra UNIX-varianter)
- Windows: Visa inte feldialogrutor (fel 371012)

### Säkerhetsinformation

Den utgivna koden GPG-signerad genom att använda följande nyckel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primärt nyckelfingeravtryck: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Det går att diskutera och dela med sig av idéer om den här utgåvan via kommentarssektionen i <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artikeln på Dot</a>.
