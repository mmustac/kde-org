---
aliases:
- ../../kde-frameworks-5.52.0
date: 2018-11-10
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Laga byggning med BUILD_QCH=TRUE
- Använd verkligen fileNameTerms och xAttrTerms
- [Balooshow] Undvik åtkomst utanför gränser när felaktig databasdata används
- [Extractor] Kontrollera inte QFile::exists för en tom webbadress
- [Scheduler] Använd flagga för att följa när ett körprogram blir overksamt
- [Extractor] Hantera dokument riktigt när Mime-typen inte ska indexeras
- [Scheduler] Rätta användning av föråldrad QFileInfo::created() tidsstämpel (fel 397549)
- [Extractor] Gör extrahering kraschtålig (fel 375131)
- Skicka FileIndexerConfig som const till de enskilda indexerarna
- [Config] Ta bort stöd för KDE4-inställningar, sluta skriva godtyckliga inställningsfiler
- [Extractor] Förbättra kommandoradsfelsökning, vidarebefordra standardfelutmatning
- [Scheduler] Återanvänd filinformation från FilteredDirIterator
- [Scheduler] Återanvänd Mime-type från UnindexedFileIterator i indexerare
- [Scheduler] Ta bort onödig variabel m_extractorIdle
- Utför kontroller av oindexerade filer och gamla indexposter vid start
- [balooctl] Skriv ut aktuellt tillstånd och indexfil när bevakning startar (fel 364858)
- [balooctl] Bevaka också tillståndsändringar
- [balooctl] Rätta kommandot "index" för redan indexerad, men flyttad fil (fel 397242)

### BluezQt

- Lägg till generering av deklarationsfiler för Media och MediaEndpoint programmeringsgränssnitt

### Breeze-ikoner

- Ändra pakethanterarens ikoner till emblem
- Lägg till svartvit länkikon som åtgärd igen
- Förbättra kontrast. läsbarhet och konsekvens för emblem (fel 399968)
- Stöd "ny" Mime-typ för .deb-filer (fel 399421)

### Extra CMake-moduler

- ECMAddQch: Hjälp doxygen genom att fördefiniera fler Q*DECL** makron
- Bindningar: Stöd användning av sys-sökvägar för Pythons installationskatalog
- Bindningar: Ta bort INSTALL_DIR_SUFFIX från ecm_generate_python_binding
- Lägg till stöd för inexakta sanitizer

### KCMUtils

- Stöd för inställningsmoduler med flera sidor

### KConfig

- Lägg till mekanism för att underrätta andra klienter av inställningsändringar via D-Bus
- Exponera hämtningsmetoder för KConfig::addConfigSources

### KConfigWidgets

- Tillåt Hjälpcentralen att öppna rätt sidor i KDE:s hjälp när KHelpClient startas med ett grepp

### KCrash

- KCrash: Rätta krasch (jovisst, ironiskt) när det används i ett program utan QCoreApplication

### KDeclarative

- Gör push/pop till en del av programmeringsgränssnittet för ConfigModule

### KDED

- Ta bort oanvändbart meddelande "No X-KDE-DBus-ServiceName found"

### KDesignerPlugin

- Referera till produkten "KF5" istället för "KDE" i metadata för grafiska komponenter

### KDocTools

- Dokumentation av programmeringsgränssnitt: Lägg till minimalt dokument i namnrymden för KDocTools, så att doxygen tar hand om det
- Skapa valfritt en QCH-fil med dokumentation av programmeringsgränssnitt, med användning av ECMAddQCH
- Vänta på att docbookl10nhelper byggs innan våra egna manualsidor byggs
- Använd angiven Perl-tolk istället för att lita på PATH

### KFileMetaData

- [ExtractorCollection] Använd bara extraheringsinsticksprogram som matchar bäst
- [KFileMetaData] Lägg till extrahering för generell XML och SVG
- [KFileMetaData] Lägg till hjälpprogram för XML-kodad Dublin Core-metadata
- Implementera stöd för att läsa ID3-taggar från aiff- och wav-filer
- Implementera fler taggar för asf-metadata
- Extrahera ape-taggar från ape- och wavpack-filer
- Tillhandahåll en lista över Mime-typer som stöds för embeddedimagedata
- Jämför med QLatin1String och harmonisera hantering av alla typer
- Krascha inte för ogiltig exiv2-data (fel 375131)
- epubextractor: Lägg till egenskapen ReleaseYear
- Omstrukturera taglibextractor till funktioner specifika för metadatatyper
- Lägg till wma-filer och asf-taggar som stödda Mime-typer
- Använd egen extrahering för att utprova taglibwriter
- Lägg till ett strängsuffix för testdata och användning vid utprovning av taglibwriter för Unicode
- Ta bort kontroll vid kompileringstillfälle av taglib-version
- Utöka testtäckningsgrad till alla Mime-typer som stöds av taglibextractor
- Använd variabler med text som redan hämtats istället för att hämta den igen

### KGlobalAccel

- Rätta underrättelser om ändring av tangentbordslayout (fel 269403)

### KHolidays

- Lägg till helgfil för Bahrein
- Gör så att KHolidays också fungerar som ett statiskt bibliotek

### KIconThemes

- Lägg till en QIconEnginePlugin för att tillåta avserialisering av QIcon (fel 399989)
- [KIconLoader] Ersätt heap-tilldelad QImageReader med en stack-tilldelad
- [KIconLoader] Justera emblemkant beroende på ikonstorlek
- Centrera ikoner på ett riktigt sätt om storleken inte passar (fel 396990)

### KIO

- Försök  inte använda "mindre säkra" SSL-protokoll som reserv
- [KSambaShare] Trimma bort / sist i delningssökväg
- [kdirlistertest] Vänta lite längre på att listningen ska bli färdig
- Visa meddelande med ursäkt om filen inte är lokal
- kio_help: Rätta krasch i QCoreApplication vid åtkomst av help:// (fel 399709)
- Undvik att vänta på användaråtgärder när förhindra stöld av fokus i Kwin är hög eller extrem
- [KNewFileMenu] Öppna inte en tom QFile
- Tillägg av saknade ikoner i platspanelen från KIO
- Bli av med de obehandlade pekarna till KFileItem i KCoreDirListerCache
- Lägg till alternativet 'Montera' för avmonterade enheter i Platser
- Lägg till alternativet 'Egenskaper' i den sammanhangsberoende menyn i platspanelen
- Rätta varning "macro expansion producing 'defined' has undefined behavior"

### Kirigami

- Rätta saknade objekt i statisk byggning
- Grundstöd för dolda sidor
- Läs in ikoner från riktiga ikonteman
- (många andra rättningar)

### KNewStuff

- Användbarare felmeddelanden

### KNotification

- Rätta en krasch som orsakades av felaktig livstidshantering av canberra-baserad ljudunderrättelse (fel 398695)

### KParts

- Rätta att Avbryt inte hanteras i föråldrade BrowserRun::askEmbedOrSave
- Konvertera till icke-föråldrad variant av KRun::runUrl
- Konvertera KIO::Job::ui() -&gt; KJob::uiDelegate()

### Kwayland

- Lägg till virtuellt skrivbordsprotokoll KWayland
- Skydda mot att datakälla tas bort innan mottagningshändelse för dataerbjudande behandlas (fel 400311)
- [server] Respektera inmatningsområde för delytor vid fokus av pekaryta
- [xdgshell] Lägg till operatorer för begränsningsjusteringsflaggor i positioneringen

### KWidgetsAddons

- Dokumentation av programmeringsgränssnitt: Rätta "Since" anmärkning för KPageWidgetItem::isHeaderVisible
- Lägg till en ny egenskap headerVisible

### KWindowSystem

- Jämför  inte iteratorer returnerade från två separata returnerade kopior

### KXMLGUI

- Ta 1..n KMainWindows i kRestoreMainWindows

### NetworkManagerQt

- Lägg till saknade inställningsalternativ för ipv4
- Lägg till inställning av vxlan

### Plasma ramverk

- Ångra ikonskalning för mobilenheter
- Stöd mnemoniska beteckningar
- Ta bort alternativet PLASMA_NO_KIO
- Leta efter reservteman på ett riktigt sätt

### Syfte

- Ställ in flaggan Dialog för jobDialog

### Solid

- [solid-hardware5] Lista ikon i enhetsinformation
- [UDisks2] Stäng av diskenhet när den tas bort om det stöds (fel 270808)

### Sonnet

- Rätta söndrig språkgissning

### Syndikering

- Lägg till saknad fil README.md (behövs av diverse skript)

### Syntaxfärgläggning

- z/OS CLIST-fil syntaxfärgläggning
- Skapar ny fil med syntaxfärgläggning för Job Control Language (JCL)
- Ta bort öppningsläge från för nya Qt-versioner
- Inkrementera version och rätta version av Kate som krävs till aktuell ramverksversion
- Nyckelordsregel: Stöd för att inkludera nyckelord från ett annat språk eller fil
- Ingen stavningskontroll av Metamath utom i kommentarer
- CMake: Lägg till XCode-relaterade variabler och egenskaper introducerade i 3.13
- CMake: Introducera nya funktioner från kommande utgåva 3.13

### Säkerhetsinformation

Den utgivna koden GPG-signerad genom att använda följande nyckel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primärt nyckelfingeravtryck: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Det går att diskutera och dela med sig av idéer om den här utgåvan via kommentarssektionen i <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artikeln på Dot</a>.
