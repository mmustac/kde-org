---
aliases:
- ../../kde-frameworks-5.19.0
date: 2016-02-13
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Attica

- Förenkla uppslagning och initiering av insticksprogrammet Attica

### Breeze-ikoner

- Många nya ikoner
- Lägg till saknade ikoner för Mime-typer från Oxygen ikonuppsättning

### Extra CMake-moduler

- ECMAddAppIcon: Använd absolut sökväg för ikonåtgärder
- Säkerställ att prefixet slås upp på Android
- Lägg till modulen FindPoppler
- Använd PATH_SUFFIXES i ecm_find_package_handle_library_components()

### KActivities

- Anropa inte exec() från QML (fel 357435)
- Biblioteket KActivitiesStats är nu i ett separat arkiv

### KAuth

- Utför också preAuthAction för gränssnitt med AuthorizeFromHelperCapability
- Rätta D-Bus tjänstnamn för polkit-modul

### KCMUtils

- Rätta problem med hög upplösning i KCMUtils

### KCompletion

- Metoden KLineEdit::setUrlDropsEnabled kan inte markeras som föråldrad

### KConfigWidgets

- Lägg till färgschemat "Komplementärt" i kcolorscheme

### KCrash

- Uppdatera dokumentation för KCrash::initialize. Programutvecklare uppmuntras att anropa det explicit.

### KDeclarative

- Rensa beroenden för KDeclarative/QuickAddons
- [KWindowSystemProxy] Lägg till set-funktion för showingDesktop
- DropArea: Rätta att dragEnter händelse med preventStealing ignoreras på ett riktigt sätt
- DragArea: Implementera greppa delegatobjekt
- DragDropEvent: Lägg till funktionen ignore()

### KDED

- Återställ fixen för BlockingQueuedConnection, Qt 5.6 kommer att innehålla en bättre lösning
- Gör så att kded registreras med alias angivna av modulerna i kded

### Stöd för KDELibs 4

- kdelibs4support kräver kded (för kdedmodule.desktop)

### KFileMetaData

- Gör det möjligt att fråga efter en fils ursprungliga webbadress

### KGlobalAccel

- Förhindra krasch om D-Bus inte är tillgänglig

### KDE GUI Addons

- Rätta listning av tillgängliga paletter i färgdialogrutan

### KHTML

- Rätta detektering av ikonens länktyp (också känt som "favoritikon")

### KI18n

- Reducera användning av programmeringsgränssnittet för gettext

### KImageFormats

- Lägg till insticksprogrammen kra och ora för in-och utmatning av bilder (skrivskyddade)

### KInit

- Ignorera viewport för aktuellt skrivbord vid initiering av startinformation
- Konvertera klauncher till xcb
- Använd xcb för interaktion med KStartupInfo

### KIO

- Ny klass FavIconRequestJob i nytt bibliotek KIOGui, för hämtning av favoritikoner
- Rätta krasch i KDirListerCache med två lyssnare på en tom katalog i cachen (fel 278431)
- Gör så att Windows implementering av KIO::stat för file:/ protocol ger ett fel om filen inte finns
- Anta inte att filer i skrivskyddade kataloger inte kan tas bort på Windows
- Rätta .pri-fil för KIOWidgets: Den beror på KIOCore, inte på sig själv
- Reparera kcookiejar automatisk laddning, värdena byttes i 6db255388532a4
- Gör det möjligt att komma åt kcookiejar med D-Bus tjänstnamnet org.kde.kcookiejar5
- kssld: Installera D-Bus tjänstfil för org.kde.kssld5
- Tillhandahåll en D-Bus tjänstfil för org.kde.kpasswdserver
- [kio_ftp] Rätta visning av ändringsdatum och tid för filer och kataloger (fel 354597)
- [kio_help] Rätta skräp som skickas vid hantering av statiska filer
- [kio_http] Prova behörighetskontroll med NTLMv2 om servern nekar till NTLMv1
- [kio_http] Rätta konverteringsfel som förstörde cachelagring
- [kio_http] Rätta hur NTLMv2 steg 3-svar skapas
- [kio_http] Rätta väntan tills cacherensning lyssnar på uttaget
- kio_http_cache_cleaner: Avsluta inte vid start om cachekatelogen inte finns ännu
- Ändra D-Bus namn på kio_http_cache_cleaner så att den inte avslutas om den från kde4 kör

### KItemModels

- KRecursiveFilterProxyModel::match: Rätta krasch

### KJobWidgets

- Rätta krasch i KJob-dialogrutor (fel 346215)

### Paketet Framework

- Undvik att samma paket hittas flera gånger med olika sökvägar

### KParts

- PartManager: Sluta följa en grafisk komponent även om den inte längre är på toppnivå (fel 355711)

### KTextEditor

- Bättre beteende för funktionen "infoga parenteser omkring" med automatiska parenteser
- Ändra alternativtangenten för att tvinga fram nytt standardvärde, nyrad vid filslut = sant
- Ta bort några suspekta anrop till setUpdatesEnabled (fel 353088)
- Fördröj att skicka verticalScrollPositionChanged till alla grejer överensstämmer för vikning (fel 342512)
- Programfix för uppdatering av etikettersättning (fel 330634)
- Uppdatera bara paletten en gång för ändringshändelsen som hör till qApp (fel 358526)
- Lägg normalt till nyrad vid filslut (EOF)
- Lägg till syntaxfärgläggning för NSIS

### Ramverket KWallet

- Duplicera fildeskriptorn när filen öppnas för att läsa miljön

### KWidgetsAddons

- Rätta hur grafiska kompiskomponenter fungerar med KFontRequester
- KNewPasswordDialog: Använd KMessageWidget
- Förhindra krasch vid avslutning i KSelectAction::~KSelectAction

### KWindowSystem

- Ändra licenshuvud från "Library GPL 2 or later" till "Lesser GPL 2.1 or later"
- Rätta krasch om KWindowSystem::mapViewport anropas utan en QCoreApplication
- Lagra QX11Info::appRootWindow i eventFilter (fel 356479)
- Bli av med beroende av QApplication (fel 354811)

### KXMLGUI

- Lägg till alternativ för att inaktivera KGlobalAccel vid kompileringstillfället
- Reparera sökväg till programmens genvägssystem
- Rätta listning av genvägsfiler (felaktig användning av QDir)

### NetworkManagerQt

- Kontrollera anslutningstillstånd och andra egenskaper för att vara säker på att de är aktuella (version 2) (fel 352326)

### Oxygen-ikoner

- Ta bort felaktiga länkade filer
- Lägg till programikoner från KDE-program
- Lägg till Breeze platsikoner i Oxygen
- Synkronisera Oxygen Mime-typ ikoner med Breeze Mime-typ ikoner

### Plasma ramverk

- Lägg till egenskapen separatorVisible
- Mer explicit borttagning från m_appletInterfaces (fel 358551)
- Använd complementaryColorScheme från KColorScheme
- AppletQuickItem: Försök inte ställa in ursprunglig storlek större än överliggande storlek (fel 358200)
- IconItem: Lägg till egenskapen usesPlasmaTheme
- Läs inte in verktygslådan för typer som inte är desktop eller panel
- IconItem: Försök att läsa in QIcon::fromTheme-ikoner som SVG (fel 353358)
- Ignorera kontroll om bara en del av storleken är noll i compactRepresentationCheck (fel 358039)
- [Units] Returnera åtminstone 1 ms för tidslängder (fel 357532)
- Lägg till clearActions() för att ta bort alla gränssnittsåtgärder för miniprogram
- [plasmaquick/dialog] Använd inte KWindowEffects för fönstertypen Notification
- Avråd från användning av Applet::loadPlasmoid()
- [PlasmaCore DataModel] Återställ inte modellen när en källa tas bort
- Rätta marginaltips i SVG med ogenomskinlig panelbakgrund
- IconItem: Lägg till egenskapen animated
- [Unity] Skalas skrivbordets ikonstorlek
- Knappen är komponera-över-kanter
- paintedWidth/paintedheight för IconItem

Det går att diskutera och dela med sig av idéer om den här utgåvan via kommentarssektionen i <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artikeln på Dot</a>.
