---
date: 2013-08-14
hidden: true
title: Platforma KDE 4.11 zapewnia lepszą wydajność
---
Platforma KDE 4 znajduje się w stanie zamrożenia cech od czasu wydania 4.9. Wobec tego wersja ta zawiera tylko kilka poprawek i wzrostów wydajności.

Semantyczna przechowalnia i silnik wyszukiwania Nepomuk otrzymał wiele poprawek wydajności, takich jak zestaw optymalizacji czytania, które czynią czytanie danych do sześciu razy szybszym. Indeksowanie stało się bardziej inteligentne, gdy zostało podzielone na dwa etapy.  W pierwszym etapie natychmiastowo pobierane są informacje ogólne (takie jak rodzaj pliku i nazwa); dodatkowe informacje, takie jak znaczniki mediów, informacje o autorze, itp. są wydobywane w drugim, w pewnym sensie wolniejszym etapie.  Wyświetlanie metadanych nowo utworzonych lub świeżo pobranej treści jest teraz znacznie szybsze. Dodatkowo, programiści Nepomuka ulepszyli system tworzenia i przywracania kopi zapasowej. Ostatnie, ale nie mniej ważne: Nepomuk może od teraz indeksować rozmaite formaty dokumentów włączając w to ODF oraz docx.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/dolphin.png" caption=`Funkcje semantyczne w akcji w Dolphinie` width="600px">}}

Zoptymalizowany format przechowywania Nepomuka i przepisany program indeksujący pocztę pociągają za sobą potrzebę ponownego zaindeksowania niektórych treści na dysku twardym. Wobec tego ponowne indeksowanie pochłonie niezwykłe ilości wydajności obliczeniowej przez pewien okres - w zależności od ilości treści, które będzie trzeba ponownie zaindeksować. Samoczynne przekształcenie bazy danych Nepomuka nastąpi przy najbliższym zalogowaniu.

Istnieje więcej pomniejszych poprawek, które <a href='https://projects.kde.org/projects/kde/kdelibs/repository/revisions?rev=KDE%2F4.11'>można znaleźć w dziennikach git</a>.

#### Instalowanie Platformy Programistycznej KDE

Oprogramowanie KDE, włączając w to wszystkie jego biblioteki i aplikacje, jest dostępne na warunkach Wolnego Oprogramowania. Oprogramowanie KDE działa na rozmaitych konfiguracjach sprzętowych i architekturach procesora, takich jak ARM i x86, systemach operacyjnych oraz działa z każdym rodzajem menadżera okien czy otoczeniem pulpitu. Poza Linuksem i innymi, opartymi o UNIKSA, systemami operacyjnymi można też znaleźć wersję na Microsoft Windows na stronie <a href='http://windows.kde.org'>Oprogramowanie KDE na Windows</a> i wersję dla Apple Mac OS X na stronie <a href='http://mac.kde.org/'>Oprogramowanie KDE na Mac</a>. Eksperymentalne wydania aplikacji KDE, dla różnych platform przenośnych, takich jak MeeGo, MS Windows Mobile i Symbian można znaleźć w sieci, aczkolwiek są one teraz niewspierane. <a href='http://plasma-active.org'>Plasma Active</a> jest środowiskiem użytkownika dla szerokiego zakresu urządzeń, takich jak komputery typu tablet i inne przenośne.

Oprogramowanie KDE można pobrać w postaci źródła i różnych formatów binarnych z <a href='http://download.kde.org/stable/Szkieletów KDE %1'> download.kde.org</a>, ale można je także uzyskać na  <a href='/download'>CD-ROM</a>lub w każdym <a href='/distributions'>znaczącym systemie operacyjnym GNU/Linuks i UNIX</a> obecnie dostępnym.

##### Pakiety

Niektórzy wydawcy Linux/UNIX OS uprzejmie dostarczyli pakiety binarne Szkieletów KDE %[1] dla niektórych wersji ich dystrybucji, a w niektórych przypadkach dokonali tego wolontariusze społeczności. <br />

##### Położenie pakietów

Po bieżącą listę dostępnych pakietów binarnych, o których został poinformowany Zespół wydawniczy KDE, proszę zajrzyj na stronę <a href='http://community.kde.org/KDE_SC/Binary_Packages#KDE_4.11.0'> Wiki Społeczności</a>.

Pełny kod źródłowy dla Szkieletów KDE %[1] można <a href='/info/4/4.11.0'> pobrać za darmo</a>. Instrukcje dotyczące kompilowania i wgrywania oprogramowania KDE Szkieletów KDE %[1] są dostępne na <a href='/info/4/4.11.0#binary'>Stronie informacyjnej Szkieletów KDE %[1]</a>.

#### Wymagania systemowe

Aby uzyskać najwięcej z tych wydań, zalecamy użycie najnowszej wersji Qt, takiej jak 4.8.4. Jest to potrzebne, aby zapewnić odczucie stabilności i wydajności, jako iż pewne ulepszenia poczynione w oprogramowaniu KDE, w rzeczywistości zostały dokonane w strukturze programistycznej Qt.<br />Aby w pełni wykorzystać możliwości oprogramowania KDE, zalecamy także użycie najnowszych sterowników graficznych dla twojego systemu, jako iż może to znacznie polepszyć odczucia użytkownika, zarówno w opcjonalnej funkcjonalności, jak i w ogólnej wydajności i stabilności.

## Ogłoszone również dzisiaj:

## <a href="../plasma"><img src="/announcements/4/4.11.0/images/plasma.png" class="app-icon" alt="Przestrzenie Pracy Plazmy od KDE, w wersji 4.11" width="64" height="64" /> Przestrzenie Pracy Plazmy kontynuują udoskonalanie wrażeń użytkowników</a>

Przygotowując się do zapewnienia wsparcia w długim czasie, Przestrzenie Robocze Plazmy dostarczają dalszych ulepszeń do podstawowej funkcjonalności z płynniejszym paskiem zadań, inteligentniejszym elementem interfejsu baterii i ulepszonym mikserem dźwięku. Wprowadzenie KEkran zapewnia inteligentną obsługę wielu monitorów na Przestrzeniach Roboczych, a zakrojone na dużą skalę ulepszenia w wydajności w połączeniu z małymi dostosowaniami używalności dają przyjemniejsze odczucie.

## <a href="../applications"><img src="/announcements/4/4.11.0/images/applications.png" class="app-icon" alt="Aplikacje KDE w wersji 4.11"/> Aplikacje KDE 4.11 są olbrzymim krokiem naprzód w zarządzaniu informacją osobistą i dostarczają wiele ogólnych ulepszeń</a>

Wydanie to oznacza znaczne ulepszenia w stosie ZIO KDE, z dużo lepszą wydajnością i wiele nowych funkcji. Kate poprawia produktywność programistów Pythona oraz Javascript dzięki nowym wtyczkom, Dolphin stał się szybszy, a aplikacje edukacyjne niosą ze sobą wiele nowych funkcji.
