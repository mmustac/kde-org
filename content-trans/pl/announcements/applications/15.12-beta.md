---
aliases:
- ../announce-applications-15.12-beta
date: 2015-11-20
description: KDE wydało Aplikacje KDE 15.12 Beta.
layout: application
release: applications-15.11.80
title: KDE wydało betę Aplikacji KDE 15.12
---
20 listopad 2015. Dzisiaj KDE wydało betę nowej wersji Aplikacji KDE. Wersja ta zamraża wszelkie zmiany w zależnościach i funkcjonalności, a zespół KDE będzie się skupiał jedynie na naprawianiu w niej błędów i dalszym polerowaniu.

Ze względu na obecność wielu programów opartych na Szkieletach KDE 5, wydanie Aplikacji 15.12 wymaga dokładnego przetestowania w celu utrzymania, a nawet poprawienia jakości wrażeń użytkownika. Obecnie użytkownicy są znaczącym czynnikiem przy utrzymywaniu wysokiej jakości KDE, bo programiści po prostu nie mogą wypróbować każdej możliwej konfiguracji. Liczymy, że wcześnie znajdziesz błędy, tak aby mogły zostać poprawione przed wydaniem końcowym. Proszę rozważyć dołączenie do zespołu 4.12 poprzez zainstalowanie bety i <a href='https://bugs.kde.org/'>zgłaszanie wszystkich błędów</a>.
