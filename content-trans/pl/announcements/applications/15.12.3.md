---
aliases:
- ../announce-applications-15.12.3
changelog: true
date: 2016-03-16
description: KDE wydało Aplikacje KDE 15.12.3
layout: application
title: KDE wydało Aplikacje KDE 15.12.3
version: 15.12.3
---
15 marca 2016. Dzisiaj KDE wydało trzecie uaktualnienie stabilizujące <a href='../15.12.0'>Aplikacji KDE 15.12</a>. To wydanie zawiera tylko poprawki błędów i uaktualnienia do tłumaczeń; jest to bezpieczne i przyjemne uaktualnienie dla każdego.

Więcej niż 15 zarejestrowanych poprawek błędów uwzględnia ulepszenia do kdepim, akonadi, ark, kblocks, kcalc, ktouch and umbrello oraz umbrello.

To wydanie zawiera także długoterminowo wspieraną wersję Platformy Programistycznej KDE 4.14.18.
