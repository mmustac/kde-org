---
aliases:
- ../announce-applications-18.12.1
changelog: true
date: 2019-01-10
description: O KDE Lança as Aplicações 18.12.1.
layout: application
major_version: '18.12'
title: O KDE Lança as Aplicações do KDE 18.12.1
version: 18.12.1
---
{{% i18n_date %}}

Hoje o KDE lançou a primeira actualização de estabilidade para as <a href='../18.12.0'>Aplicações do KDE 18.12</a>. Esta versão contém apenas correcções de erros e actualizações de traduções, pelo que será uma actualização segura e agradável para todos.

As mais de 20 correcções de erros registadas incluem as melhorias no Kontact, no Cantor, no Dolphin, no Juk, no Kdenlive, no Konsole, no Okular, entre outros.

As melhorias incluem:

- O Akregator agora funciona com o WebEngine do Qt 5.11 ou posterior
- Foi corrigida a ordenação das colunas no leitor de música JuK
- O Konsole desenha de novo os caracteres de desenho de caixas correctamente
