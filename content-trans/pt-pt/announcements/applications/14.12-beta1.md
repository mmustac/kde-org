---
aliases:
- ../announce-applications-14.12-beta1
custom_spread_install: true
date: '2014-11-06'
description: O KDE Lança as Aplicações 14.12 Beta 1.
layout: application
title: O KDE Lança a Primeira Versão 14.12 Beta das Aplicações
---
6 de Novembro de 2014. Hoje o KDE lançou a primeira das versões beta das novas Aplicações do KDE. Com as dependências e as funcionalidades estabilizadas, o foco da equipa do KDE é agora a correcção de erros e mais algumas rectificações.

Com as diversas aplicações a basearem-se nas Plataformas do KDE 5, as versões 14.12 precisam de testes aprofundados para manter e melhorar a qualidade e a experiência do utilizador. Os utilizadores actuais são críticos para manter a alta qualidade do KDE, dado que os programadores não podem simplesmente testar todas as configurações possíveis. Contamos consigo para nos ajudar a encontrar erros antecipadamente, para que possam ser rectificados antes da versão final. Por favor, pense em juntar-se à equipa do 4.12, instalando a versão beta e <a href='https://bugs.kde.org/'>comunicando todos os erros encontrados</a>.

#### Instalar os Pacotes Binários das Aplicações do KDE 14.12 Beta1

<em>Pacotes</em>. Alguns distribuidores de SO's Linux/UNIX forneceram simpaticamente alguns pacotes binários das Aplicações do KDE 14.12 Beta1 (internamente 14.11.80) para algumas versões das suas distribuições e, em alguns casos, outros voluntários da comunidade também o fizeram. Os pacotes binários adicionais, assim como as actualizações dos pacotes agora disponíveis, poderão aparecer nas próximas semanas.

<em>Localizações dos Pacotes</em>. Para uma lista actualizada dos pacotes binários disponíveis, dos quais a Equipa de Versões do KDE foi informada, visite por favor o <a href='http://community.kde.org/KDE_SC/Binary_Packages'>Wiki da Comunidade</a>.

#### Compilar as Aplicações do KDE 14.12 Beta1

Poderá <a href='http://download.kde.org/unstable/applications/14.11.80/src/'>transferir à vontade</a> o código-fonte completo das Aplicações do KDE 14.12 Beta1. As instruções de compilação e instalação estão disponíveis na <a href='/info/applications/applications-14.11.80'>Página de Informações das Aplicações do KDE Beta 1</a>.
