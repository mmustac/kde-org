---
aliases:
- ../../kde-frameworks-5.35.0
date: 2017-06-10
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Attica

- Melhoria da notificação de erros

### BluezQt

- Passagem de uma lista explícita de argumentos. Isto impede que o QProcess tente lidar com o espaço que contém um dado local através de uma consola
- Correcção da perda das mudanças de propriedades, imediatamente a seguir à adição de um objecto (erro 377405)

### Ícones do Brisa

- actualização do tipo MIME do 'awk', dado que é uma linguagem de programação (erro 376211)

### Módulos Extra do CMake

- reposição do teste de 'hidden-visibility' (visibilidade escondida) com o Xcode 6.2
- ecm_qt_declare_logging_category(): mais guardas de inclusão única dos ficheiros
- Adição ou melhoria das mensagens "Gerado. Não editar" e uso consistente das mesmas
- Adição de um novo módulo FindGperf
- Mudança do local de instalação por omissão do 'pkgconfig' no FreeBSD

### KActivitiesStats

- Correcção do 'kactivities-stats' para o nível 3

### Ferramentas de Doxygen do KDE

- Não ter em consideração a palavra-chave Q_REQUIRED_RESULT

### KAuth

- Verificar se quem nos chama é realmente quem diz

### KCodecs

- Geração do resultado do 'gperf' na altura da compilação

### KCoreAddons

- Garantia de um valor de base adequado por tarefa no KRandom
- Não vigiar as localizações do QRC (erro 374075)

### KDBusAddons

- Não incluir o PID na localização de DBus quando usar o 'flatpak'

### KDeclarative

- Emissão consistente do sinal MouseEventListener::pressed
- Não gerar fugas de memória no objecto MimeData (erro 380270)

### Suporte para a KDELibs 4

- Tratamento dos espaços na localização em CMAKE_SOURCE_DIR

### KEmoticons

- Correcção: o Qt5::DBus só é usado a nível privado

### KFileMetaData

- Uso do '/usr/bin/env' para localizar o 'python2'

### KHTML

- Geração do resultado do 'gperf' para o 'kentities' na altura da compilação
- Geração do resultado do 'gperf' para os 'doctypes' na altura da compilação

### KI18n

- Aumentar o Guia do Programador com notas sobre a influência do 'setlocale()'

### KIO

- Resolução de um problema em que certos elementos nas aplicações (p.ex., a vista de ficheiros do Dolphin) ficavam inacessíveis numa configuração multi-ecrã de densidade em PPP's elevada (erro 363548)
- [Janela de Mudança de Nome] Forçar o formato de texto simples
- Identificação dos binários PIE (application/x-sharedlib) como ficheiros executáveis (erro 350018)
- base: exposição do GETMNTINFO_USES_STATVFS no cabeçalho da configuração
- PreviewJob: ignorar as pastas remotas. Demasiado dispendioso nas antevisões (erro 208625)
- PreviewJob: limpeza de ficheiros temporários em caso de falha no get() (erro 208625)
- Aceleração da apresentação da árvore de detalhes, evitando o dimensionamento de colunas a mais

### KNewStuff

- Uso de um único QNAM (e 'cache' de disco) para as tarefas de HTTP
- 'Cache' interna dos ados do fornecedor na inicialização

### KNotification

- Correcção dos KSNI's serem incapazes de registar serviços no 'flatpak'
- Uso do nome da aplicação em vez do PID ao criar o serviço de DBus do SNI

### KPeople

- Não exportar os símbolos das bibliotecas privadas
- Correcção das exportação de símbolos no KF5PeopleWidgets e no KF5PeopleBackend
- limitação do #warning no GCC

### Plataforma da KWallet

- Substituição do 'kwalletd4' após o fim da migração
- Assinalar a finalização do agente de migração
- Só iniciar o cronómetro do agente de migração se for necessário
- Verificação de instância única da aplicação o mais cedo possível

### KWayland

- Adição do 'requestToggleKeepAbove/below'
- Manutenção do QIcon::fromTheme na tarefa principal
- Remoção do PID no 'changedSignal' em Client::PlasmaWindow
- Adição do PID ao protocolo de gestão de janelas do Plasma

### KWidgetsAddons

- KViewStateSerializer: Correcção de estoiro quando a vista é destruída antes do serializador do estado (erro 353380)

### KWindowSystem

- Melhor correcção para o NetRootInfoTestWM num local com espaços

### KXMLGUI

- Definição da janela principal como mãe dos menus de contexto autónomos
- Ao criar hierarquias de menus, associar como pais dos menus os seus contentores

### Plataforma do Plasma

- Adição do ícone de bandeja do VLC
- Modelos de plasmóides: usar a imagem que faça parte do pacote (de novo)
- Adição de modelo para a 'Applet' em QML do Plasma com a extensão QML
- Recriação do 'plasmashellsurf' se estiver exposto e destruição se estiver escondido

### Realce de Sintaxe

- Haskell: realce do módulo de aspas "julius", usando as regras do Normal##Javascript
- Haskell: activação do realce 'hamlet' também para o módulo de aspas "shamlet"

### Informação de segurança

O código lançado foi assinado com GPG, usando a seguinte chave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Impressão digital da chave primária: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB

Poderá discutir e partilhar ideias sobre esta versão na secção de comentários do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-'>artigo do Dot</a>.
