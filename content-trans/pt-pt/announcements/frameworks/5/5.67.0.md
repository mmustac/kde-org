---
aliases:
- ../../kde-frameworks-5.67.0
date: 2020-02-02
layout: framework
libCount: 70
---
### Geral

- Retirada de muitas chamadas a métodos obsoletos do Qt 5.15, o que reduz o número de avisos durante a compilação.

### Baloo

- Migração da configuração do KConfig para o KConfigXt para possibilitar ao KCM usá-la

### Ícones do Brisa

- criação de um ícone do Kate no estilo Brisa, com base no novo desenho de Tyson Tan
- Mudança do ícone do VLC para ser mais parecido com os oficiais do VLC
- adição de ícone do KTrip a partir do seu repositório respectivo
- Adição de ícone para o 'application/sql'
- Limpeza e adição de ícones de repetição multimédia a 22px
- Adição de ícone para o text/vnd.kde.kcrash-report
- Modificação do application/x-ms-shortcut para ser um ícone de atalho real

### Módulos Extra do CMake

- Adição de Importação de Variável de Ambiente em falta
- ECMAddAppIcon: Adição de 'sc' na expressão regular para extrair a extensão dos nomes válidos
- ECMAddQch: suporte &amp; documentação da utilização da macro K_DOXYGEN

### Integração da Plataforma

- Remoção da dependência do QtDBus não usada

### KActivitiesStats

- Correcção de comando SQL com problemas no 'allResourcesQuery'

### KActivities

- Remoção de ficheiros que o Windows não consegue lidar
- Garantia de armazenamento do URI do recurso sem uma barra final

### Ferramentas de Doxygen do KDE

- Correcção das importações de módulos com o Python2
- Fixação do 'utf-8' como codificação do sistema de ficheiros com o Python2 para ajudar no api.kde.org

### KCMUtils

- preferência pelos novos 'plugins' do KCM face aos antigos
- KCModuleQml: Garantia de que o 'defaulted' é omitido com o configModule-&gt;representsDefaults actual no carregamento
- Apresentação de um botão que respeita o que é declarado pelo KCModule
- Actualização do KPluginSelector para permitir ao KCM apresentar um estado correcto para os botões 'Reiniciar', 'Aplicar' e 'Predefinições'

### KConfig

- Reorganização do KConfigXT
- Correcção da compilação das interfaces em Python após a alteração 'ebd14f29f8052ff5119bf97b42e61f404f223615'
- KCONFIG_ADD_KCFG_FILES: regeneração em caso de uma nova versão do 'kconfig_compiler'
- Possibilidade de também passar um alvo em vez de uma lista de ficheiros de código ao KCONFIG_ADD_KCFG_FILES
- Adição do KSharedConfig::openStateConfig para guardar a informação do estado
- Correcção da compilação das interfaces em Python depois da modificação '7ab8275bdb56882692846d046a5bbeca5795b009'

### KConfigWidgets

- KStandardAction: adição de método para a criação de acções do SwitchApplicationLanguage
- [KColorSchemeManager] Não listar elementos duplicados
- [KColorschemeManager] Adição de opção para reactivar o seguimento do tema global

### KCoreAddons

- despromoção dos erros de carregamento dos 'plugins' de aviso para depuração
- Documentação sobre como filtrar da forma correcta por tipo de serviço
- Adição de método substituto do perlSplit() que recebe uma QRegularExpression e descontinuação da versão com QRegExp
- Adição de tipo MIME para os registos de chamadas gravados a partir do DrKonqi
- Adição de função utilitária de texto KShell::tildeCollapse
- KPluginMetadata: adição de método de leitura 'initialPreference()'
- desktoptojson: conversão também da chave InitialPreference

### KDeclarative

- Cálculo correcto da margem inferior para os métodos delegados da grelha com sub-títulos
- [ConfigModule] Indicação de qual o pacote inválido

### KHolidays

- Actualização dos feriados e adição de dias da bandeira e de nomes para a Suécia

### KI18n

- ki18n_wrap_ui: erro quando o ficheiro não existe
- [Kuit] Reversão das modificações no parseUiMarker()

### KIO

- Adição de um evento em falta que mudou de nome quando o ficheiro de destino já existe
- KFilePlacesModel: Num perfil novo, nos documentos recentes mostrar apenas os itens recentlyused:/ por omissão
- Adição de construtor do KFileCustomDialog com um parâmetro 'startDir'
- Correcção da utilização do QRegularExpression::wildcardToRegularExpression()
- Possibilidade de tratar as aplicações com Terminal=True no seu ficheiro 'desktop' tratar o seu tipo MIME associado de forma adequada (erro 410506)
- KOpenWithDialog: Possibilidade de devolver um KService acabado de criar e associado a um tipo MIME
- Adição do KIO::DropJobFlag para possibilitar a apresentação manual do menu (erro 415917)
- [KOpenWithDialog] Esconder um grupo colapsável quando todas as opções dentro dele estão escondidas (erro 415510)
- Reversão da remoção efectiva do KUrlPixmapProvider da API
- SlaveBase::dispatchLoop: Correcção do cálculo do tempo-limite (erro 392768)
- [KDirOperator] Possibilidade de mudança dos nomes dos ficheiros a partir do menu de contexto (erro 189482)
- Aplicação na versão oficial da janela de mudança de nomes dos ficheiros do Dolphin (erro 189482)
- KFilePlaceEditDialog: passagem da lógica para o isIconEditable()

### Kirigami

- Recorte do item-pai intermitente (erro 416877)
- Remoção da margem superior do cabeçalho a partir de um ScrollView privado
- sugestões do tamanho preferido para a disposição em grelha (erro 416860)
- Uso da propriedade 'attached' no 'isCurrentPage'
- Eliminação de alguns avisos
- tentar manter o cursor na janela ao escrever num OverlaySheet
- expansão adequada dos itens 'fillWidth' no modo móvel
- Adição de cores de fundo activas, da ligação, visitadas, negativas, neutras e positivas
- Exposição do nome do ícone do botão de expansão do ActionToolBar
- Uso do Page do QQC2 como base para o Page do Kirigami
- Indicação de onde vem o código como URL
- Não associar de forma cega o AbstractApplicationHeader
- emissão dos eventos 'pooled' após as propriedades terem sido reatribuídas
- adição dos sinais 'reused' e 'pooled' como no TableView

### KJS

- Interrupção da execuçaõ da máquina assim que tenha sido observado um sinal 'timeout'
- Suporte ao operador exponencial ** do ECMAScript 2016
- Adição da função shouldExcept() que funciona com base numa função

### KNewStuff

- Resolução de problemas na funcionalidade do KNSQuick::Engine::changedEntries

### KNotification

- Adição de um novo sinal para a activação da acção predefinida
- Remoção da dependência do KF5Codecs, usando a nova função 'stripRichText'
- Eliminação de texto formatado no Windows
- Adaptação às alterações de Android no Qt 5.14
- Descontinuação do 'raiseWidget'
- Migração do KNotification a partir do KWindowSystem

### KPeople

- Ajuste do 'metainfo.yaml' para um novo nível
- Remoção do código antigo de carregamento dos 'plugins'

### KQuickCharts

- Correcção da verificação da versão do Qt
- Registo do QAbstractItemModel como tipo anónimo para as atribuições de propriedades
- Esconder a linha de um gráfico de linhas se a sua espessura for igual a 0

### Kross

- 'addHelpOption' já adicionado pelo 'kaboutdata'

### KService

- Suporte para múltiplos valores no XDG_CURRENT_DESKTOP
- Descontinuação do 'allowAsDefault'
- Transformação das "Aplicações Predefinidas" no 'mimeapps.list' nas aplicações preferidas (erro 403499)

### KTextEditor

- Reversão da "melhoria da completação de palavras para usar o realce, de forma a detectar os limites das palavras" (erro 412502)
- importação do ícone final do Brisa
- Métodos relacionados com as mensagens: Uso de um 'connect' baseado em ponteiros para funções-membros
- DocumentPrivate::postMessage: evitar várias pesquisas por código
- correcção da função de arrastamento&amp;cópia (usando a tecla Ctrl) (erro 413848)
- garantia da existência de um ícone quadrático
- configuração do ícone adequado do Kate na janela 'Acerca' do KatePart
- notas incorporadas: configuração correcta do 'underMouse()' para as notas incorporadas
- evitar o uso da mascote antiga ATM
- Expansão de variáveis: Adição da variável PercentEncoded (erro 416509)
- Correcção de estoiro na expansão de variáveis (usada pelas ferramentas externas)
- KateMessageWidget: remoção da instalação de filtro de eventos não usados

### KTextWidgets

- Remoção da dependência do KWindowSystem

### Plataforma da KWallet

- Reversão do readEntryList() para usar o QRegExp::Wildcard
- Correcção da utilização do QRegularExpression::wildcardToRegularExpression()

### KWidgetsAddons

- [KMessageWidget] Subtracção da margem correcta
- [KMessageBox] Só permitir a selecção de texto na janela com o rato (erro 416204)
- [KMessageWidget] Uso do devicePixelRatioF para a imagem da animação (erro 415528)

### KWindowSystem

- [KWindowShadows] Verificação da ligação ao X
- Introdução da API de sombras
- Descontinuação do KWindowEffects::markAsDashboard()

### KXMLGUI

- Uso do método de conveniência para o 'switchApplicationLanguage'
- Possibilidade do 'programLogo' ser também um QIcon
- Remoção da capacidade de relatório de erros contra coisas arbitrárias a partir de uma lista estática
- Remoção da informação do compilador a partir da janela de relatório de erros
- KMainWindow: correcção do autoSaveSettings para impedir os QDockWidgets de serem apresentados de novo
- i18n: Adição de mais textos de contexto semântico
- i18n: Divisão das traduções dos textos "Translation"

### Plataforma do Plasma

- Correcção dos cantos das dicas e remoção de atributos de cores inúteis
- Remoção de cores fixas nos SVG's do fundo
- Correcção do tamanho e alinhamento de pixels nas opções de marcação e exclusivas
- Actualização das sombras do tema Brisa
- [Plasma Quick] Adição da classe WaylandIntegration
- Mesmo comportamento para a barra de deslocamento e para o estilo do ambiente de trabalho
- Utilização do KPluginMetaData onde for possível
- Adição do item do menu para o modo de edição no menu de contexto do ecrã
- Consistência: botões seleccionados coloridos
- Migração do 'endl' para \n Não necessário fazer o 'flush', dado que o QTextStream usa o QFile que efectua essa operação quando removido

### Purpose

- Correcção da utilização do QRegularExpression::wildcardToRegularExpression()

### QQC2StyleBridge

- Remoção de truques relacionados com a barra de deslocamento nos métodos delegados da lista
- [TabBar] Remoção dos contornos
- Adição de cores de fundo activas, da ligação, visitadas, negativas, neutras e positivas
- Uso do 'hasTransientTouchInput'
- arredondar sempre o 'x' e o 'y'
- suporte para barras de deslocamento no modo para dispositivos móveis
- ScrollView: Não sobrepor as barras de deslocamento sobre os conteúdos

### Solid

- Adição de sinais para os eventos do 'udev' com a associação e dissociação das acções
- Clarificação da referência ao DeviceInterface (erro 414200)

### Realce de Sintaxe

- Actualização do 'nasm.xml' com as últimas instruções
- Perl: Adição do 'say' à lista de palavras-chave
- cmake: Correcção da expressão regular <code>CMAKE_POLICY**_CMP&amp;lt;N&amp;gt;</code> e adição de argumentos especiais ao <code>get_cmake_property</code>
- Adição da definição de realce do GraphQL

### Informação de segurança

O código lançado foi assinado com GPG, usando a seguinte chave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Impressão digital da chave primária: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
