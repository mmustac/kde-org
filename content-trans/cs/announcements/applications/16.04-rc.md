---
aliases:
- ../announce-applications-16.04-rc
custom_spread_install: true
date: 2016-04-05
description: KDE přináší kandidáta na vydání Aplikací KDE 16.04
layout: application
release: applications-16.03.90
title: KDE přináší kandidáta na vydání Aplikací KDE 16.04
---
April 7, 2016. Today KDE released the release candidate of the new versions of KDE Applications. With dependency and feature freezes in place, the KDE team's focus is now on fixing bugs and further polishing.

With the various applications being based on KDE Frameworks 5, the KDE Applications 16.04 the quality and user experience. Actual users are critical to maintaining high KDE quality, because developers simply cannot test every possible configuration. We're counting on you to help find bugs early so they can be squashed before the final release. Please consider joining the team by installing the release <a href='https://bugs.kde.org/'>and reporting any bugs</a>.

#### Instalace binárních balíčků Aplikací KDE 16.04 kandidáta na vydání

<em>Packages</em>. Some Linux/UNIX OS vendors have kindly provided binary packages of KDE Applications 16.04 Release Candidate (internally 16.03.90) for some versions of their distribution, and in other cases community volunteers have done so. Additional binary packages, as well as updates to the packages now available, may become available over the coming weeks.

<em>Package Locations</em>. For a current list of available binary packages of which the KDE Project has been informed, please visit the <a href='http://community.kde.org/KDE_SC/Binary_Packages'>Community Wiki</a>.

#### Kompilace Aplikací KDE 16.04 kandidáta na vydání

Kompletní zdrojový kód KDE 16.04 kandidáta na vydání lze <a href='http://download.kde.org/unstable/applications/16.03.90/src/'>volně stáhnout</a>. Instrukce pro kompilaci a instalaci jsou dostupné ze <a href='/info/applications/applications-16.03.90'>stránek s informacemi o kandidátu na vydání Aplikací KDE</a>.
