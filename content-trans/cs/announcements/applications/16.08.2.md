---
aliases:
- ../announce-applications-16.08.2
changelog: true
date: 2016-10-13
description: KDE vydává Aplikace KDE 16.08.2
layout: application
title: KDE vydává Aplikace KDE 16.08.2
version: 16.08.2
---
October 13, 2016. Today KDE released the second stability update for <a href='../16.08.0'>KDE Applications 16.08</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

More than 30 recorded bugfixes include improvements to kdepim, ark, dolphin, kgpg, kolourpaint, okular, among others.

This release also includes Long Term Support version of KDE Development Platform 4.14.25.
