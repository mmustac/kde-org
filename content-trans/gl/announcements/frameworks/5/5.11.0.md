---
aliases:
- ../../kde-frameworks-5.11.0
date: 2015-06-12
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Módulos adicionais de CMake

- Novos argumentos para ecm_add_tests(). (fallo 345797)

### Integración de infraestruturas

- Usar o initialDirectory correcto para o KDirSelectDialog
- Asegurarse de que se indica o esquema ao sobrepor o valor de URL inicial
- Só aceptar directorios existentes no modo FileMode::Directory

### KActivities

(non se forneceu un historial de cambios)

### KAuth

- Poñer KAUTH_HELPER_INSTALL_ABSOLUTE_DIR a disposición de todos os usuarios de KAuth

### KCodecs

- KEmailAddress: engadir unha sobreposición para extractEmailAddress e firstEmailAddress que devolve unha mensaxe de erro.

### KCompletion

- Corrixir unha selección non desexada ao editar o nome de ficheiro no diálogo de ficheiro (fallo 344525)

### KConfig

- Evitar unha quebra se QWindow::screen() é nulo
- Engadir KConfigGui::setSessionConfig() (fallo 346768)

### KCoreAddons

- Nova API de comodidade KPluginLoader::findPluginById()

### KDeclarative

- permitir a creación de ConfigModule desde KPluginMetdata
- corrixir os eventos pressAndhold

### Compatibilidade coa versión 4 de KDELibs

- Usar QTemporaryFile en vez de usar un ficheiro temporal fixo.

### KDocTools

- Actualizar as traducións
- Personalización de traducións/ru
- Corrixiras entradas con ligazóns incorrectas

### KEmoticons

- Gardar en caché o tema no complemento de integración

### KGlobalAccel

- [tempo de execución] Mover código específico de plataformas a complementos

### KIconThemes

- Optimizar KIconEngine::availableSizes()

### KIO

- Non intentar completar usuarios e aseverar cando engadir ao principio non estea baleiro (fallo 346920)
- Usar KPluginLoader::factory() ao cargar KIO::DndPopupMenuPlugin
- Corrixir o bloqueo indefinido ao usar proxys de rede (fallo 346214)
- Corrixiuse KIO::suggestName para que conserve as extensións de ficheiro
- Lanzar kbuildsycoca4 ao actualizar sycoca5.
- KFileWidget: Non aceptar ficheiros no modo de só directorios
- KIO::AccessManager: permitir tratar un QIODevice secuencial como asíncrono

### KNewStuff

- Engadir o novo método fillMenuFromGroupingNames
- KMoreTools: engadir moitas agrupacións novas
- KMoreToolsMenuFactory: xestión de «git-clients-and-actions»
- createMenuFromGroupingNames: facer o parámetro «url» opcional

### KNotification

- Corrixir a quebra en NotifyByExecute cando non se estabeleceu ningún trebello (fallo 348510)
- Mellorar a xestión de notificacións que se pechan (fallo 342752)
- Substituír o uso de QDesktopWidget por QScreen
- Asegurarse de que KNotification pode usarse desde un fío que non é de interface gráfica de usuario

### Infraestrutura de paquetes

- Gardar o acceso de qpointer da estrutura (fallo 347231)

### KPeople

- Usar QTemporaryFile en vez de usar o valor fixo /tmp.

### KPty

- Usar tcgetattr e tcsetattr se están dispoñíbeis

### Kross

- Corrixir a carga dos módulos de Kross «forms» e «kdetranslation»

### KService

- Ao executar como root preservar os permisos de ficheiro dos ficheiros existentes da caché (fallo 342438)
- Protexer contra non poder abrir un fluxo (fallo 342438)
- Corrixir a comprobación de permisos incorrectos ao escribir un ficheiro (fallo 342438)
- Corrixir a consulta de ksycoca dos tipos pseudo-MIME x-scheme-handler/*. (fallo 347353)

### KTextEditor

- Permitir, nos tempos de KDE 4.x, que as aplicacións e complementos de terceiros poidan instalar os seus propios ficheiros XML de realce en katepart5/syntax
- Engadir KTextEditor::Document::searchText()
- Recuperar o uso de KEncodingFileDialog (fallo 343255)

### KTextWidgets

- Engadir un método para baleirar o decorador
- Permitir usar un decorador personalizado de Sonnet
- Permitir «Atopar o anterior» en KTextEdit.
- Engadir de novo compatibilidade con texto-para-fala

### KWidgetsAddons

- KAssistantDialog: Engadir de novo o botón de axuda que estaba presente na versión 4 de KDELibs

### KXMLGUI

- Engadir xestión de sesións para KMainWindow (fallo 346768)

### NetworkManagerQt

- Retirar a compatibilidade con WiMAX para NM ≥1.2.0

### Infraestrutura de Plasma

- Agora os compoñentes de calendario poden mostrar os números de semana (fallo 338195)
- Usar QtRendering para as fontes nos campos de contrasinal
- Corrixir a busca de AssociatedApplicationManager cando un tipo MIME ten (fallo 340326)
- Corrixir a coloración do fondo do panel (fallo 347143)
- Retirar a mensaxe de «Non se puido cargar o miniaplicativo»
- Posibilidade de cargar kcm de QML nas xanelas de configuración de plasmoides
- Non usar DataEngineStructure para Applets
- Retirar o uso de sycoca en libplasma dentro do posíbel
- [plasmacomponents] Facer que SectionScroller obedeza a ListView.section.criteria
- As barras de desprazamento xa non se agochan automaticamente cando hai unha pantalla táctil presente (fallo 347254)

### Sonnet

- Usar unha caché central para os SpellerPlugins.
- Reducir as asignacións temporais.
- Optimizar: Non borrar a caché de dicionario ao copiar obxectos de corrector ortográfico.
- Optimizar evitando chamadas innecesarias a save() chamando só unha vez ao final cando sexa necesario.

Pode comentar e compartir ideas sobre esta versión na sección de comentarios do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artigo do Dot</a>.
