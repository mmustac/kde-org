---
aliases:
- ../../kde-frameworks-5.46.0
date: 2018-05-12
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Evitar os bucles infinitos ao obter o URL de DocumentUrlDB (fallo 393181)
- Engadir sinais de D-Bus de Baloo para ficheiros movidos ou retirados
- Instalar un ficheiro pri para compatibilidade con qmake e documentalo en metainfo.yaml
- baloodb: Engadir a orde «clean»
- balooshow: colorar só cando estea anexado a un terminal
- Retirar FSUtils::getDirectoryFileSystem
- Evitar definir a man os sistemas de ficheiros que permiten copiar ao escribir
- Permitir desactivar CoW para fallar cando o sistema de ficheiros non o permita
- databasesanitizer: Usar marcas para filtrar
- Corrixir a fusión de termos en AdvancedQueryParser
- Usar QStorageInfo en vez de código propio
- sanitizer: Mellorar a lista de dispositivos
- Aplicar termInConstruction inmediatamente cando se completa o termo
- Xestionar correctamente caracteres especiais adxacentes (fallo 392620)
- Engadir un caso de proba para analizar aperturas duplas «((» (fallo 392620)
- Usar statbuf de maneira consistente

### Iconas de Breeze

- Engadir unha icona da área de notificacións de plasma-browser-integration
- Engadir unha icona para Virt-manager grazas a ndavis
- Engadir video-card-inactive
- overflow-menu como view-more-symbolic e horizontal
- Usar a icona «dous controis desprazábeis», máis axeitada para «configurar»

### Módulos adicionais de CMake

- Incluír FeatureSummary antes de chamar a set_package_properties
- Non instalar complementos dentro de lib en Android
- Permitir construír varios APK a partir dun proxecto
- Comprobar se o paquete de aplicación androiddeployqt ten un símbolo main()

### KCompletion

- [KLineEdit] Usar a funcionalidade do botón de limpar incluída en Qt
- Corrixir KCompletionBox en Wayland

### KCoreAddons

- [KUser] Comprobar se .face.icon pode lerse antes de devolvelo
- Publicar os sinais de KJob para que a sintaxe de Qt 5 de connect poidan funcionar

### KDeclarative

- Cargar gráficos de NV restabelecidos segundo a configuración
- [KUserProxy] Axustar ao servizo de contas (fallo 384107)
- Optimizacións de Plasma Mobile
- Facer oco para o pé e a cabeceira
- nova política de cambio de tamaño (fallo 391910)
- engadir compatibilidade coa visibilidade das accións
- Permitir notificacións de restabelecemento de NVidia en QtQuickViews

### KDED

- Add platform detection and adjustment to kded (automatic setting of $QT_QPA_PLATFORM)

### KFileMetaData

- Engadir unha descrición e o motivo da dependencia Xattr
- extractores: agochar avisos sobre cabeceiras do sistema
- corrixir a detección de taglib ao compilar para Android
- Instalar un ficheiro pri para compatibilidade con qmake e documentalo en metainfo.yaml
- Permitir usar saltos de liña automáticos en cadeas concatenadas
- ffmpegextractor: silenciar os avisos de obsolescencia
- taglibextractor: Corrixir un fallo de xénero baleiro
- xestionar máis etiquetas en taglibextractor

### KHolidays

- holidays/plan2/holiday_sk_sk - corrección do día do mestre (fallo 393245)

### KI18n

- [Documentación da API] Novo marcador de interface de usuario @info:placeholder
- [Documentación da API] Novo marcador de interface de usuario @item:valuesuffix
- Non se necesita executar de novo as ordes das iteracións anteriores (fallo 393141)

### KImageFormats

- [Cargador de XCF e GIMP] Aumentar o tamaño de imaxe máximo permitido a 32767x32767 en plataformas de 64 bits (fallo 391970)

### KIO

- Cambio de escala suave de miniatura no selector de ficheiros (fallo 345578)
- KFileWidget: Aliñar perfectamente o trebello de nome de ficheiro coa vista de icona
- KFileWidget: Gardar a anchura do panel de lugares tamén tras agochar o panel
- KFileWidget: evitar que a anchura do panel de lugares medre 1 px de cada vez
- KFileWidget: Desactivar os botóns de ampliación tras acadar mínimo e máximo
- KFileWidget: definir o tamaño mínimo do control de ampliación
- Non seleccionar a extensión de ficheiro
- concatPaths: procesar correctamente un path1 baleiro
- Mellorar a disposición de iconas en grade no diálogo de selección de ficheiro (fallo 334099)
- Agochar KUrlNavigatorProtocolCombo se só se permite un protocolo
- Só mostrar esquemas compatíbeis en KUrlNavigatorProtocolCombo
- O selector de ficheiros le a vista previa das miniaturas da configuración de Dolphin (fallo 318493)
- Engadir Escritorio e Descargar á lista predeterminada de Lugares
- Agora KRecentDocument almacena QGuiApplication::desktopFileName en vez de applicationName
- [KUrlNavigatorButton] Tampouco iniciar MTP
- getxattr acepta 6 parámetros en macOS (fallo 393304)
- Engadir un elemento de menú de «Cargar de novo» ao menú contextual de KDirOperator (fallo 199994)
- Gardar a configuración da vista do diálogo aínda que se cancele (fallo 209559)
- [KFileWidget] Definir manualmente o nome de usuario de exemplo
- Non mostrar a aplicación superior «Abrir con» para cartafoles, só para ficheiros
- Detectar un parámetro incorrecto en findProtocol
- Usar o texto «Outra aplicación…» no menú subordinado «Abrir con»
- Codificar correctamente os URL de miniaturas (fallo 393015)
- Axustar as anchuras das columnas na vista de árbore dos diálogos de abrir e gardar ficheiros (fallo 96638)

### Kirigami

- Non avisar cando se use Page {} fóra de pageStack
- Modificar InlineMessages para solucionar varios problemas
- corrixir en 7Qt 5.11
- basear en unidades para o tamaño do botón de ferramenta
- colorar a icona de pechar as cubrila
- mostrar unha marxe baixo o pé cando sexa necesario
- corrixir isMobile
- esvaer tamén nas animacións de abrir e pechar
- incluír as cousas de D-Bus só en UNIX excluídos Android e Apple
- vixiar o tabletMode desde KWin
- no modo de escritorio mostras as accións ao cubrir (fallo 364383)
- asa na barra de ferramentas superior
- usar un botón de pechar gris
- menor dependencia de applicationwindow
- menos avisos sen applicationwindow
- funcionar correctamente sen applicationWindow
- Non ter un tamaño non integral en separadores
- Non mostrar as accións se están desactivadas
- elementos marcábeis de FormLayout
- usar iconas distintas no exemplo de conxunto de cores
- incluír iconas só en Android
- facelo funcionar con Qt 5.7

### KNewStuff

- Corrixir marxes duplas arredor de DownloadDialog
- Corrixir os consellos dos ficheiros de interface gráfica sobre subclases de trebellos personalizados
- Non suxerir un complemento de QML como destino de ligazón

### Infraestrutura KPackage

- usar KDE_INSTALL_DATADIR en vez de FULL_DATADIR
- Engadir os URL de doazón aos datos de proba

### KPeople

- Corrixir o filtrado de PersonSortFilterProxyModel

### Kross

- Tamén facer opcional a instalación de documentación traducida

### KRunner

- Permitir caracteres de substitución no nome de servizo de executores de D-Bus

### KTextEditor

- optimización de KTextEditor::DocumentPrivate::views()
- [ktexteditor] positionFromCursor moito máis rápido
- Permitir usar un clic único nun número de liña para seleccionar a liña
- Corrixir a falta de etiquetas de letra grosa, inclinada e demais con versións modernas de Qt (≥ 5.9)

### Infraestrutura de Plasma

- Corrixir un marcador de evento que non se mostra no calendario cos temas Air e Oxygen
- Usar «Configurar %1…» para o texto da acción de configuración de miniaplicativo
- [Estilos de botóns] Encher a altura e aliñar verticalmente (fallo 393388)
- engadir a icona video-card-inactive para a área de notificacións
- corrixir a aparencia dos botóns planos
- [Containment Interface] Non entrar no modo de edición cando sexa inmutábel
- asegurarse de que largespacing é un múltiplo perfecto de small
- chamar a addContainment cos parámetros axeitados
- Non mostrar o fondo se Button.flat é verdadeiro
- asegurarse de que o contedor que creamos ten a actividade que pedimos
- engadir un containmentForScreen de versión con actividade
- Non alterar a xestión de memoria para agochar un elemento (fallo 391642)

### Purpose

- Asegurarse de que damos espazo vertical a complementos de configuración
- Migrar a configuración do complemento KDEConnect a QQC2
- Migrar AlternativesView a QQC2

### QQC2StyleBridge

- exportar os espazamentos de disposición de qstyle, iniciar desde Control
- [ComboBox] Corrixir a xestión da roda do rato
- facer que a zona de rato non interfira cos controis
- corrixir acceptableInput

### Solid

- Actualizar o punto de montaxe tras operacións de montaxe (fallo 370975)
- Invalidar a caché de propiedades cando se retira unha interface
- Evitar crear entradas de propiedade duplicadas na caché
- [UDisks] Optimizar varias comprobacións de propiedades
- [UDisks] Xestión correcta de sistemas de ficheiros extraíbeis (fallo 389479)

### Sonnet

- Corrixir a activación e desactivación do botón de Retirar
- Corrixir a activación e desactivación do botón de Engadir
- Buscar dicionarios nos subdirectorios

### Realce da sintaxe

- Actualizar o URL do proxecto
- «Headline» é un comentario, así que basealo en dsComment
- Engadir realce para listas de ordes de GDB e ficheiros gdbinit
- Engadir salientado de sintaxe para Logcat

### Información de seguranza

The released code has been GPG-signed using the following key: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Pode comentar e compartir ideas sobre esta versión na sección de comentarios do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artigo do Dot</a>.
