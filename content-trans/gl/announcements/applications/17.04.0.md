---
aliases:
- ../announce-applications-17.04.0
changelog: true
date: 2017-04-20
description: KDE publica a versión 17.04.0 das aplicacións de KDE
layout: application
title: KDE publica a versión 17.04.0 das aplicacións de KDE
version: 17.04.0
---
20 de abril de 2017. Chegou a versión 17.04 das aplicacións de KDE. En xeral traballamos en estabilizar e facilitar o uso tanto das aplicacións como das bibliotecas que usan. Solucionando pequenos problemas e escoitando as vosas experiencias fixemos que a colección de aplicacións de KDE falle menos e sexa máis fácil de usar.

Goce das súas novas aplicacións!

#### <a href="https://edu.kde.org/kalgebra/">KAlgebra</a>

{{<figure src="/announcements/applications/17.04.0/kalgebra1704.jpg" width="600px" >}}

Os desenvolvedores de KAlgebra están traballando na converxencia, e migraron a versión para móbiles do programa educativo a Kirigami 2.0, a infraestrutura preferida para integrar aplicacións de KDE con plataformas de escritorio e móbiles.

Ademais, a versión de escritorio tamén se migrou a infraestrutura de 3D a GLES, o software que permite que o programa renderize funcións 3D tanto no escritorio como en dispositivos móbiles. Isto simplifica o código e faino máis fácil de manter.

#### <a href="http://kdenlive.org/">Kdenlive</a>

{{<figure src="/announcements/applications/17.04.0/kdenlive1704.png" width="600px" >}}

O editor de vídeo de KDE está estabilizándose e aumentando as súas funcionalidades con cada nova versión. Esta vez os desenvolvedores cambiaron o deseño do diálogo de selección de perfil para facer máis doado definir o tamaño de pantalla, a taxa de fotogramas e outros parámetros do filme.

Agora tamén pode reproducir o seu vídeo directamente desde a notificación cando remata a renderización. Corrixíronse algunhas quebras que se producían ao mover fragmentos na liña de tempo, e mellorouse o asistente de DVD.

#### <a href="https://userbase.kde.org/Dolphin">Dolphin</a>

{{<figure src="/announcements/applications/17.04.0/dolphin1704.png" width="600px" >}}

O noso explorador de ficheiros favorito e portal para todo (salvo, quizais, o inframundo) recibiu varios cambios de deseño e melloras de facilidade de uso para facer que sexa aínda máis potente.

Limpáronse os menús contextuais do panel de <i>Lugares</i> (situado de maneira predeterminada á esquerda da zona de visualización principal), e agora pódese interactuar cos trebellos de metadatos nos consellos. Os consellos, por certo, xa funcionan tamén en Wayland.

#### <a href="https://www.kde.org/applications/utilities/ark/">Ark</a>

{{<figure src="/announcements/applications/17.04.0/ark1704.png" width="600px" >}}

A popular aplicación gráfica para crear, descomprimir e xestionar arquivos comprimidos de ficheiros e cartafoles inclúe agora unha función de <i>Busca</i> para axudarlle a atopar ficheiros en arquivos que teñan moitos.

Tamén lle permite activar e desactivar complementos directamente desde o diálogo de <i>Configurar</i>. Falando de complementos, o novo complemento de Libzip mellora a compatibilidade con arquivos Zip.

#### <a href="https://minuet.kde.org/">Minuet</a>

{{<figure src="/announcements/applications/17.04.0/minuet1704.png" width="600px" >}}

Se está ensinando ou aprendendo música, ten que probar Minuet. A nova versión permite máis exercicios de escalas e tarefas de adestramento de oído para escalas bebop, harmónica menor e maior, pentatónica e simétrica.

You can also set or take tests using the new <i>Test mode</i> for answering exercises. You can monitor your progress by running a sequence of 10 exercises and you'll get hit ratio statistics when finished.

### E máis!

<a href='https://okular.kde.org/'>Okular</a>, KDE's document viewer, has had at least half a dozen changes that add features and crank up its usability on touchscreens. <a href='https://userbase.kde.org/Akonadi'>Akonadi</a> and several other apps that make up <a href='https://www.kde.org/applications/office/kontact/'>Kontact</a> (KDE's email/calendar/groupware suite) have been revised, debugged and optimized to help you become more productive.

<a href='https://www.kde.org/applications/games/kajongg'>Kajongg</a>, <a href='https://www.kde.org/applications/development/kcachegrind/'>KCachegrind</a> and more (<a href='https://community.kde.org/Applications/17.04_Release_Notes#Tarballs_that_were_based_on_kdelibs4_and_are_now_KF5_based'>Release Notes</a>) have now been ported to KDE Frameworks 5. We look forward to your feedback and insight into the newest features introduced with this release.

<a href='https://userbase.kde.org/K3b'>K3b</a> has joined the KDE Applications release.

### Destrución de fallos

Solucionáronse máis de 95 fallos en aplicacións como, entre outras, Kopete, KWalletManager, Marble e Spectacle!

### Historial completo de cambios
