---
aliases:
- ../announce-applications-18.04.1
changelog: true
date: 2018-05-10
description: KDE publica a versión 18.04.1 das aplicacións de KDE
layout: application
title: KDE publica a versión 18.04.1 das aplicacións de KDE
version: 18.04.1
---
May 10, 2018. Today KDE released the first stability update for <a href='../18.04.0'>KDE Applications 18.04</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Arredor de 20 correccións de erros inclúen melloras en, entre outros, Kontact, Cantor, Dolphin, Gwenview, JuK, Okular e Umbrello.

Entre as melloras están:

- As entradas duplicadas nos lugares de Dolphin xa non causan quebras
- Corrixiuse un vello fallo que ocorría ao cargar de novo ficheiros SVG en Gwenview
- Agora o «import» de C++ de Umbrello entende a palabra clave «explicit»
