---
aliases:
- ../announce-4.11.4
date: 2013-12-03
description: KDE Ships Plasma Workspaces, Applications and Platform 4.11.4.
title: KDE distribúe as actualizacións de decembro para os espazos de traballo de
  Plasma, as aplicacións e a plataforma
---
3 de decembro de 2013. Hoxe KDE publica actualizacións dos seus espazos de traballo, das aplicacións e da plataforma de desenvolvemento. Estas actualizacións son as cuartas dunha serie de actualizacións de estabilización mensuais para a serie 4.11. Tal e como se anunciou na publicación, os espazos de traballo continuarán recibindo actualizacións durante os próximos dous anos. Esta versión só contén correccións e actualizacións de traducións e será unha actualización segura e agradábel para todo o mundo.

Entre as máis de 65 correccións están melloras na colección de xestión de información persoal Kontact, a ferramenta de UML Umbrello, o xestor de xanelas KWin e o navegador web Konqueror. Hai moitas correccións de estabilidade.

A more complete <a href='https://bugs.kde.org/buglist.cgi?query_format=advanced&amp;short_desc_type=allwordssubstr&amp;short_desc=&amp;long_desc_type=substring&amp;long_desc=&amp;bug_file_loc_type=allwordssubstr&amp;bug_file_loc=&amp;keywords_type=allwords&amp;keywords=&amp;bug_status=RESOLVED&amp;bug_status=VERIFIED&amp;bug_status=CLOSED&amp;emailtype1=substring&amp;email1=&amp;emailassigned_to2=1&amp;emailreporter2=1&amp;emailcc2=1&amp;emailtype2=substring&amp;email2=&amp;bugidtype=include&amp;bug_id=&amp;votes=&amp;chfieldfrom=2013-06-01&amp;chfieldto=Now&amp;chfield=cf_versionfixedin&amp;chfieldvalue=4.11.4&amp;cmdtype=doit&amp;order=Bug+Number&amp;field0-0-0=noop&amp;type0-0-0=noop&amp;value0-0-0='>list</a> of changes can be found in KDE's issue tracker. For a detailed list of changes that went into 4.11.4, you can also browse the Git logs.

Para descargar o código fonte ou paquetes para instalar vaia á <a href='/info/4/4.11.4'>páxina de información de 4.11.4</a>. Se quere saber máis sobre as versións 4.11 dos espazos de traballo, das aplicacións e da plataforma de desenvolvemento de KDE, consulte as <a href='/announcements/4.11/'>notas de publicación de 4.11</a>.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/send-later.png" caption=`O novo fluxo de traballo de enviar máis tarde en Kontact` width="600px">}}

KDE software, including all libraries and applications, is available for free under Open Source licenses. KDE's software can be obtained as source code and various binary formats from <a href='http://download.kde.org/stable/4.11.4/'>download.kde.org</a> or from any of the <a href='/distributions'>major GNU/Linux and UNIX systems</a> shipping today.
