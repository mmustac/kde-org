---
aliases:
- ../../kde-frameworks-5.43.0
date: 2018-02-12
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Uued moodulid

KHolidays: Holiday calculation library

This library provides a C++ API that determines holiday and other special events for a geographical region.

Purpose: Offers available actions for a specific purpose

This framework offers the possibility to create integrate services and actions on any application without having to implement them specifically. Purpose will offer them mechanisms to list the different alternatives to execute given the requested action type and will facilitate components so that all the plugins can receive all the information they need.

### Baloo

- balooctl status: parsitava väljundi loomine
- KIO-mooduli sildiga kataloogide süvakoopiate parandus. See rikub ära sildiga kataloogide näitamise sildipuus, aga see on siiski parem kui katkised koopiad
- Uute indekseerimatute failide järjekorda panemise välistamine ja nende kohene eemaldamine indeksist
- Uute indekseerimatute failide kustutamine indeksist

### Breeze'i ikoonid

- Puuduvate Krusaderi ikooni lisamine kataloogide sünkroonimiseks (veateade 379638)
- list-remove'i ikooni uuendamine - cancel'i ikooni asemel (veateade 382650)
- ikoonide lisamine pulseaudio plasmoidile (veateade 385294)
- kõikjal sama läbipaistmatuse 0,5 kasutamine
- Uus virtualbox'i ikoon (veateade 384357)
- päeva/öö weather-fog'i muutmine neutraalseks (veateade 3888865)
- uute animatsioonide konteksti tegelik paigaldamine
- QML-faili mime näeb nüüd ühesugune välja kõigis suurustes (veateade 376757)
- Animatsiooniikoonide uuendamine (veateade 368833)
- emblem-shared'i värvilise ikooni lisamine
- Katkiste index.theme-failide parandus, mille status/64's puudus "Context=Status"
- .svg-failidelt täitmisfaili õiguse eemaldamine
- Allalaadimise toiminguikoon on lingitud edit-download'iga (veateade 382935)
- Dropbox'i süsteemisalve ikooni teema uuendamine (veateade 383477)
- Puuduv emblem-default-symbolic (veateade 382234)
- mimetype'i failinime kirjutamine (veateade 386144)
- Konkreetsema octave'i logo kasutamine (veateade 385048)
- Seifiikoonide lisamine (veateade 386587)
- scalle px status icons (bug 386895)

### CMake'i lisamoodulid

- FindQtWaylandScanner.cmake: qmake-query kasutamine HINT'iks
- Qt5-põhise qmlplugindump'i otsimise tagamine
- ECMToolchainAndroidTest'i ei ole enam olemas (veateade 389519)
- LD_LIBRARY_PATH'i ei määrata prefix.sh's
- FindSeccomp'i lisamine moodulite leidmiseks
- Tõlgete keele kasutamine tagavaravariandina, kui lokaadi otsing nurjub
- Android: rohkemate kaasatute lisamine

### KAuth

- Linkimise tagasilanguse parandus, mis tekkis 5.42-s

### KCMUtils

- Iga kirje kahele nupule kohtspikrite lisamine

### KCompletion

- KLineEdit'i vale textEdited() väljastamise parandus (veateade 373004)

### KConfig

- Ctrl+Shift+ , kasutamine "&lt;Rakenduse&gt; seadistused" kiirklahvina

### KCoreAddons

- Ka spdx võtmetele LGPL-2.1 &amp; LGPL-2.1+ vastamine
- QMimeData palju kiirema urls() meetodi kasutamine (veateade 342056)
- inotify KDirWatch'i taustaprogrammi optimeerimine: inotify wd seondamine Entry'ga
- Optimeerimine:  QMetaObject::invokeMethod'i kasutamine koos functor'iga

### KDeclarative

- [ConfigModule] QML-i konteksti ja mootori taaskasutamine, kui võimalik (veateade 388766)
- [ConfigPropertyMap] puuduva kaasatu lisamine
- [ConfigPropertyMap] algsel loomisel ei väljastata valueChanged'i

### KDED

- kded5 ei ekspordita CMake'i sihtmärgina

### KDELibs 4 toetus

- Solid::NetworkingPrivate'i ümberkorraldamine tagamaks jagatud ja platvormipõhist teostust
- mingw kompileeerimistõrke "src/kdeui/kapplication_win.cpp:212:22: error: 'kill' was not declared in this scope" parandus
- kded dbus'i nime parandus solid'i-võrgunduse howto's

### KDesignerPlugin

- kdoctools'i sõltuvus ei ole enam kohustuslik

### KDESU

- KDESU_USE_SUDO_DEFAULT režiimi muutmine taas ehitatavaks
- kdesu töötab, kui PWD on /usr/bin

### KGlobalAccel

- cmake'i funktsiooni 'kdbusaddons_generate_dbus_service_file' (kdbusaddons'ist) kasutamine dbus'i teenusefaili genereerimiseks 382460)

### KDE GUI Addons

- Loodud OCH-faili QtGui dokumentatsiooniga linkimise parandus

### KI18n

- libintl'i otsimise parandus Yocto omapakettide "ristkompileerimisel"

### KInit

- Ristkompileerimise parandus MinGW (MXE) peal

### KIO

- Faili VFAT-ile ilma hoiatusteta kopeerimine parandus
- kio_file: tõrkekäitluse vahelejätmine algsete õiguste määramisel faili kopeerimise korral
- kio_ftp: tõrkesignaali ei väljastata, enne kui pole läbi proovitud kõiki loendikäske (veateade 387634)
- Jõudlus: KFileItem'i sihtobjekti kasutamine selgitamaks, kas see on kirjutatav, KFileItemListProperties'i loomise asemel
- Jõudlus: KFileItemListProperties'i copy lonstruktori kasutamine teisendamise asemel KFileItemList'ist KFileItemListProperties'isse. See säästab vaevast hinnata kõiki elemente uuesti
- tõrkekäitluse täiustus faili KIO-moodulis
- PrivilegeExecution'i töö lipu eemaldamine
- KRun: "lisa võrgukataloog" täitmise lubamine ilma kinnitust küsimata
- Asukohtade filtreerimise lubamine alternatiivete rakendusenimede põhjal
- [Uri Filter Search Provider] topeltkustutamise vältimine (veateade 388983)
- KFilePlacesView esimese elemendi kattumise parandus
- Ajutiselt keelatakse KAuth'i toetus KIO-s
- previewtest: lubatufd pluginate määramise lubamine
- [KFileItem] "emblem-shared" kasutamine jagatud failide korral
- [DropJob] lohistamise lubamine kirjutuskaitstud kataloogis
- [FileUndoManager] muudatuste tühistamise lubamine kirjutuskaitstud kataloogides
- Privilegeeeritud täitmise toetuse lisamine KIO töödesse (selles väljalaskes ajutiselt keelatud)
- Failideskriptori faili KIO-mooduli ja selle KAuth'i abilise vahel jagamise toetuse lisamine
- KFilePreviewGenerator::LayoutBlocker'i parandus (veateade 352776)
- KonqPopupMenu/Plugin võib nüüd tarvitadaX-KDE-RequiredNumberOfUrls'i võtit teatava arvu failide valimise nõudmiseks enne nende näitamist
- [KPropertiesDialog] reamurdmise lubamine kontrollsumma kirjelduses
- cmake'i funktsiooni 'kdbusaddons_generate_dbus_service_file' (kdbusaddons'ist) kasutamine dbus'i teenusefaili genereerimiseks (veateade 388063)

### Kirigami

- ColorGroups'i toetus
- klõpsu tagasisidet ei anta, kui element ei tunnista hiiresündmusi
- hädalahendus rakendustele, mis kasutavad listitems'it valesti
- ruumi kerimisribale (veateade 389602)
- Põhitoimingule kohtspikri pakkumine
- cmake: ametliku CMake'i muutuja kasutamine ehitamiseks staatilise pluginana
- inimmõistetavate kihitähistuste kasutamine API dokumentatsioonis
- [ScrollView] ühe lehe kaupa kerimine tõstuklahvi+hiirerattaga
- [PageRow] tasemete vahel liikumine hiire edasi-tagasinuppudega
- Tagamine, et DesktopIcon joonistatakse õige proportsiooniga (veateade 388737)

### KItemModels

- KRearrangeColumnsProxyModel: krahhi vältimine, kui lähtemudelit ei ole
- KRearrangeColumnsProxyModel: sibling() taasteostus, et see töötaks oodatult

### KJobWidgets

- Topeltkoodi eemaldamine byteSize(double size)-s (veateade 384561)

### KJS

- kdoctools'i sõltuvus ei ole enam kohustuslik

### KJSEmbed

- kjscmd ekspordi tühistamine
- kdoctools'i sõltuvus ei ole enam kohustuslik

### KNotification

- "Käivita käsk" märguande toimingu parandus (veateade 389284) 

### KTextEditor

- Parandus: vaade hüppab, kui kerimine üle dokumendi lõpu on lubatud (veateade 306745)
- Vähemalt nõutud laiuse kasutamine argumendi vihjepuus
- ExpandingWidgetModel: parempoolseima veeru leidmine asukoha põhjal

### KWidgetsAddons

- KDateComboBox: parandus dateChanged() jäi kuupäeva kirjutamise järel väljastamata (veateade 364200)
- KMultiTabBar: tagasilanguse parandus teisendamisel uues stiilis  connect() peale

### Plasma raamistik

- Omaduse defineerimine Units.qml'is Plasma stiilidele
- windowthumbnail: GLXFBConfig'i valiku koodi parandus
- [vaikimisi kohtspikker] suuruse muutmise parandus (veateade 389371)
- [Plasma dialoog] aknaefektide väljakutsumine ainult siis, kui on nähtav
- Ühe logi rämpsuga täitnud allika parandus, millele osutati veateates 388389 (funktsioonile edastati tühi failinimi)
- [Kalender] kalendri tööriistariba ankrute kohandamine
- [ConfigModel] QML-i konteksti määramine ConfigModule'ile (veateade 388766)
- [Icon Item] kaldkriipsuga algavaid allikaid käsitletakse kohalike failidena
- RTL-i välimuse parandus liitkastis (veateade 387558)

### QQC2StyleBridge

- BusyIndicator'i lisamine stiiliga juhtelementide loendisse
- plinkimise eemaldamine hiirekursoriga kerimisriba kohal viibides

### Solid

- [UDisks] mittekasutaja taustafaili eiramine ainult siis, kui see on teada  (bveateade 389358)
- Storage devices mounted outside of /media, /run/media, and $HOME are now ignored, as well as Loop Devices whose (bug 319998)
- [UDisks Device] silmuseseadme näitamine koos taustanime ja ikooniga

### Sonnet

- Aspelli sõnastike leidmine Windowsis

### Süntaksi esiletõstmine

- C# muutuja regulaaravaldise parandus
- Alakriipsude toetus arvliteraalides (Python 3.6) (veateade 385422)
- Khronos Collada ja glTF-failide esiletõstmine
- ; või # märki sisaldavate ini väärtuste esiletõsmise parandus
- AppArmor: uued võtmesõnad, täiustused ja parandused

### Turbeteave

The released code has been GPG-signed using the following key: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Väljalaske üle arutada ja mõtteid jagada saab <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>meie uudistelehekülje artikli</a> kommentaarides.
