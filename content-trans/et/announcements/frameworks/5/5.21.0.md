---
aliases:
- ../../kde-frameworks-5.21.0
date: 2016-04-09
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
Uus raamistik KActivitiesStats - teek KDE tegevushalduri kogutud kasutusstatistika andmetele juurdepääsu võimaldamiseks

### Kõik raamistikud

Nüüd on nõutav Qt &gt;= 5.4, s.t. Qt 5.3 ei ole enam toetatud.

### Attica

- Meetodile getter variandi const lisamine

### Baloo

- Pakisuuruse tsentraliseerimine seadistuses
- Koodi eemaldamine, mis blokkis laiendita .txt tekstifailide indekseerimise (veateade 358098)
- MIME tüübi tuvastamiseks nii failinime kui ka failisisu kontrollimine (veateade 353512)

### BluezQt

- ObexManager: puuduvate objektide tõrketeadete eraldamine

### Breeze'i ikoonid

- Lokalize Breeze'i ikoonide lisamine
- Breeze'i ja Breeze Dark'i rakenduseikoonide sünkroonimine
- update theme icons and remove application-system icon fix kicker groups
- xpi toetuse lisamine firefoxi lisadesse (veateade 359913)
- okulari ikooni uuendamine õige ikooniga
- ktnef'i rakenduseikooni toetuse lisamine
- kmenueditor'i, kmouse'i ja knotes'i ikooni lisamine
- helitugevuse vaigistamise ikooni muutmine: tummaks määramise korral ainult punase värvi kasutamine (veateade 360953)
- djvu MIME tüübi toetuse lisamine (veateade 360136)
- lingi lisamine topeltkirje asemel
- ms-shortcut'i ikooni lisamine gnucash'ile (veateade 360776)
- taustapildi tausta muutmine üldiseks
- ikoonide uuendamine üldise taustapildi kasutamiseks
- Konquerori ikooni lisamine (veateade 360304)
- process-working ikooni lisamine KDE edenemisanimatsioonis (veateade 360304)
- tarkvara paigaldamise ikooni lisamine ja ikooni uuendamine õigetesse värvidesse
- lisamise ja eemaldamise ikoonide lisamine Dolphini valimisse ja ühendamisikoon
- Analoogkella ja kickerdas'i apleti ikoonide laaditabeli eemaldamine
- breeze'i ja breeze dark'i sünkroonimine

### CMake'i lisamoodulid

- _ecm_update_iconcache parandus, et see uuendaks ainult paigaldamise asukohta
- "ECMQtDeclareLoggingCategory: &lt;QDebug&gt; kaasamine genereeritud failiga" tagasivõtmine

### Raamistike lõimimine

- Varuvariandina standardicon'i QCommonStyle'i teostus
- Vaikimisi menüü sulgemise ajapiirangu määramine

### KActivities

- Kompileerimiskontrollide eemaldamine, kuna kõik raamistikud nõuavad nüüd c++11
- QML ResourceModel'i eemaldamine, mille asemele astub KAStats::ResultModel
- Lisamine tühja QFlatSet'i tagastas vigase iteraatori

### KCodecs

- Koodi lihtsustamine (qCount -&gt; std::count, kodukootud isprint -&gt; QChar::isPrint)
- kodeeringu tuvastamine: krahhi vältimine isprint'i väära kasutuse korral (veateade 357341)
- Krahhi vältimine initsialiseerimata muutuja korral (veateade 357341)

### KCompletion

- KCompletionBox: jõuga raamita akna kehtestamine ja fookusest ilmajätmine
- KCompletionBox *ei tohi* olla kohtspikker

### KConfig

- Toetuse lisamine QStandardPath'i hankimiseks töölauafailidest

### KCoreAddons

- kcoreaddons_desktop_to_json() parandus windowsis
- src/lib/CMakeLists.txt - teegiga Threads linkimise parandus
- Makettide lisamine võimaldamaks kompileerimist Androidis

### KDBusAddons

- DBus'i liideses introspektiivsuse vältimine, kui me seda ei kasuta

### KDeclarative

- std::numeric_limits kasutuse ühtlustamine
- [DeclarativeDragArea] MIME andmete "text" jäetakse tühistamata

### KDELibs 4 toetus

- Iganenud lingi parandus kdebugdialog5 docbook'is
- Qt5::Network'i kui teiste ConfigureCheck'ide nõutavat teeki ei lekitata

### KDESU

- omaduste makrode määramine ehitamise võimaldamiseks musl libc'i peal

### KEmoticons

- KEmoticons: krahhi vältimine, kui loadProvider mingil põhjusel nurjub

### KGlobalAccel

- kglobalaccel5 muutmine kohaselt tapetavaks, mis parandab ära üliaeglase väljalülitamise

### KI18n

- Qt süsteemse lokaadi keelte kasutamine varuvariandina UNIXi-välistes süsteemides

### KInit

- KLauncher'i xcb pordi puhastamine ja ümberkorraldamine

### KIO

- FavIconsCache: sünkroonimine kirjutamise järel, et teised rakendused seda näeksid, ja krahhi vältimine hävitamisel
- Paljude lõimeprobleemide parandus KUrlCompletion'is
- Krahhi vältimine nime muutmise dialoogis (veateade 360488)
- KOpenWithDialog: akna tiitli ja kirjelduse teksti täiustamine (veateade 359233)
- IO-moodulite parema platvormiülese kasutamise võimaldamine protokolliteavet plugina metaandmetesse lisades

### KItemModels

- KSelectionProxyModel: rea eemaldamise käitlemise lihtsustamine, valiku tühistamise loogika lihtsustamine
- KSelectionProxyModel: seoste taasloomine eemaldamisel ainult vajaduse korral (veateade 352369)
- KSelectionProxyModel: ainult firstChild'i seoste puhastamine tipptasemel
- KSelectionProxyModel: kohaste signaali väljastamine viimase valitu eemaldamisel
- DynamicTreeModel'i muutmine otsitavaks esitamisrolli järgi

### KNewStuff

- Krahhi vältimine, kui .desktop-failid puuduvad või on vigased

### KNotification

- Vasaku nupuga klõpsamise käitlemine süsteemisalve pärandikoonidel (veateade 358589)
- Lipu X11BypassWindowManagerHint kasutamine ainult X11 platvormil

### Paketiraamistik

- Paketi paigaldamise järel selle laadimine
- kui pakett on olemas ja värske, ei nurjuta
- Package::cryptographicHash(QCryptographicHash::Algorithm) lisamine

### KPeople

- Kontakti uri määratakse isiku uri'ks PersonData's, kui isikut ei ole olemas
- Andmebaasiühendusele nime määramine

### KRunner

- Käiviti malli import KAppTemplate'ist

### KService

- Uue kbuildsycoca hoiatuse parandus, kui MIME tüüp pärineb aliasest
- x-scheme-handler/* käitlemise parandus mimeapps.list'is
- x-scheme-handler/* käitlemise parandus mimeapps.list'i parsimisel (veateade 358159)

### KTextEditor

- "Avamise/salvestamise seadistuslehekülg: "kausta" (Folder) kasutamine "kataloogi" (Directory) asemel" tagasivõtmine
- jõuga UTF-8
- Avamise/salvestamise seadistuslehekülg: "kausta" (Folder) kasutamine "kataloogi" (Directory) asemel
- kateschemaconfig.cpp: korrektsete filtrite kasutamine avamis/salvestamisdialoogides (veateade 343327)
- c.xml: vaikestiili kasutamine juhtimisvoo võtmesõnadel
- isocpp.xml: vaikimisi stiili "dsControlFlow" kasutamine juhtimisvoo võtmesõnadel
- c/isocpp: enamate C standardtüüpide lisamine
- KateRenderer::lineHeight() tagastab int'i
- trükkimine: valitud trükkimisskeemi fondi suuruse kasutamine (veateade 356110)
- cmake.xml liirendamine: WordDetect'i kasutamine RegExpr'i asemel
- Tabeldusmärgi laiuseks määrati 8 asemel 4
- Aktiivse rea numbri värvi muutmise parandus
- Lõpetuskirje hiirega valimise parandus (veateade 307052)
- gcode süntaksi esiletõstmise lisamine
- Minikaardi valiku tausta näitamise parandus
- gap.xml kodeeringu parandus (kasutatakse UTF-8)
- Pesastatud kommentaariplokkide parandus (veateade 358692)

### KWidgetsAddons

- Sisu veeriste arvestamine suurusevihje arvutamisel

### KXMLGUI

- Parandus: tööriistariba muutmisel läksid lisatud toimingud kaotsi

### NetworkManagerQt

- ConnectionSettings: lüüsi pingi aegumise initsialiseerimine
- Uus TunSetting ja Tuni ühenduse tüüp
- Kõigi teadaolevate tüüpide seadmete loomine

### Oxygeni ikoonid

- index.theme paigaldamise samasse kataloogi, kus see on kogu aeg olnud
- Paigaldamine kataloogi oxygen/base, et ikoonide liigutamine rakendustest ei satuks vastuollu nende rakenduste paigaldatud versiooniga
- Breeze'i ikoonide nimeviitade kloonimine
- Uue lisamise ja eemaldamise ikooni lisamine Breeze'iga sünkroonimiseks

### Plasma raamistik

- [kalender] parandus: kalendriaplett ei puhastanud peitmisel valikut (veateade 360683)
- heliikooni uuendamine kasutama laaditabelit
- heli vaigistamise ikooni uuendamine (veateade 360953)
- Jõuga aplettide loomise parandus, kui plasma on märgitud muudetamatuks
- [Fading Node] läbipaistmatust ei miksita eraldi (veateade 355894)
- [Svg] seadistust ei parsita uuesti Theme::applicationPaletteChanged peale
- Dialoog: SkipTaskbar/Pager olekute määramine enne akna näitamist (veateade 332024)
- Omaduse busy taastamine apletis
- PlasmaQuick'i eksportfaili sobiva leidmise tagamine
- Olematut paigutust ei impordita
- Apletil lubatakse pakkuda testobjekti
- QMenu::exec asendamine QMenu::popup-iga
- FrameSvg: rippuvate viitade parandus sharedFrames'is teema muutumisel
- IconItem: pikselrastri uuendamise ajastamine akna muutumisel
- IconItem: aktiivse ja lubatud oleku muutmise animeerimine, isegi kui animatsioonid on keelatud
- DaysModel: pesa uuendamine
- [Icon Item] eelmist pikselrastrit ei animeerita, kui see oli peidetud
- [Icon Item] loadPixmap'i ei kutsuta setColorGroup'is välja
- [Applet] tagasivõtmise märguande lippu "Püsiv" ei kirjutata üle
- Plasma muutmatuse seadistuse tühistamise lubamine konteineri loomisel
- icon/titleChanged lisamine
- QtScript'i sõltuvuse eemaldamine
- plasmaquick_export.h päis paikneb plasmaquick'i kataloogis
- Mõne plasmaquick'i päise paigaldamine

Väljalaske üle arutada ja mõtteid jagada saab <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>meie uudistelehekülje artikli</a> kommentaarides.
