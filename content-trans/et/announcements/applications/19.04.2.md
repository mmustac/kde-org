---
aliases:
- ../announce-applications-19.04.2
changelog: true
date: 2019-06-06
description: KDE Ships Applications 19.04.2.
layout: application
major_version: '19.04'
release: applications-19.04.2
title: KDE toob välja KDE rakendused 19.04.2
version: 19.04.2
---
{{% i18n_date %}}

Today KDE released the second stability update for <a href='../19.04.0'>KDE Applications 19.04</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Ligemale 50 teadaoleva veaparanduse hulka kuuluvad muu hulgas Kontacti, Arki, Dolphini, Kdenlive'i, KmPloti, Okulari ja Spectacle'i täiustused.

Täiustused sisaldavad muu hulgas:

- Parandati krahh teatavate EPUB-dokumentide näitamisel Okularis
- Krüptohaldurist Kleopatra saab taas eksportida salajasi võtmeid
- Sündmuste meeldetuletaja KAlarm suudab käivituda ka uusimate PIM-i teekidega
