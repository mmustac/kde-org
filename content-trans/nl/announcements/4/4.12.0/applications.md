---
date: '2013-12-18'
hidden: true
title: KDE Applications 4.12 biedt een geweldige stap voorwaarts in Persoonlijk informatiebeheer
  en overal verbeteringen
---
De KDE-gemeenschap is er trots op om de laatste grote update van de KDE-toepassingen, die nieuwe functies biedt en reparaties, aan te kondigen.Deze uitgave markeert grote verbeteringen in de PIM-stack van KDE, waarmee veel betere prestaties en veel nieuwe functies verkregen worden. Kate heeft de integratie met plug-ins van Python gestroomlijnd en heeft ondersteuning van Vim-macro's toegevoegd. De spellen en onderwijstoepassingen brachten een verscheidenheid aan nieuwe functies.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.12.0/screenshots/kate.png" width="600px">}}

Aan de meest geavanceerde grafische tekstbewerker Kate is opnieuw gewerkt aan codeaanvullen, deze keer invoering van <a href='http://scummos.blogspot.com/2013/10/advanced-code-completion-filtering-in.html'>geavanceerd overeenkomen van code, behandeling van afkortingen en gedeeltelijke overeenkomst in klassen</a>. De nieuw code zou bijvoorbeeld een getypeerd 'QualIdent' met 'QualifiedIdentifier' laten overeenkomen. Kate kreeg ook <a href='http://dot.kde.org/2013/09/09/kde-commit-digest-18th-august-2013'>initiële ondersteuning voor Vim-macro</a>. Het beste van alles is dat deze verbeteringen ook doordruppelen naar KDevelop en andere toepassingen die de technologie van Kate gebruiken.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.12.0/screenshots/okular.png" width="600px">}}

De documentviewer Okular <a href='http://tsdgeos.blogspot.com/2013/10/changes-in-okular-printing-for-412.html'>neemt nu marges van printerhardware in acht</a>, heeft ondersteuning van audio en video voor epub, beter zoeken en kan no meer transformaties behandelen inclusief die van Exif-metagegevens voor afbeeldingen. In het hulpmiddel Umbrello voor UML-diagrammen, kunnen associaties nu <a href='http://dot.kde.org/2013/09/20/kde-commit-digest-1st-september-2013'>getekend worden met verschillende layouts</a> en Umbrello <a href='http://dot.kde.org/2013/09/09/kde-commit-digest-25th-august-2013'>voegt visuele terugkoppeling als een widget is gedocumenteerd</a>.

Bewaker van privacy KGpg toont meer informatie aan gebruikers en aan KWalletManager, het hulpmiddel om uw wachtwoorden op te slaan, kan ze nu <a href='http://www.rusu.info/wp/?p=248'>opslaan in GPG-vorm</a>. Konsole introduceert een nieuwe functie: Ctrl-klik om direct URL's te starten in de uitvoer van console. Het kan nu ook <a href='http://martinsandsmark.wordpress.com/2013/11/02/mangonel-1-1-and-more/'>een lijst maken van processen bij het waarschuwen over afbreken</a>.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.12.0/screenshots/dolphin.png" width="600px">}}

KWebKit voegt de mogelijkheid toe om <a href='http://dot.kde.org/2013/08/09/kde-commit-digest-7th-july-2013'>automatisch inhoud te schalen om overeen te komen de resolutie van het bureaublad</a>. Bestandsbeheerder Dolphin introduceert een aantal verbeteringen van de prestaties bij het sorteren en tonen van bestanden, reduceren van geheugengebruik en de dingen te versnellen. KRDC introduceert automatisch opnieuw verbinden in VNC en KDialog biedt nu toegang tot 'detailedsorry'- en 'detailederror'-berichtvakken voor meer informatieve console-scripts. Kopete heeft zijn OTR-plug-in bijgewerkt en het Jabber-protocol heeft ondersteuning voor XEP-0264: miniaturen voor bestandsoverdracht. Naast deze mogelijkheden lag de hoofdaandacht op opschonen van code en repareren van waarschuwingen bij compileren.

### Spellen en software voor het onderwijs

Aan KDE Games is gewerkt in verschillende gebieden. KReversi is <a href='http://tsdgeos.blogspot.ch/2013/10/kreversi-master-is-now-qt-quick-based.html'>nu gebaseerd op QML en Qt Quick</a>, waarmee een prettige en meer vloeiend spel wordt ervaren. KNetWalk is ook <a href='http://tsdgeos.blogspot.ch/2013/08/knetwalk-portedx-to-qtquick.html'>geport</a> met hetzelfde voordeel als de mogelijkheid om een raster op te zetten met aangepaste breedte en hoogte. Konquest heeft nu een nieuwe uitdagende AI-speler genaamd 'Becai'.

In de onderwijstoepassingen zijn er enige grote wijzigingen. KTouch <a href='http://blog.sebasgo.net/blog/2013/11/12/what-is-new-for-ktouch-in-kde-sc-4-dot-12/'>introduceert ondersteuning voor eigen lessen en verschillende nieuwe cursussen</a>; KStars heeft een nieuwe, meer accurate <a href='http://knro.blogspot.ch/2013/10/demo-of-ekos-alignment-module.html'>uitlijnmodule voor telescopen</a>, u vindt <a href='http://www.youtube.com/watch?v=7Dcn5aFI-vA'>hier een youtube-video</a> van de nieuwe mogelijkheden. Cantor, die een gemakkelijke en krachtige GUI voor een variëteit aan mathematische backends, heeft nu backends <a href='http://blog.filipesaraiva.info/?p=1171'>voor Python2 en Scilab</a>. Lees <a href='http://blog.filipesaraiva.info/?p=1159'>hier</a> meer over de krachtige Scilab backend. Marble voegt integratie toe met ownCloud (instellingen zijn beschikbaar in Voorkeuren) en voegt ondersteuning toe voor overlay rendering. KAlgebra maakt het mogelijk om 3D-plots naar PDF te exporteren, waarmee een goede manier van delen van uw werk wordt gegeven. Tenslotte zijn vele bugs gerepareerd in de verschillende KDE onderwijstoepassingen.

### E-mail, agenda en persoonlijke informatie

KDE PIM, de set toepassingen voor afhandeling van e-mail, agenda en andere persoonlijke informatie, heeft heel wat werk ontvangen.

Beginnend met de e-mailclient KMail, is er nu <a href='http://dot.kde.org/2013/10/11/kde-commit-digest-29th-september-2013'>ondersteuning van AdBlock</a> (wanneer HTML is ingeschakeld) en verbeterde detectie van scam door uitbreiding van ingekorte URL's. Een nieuwe Agent van Akonadi genaamd FolderArchiveAgent stelt gebruikers in staat om gelezen e-mailberichten in specifieke mappen te archiveren en de GUI van de functionaliteit van Later verzenden is opgeschoond. KMail heeft ook voordeel van verbeterde ondersteuning van Sieve-filtering. Sieve stelt ook in staat om filtering van e-mailberichten te doen aan de kant van de server en u kunt nu <a href='http://www.aegiap.eu/kdeblog/2013/08/new-in-kdepim-4-12-sieve-script-parsing-22/'>filters aanmaken en wijzigen op de servers</a> en <a href='http://www.aegiap.eu/kdeblog/2013/08/new-in-kdepim-4-12-sieve-12/'>bestaande filters van KMail filters converteren naar filters op de server</a>. Ondersteuning van mbox  in KMail <a href='http://www.aegiap.eu/kdeblog/2013/08/new-in-kdepim-4-12-mboximporter/'>is ook verbeterd</a>.

In andere toepassingen zijn verschillende wijzigingen aangebracht om werken gemakkelijker en meer vreugdevol te maken. Er is een nieuw hulpmiddel geïntroduceerd, <a href='http://www.aegiap.eu/kdeblog/2013/11/new-in-kdepim-4-12-kaddressbook/'>de ContactThemeEditor</a>, die in staat stelt om Grantlee-thema's te maken voor KAddressBook voor het tonen van contactpersonen. Het adresboek kan nu ook voorbeelden tonen alvorens gegevens af te drukken. Aan KNotes is <a href='http://www.aegiap.eu/kdeblog/2013/11/what-news-in-kdepim-4-12-knotes/'>serieus werk verricht om bugs te repareren</a>. Blogilo, het hulpmiddel voor blogging, kan nu met vertalingen werken en is een brede variëteit van reparaties en verbeteringen in alle KDE PIM toepassingen uitgevoerd.

Tot voordeel van alle toepassingen heeft de onderliggende KDE PIM gegevenscache <a href='http://ltinkl.blogspot.ch/2013/11/this-month-october-in-red-hat-kde.html'>veel werk ontvangen op het gebied van prestaties, stabiliteit en schaalbaarheid</a>, reparatie van de <a href='http://www.progdan.cz/2013/10/akonadi-1-10-3-with-postgresql-fix/'>ondersteuning voor PostgreSQL met de laatste Qt 4.8.5</a>. Verder is er een nieuw hulpmiddel voor de commandoregel, de calendarjanitor die alle agendagegevens kan scannen op buggy incidenten en een debugdialoog toevoegt voor zoeken. Zeer speciale dank gaat uit naar Laurent Montel voor het werk dat hij doet op het gebied van functies in KDE PIM!

#### KDE-toepassingen installeren

KDE software, inclusief al zijn bibliotheken en toepassingen, zijn vrij beschikbaar onder Open-Source licenties. KDE software werkt op verschillende hardware configuraties en CPU architecturen zoals ARM- en x86-besturingssystemen en werkt met elk soort windowmanager of bureaubladomgeving. Naast Linux en andere op UNIX gebaseerde besturingssystemen kunt u Microsoft Windows versies van de meeste KDE-toepassingen vinden op de site <a href='http://windows.kde.org'>KDE-software op Windows</a> en Apple Mac OS X versies op de site <a href='http://mac.kde.org/'>KDE software op Mac</a>. Experimentele bouwsels van KDE-toepassingen voor verschillende mobiele platforms zoals MeeGo, MS Windows Mobile en Symbian zijn te vinden op het web maar worden nu niet onderssteund. <a href='http://plasma-active.org'>Plasma Active</a> is een gebruikservaring voor een breder spectrum van apparaten, zoals tabletcomputers en andere mobiele hardware.

KDE software is verkrijgbaar in broncode en verschillende binaire formaten uit<a href='http://download.kde.org/stable/4.12.0'>download.kde.org</a> en kan ook verkregen worden op <a href='/download'>CD-ROM</a> of met elk van de <a href='/distributions'>belangrijke GNU/Linux en UNIX systemen</a> van vandaag.

##### Pakketten

Sommige Linux/UNIX OS leveranciers zijn zo vriendelijk binaire pakketten van 4.12.0 voor sommige versies van hun distributie te leveren en in andere gevallen hebben vrijwilligers uit de gemeenschap dat gedaan.

##### Locaties van pakketten

Voor een huidige lijst met beschikbare binaire pakketten waarover het KDE Project is geïnformeerd, bezoekt u de <a href='http://community.kde.org/KDE_SC/Binary_Packages#KDE_SC_4.12.0'>Wiki van de gemeenschap</a>.

De complete broncode voor 4.12.0 kan <a href='/info/4/4.12.0'>vrij gedownload worden</a>. Instructies over compileren en installeren van KDE software 4.12.0 zijn beschikbaar vanaf de <a href='/info/4/4.12.0#binary'>4.12.0 informatiepagina</a>.

#### Systeemvereisten

Om het meeste uit deze uitgaven te halen, bevelen we aan om een recente versie van Qt, zoals 4.8.4, te gebruiken. Dit is noodzakelijk om een stabiele ervaring met hoge prestaties te verzekeren omdat sommige verbeteringen, die zijn gemaakt in KDE software, eigenlijk zijn gedaan in het onderliggende Qt-framework.

Om volledige gebruik te maken van de mogelijkheden van de software van KDE, bevelen we ook aan de laatste grafische stuurprogramma's voor uw systeem te gebruiken, omdat dit de gebruikerservaring aanzienlijk verhoogt, beiden in optionele functionaliteit en in algehele prestaties en stabiliteit.
