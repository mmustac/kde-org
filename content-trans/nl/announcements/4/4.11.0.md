---
aliases:
- ../4.11
custom_about: true
custom_contact: true
date: 2013-08-14
description: KDE stelt Plasma Workspaces, Applicaties en Platform 4.11 beschikbaar
title: KDE Software Compilation 4.11
---
{{<figure src="/announcements/4/4.11.0/screenshots/plasma-4.11.png" class="text-center" caption=`De KDE Plasma Workspaces 4.11` >}} <br />

14 augustus 2013. De KDE gemeenschap is er trots op om de nieuwste hoofdverbeteringen op Plasma Workspaces, Applications and Development Platform aan te kondigen, die nieuwe functies en reparaties laat zien, samen met het gereed maken van het platform voor verdere evoluties. Plasma Workspaces 4.11 zal lange termijn onderhoud ontvangen terwijl het team zich zal richten op de technische transitie naar Frameworks 5. Dit biedt dus de laatste gecombineerde uitgave van Workspaces, Applications and Platform onder hetzelfde versienummer.<br />

Deze uitgave is opgedragen aan de herinnering van <a href='http://en.wikipedia.org/wiki/Atul_Chitnis'>Atul 'toolz' Chitnis</a>, een grote kampioen voor Vrije en Opensource Software uit India. Atul leidde de conferenties "Linux Bangalore and FOSS.IN" sinds 2001 en beide waren opvallende gebeurtenissen in de Indian FOSS scene. KDE India is geboren op de eerste FOSS.in in december 2005. Velen die hebben bijgedragen aan KDE in India zijn begonnen op deze gebeurtenissen. Het was alleen aan de aanmoedigingen van Atul te danken dat de KDE Project-dag op FOSS.IN altijd een groot succes was. Atul heeft ons op 3 juni verlaten na een gevecht met kanker. Moge zijn ziel rusten in vrede. We zijn dankbaar voor zijn bijdragen aan een betere wereld.

Deze uitgaven zijn allen vertaald in 54 talen; we verwachten dat meer talen toegevoegd zullen worden in volgende maandelijkse kleine bugfix-uitgaven door KDE. Het documentatieteam heeft 91 handboeken van toepassingen voor deze uitgave bijgewerkt.

## <a href="./plasma"><img src="/announcements/4/4.11.0/images/plasma.png" class="app-icon" alt="De KDE Plasma Workspaces 4.11" width="64" height="64" /> Plasma Workspaces 4.11 gaat door met verfijnen van gebruikservaring</a>

Versnellen voor onderhoud op lange termijn, levert Plasma Workspaces verdere verbeteringen aan de basis functionaliteit met een soepelere taakbalk, slimmere widget voor de batterij en verbeterde soundmixer. De introductie van KScreen brengt intelligentere behandeling van meerdere monitoren naar de Werkruimten en verbeteringen van de prestaties op grote schaal, gecombineerd met kleine verbeteringen aan bruikbaarheid, maken dat over het geheel de ervaring plezieriger is.

## <a href="./applications"><img src="/announcements/4/4.11.0/images/applications.png" class="app-icon" alt="The KDE Applications 4.11"/> KDE Applications 4.11 biedt een geweldige stap voorwaarts in Persoonlijk informatiebeheer en overal verbeteringen</a>

Deze uitgave markeert grote verbeteringen in de PIM-stack van KDE, waarmee veel betere prestaties en veel nieuwe functies verkregen worden. Kate verbetert de productiviteit van Python en Javascript ontwikkelaars met nieuwe plug-ins, Dolphin werd sneller en de onderwijstoepassingen brachten verschillende nieuwe functies.

## <a href="./platform"><img src="/announcements/4/4.11.0/images/platform.png" class="app-icon" alt="The KDE Development Platform 4.11"/> KDE Platform 4.11 levert betere prestaties</a>

Deze uitgave van KDE Platform 4.11 gaat door met focus op stabiliteit. Nieuwe functies zullen geïmplementeerd worden in onze toekomstige uitgave KDE Frameworks 5.0, maar voor de stabiele uitgave is het gelukt om optimalisatie samen te ballen in ons Nepomuk framework.

<br />
Bij opwaardering, kijk naar de <a href='http://community.kde.org/KDE_SC/4.11_Release_Notes'>uitgavenotities</a>.<br />

## Zeht het voort en Zie wat er gebeurt: Tag als &quot;KDE&quot;

KDE moedigt mensen aan om op het Sociale Web de nieuwe versie rond te bazuinen. Dien verhalen in op nieuwssites, gebruik kanalen zoals delicious, digg, reddit, twitter en identi.ca. Upload schermafdrukken naar services zoals Facebook, Flickr, ipernity en Picasa en stuur ze aan van toepassing zijnde groepen. Maak screencasts en upload ze naar YouTube, Blip.tv, en Vimeo. Geef er tags als &quot;KDE&quot; aan. Dit maakt ze gemakkelijk te vinden en geeft het KDE Promoteam een manier om de dekking van de uitgave 4.11 van KDE software te analyseren.

## Uitgavefeestjes

Zoals gebruikelijk organiseren, leden van de KDE-gemeenschap overal op de wereld uitgavefeestjes. Er zijn er al heel wat gepland en later komen er meer. Zoek <a href='http://community.kde.org/Promo/Events/Release_Parties/4.11'>een lijst met parties hier</a>. iedereen is welkom om mee te doen! Er zal een combinatie van geïnteresseerde bedrijven en inspirerende gesprekken zijn evenals wat te eten en te drinken. Het is een goede gelegenheid om meer te weten te komen over wat er gaande is in KDE, er in mee te doen of gewoon andere gebruikers en medewerkers te ontmoeten.

We moedigen mensen aan om hun eigen parties te organiseren. Ze bieden plezier om gastheer/vrouw te zijn en open voor iedereen. Kijk op <a href='http://community.kde.org/Promo/Events/Release_Parties'>tips over hoe een party te organiseren</a>.

## Over deze aankondigingen van uitgave

Deze aankondigingen van uitgave zijn voorbereid door Jos Poortvliet, Sebastian Kügler, Markus Slopianka, Burkhard Lück, Valorie Zimmerman, Maarten De Meyer, Frank Reininghaus, Michael Pyne, Martin Gräßlin en andere leden van het KDE promotieteam en de bredere KDE-gemeenschap. Ze dekken de belangrijkste van de vele wijzigingen die zijn gemaakt in de KDE-software in de laatste zes maanden.

#### KDE ondersteunen

<a href="http://jointhegame.kde.org/"> <img src="/announcements/4/4.9.0/images/join-the-game.png" class="img-fluid float-left mr-3" alt="Doe mee met het spel"/> </a>

Het nieuwe <a href='http://jointhegame.kde.org/'>programma voor ondersteunende leden</a> van KDE e.V. is nu open.  Voor &euro;25 per kwartaal verzekert u dat de internationale gemeenschap van KDE blijft groeien met het maken Vrije Software van wereldklasse.
