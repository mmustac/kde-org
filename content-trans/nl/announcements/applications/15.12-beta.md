---
aliases:
- ../announce-applications-15.12-beta
date: 2015-11-20
description: KDE stelt KDE Applicaties 15.12 Beta beschikbaar
layout: application
release: applications-15.11.80
title: KDE stelt de beta van KDE Applicaties 15.12 beschikbaar
---
20 november 2015. Vandaag heeft KDE de beta van de nieuwe versies van KDE Applications vrijgegeven. Met het bevriezen van afhankelijkheden en functies, is het team van KDE nu gefocust op repareren van bugs en verder oppoetsen.

Met de verschillende toepassingen gebaseerd op KDE Frameworks 5, heeft de KDE Applications 15.12 uitgave grondig testen nodig om de kwaliteit en gebruikservaring te handhaven en te verbeteren. Echte gebruikers zijn kritisch in het proces om de hoge kwaliteit van KDE te handhaven, omdat ontwikkelaars eenvoudig niet elke mogelijke configuratie kunnen testen. We rekenen op u om in een vroeg stadium bugs te vinden zodat ze gekraakt kunnen worden voor de uiteindelijke vrijgave. Ga na of u met het team mee kunt doen door de beta te installeren <a href='https://bugs.kde.org/'>en elke bug te rapporteren</a>.
