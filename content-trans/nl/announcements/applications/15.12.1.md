---
aliases:
- ../announce-applications-15.12.1
changelog: true
date: 2016-01-12
description: KDE stelt KDE Applicaties 15.12.1 beschikbaar
layout: application
title: KDE stelt KDE Applicaties 15.12.1 beschikbaar
version: 15.12.1
---
12 januari 2016. Vandaag heeft KDE de eerste stabiele update vrijgegeven voor <a href='../15.12.0'>KDE Applicaties 15.12</a> Deze uitgave bevat alleen reparaties van bugs en updates van vertalingen, die een veilige en plezierige update voor iedereen levert.

Meer dan 30 aangegeven bugreparaties inclusief verbeteringen aan kdelibs, kdepim, kdenlive, marble, konsole, spectacle, akonadi, ark en umbrello, naast anderen.

Deze uitgave bevat ook ondersteuning op de lange termijn versies van KDE Development Platform 4.14.16.
