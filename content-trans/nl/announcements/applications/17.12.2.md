---
aliases:
- ../announce-applications-17.12.2
changelog: true
date: 2018-02-08
description: KDE stelt KDE Applicaties 17.12.2 beschikbaar
layout: application
title: KDE stelt KDE Applicaties 17.12.2 beschikbaar
version: 17.12.2
---
8 februari 2018. Vandaag heeft KDE de tweede stabiele update vrijgegeven voor <a href='../17.12.0'>KDE Applicaties 17.12</a> Deze uitgave bevat alleen bugreparaties en updates van vertalingen, die een veilige en plezierige update voor iedereen levert.

Ongeveer 20 aangegeven reparaties van bugs, die verbeteringen bevatten aan Kontact, Dolphin, Gwenview, KGet, Okular, naast andere.
