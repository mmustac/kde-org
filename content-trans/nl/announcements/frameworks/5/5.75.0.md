---
aliases:
- ../../kde-frameworks-5.75.0
date: 2020-10-10
layout: framework
libCount: 70
qtversion: 5.12
---
### Baloo

+ [AdvancedQueryParser] ontleden van tekenreeks die eindigt met haakjes verzachten
+ [AdvancedQueryParser] ontleden van tekenreeks die eindigt met vergelijker verzachten
+ [AdvancedQueryParser] Toegang buiten grenzen repareren als laatste teken een vergelijkingsteken is
+ [Term] constructor Term::Private vervangen door standaard waarden
+ [BasicIndexingJob] XAttr ophalen kortsluiten voor bestanden zonder attributen
+ [extractor] documenttype repareren in extractieresultaat
+ Licentie van bestand wijzigen naar LGPL-2.0-of-later

### BluezQt

+ Eigenschap rfkill aan beheerder toevoegen
+ Status eigenschap aan rfkill toevoegen
+ Rfkill registreren voor QML
+ Rfkill exporteren
+ Leveren van servicegegevenwaarden voor LE advertenties ondersteunen

### Breeze pictogrammen

+ Nieuw algemeen pictogram van "gedrag" toevoegen
+ Pictogramvalidatie afhankelijk maken van pictogramgeneratie alleen indien ingeschakeld
+ 24px pictogram van bash-script vervangen door python-script
+ Pictogram in stijl met vlag gebruiken voor view-calendar-holiday
+ Plasma Nano-logo toevoegen
+ application-x-kmymoney toevoegen
+ KMyMoney-pictogram toevoegen

### Extra CMake-modules

+ ophalen van vertalingen voor invent-url's repareren
+ FeatureSummary en zoekmodulen insluiten
+ Controle op plausibiliteit introduceren voor uitgaande licentie
+ CheckAtomic.cmake toevoegen
+ Configuratie met pthread op Android 32 bits repareren
+ RENAME-parameter toevoegen aan ecm_generate_dbus_service_file
+ find_library op Android met NDK &lt; 22 repareren
+ Android versielijsten expliciet sorteren
+ Android {min,target,compile}Sdk in variabelen opslaan

### KDE Doxygen hulpmiddelen

+ Verbeteringen aan licentie uitgeven
+ api.kde.org op mobiel repareren
+ api.kde.org een PWA maken

### KArchive

+ Licentie van bestand wijzigen naar LGPL-2.0-of-later

### KAuth

+ nieuwe install-var gebruiken (bug 415938)
+ Mark David Edmundson als onderhouder voor KAuth

### KCalendarCore

+ Licentie van bestand wijzigen naar LGPL-2.0-of-later

### KCMUtils

+ Behandeling voor inwendige gebeurtenissen uit tab-hack verwijderen (bug 423080)

### KCompletion

+ Licentie van bestanden wijzigen naar LGPL-2.0-of-later

### KConfig

+ CMake: SKIP_AUTOUIC ook op gegenereerde bestanden zetten
+ Omgekeerde volgorde in KDesktopFile::locateLocal gebruiken om over algemene configuratiepaden te lopen

### KConfigWidgets

+ isDefault repareren die veroorzaakt dat de KCModule niet juist zijn standaardstatus bijwerkt
+ [kcolorscheme]: isColorSetSupported toevoegen om te controleren of een kleurenschema een gegeven kleurenset bevat
+ [kcolorscheme] aangepaste Inactive kleuren op de juiste manier lezen voor de achtergronden

### KContacts

+ Afgekeurd licentiebestand voor LGPL-2.0-only verwijderen
+ Licentie van bestanden wijzigen naar LGPL-2.0-of-later

### KCoreAddons

+ KJob: result() en finished() hooguit eenmaal uitsturen
+ beschermde KJob::isFinished() ophaler toevoegen
+ KRandomSequence afkeuren met voorkeur voor QRandomGenerator
+ Variabele initialiseren in header-class + const'ify variable/pointer
+ op bericht gebaseerde testen tegen omgeving harder maken (bug 387006)
+ qrc bewakingstest vereenvoudigen (bug 387006)
+ refcount en verwijder KDirWatchPrivate exemplaren (bug 423928)
+ KBackup::backupFile() afkeuren vanwege verloren functionaliteit
+ KBackup::rcsBackupFile(...) afkeuren vanwege niet-bekende gebruikers

### KDBusAddons

+ Licentie van bestanden wijzigen naar LGPL-2.0-of-later

### KDeclarative

+ QML voor I18n zijn toegevoegd in KF 5.17
+ Licentie van bestanden wijzigen naar LGPL-2.0-of-later
+ Sneltoetsen blokkeren bij opnemen van toetsreeksen (bug 425979)
+ SettingHighlighter toevoegen als een handleidingversie van de accentuering gedaan door SettingStateBinding

### Ondersteuning van KDELibs 4

+ KStandardDirs: symbolische koppelingen altijd volgen voor configuratie bestanden

### KHolidays

+ Commentaar verwijderd uit beschrijvingsvelden voor mt_* vakantiebestanden
+ Nationale vakantiedagen voor Malta toevoegen in zowel Engels (en-gb) en Maltees (mt)

### KI18n

+ Licentie van bestand wijzigen naar LGPL-2.0-of-later

### KIO

+ KUrlNavigator: altijd "desktop:/" gebruiken geen "desktop:"
+ DuckDuckGo bang-syntaxis ondersteunen in Webshortcuts (bug 374637)
+ KNewFileMenu: KIO::mostLocalUrl is nuttig met alleen :local-protocollen
+ KIO::pixmapForUrl afkeuren
+ kio_trash: onnodige strikte controle op rechten verwijderen (bug 76380)
+ OpenUrlJob: alle tekstscripts consistent behandelen (bug 425177)
+ KProcessRunner: meer systemd metagegevens
+ KDirOperator: setCurrentItem niet aanroepen op een lege url (bug 425163)
+ KNewFileMenu: aanmaken van nieuwe map met naam beginnend met ':' repareren (bug 425396)
+ StatJob: helderder maken dat mostLocalUrl alleen werkt met protocollen :local
+ Hoe nieuwe "random" rollen toevoegen documenteren in kfileplacesmodel.h
+ Oude kio_fonts hack in KCoreDirLister verwijderen, hostnaam was onjuist gestript
+ KUrlCompletion: neem ":local" protocollen mee die hostnaam in url gebruiken
+ Code van addServiceActionsTo methode splitsen in kleinere methoden
+ [kio] BUG: aanhalingstekens toestaan in openen/opslaan-dialoog (bug 185433)
+ StatJob: job annuleren als url ongeldig is (bug 426367)
+ Slots expliciet verbinden in plaats van automatische verbindingen gebruiken
+ Zorgen dat filesharingpage API echt werkt

### Kirigami

+ AbstractApplicationHeader: anchors.fill in plaats van positie-afhankelijke verankering
+ Uiterlijk en consistentie van GlobalDrawerActionItem verbeteren
+ formulierinspringen voor smalle indelingen verwijderen
+ "aanpassen van de kleuren van koppen toestaan" terugdraaien
+ aanpassen van de kleuren van koppen toestaan
+ Ontbrekende @since toevoegen voor de getekende gebiedseigenschappen
+ QtQuick afbeeldingsstijl paintedWidth/paintedHeight eigenschappen introduceren
+ Een plaatshouder afbeeldingseigenschap naar pictogram toevoegen (in de stijl van terugvallen)
+ Aangepast implicitWidth/implicitHeight gedrag van pictogram verwijderen
+ Potentiële nul-aanwijzers bewaken (blijkt verrassend algemeen te zijn)
+ beveiligde setStatus
+ Statuseigenschap introduceren
+ ImageResponse en Texture type leveranciers van afbeeldingen in Kirigami::Icon ondersteunen
+ Mensen waarschuwen geen ScrollView in ScrollablePage laten gebruiken
+ "altijd scheiding tonen" terugdraaien
+ mobilemode aangepaste titelgedelegeerden laten ondersteunen
+ Broodkruimelscheidingslijn verbergen als indeling knoppen zichtbaar is maar breedte 0 (bug 426738)
+ [icon] pictogram als ongeldig beschouwen wanneer bron een lege URL is
+ Units.fontMetrics wijzigen naar actueel gebruik van FontMetrics
+ Eigenschap van Kirigami.FormData.labelAlignment toevoegen
+ scheiding altijd tonen
+ Kopkleuren voor bureaubladstijl gebruiken van AbstractApplicationHeader
+ De context van de component gebruiken bij aanmaken van gedelegeerden voor ToolBarLayout
+ Incubators afbreken en verwijderen bij verwijderen van ToolBarLayoutDelegate
+ Acties en gedelegeerden verwijderen uit ToolBarLayout wanneer ze verwijderd worden (bug 425670)
+ Gebruik van c-stijl aanwijzercast in sizegroup vervangen
+ binaire constanten zijn een C++14 extensie
+ sizegroup: enum met niet behandelde waarschuwingen repareren
+ sizegroup: 3arg verbindingen repareren
+ sizegroup: CONSTANT toevoegen aan signaal
+ Een paar gevallen van gebruik van reekslussen op niet-constante Qt containers repareren
+ Knophoogte beperken in globale werkbalk
+ Een scheiding tonen tussen broodkruimels en de pictogrammen links
+ KDE_INSTALL_TARGETS_DEFAULT_ARGS gebruiken
+ ApplicationHeaders grootte geven met gebruik van de SizeGroup
+ SizeGroup introduceren
+ Reparatie: vernieuwingsindicator laten verschijnen boven lijstkoppen
+ overlay-vellen over laden zetten

### KItemModels

+ sourceDataChanged op ongeldige indexen negeren
+ Ondersteuning voor KDescendantsProxyModel nodes die "invouwen"

### KNewStuff

+ De lifecycle van onze kpackage runner internals handmatig volgen
+ Versies voor "fake" kpackage updates bijwerken (bug 427201)
+ Standaard parameter niet gebruiken indien niet nodig
+ Crash repareren bij installeren van kpackages (bug 426732)
+ Wanneer de cache wijzigt detecteren en overeenkomstig reageren
+ bijwerken van item als versienummer leeg is repareren (bug 417510)
+ Aanwijzer + initialiseren van variabele in kop constante maken
+ Licentie van bestand wijzigen naar LGPL-2.0-of-later
+ Suggestie voor overnam van onderhouder accepteren

### KNotification

+ Android plug-in verlagen totdat nieuw Gradle beschikbaar is
+ Android SDK-versies uit ECM gebruiken
+ KNotification-constructor die widgetparameter gebruikt afkeuren

### KPackage-framework

+ DBus-meldingen repareren indien geïnstalleerd/bijgewerkt
+ Licentie van bestand wijzigen naar LGPL-2.0-of-later

### KParts

+ krop &amp; krwp servicetype definitiebestanden installeren op overeenkomst van type in bestandsnaam 

### KQuickCharts

+ Binding loop binnen Legend vermijden
+ Controle op GLES3 in SDFShader verwijderen (bug 426458)

### KRunner

+ Eigenschap matchRegex toevoegen om onnodig afsplitsen van thread te voorkomen
+ Acties instellen toestaan in QueryMatch
+ Specifieke individuele acties toestaan voor D-Bus overeenkomsten met runner
+ Licentie van bestanden wijzigen naar LGPL-2.0-of-later
+ Eigenschap minimaal aantal letters toevoegen
+ XDG_DATA_HOME omgevingsvariabele overwegen voor installatiemappen van sjablonen
+ Foutmeldingen verbeteren voor D-Bus uitvoerders
+ Waarschuwingen beginnen uit te zenden bij overzetten van metagegevens bij uitvoeren

### KService

+ disableAutoRebuild van het randje terugbrengen (bug 423931)

### KTextEditor

+ [kateprinter] ga weg van afgekeurde QPrinter-methoden
+ Geen tijdelijke buffer aanmaken om mimetype voor opgeslagen lokale bestand te detecteren
+ hangen vermijden vanwege laden van woordenboeken en trigrams bij eerste typen
+ [Vimode] a-z buffers altijd in kleine letters tonen
+ [KateFadeEffect]emit hideAnimationFinished() wanneer een uitvagen onderbroken wordt door een invagen in
+ ga na pixel perfecte rand zelfs voor geschaalde rendering
+ ga na dat we de randscheider overschrijven over alle andere zaken zoals uitvouwaccentuering
+ scheidingsteken van tussen pictogramrand en regelnummers naar tussen balk en tekst verplaatsen
+ [Vimode] gedrag van genummerde registers repareren
+ [Vimode] verwijderde tekst in het juiste register stoppen
+ Gedrag van geselecteerde zoeken herstellen wanneer geen selectie beschikbaar is
+ geen verdere LGPL-2.1-alleen of LGPL-3.0-alleen bestanden
+ Licentie van bestanden wijzigen naar LGPL-2.0-of-later
+ instellingsmethode gebruik niet nodig voor sommige themakleuren
+ 5.75 zal eenmaal incompatibel zijn, standaard naar 'Automatische selectie van kleurthema' voor thema's
+ schema =&gt; thema in de code veranderen om verwarring te vermijden
+ voorgestelde licentiekoppen inkorten naar huidige status
+ ga na dat we altijd eindigen met een geldig thema
+ Kopieer... dialoog verbeteren
+ meer nieuw =&gt; kopie benaming repareren
+ enige KMessageWidget toevoegen die hinten naar alleen-lezen thema's om ze te kopiëren, hernoem nieuw =&gt; kopie
+ bewerken van alleen-lezen thema's uitschakelen
+ opslaan van accentuering van specifieke stijloverschrijving werkt, alleen verschillen worden opgeslagen
+ aanmaken van attribuut vereenvoudigen, transparante kleuren worden nu juist behandeld in Format
+ exporteren naar attributen voor wijzigingen beperken, dit werkt halfweg, maar we exporteren nog steeds de verkeerde namen voor ingevoegde definities
+ start met berekenen van de 'echte' standaarden gebaseerd op het huidige thema en formaten zonder stijloverschrijving voor de accentuering
+ resetactie repareren
+ syntaxis specifieke overschrijving opslaan, op het moment dat alle zaken die zijn geladen in de boomstructuurweergave zijn opgeslagen
+ start met werken aan syntaxisaccentuering specifieke overschrijving, op het moment, alleen de stijlen die echt een accentuering zelf hebben tonen
+ standaard wijzigingen in stijl toestaan te worden opgeslagen
+ kleurwijzigingen toestaan te worden opgeslagen
+ thema exporteren implementeren: eenvoudig kopiëren van bestand
+ geen accentuering van specifiek importeren/exporteren, heeft geen zin met nieuw formaat .theme
+ bestandsimport van .theme implementeren
+ thema nieuw &amp; werk verwijderen, nieuw zal het huidige thema als startpunt kopiëren
+ themakleuren overal gebruiken
+ meer oude schemacode verwijderen met voorkeur voor KSyntaxHighlighting::Theme
+ met de kleuren gebruiken beginnen zoals ingesteld door het thema, zonder eigen logica er omheen
+ m_pasteSelection initialiseren en UI bestandsversie verhogen
+ sneltoets toevoegen voor muisselectie plakken
+ setTheme vermijden, we geven ons thema door aan de helperfuncties
+ tekstballon repareren, dit is gewoon een reset naar het standaard thema
+ standaard stijlenconfiguratie naar json-thema exporteren
+ start met werken aan thema json exporteren, geactiveerd door de extensie .theme in de exportdialoog te gebruiken
+ 'KDE kleurenthema gebruiken' hernoemen naar 'Standaard kleuren gebruiken', dat is het echte effect
+ leeg 'KDE'-thema niet standaard meeleveren
+ automatische selectie van juiste thema voor huidige Qt/KDE kleurenthema ondersteunen
+ oude themanamen naar nieuwe converteren, nieuw configuratiebestand gebruiken, gegevens eenmaal verplaatsen
+ configuratie van lettertypen verplaatsen naar uiterlijk, hernoem schema =&gt; kleurthema
+ hard gecodeerde standaard themanaam verwijderen, KSyntaxHighlighting accessors gebruiken
+ terugval kleuren uit thema laden
+ Ingebedde kleuren helemaal niet bundelen
+ juiste functie gebruiken om thema op te zoeken
+ Kleuren van bewerker worden nu gebruikt uit thema
+ de KSyntaxHighlighting::Theme::EditorColorRole enum gebruiken
+ Normaal =&gt; behandelen als standaardovergang
+ eerste stap: themalijst uit KSyntaxHighlighting laden, nu we ze al als bekende schema's in KTextEditor hebben

### KUnitConversion

+ Hoofdletters gebruiken voor brandstofefficiëntie

### KWayland

+ QList::end() iterator niet in cache opslaan als erase() wordt gebruikt

### KWidgetsAddons

+ kviewstateserializer.cpp - crash bewaken in restoreScrollBarState()

### KWindowSystem

+ Bestanden van licentie voorzien om compatibel te zijn met LGPL-2.1

### KXMLGUI

+ [kmainwindow] Geen items verwijderen uit een ongeldig kconfiggroup (bug 427236)
+ Hoofdvensters niet laten overlappen bij openen van extra exemplaren (bug 426725)
+ [kmainwindow] Geen inheemse vensters aanmaken voor niet-topniveau vensters (bug 424024)
+ KAboutApplicationDialog: leeg tabblad "Bibliotheken" vermijden als HideKdeVersion is ingesteld
+ Taalcode tonen naast (vertaalde) taalnaam in dialoog voor omschakelen van taal van toepassing
+ KShortcutsEditor::undoChanges() afkeuren met voorkeur voor nieuwe undo()
+ Dubbel sluiten in hoofdvenster behandelen (bug 416728)

### Plasma Framework

+ plasmoidheading.svgz die op de verkeerde plaats is geïnstalleerd repareren (bug 426537)
+ Een lastModified waarde in ThemeTest leveren
+ Ontdekken dat we zoeken naar een leeg element en vroeg afsluiten
+ PlasmaComponents3 tekstballonnen de typische tekstballonnenstijl laten gebruiken (bug 424506)
+ Kopkleuren in PlasmoidHeading koppen gebruiken
+ PC2 TabBar accentueringsverplaatsingsanimatie wijzigen type naar OutCubic vergemakkelijken
+ Ondersteuning voor kleurenset van tekstballon toevoegen
+ PC3 Button/ToolButton pictogrammen die niet altijd de juiste kleurset hebben repareren (bug 426556)
+ Verzekeren dat FrameSvg lastModified tijdstempel gebruikt bij gebruik van cache (bug 426674)
+ Verzekeren dat we altijd een geldig lastModified tijdstempel hebben wanneer setImagePath wordt aangeroepen
+ Een lastModified tijdstip van 0 in Theme::findInCache afkeuren (bug 426674)
+ QQC2-import aanpassen aan nieuw schema voor versies
+ [windowthumbnail] bestaan van de relevante GLContext verifiëren, niet een
+ Ontbrekend importeren van PlasmaCore aan ButtonRow.qml toevoegen
+ Een paar meer referentiefouten in PlasmaExtras.ListItem repareren
+ Fout voor implicitBackgroundWidth in PlasmaExtras.ListItem repareren
+ Bewerkingsmodus "Bewerkingsmodus" aanroepen
+ Een TypeError in QueryDialog.qml repareren
+ ReferenceError naar PlasmaCore in Button.qml repareren

### QQC2StyleBridge

+ Tekstkleur voor accentuering ook gebruiken wanneer checkDelegate wordt geaccentueerd (bug 427022)
+ Klikken in schuifbalk respecteren om naar instelling van positie te springen (bug 412685)
+ Geen kleuren in bureaubladwerkbalkstijl standaard erven
+ Licentie van bestand wijzigen naar LGPL-2.0-of-later
+ Kleuren voor koppen alleen gebruiken voor kopwerkbalken
+ Declaratie van kleurenset verplaatsen naar een plaats waar het overschreven kan worden
+ Kopkleuren voor bureaubladstijl werkbalk gebruiken
+ de ontbrekende eigenschap isItem toevoegen nodig voor boomstructuur

### Sonnet

+ Uitvoer van trigrams degraderen

### Accentuering van syntaxis

+ AppArmor: reguliere expressie van detectie van paden repareren
+ AppArmor: accentuering voor AppArmor 3.0 bijwerken
+ kleurencache voor rgb naar ansi256colors conversies (versnelt laden van markdown)
+ SELinux: include sleutelwoorden gebruiken
+ SubRip Subtitles &amp; Logcat: kleine verbeteringen
+ generator voor doxygenlua.xml
+ doxygen latex-formules repareren (bug 426466)
+ Q_ASSERT gebruiken zoals in overblijvend framework + reparatie van toekenningen
+ hernoem --format-trace naar --syntax-trace
+ een stijl op gebieden toepassen
+ contexten en gebieden traceren
+ achtergrondkleur van bewerker standaard gebruiken
+ ANSI accentueringsprogramma
+ Copyright toevoegen en scheidingstekenkleur in Radical thema bijwerken
+ Scheidingstekenkleur in de overbelichte thema's bijwerken
+ Kleur van scheidingsteken en pictogramrand voor Ayu, Nord en Vim Dark thema's verbeteren
+ scheidingstekenkleur minder opdringerig maken
+ Kate-schema naar themaconverter door Juraj Oravec importeren
+ prominentere sectie over licenties, koppel onze kopie MIT.txt
+ eerste sjabloon voor base16 generator, https://github.com/chriskempson/base16
+ juiste licentie-informatie toevoegen aan alle thema's
+ Kleurenthema Radical toevoegen
+ Nord kleurenthema toevoegen
+ showcase van thema's verbeterd om meer stijlen te tonen
+ gruvbox light en dark thema's zijn toegevoegd, met MIT licentie
+ ayu kleurenthema toevoegen (met light, dark en mirage varianten)
+ POSIX alternatief voor eenvoudige variabele toekenning
+ hulpmiddelen om een grafiek te genereren uit een syntaxisbestand
+ auto-conversie van unset QRgb == 0 kleur naar "black" in plaats van "transparant"
+ Debian changelog en controle voorbeeldbestanden toevoegen
+ het kleurenthema 'Dracula' toevoegen

### Beveiligingsinformatie

De vrijgegeven code is ondertekend met GPG met de volgende sleutel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Vingerafdruk van primaire sleutel: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
