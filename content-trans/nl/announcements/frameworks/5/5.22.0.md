---
aliases:
- ../../kde-frameworks-5.22.0
date: 2016-05-15
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Attica

- Controleer op de juiste manier of een URL een lokaal bestand is

### Baloo

- Reparaties bij compileren van Windows

### Breeze pictogrammen

- Veel nieuwe pictogrammen voor acties en toepassingen
- Aangeboden extensies specificeren volgens wijziging in kiconthemes

### Extra CMake-modules

- Toepassing inAndroid: projecten ondersteunen zonder dingen in share of lib/qml (bug 362578)
- Schakelt KDE_INSTALL_USE_QT_SYS_PATHS in indien CMAKE_INSTALL_PREFIX een Qt5 prefix is
- ecm_qt_declare_logging_category: foutbericht verbeteren bij gebruik zonder invoegen

### Frameworkintegratie

- Plug-in platformtheme verwijderen omdat het in plasma-integration is

### KCoreAddons

- Een manier bieden om het gebruik van inotify in KDirWatch uit te schakelen
- KAboutData::applicationData() repareren om te initiëren uit huidige Q*Application metagegevens
- Helder maken dat KRandom niet wordt aanbevolen voor cryptografische doelen

### KDBusAddons

- KDBusService: wijzig '-' in '_' in objectpaden

### KDeclarative

- Niet crashen als we geen openGL context hebben

### Ondersteuning van KDELibs 4

- Een terugval MAXPATHLEN bieden indien niet gedefinieerd
- KDateTime::isValid() repareren voor ClockTime waarden (bug 336738)

### KDocTools

- Entities van toepassingen toegevoegd

### KFileMetaData

- Branch 'externalextractors' samenvoegen
- Externe plug-ins en testen gerepareerd
- Ondersteuning voor externe writer-plug-ins toegevoegd
- Ondersteuning voor plug-in voor writer toegevoegd
- Ondersteuning voor plug-in voor externe extractor toegevoegd

### KHTML

- toString implementeren voor Uint8ArrayConstructor en vrienden
- Verschillende aan Coverity gerelateerde reparaties ingevoegd
- QCache::insert op de juiste manier gebruiken
- Enkele geheugenlekken gerepareerd
- Controle op juistheid van ontleden van CSS weblettertype, vermijden van potentieel geheugenlek
- dom: prioriteiten van tags toevoegen voor 'commentaar' tag

### KI18n

- libgettext: potentieel gebruik-na-vrijgave met niet-g++ compilers

### KIconThemes

- Toepasselijke container gebruiken voor intern pointer-array
- Gelegenheid toevoegen om onnodige toegangen tot de schijf te verminderen, introduceert KDE-Extensions
- Op toegang tot de schijf besparen

### KIO

- kurlnavigatortoolbutton.cpp - buttonWidth in paintEvent() gebruiken
- Nieuw bestandsmenu: filter duplicaten uit (bijv. tussen .qrc en systeembestanden) (bug 355390)
- Foutmelding repareren bij opstarten van de cookies-KCM
- kmailservice5 verwijderen, het kan op dit punt alleen schade toebrengen (bug 354151)
- KFileItem::refresh() voor symbolische koppelingen repareren. De verkeerde grootte, bestandstype en rechten werden ingesteld
- Regressie in KFileItem repareren: refresh() zou het bestandstype verliezen, zodat een map een bestand werd (bug 353195)
- Tekst op QCheckbox widget zetten in plaats van een separaat label te gebruiken (bug 245580)
- Schakel geen acl widget voor rechten in als we geen eigenaar zijn van het bestand (bug 245580)
- Een dubbele slash in KUriFilter resultaten wanneer een naamfilter is ingesteld repareren
- KUrlRequester: signaal textEdited toevoegen (doorgestuurd vanuit QLineEdit)

### KItemModels

- Sjabloon syntaxis gerepareerd voor generatie van testcase
- Linken met Qt 5.4 repareren (fout geplaatste #endif)

### KParts

- De layout van de BrowserOpenOrSaveQuestion dialoog repareren

### KPeople

- Een controle op het geldig zijn van PersonData toevoegen

### KRunner

- metainfo.yaml repareren: KRunner is geen hulpmiddel voor porten en niet verouderd

### KService

- Te strikte maximum lengte van tekenreeks in KSycoca database verwijderen

### KTextEditor

- Juiste syntaxis van teken gebruiken, '"' in plaats van '"'
- doxygen.xml: standaard stijl dsAnnotation ook gebruiken voor "Custom Tags" (minder hard gecodeerde kleuren)
- Optie toevoegen om de telling van woorden te tonen
- Kleurcontrast verbeteren van voorgrond van accentuering voor zoeken &amp; vervangen
- Crash repareren bij sluiten van Kate via dbus terwijl de afdrukdialoog open is (bug #356813) (bug 356813)
- Cursor::isValid(): opmerking over isValidTextPosition() toevoegen
- API {Cursor, Range}::{toString, static fromString} toevoegen

### KUnitConversion

- Informeer de client als we de conversiesnelheid niet weten
- Valuta voor ILS (Israeli New Shekel) toevoegen (bug 336016)

### KWallet Framework

- sessie herstellen voor kwalletd5 uitschakelen

### KWidgetsAddons

- KNewPasswordWidget: hint voor grootte op spacer verwijderen, die leidde tot altijd enige lege ruimte in de layout
- KNewPasswordWidget: QPalette repareren wanneer widget is uitgeschakeld

### KWindowSystem

- Generatie van pad naar xcb plug-in repareren

### Plasma Framework

- [QuickTheme] eigenschappen repareren
- highlight/highlightedText uit juiste kleurgroep
- ConfigModel: probeer geen leeg bronpad uit pakket op te lossen
- [calendar] alleen de markeringen van gebeurtenissen op het dagenraster tonen, niet op die van maand of jaar
- declarativeimports/core/windowthumbnail.h - -Wreorder waarschuwing repareren
- pictogramthema juist herladen
- De themanaam altijd naar plasmarc wegschrijven, ook als het standaard thema is gekozen
- [calendar] een markering aan dagen toevoegen die een gebeurtenis bevatten
- Positieve, neutrale en negatieve tekstkleurren toevoegen
- ScrollArea: waarschuwing repareren wanneer contentItem niet Flickable is
- Een eigenschap en methode voor uitlijnen van het menu tegen een hoek van zijn visuele ouder toevoegen
- Instelling van minimum breedte op Menu toestaan
- Volgorde in opgeslagen lijst met items behouden
- API uitbreiden om (her)positionering van menu-items bij een procedurele invoeging toestaan
- bind highlightedText kleur in Plasma::Theme
- Instelling verwijderen van geassocieerde toepassingen/url's voor Plasma::Applets repareren
- Symbolen met privé classe DataEngineManager niet blootstellen
- een element "event" in de agenda-svg toevoegen
- SortFilterModel: filter ongeldig maken bij wijzigen van filter-callback

### Sonnet

- parsetrigrams hulpmiddel voor cross compilatie installeren
- hunspell: een persoonlijk woordenboek laden/opslaan
- hunspell 1.4 ondersteunen
- configwidget: een melding maken van gewijzigde configuratie wanneer genegeerde woorden worden bijgewerkt
- settings: de configuratie niet onmiddellijk opslaan wanneer de lijst met te negeren dingen wordt bijgewerkt
- configwidget: opslaan repareren wanneer te negeren woorden worden bijgewerkt
- Probleem met mislukken van opslaan van te negeren woord repareren (bug 355973)

U kunt discussiëren en ideeën delen over deze uitgave in de section voor commentaar van <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>het artikel in the dot</a>.
