---
aliases:
- ../../kde-frameworks-5.31.0
date: 2017-02-11
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Algemeen

Veel modules hebben nu bindingen met python.

### Attica

- ondersteuning toegevoegd voor display_name in categorieën

### Breeze pictogrammen

- teveel wijzigingen in pictogrammen om ze hier te tonen

### Extra CMake-modules

- Inschakelen van -Wsuggest-override Voor g++ &gt;= 5.0.0
- Geef -fno-operator-names door wanneer ondersteund
- ecm_add_app_icon : SVG bestanden stil negeren i dien niet ondersteund
- Bindings: veel reparaties en verbeteringen

### Frameworkintegratie

- Enige van de KNSCore vragen met meldingen ondersteunen

### KArchive

- KCompressionDevice (zoeken) repareren om te werken met Qt &gt;= 5.7

### KAuth

- Het meeste van de voorbeelden bijwerken, verouderden laten vallen

### KConfig

- Koppelingen onder Windows repareren: maak geen koppeling van kentrymaptest naar KConfigCore

### KConfigWidgets

- Niets doen in ShowMenubarActionFilter::updateAction als er geen menubalken zijn

### KCoreAddons

- Bug 363427 repareren - onveilige tekens onjuist verwerken als onderdeel van een URL (bug 363427)
- kformat: maak het mogelijk om relatieve datums juist te vertalen (bug 335106)
- KAboutData: documenteren dat e-mailadres met bug ook een URL kan zijn

### KDeclarative

- [IconDialog] juiste pictogramgroep instellen
- [QuickViewSharedEngine] setSize gebruiken in plaats van setWidth/setHeight

### Ondersteuning van KDELibs 4

- KDE4Defaults.cmake uit kdelibs synchroniseren
- Controle op HAVE_TRUNC cmake repareren

### KEmoticons

- KEmoticons: DBus gebruiken om actieve processen te melden over wijzigingen gemaakt in de KCM
- KEmoticons: belangrijke verbeteringen in prestatie

### KIconThemes

- KIconEngine: pictogram centreren in gevraagde rect

### KIO

- KUrlRequester::setMimeTypeFilters toevoegen
- Ontleden van lijst met mappen op een specifieke ftp-server (bug 375610)
- groep/eigenaar behouden bij kopiëren van bestand (bug 103331)
- KRun: maak runUrl() verouderd in het voordeel van runUrl() met RunFlags
- kssl: ga na dat map voor gebruikerscertificaat is aangemaakt vóór gebruik (bug 342958)

### KItemViews

- Filter naar proxy onmiddellijk toepassen

### KNewStuff

- Het mogelijk maken om hulpbronnen aan te passen, meestal voor systeem brede instellingen
- Niet mislukken bij verplaatsen naar de tijdelijke map bij installeren
- Maak de beveiligingsklasse verouderd
- Niet blokkeren bij uitvoeren van de opdracht in post-install (bug 375287)
- [KNS] Neem het type distributie in acht
- Niet vragen of we het bestand in /tmp krijgen

### KNotification

- Opnieuw toevoegen van logmeldingen aan bestanden (bug 363138)
- Niet blijvende meldingen als vergankelijk markeren
- "Standaard acties" ondersteunen

### KPackage-framework

- Genereer geen appdata als het gemarkeerd is als NoDisplay
- Lijst repareren wanneer het gevraagde pad absoluut is
- behandeling van archieven met een map erin repareren (bug 374782)

### KTextEditor

- minimap rendering repareren voor HiDPI omgevingen

### KWidgetsAddons

- Methoden toevoegen om de actie van het onthullen van een wachtwoord te verbergen
- KToolTipWidget: geen eigenaar worden van de inhoud van een widget
- KToolTipWidget: onmiddellijk verbergen als de inhoud wordt vernietigd
- Focus overnemen repareren in KCollapsibleGroupBox
- Waarschuwing repareren bij weggooien van een KPixmapSequenceWidget
- Installeer ook CamelCase voorwaartse headers voor klassen uit multi-klasse headers
- KFontRequester: Zoek de dichts bijzijnde overeenkomst voor een ontbrekend lettertype (bug 286260)

### KWindowSystem

- Tab gewijzigd door Shift toestaan (bug 368581)

### KXMLGUI

- Bugrapporteerder: een URL toestaan (niet alleen een e-mailadres) voor eigen rapportage
- Lege sneltoetsen overslaan bij controle op meerduidigheid

### Plasma Framework

- [Container-interface] values() niet nodig omdat contains() toetsen opzoekt
- Dialoog: als focus wijzigt verbergen naar ConfigView met hideOnWindowDeactivate
- [PlasmaComponentenmenu] eigenschap maximumWidth toevoegen
- Ontbrekend pictogram bij verbonden naar openvpn via bluetooth netwerk (bug 366165)
- Ingeschakeld ListItem bij er boven zweven tonen zeker maken
- alle hoogten in de agendakop gelijk maken (bug 375318)
- kleurstyling repareren in netwerk pictogram van plasma (bug 373172)
- wrapMode naar Text.WrapAnywhere instellen (bug 375141)
- kalarm-pictogram bijwerken (bug 362631)
- status uit applets juist doorgeven naar container (bug 372062)
- KPlugin gebruiken om Calendar-plug-ins te laden
- de kleurvoor accentuering voor geselecteerde tekst gebruiken (bug 374140)
- [Pictogram-item] grootte afronden waarin we een pixmap willen laden
- eigenschap van portrait is niet relevant wanneer er geen is tekst is (bug 374815)
- De eigenschappen van renderType repareren voor verschillende componenten

### Solid

- Om bug in ophalen van DBus eigenschap heenwerken (bug 345871)
- Geen wachtwoordzin behandelen als fout Solid::UserCanceled

### Sonnet

- Grieks trigram gegevensbestand toegevoegd
- Segmentatiefout in generatie van trigrams repareren en toon constante MAXGRAMS in de header
- Zoek naar libhunspell.so zonder versie, zou meer toekomstbestendig

### Accentuering van syntaxis

- C++ accentuering: bijwerken tot Qt 5.8

### Beveiligingsinformatie

De vrijgegeven code is ondertekend met GPG met de volgende sleutel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Vingerafdruk van primaire sleutel: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB

U kunt discussiëren en ideeën delen over deze uitgave in de section voor commentaar van <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>het artikel in the dot</a>.
