---
aliases:
- ../../kde-frameworks-5.6.0
date: '2015-01-08'
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
Twee nieuwe frameworks in deze versie: KPackage en NetworkManagerQt

### KActivities

- Introducing the ActivityInfo QML object
- Linking now supports special values alongside activity ids. Supported values:
  - :global - links to all activities
  - :current - links to the current activity

### KCoreAddons

- KDirWatch can now watch /dev/shm as well (bug 314982)
- KDELibs4Migration now always returns an absolute path

### KCrash

- Fix build on FreeBSD

### Kdelibs4Support

- Add Lithuania to the list of Euro countries

### KdeSU

- Fix build on OSX and on FreeBSD

### KHtml

- 6 bug fixes forward-ported from kdelibs4.

### KIO

- Add support for .hidden files, for users to hide some files from views. (feature 246260)
- New method KRun::runService, like KRun::run but returns the PID of the newly started process.
- kioexec: fixed many porting bugs, to make it work again
- KRun::run: fixed porting bug, to make it work again in the case where KProcess is used directly
- KRun: make klauncher runtime dependency optional
- Fix compilation on MSVC
- Performance: decrease memory consumption of UDSEntry
- Actions in popupmenus: in case of multiple mimetypes selected, services that support all mimetypes
  are now added to the menu.
- New job: KIO::DropJob <em>KIO::drop(QDropEvent</em> ev, QUrl destUrl). Replaces KonqOperations::doDrop.
- Restart directory watcher after a failed delete operation
- Fix false warning about X-KDE-Protocols unused in desktop files without that field.
- Merge various settings modules (KCMs) related to kio, into the kio framework.
- When copying/moving out the trash, make the files writable.
- KIO::file_move now does the chmod on the destination file before emitting result.

### KNotifications

- Remove NotifyBySound. NotifyByAudio implements the "Sound" notification already
- Fix crash accessing dangling pointer in NotifyByPopup

### KRunner

- Do not detect anything with a '.' as a NetworkLocation (porting bug, bug 340140).
  One can also uses a decimal point in a calculator.

### KService

- Fix build on MSVC.

### KTextEditor

- Fix build on MSVC.
- vimode bugfixes
- add syntax file for Oracle PL/SQL
- ppd highlighting: better support for multiline values

### KWidgetsAddons

- Add runtime style element extension convenience functions for widgets: KStyleExtensions

### KWindowSystem

- Add OnScreenDisplay window type
- Fix build on FreeBSD

### Plasma-framework

- Let month menu navigate in current year (bug 342327)
- Expose new OnScreenDisplay window type in Dialog
- Migrate Plasma::Package to KPackage
- Fix labels not picking up changes to font settings at runtime
- Fix text not properly updating its color when switching themes (especially dark&lt;-->light)
- Fix placeholder text in textfields being too strong when using a light theme
- Set visibility on mainItem to match Dialog
- Load IconItem immediately upon componentComplete()
- Use the same text colour for comboboxes as buttons
- Performance improvements (less config-file reparsing, use shared config...)
- roundToIconSize(0) now returns 0
- Give undo notifications a title

### Solid

- Enable fstab and upower backends on FreeBSD
- Power: Add aboutToSuspend signal

### Buildsystem changes

- ECM's KDEInstallDirs now supports KDE*INSTALL*_ variables, which should
  be used in preference to the CMAKE*INSTALL*_ variables or the older
  KDELibs4-compatible variables. The older forms of the variables are still
  supported (and kept in sync) unless KDE_INSTALL_DIRS_NO_DEPRECATED
  or KDE_INSTALL_DIRS_NO_CMAKE_VARIABLES are set to TRUE. See the
  documentation for more details.
- Add COMPATIBILITY argument to ecm_setup_version().
  Lots of libraries will want to use SameMajorVersion to make sure
  searching for version 1 of a library doesn't give you version 2, for
  example.
- Fix ECMQueryQmake when Qt5Core is missing.

### Additional buildsystem changes in Extra-Cmake-Modules 1.6.1

- Fix building projects that use both GNUInstallDirs and KDEInstallDirs in
  different subdirectories by not unsetting cache variables in KDEInstallDirs.
- Fix KDE_INSTALL_TARGETS_DEFAULT_ARGS value on OSX.

### Frameworkintegration

- Fix handling of palette change events (bug 336813)

You can discuss and share ideas on this release in the comments section of <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>the dot article</a>.
