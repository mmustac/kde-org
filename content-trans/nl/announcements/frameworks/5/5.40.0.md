---
aliases:
- ../../kde-frameworks-5.40.0
date: 2017-11-11
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- DjVu-bestanden als documenten beschouwen (bug 369195)
- Spelling repareren zodat WPS Office presentaties juist herkend worden

### Breeze pictogrammen

- map-stapel toevoegen voor het pictogram van de werkbalk voor stapels in Dolphin

### KArchive

- Potentieel geheugenlek repareren. Logica repareren

### KCMUtils

- geen marges voor qml-modulen vanaf de kant van qwidget
- Variabelen initialiseren (gevonden door coverity)

### KConfigWidgets

- Pictogram van KStandardAction::MoveToTrash repareren

### KCoreAddons

- URL detectie met dubbele url's zoals "http://www.foo.bar&lt;http://foo.bar/&gt;" repareren
- https voor KDE url's gebruiken

### Ondersteuning van KDELibs 4

- volledige vervanging van document voor disableSessionManagement()
- Zorg dat kssl compileert tegen OpenSSL 1.1.0 (bug 370223)

### KFileMetaData

- Displaynaam van Generator-eigenschap repareren

### KGlobalAccel

- KGlobalAccel: ondersteuning van numpad toetsen (opnieuw) repareren

### KInit

- Installatie van start_kdeinit wanneer DESTDIR en libcap samen gebruikt worden corrigeren

### KIO

- Tonen van remote:/ in de qfiledialog gerepareerd
- Ondersteuning voor categorieën op KfilesPlacesView implementeren
- HTTP: fouttekenreeks voor het geval van de 207 Multi-Status repareren
- KNewFileMenu: dode code opschonen, ontdekt door Coverity
- IKWS: mogelijke oneindige lus, ontdekt door Coverity
- KIO::PreviewJob::defaultPlugins() functie

### Kirigami

- syntaxis werkend op oudere Qt 5.7 (bug 385785)
- de overlaysheet anders stapelen (bug 386470)
- De delegatie geaccentueerde eigenschap ook tonen wanneer er geen focus is
- hints voor grootte van het scheidingsteken met voorkeur
- gebruik van Settings.isMobile corrigeren
- Sta toepassingen toe om een beetje convergent te zijn op een bureaublad-y systeem
- Verzeker dat de inhoud van het SwipeListItem niet overlapt met het handvat (bug 385974)
- Scrollview van Overlaysheet is altijd interactief
- Categorieën in galerij-bureaubladbestand toevoegen (bug 385430)
- Het kirigami.pri bestand bijwerken
- de niet geïnstalleerde plug-in gebruiken om de testen te doen
- Markeer Kirigami.Label als verouderd
- Gebruik van galerijvoorbeeld van labels overzetten om consistent te zijn met QQC2
- Gebruik van Kirigami.Controls van Kirigami.Label overzetten
- het scrollgebied interactief maken bij aanraakgebeurtenissen
- De git-oproep van find_package verplaatsen naar waar het wordt gebruikt
- standaard is transparante lijstweergave-items

### KNewStuff

- PreferCache verwijderen uit netwerkverzoeken
- Gedeelde aanwijzers naar privé gegevens niet loskoppelen bij instellen voorbeeldweergave
- KMoreTools: desktop-bestanden bijwerken en repareren (bug 369646)

### KNotification

- Controle op SNI-hosts verwijderen bij kiezen verouderde modus gebruiken (bug 385867)
- Alleen controleren op verouderde systeemvakpictogrammen als we er een gaan maken (bug 385371)

### KPackage-framework

- de niet geïnstalleerde servicebestanden gebruiken

### KService

- Waarden initialiseren
- Een aanwijzer initialiseren

### KTextEditor

- API dox: verkeerde namen van methoden en argumenten repareren, ontbrekende \\since toevoegen
- (Bepaalde) crashes vermijden bij uitvoeren van QML-scripts (bug 385413)
- Een QML-crash getriggerd door C-stijl inspringscripts vermijden
- Grootte van markering achteraan verhogen
- enige indenteringen uit indentering op willekeurige tekens repareren
- Waarschuwingen over veroudering repareren

### KTextWidgets

- Waarde initialiseren

### KWayland

- [client] de controles op platformName die "wayland" is laten vervallen
- Verbinding met wl_display_flush niet dupliceren
- Wayland buitenlandsprotocol

### KWidgetsAddons

- inconsistentie in focus widget van createKMessageBox repareren
- meer compacte wachtwoorddialoog (bug 381231)
- KPageListView-breedte juist instellen

### KWindowSystem

- KKeyServer: behandeling van Meta+Shift+Print, Alt+Shift+pijltjestoets etc. repareren
- Flatpak platform ondersteunen
- Eigen platformdetectie-API van KWindowSystem gebruiken in plaats van gedupliceerde code

### KXMLGUI

- https voor KDE url's gebruiken

### NetworkManagerQt

- 8021xSetting: overeenkomst in achtervoegsel bij domeinnaam is gedefinieerd in NM 1.2.0 en nieuwer
- Overeenkomst in achtervoegsel bij domeinnaam ondersteunen in Security8021xSetting

### Plasma Framework

- teken met de hand de cirkelboog
- [PlasmaComponentenmenu] ungrabMouseHack toevoegen
- [FrameSvg] Optimaliseer updateSizes
- Positioneer geen dialoog als het type OSD is

### QQC2StyleBridge

- Compilatie als een statische plug-in verbeteren
- maak het keuzerondje een keuzerondje
- qstyle gebruiken om de draaischijf te tekenen
- een ColumnLayout voor menu's gebruiken
- Dialoog repareren
- ongeldige groepseigenschap verwijderen
- Formattering van het md-bestand repareren zodat het overeenkomt met de andere modulen
- gedrag van keuzelijst dichter bij qqc1
- workaround voor QQuickWidgets

### Sonnet

- Methode assignByDictionnary toevoegen
- Geef een signaal als we in staat zijn het woordenboek toe te kennen

### Accentuering van syntaxis

- Makefile: regexpr overeenkomst in "CXXFLAGS+" repareren

### ThreadWeaver

- Opschonen van CMake: geen hard gecodeerd -std=c++0x

### Beveiligingsinformatie

De vrijgegeven code is ondertekend met GPG met de volgende sleutel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Vingerafdruk van primaire sleutel: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB

U kunt discussiëren en ideeën delen over deze uitgave in de section voor commentaar van <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>het artikel in the dot</a>.
