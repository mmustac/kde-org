---
aliases:
- ../../kde-frameworks-5.32.0
date: 2017-03-11
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Geneste tags implementeren

### Breeze pictogrammen

- Pictogrammen voor Plasma Vault toegevoegd
- Pictogrammen voor versleutelde en ontsleutelde mappen
- Pictogram voor torrents van 22px toegevoegd
- Nm-tray pictogrammen toevoegen (bug 374672)
- kleurbeheer: ongedefinieerde koppelingen verwijderd (bug 374843)
- system-run is nu een actie tot &lt;= 32px en 48px een app-pictogram (bug 375970)

### Extra CMake-modules

- inotify detecteren
- Draai terug "Automatisch klassen markeren met pure virtuele functies als /Abstract/."

### KActivitiesStats

- Vooruit plannen toestaan en de volgorde voor een item nog niet in de lijst instellen

### KArchive

- Potentieel geheugenlek naar verwezen door 'limitedDev' repareren

### KCMUtils

- Potentiële crash in QML KCM's wanneer toepassingspallet wijzigt gerepareerd

### KConfig

- KConfig: stop het exporteren en installeren van KConfigBackend

### KConfigWidgets

- KColorScheme: is standaard het toepassingsschema indien ingesteld door KColorSchemeManager (bug 373764)
- KConfigDialogManager: wijziging van signaal van metaObject of speciale eigenschap krijgen
- Herstel foutencontrole van KCModule::setAuthAction

### KCoreAddons

- (6) uitsluiten van herkenning van emoticons
- KDirWatch: geheugenlekken bij vernietiging repareren

### Ondersteuning van KDELibs 4

- Bug in kfiledialog.cpp repareren die crashen veroorzaakt wanneer native widgets worden gebruikt

### KDocTools

- meinproc5: maak koppeling naar de bestanden, niet naar de bibliotheek (bug 377406)
- De KF5::XsltKde statische bibliotheek verwijderen
- Een juist gedeelde bibliotheek voor KDocTools exporteren
- Overgezet naar gecategoriseerde logging en schoon invoegen
- Functie toevoegen om een enkel bestand uit te pakken
- Laat het bouwen eerder afbreken als xmllint niet beschikbaar is (bug 376246)

### KFileMetaData

- Nieuwe onderhouder voor kfilemetadata
- [ExtractorCollection] overerven van MIME-type gebruiken om plug-ins terug te laten keren
- een nieuwe eigenschap DiscNumber voor geluidsbestanden uit albums met meerdere discs toevoegen

### KIO

- Cookies KCM: knop "verwijderen" uitschakelen wanneer er geen huidig item is
- kio_help: de nieuwe gedeelte bibliotheek gebruiken geëxporteerd door KDocTools
- kpac: URL's opschonen alvorens ze door te geven aan FindProxyForURL (beveiligingsreparatie)
- Remote ioslave uit plasma-workspace importeren
- kio_trash: hernoemen van topniveau bestanden en mappen
- PreviewJob: maximum grootte voor locale bestanden standaard verwijderen
- DropJob: sta toe om toepassingsacties aan een open menu toe te voegen
- ThumbCreator: keur DrawFrame af, zoals besproken in https://git.reviewboard.kde.org/r/129921/

### KNotification

- Ondersteuning voor flatpak portals toevoegen
- desktopfilename verzenden als onderdeel van notifyByPopup aanwijzingen
- [KStatusNotifierItem] herstel geminimaliseerd venster als normaal

### KPackage-framework

- Ondersteuning voltooien voor openen van gecomprimeerde pakketten

### KTextEditor

- Type bestand herinneren ingesteld door gebruiker over sessies
- Type bestand resetten bij openen van url
- getter voor configuratiewaarde van aantal woorden toegevoegd
- Consistente conversie van/naar cursor naar/van coördinaten
- Type bestand bijwerken alleen bij opslaan als pad wijzigt
- Ondersteuning voor EditorConfig configuratiebestanden (voor details: http://editorconfig.org/)
- FindEditorConfig aan ktexteditor toevoegen
- Reparatie: emmetToggleComment actie werkt niet (bug 375159)
- Hoofdlettergebruiksstijl in zin gebruiken met labelteksten van te bewerken velden
- Draait de betekenis om van van :split, :vsplit om acties in vi en Kate overeen te laten komen
- C++11 log2() gebruiken in plaats van log() / log(2)
- KateSaveConfigTab: zet spacer achter laatste groep op tabblad Geavanceerd, niet erin
- KateSaveConfigTab: verkeerde marge rond inhoud van tabblad Geavanceerd verwijderen
- Randen configuratie subpagina: zichtbaarheid van schuifbalk in keuzelijst op verkeerde plaats repareren

### KWidgetsAddons

- KToolTipWidget: tekstballon verbergen in enterEvent als hideDelay nul is
- KEditListWidget die focus verliest bij klik mat knoppen repareren
- Decompositie van Hangul uitdrukkingen in Hangul Jamo toevoegen
- KMessageWidget: gedrag bij overlappende aanroepen van animatedShow/animatedHide repareren

### KXMLGUI

- KConfig-toetsen niet gebruiken met backslashes

### NetworkManagerQt

- Introspecties end gegenereerde bestanden synchroniseren met NM 1.6.0
- Manager: tweemaal uitzenden van deviceAdded wanneer NM opnieuw start repareren

### Plasma Framework

- standaard hints instellen wanneer repr Layout.* niet exporteert (bug 377153)
- mogelijkheid om expanded=false in te stellen voor een container
- [Menu] beschikbare correctie voor ruimte verbeteren voor openRelative
- logica van setImagePath verplaatsen in updateFrameData() (bug 376754)
- IconItem: eigenschap roundToIconSize toevoegen
- [SliderStyle] sta leveren van een element "hint-handle-size" toe
- Maak alle verbindingen naar actie in QMenuItem::setAction
- [ConfigView] beperkingen in KIOSK Control Module honoreren
- Uitschakelen van de animatie met draaier repareren wanneer de bezetindicator niet doorzichtig is
- [FrameSvgItemMargins] niet bijwerken bij repaintNeeded
- Applet-pictogrammen voor de Plasma-kluis
- AppearAnimation en DisappearAnimation migreren naar Animators
- Onderrand naar bovenrand van visualParent uitlijnen in het geval van TopPosedLeftAlignedPopup
- [ConfigModel] dataChanged uitzenden wanneer een ConfigCategory wijzigt
- [ScrollViewStyle] eigenschap frameVisible evalueren
- [Button Styles] Layout.fillHeight gebruiken in plaats van parent.height in een Layout (bug 375911)
- [ContainmentInterface] containment contextmenu in lijn brengen aan panel

### Prison

- min qt-version repareren

### Solid

- Floppy disks verschijnen nu als "Floppy Disk" in plaats van "0 B verwijderbaar medium"

### Accentuering van syntaxis

- Meer trefwoorden toevoegen. Spellingcontrole uitschakelen voor trefwoorden
- Meer trefwoorden toevoegen
- Bestandsextensie *.RHTML toevoegen aan accentuering in Ruby on Rails (bug 375266)
- SCSS en CSS syntaxis accentuering bijwerken (bug 376005)
- minder accentuering: commentaar op een enkele regel die nieuwe regio's beginnen repareren
- Accentueren van LaTeX: omgeving alignat repareren (bug 373286)

### Beveiligingsinformatie

De vrijgegeven code is ondertekend met GPG met de volgende sleutel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Vingerafdruk van primaire sleutel: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB

U kunt discussiëren en ideeën delen over deze uitgave in de section voor commentaar van <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>het artikel in the dot</a>.
