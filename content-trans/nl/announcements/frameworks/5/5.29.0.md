---
aliases:
- ../../kde-frameworks-5.29.0
date: 2016-12-12
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Nieuw framework

Deze uitgave bevat Prison, een nieuw framework voor barcode generatie (inclusief QR-codes).

### Algemeen

FreeBSD is toegevoegd aan metainfo.yaml in alle frameworks getest om te werken op FreeBSD.

### Baloo

- Prestatieverbetering bij schrijven (4 * versnelling voor wegschrijven van gegevens)

### Breeze pictogrammen

- BINARY_ICONS_RESOURCE ON standaard maken
- vnd.rar MIME voor shared-mime-info 1.7 toevoegen (bug 372461)
- claws-pictogram toevoegen (bug 371914)
- gdrive pictogram in plaats van een algemeen cloud-pictogram (bug 372111)
- bug "list-remove-symcolic use wrong image" repareren (bug 372119)
- overige toevoegingen en verbeteringen

### Extra CMake-modules

- Python bindings test overslaan als PyQt niet is geïnstalleerd
- De test alleen toevoegen als python is gevonden
- Het vereiste minimum van CMake verminderen
- Module ecm_win_resolve_symlinks toevoegen

### Frameworkintegratie

- zoek QDBus, nodig voor appstream kpackage handler
- Laat KPackage afhankelijk zijn van packagekit &amp; appstream

### KActivitiesStats

- Op de juiste manier verzenden van het aan hulpbron gekoppelde evenement

### KDE Doxygen hulpmiddelen

- Aanpassing aan wijziging quickgit -&gt; cgit
- Bug repareren als groepsnaam niet is gedefineerd. Kan nog steeds breken onder slechte omstandigheden

### KArchive

- errorString() methode toevoegen om foutinformatie te leveren

### KAuth

- eigenschap van timeout toevoegen (bug 363200)

### KConfig

- kconfig_compiler - genereer code met overschrijven
- Op de juiste manier functiesleutelwoorden ontleden (bug 371562)

### KConfigWidgets

- Ga na dat menu-acties de bedoelde MenuRole krijgen

### KCoreAddons

- KTextToHtml: bug "[1] added at the end of a hyperlink" repareren (bug 343275)
- KUser: alleen naar een avatar zoeken als loginName niet leeg is

### KCrash

- Met KInit overeen laten komen en DISPLAY niet gebruiken op Mac
- Niet alle file descriptors sluiten op OS X

### KDesignerPlugin

- src/kgendesignerplugin.cpp - overschrijven toevoegen om code te genereren

### KDESU

- Haalt instelling van XDG_RUNTIME_DIR weg in processen uitgevoerd met kdesu

### KFileMetaData

- Zoek naar actuele FFMpeg's libpostproc

### KHTML

- java: pas de namen toe op de juiste knoppen
- java: stel namen in in rechtendialoog

### KI18n

- Controleer juiste ongelijkheid van pointer uit dngettext (bug 372681)

### KIconThemes

- Sta het tonen van pictogrammen uit alle categorieën toe (bug 216653)

### KInit

- Omgevingsvariabelen instellen uit KLaunchRequest bij starten van nieuw proces

### KIO

- Overgezet naar gecategoriseerde logging
- Compilatie repareren tegen WinXP SDK
- Sta controlesom overeenkomst met hoofdletters toe in tabblad Controlesommen (bug 372518)
- Maak de laatste (=datum) kolom in de bestandsdialoog niet breder (bug 312747)
- Importeer en werk bij de kcontrol-docbooks voor code in kio uit kde-runtime master
- [OS X] laat de prullenbak van KDE die van OS X gebruiken
- SlaveBase: documentatie over gebeurtenislussen en meldingen en kded modules toevoegen

### KNewStuff

- Nieuwe optie archiefbeheer (subdir) aan knsrc toevoegen
- Accepteer de nieuwe foutsignalen (stel taakfouten in)
- Behandel vreemde zaken met bestanden die verdwijnen bij zojuist aanmaken
- Installeer de kernheaders echt, met CamelCases

### KNotification

- [KStatusNotifierItem] Sla op / herstel positie van widget bij verberg / herstel dit venster (bug 356523)
- [KNotification] Sta toe melding aan te vullen met URL's

### KPackage-framework

- blijf metadata.desktop installeren (bug 372594)
- laad metagegevens handmatig als absoluut pad wordt doorgegeven
- Potentiële mislukking repareren als pakket niet compatibel is met appstream
- Geef informatie over X-Plasma-RootPath door aan KPackage
- Genereren van het bestand metadata.json repareren

### KPty

- Meer zoeken naar pad van utempter (neem /usr/lib/utempter/ mee)
- Bibliotheekpad toevoegen zodat binair programma van utempter gevonden wordt in Ubuntu 16.10

### KTextEditor

- Voorkom waarschuwingen van Qt over een niet ondersteunde modus van klembord op Mac
- Syntaxis definities uit KF5::SyntaxHighlighting gebruiken

### KTextWidgets

- Vervang vensterpictogrammen niet door het resultaat van een mislukt opzoeken

### KWayland

- [client] nullptr dereference in ConfinedPointer en LockedPointer repareren
- [client] pointerconstraints.h installeren
- [server] Regressie in SeatInterface::end/cancelPointerPinchGesture repareren
- Implementatie van PointerConstraints-protocol
- [server] overhead van pointersForSurface reduceren
- SurfaceInterface::size teruggeven in globale ruimte van compositor
- [tools/generator] enum FooInterfaceVersion genereren aan de kant van de server
- [tools/generator] Prik argumenten van wl_fixed request in wl_fixed_from_double
- [tools/generator] implementatie van verzoeken aan de kant van de client genereren
- [tools/generator] hulpbronfabrieken aan de kant van de client genereren
- [tools/generator] callbacks genereren en luisteraar aan clientkant
- [tools/generator] Geef dir door als q-pointer aan Client::Resource::Private
- [tools/generator] Private::setup(wl_foo *arg) genereren aan clientkant
- Implementatie van PointerGestures-protocol

### KWidgetsAddons

- Crashen voorkomen op Mac
- Vervang pictogrammen niet door het resultaat van een mislukt opzoeken
- KMessageWidget: opmaak repareren wanneer wordWrap is ingeschakeld zonder acties
- KCollapsibleGroupBox: widgets niet verbergen, in plaats daarvan overschrijf focusbeleid

### KWindowSystem

- [KWindowInfo] pid() en desktopFileName() toevoegen

### Oxygen-pictogrammen

- Application-vnd.rar pictogram toevoegen (bug 372461)

### Plasma Framework

- Controleren op geldige metagegevens in settingsFileChanged (bug 372651)
- Indeling van tabbladbalk niet spiegelen indien verticaal
- radialGradient4857 verwijderen (bug 372383)
- [AppletInterface] focus nooit weghalen van fullRepresentation (bug 372476)
- SVG pictogram ID prefix repareren (bug 369622)

### Solid

- winutils_p.h: compatibiliteit met WinXP SDK herstellen

### Sonnet

- Ook zoeken naar hunspell-1.5

### Accentuering van syntaxis

- Attribuutwaarden van XML licentie normaliseren
- Syntaxisdefinities uit ktexteditor synchroniseren
- Regiosamenvoeging invouwen repareren

### Beveiligingsinformatie

De vrijgegeven code is ondertekend met GPG met de volgende sleutel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Vingerafdruk van primaire sleutel: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB

U kunt discussiëren en ideeën delen over deze uitgave in de section voor commentaar van <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>het artikel in the dot</a>.
