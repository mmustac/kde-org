---
aliases:
- ../announce-applications-16.12-beta
date: 2016-11-18
description: KDE Ships Applications 16.12 Beta.
layout: application
release: applications-16.11.80
title: Сообщество KDE выпустило бета-версию KDE Applications 16.12
---
18 ноября 2016 года. Сегодня KDE выпустило бета-версию KDE Applications. Теперь, когда программные зависимости и функциональность «заморожены», команда KDE сконцентрируется на исправлении ошибок и наведении красоты.

Check the <a href='https://community.kde.org/Applications/16.12_Release_Notes'>community release notes</a> for information on new tarballs, tarballs that are now KF5 based and known issues. A more complete announcement will be available for the final release

Выпуски KDE Applications 16.12 требуют тщательного тестирования для обеспечения должного уровня качества и удовлетворения пользователей. Очень важную роль в этом играют те, кто используют нашу систему повседневно, ведь сами разработчики просто не могут протестировать все возможные конфигурации. Мы рассчитываем на вашу помощь в поиске ошибок на этом этапе, чтобы мы могли исправить их до выхода финальной версии. По возможности включайтесь в команду — установите эту бета-версию и <a href='https://bugs.kde.org/'>сообщайте нам о найденных ошибках</a>.
