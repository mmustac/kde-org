---
aliases:
- ../../kde-frameworks-5.48.0
date: 2018-07-14
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
###  Attica

- Port remaining uses of qDebug() to qcDebug(ATTICA)
- Fix checking invalid provider url
- Fix broken url to API specification

### Baloo

- Remove unused entry X-KDE-DBus-ModuleName from the kded plugin metadata
- [tags_kio] The url query should be a key-value pair
- The power state signal should only be emitted when the power state changes
- baloodb: Make changes to cmdline arg description after rename prune -&gt; clean
- Clearly show duplicate filenames in tag folders

### BluezQt

- Update D-Bus xml files to use "Out*" for signal type Qt annotations
- Add signal for devices's address changing

### Breeze Icons

- Use the broom-style icon for edit-clear-all too
- Use a broom-style icon for edit-clear-history
- change 24px view-media-artist icon

### Extra CMake-Module

- Android: Make it possible to override a target's APK directory
- Drop outdated QT_USE_FAST_OPERATOR_PLUS
- Add -Wlogical-op -Wzero-as-null-pointer-constant to KF5 warnings
- [ECMGenerateHeaders] Add option for other header file extension than .h
- Don't include a 64 when building 64bit architectures on flatpak

### KActivitiesStats

- Fix off by one error in Cache::clear (bug 396175)
- Fix ResultModel item moving (bug 396102)

### KCompletion

- Remove unneeded moc include
- Make sure KLineEdit::clearButtonClicked is emitted

### KCoreAddons

- Remove QT definitions duplicated from KDEFrameworkCompilerSettings
- Make sure that it compiles with strict compile flags
- Remove unused key X-KDE-DBus-ModuleName from test servicetype metadata

### KCrash

- Reduce QT_DISABLE_DEPRECATED_BEFORE to minimum dep of Qt
- Remove QT definitions duplicated from KDEFrameworkCompilerSettings
- Make sure to build with strict compile flags

### KDeclarative

- check the node actually has a valid texture (bug 395554)

### KDED

- KDEDModule servicetype definition: remove unused key X-KDE-DBus-ModuleName

### KDELibs 4 Support

- Set QT_USE_FAST_OPERATOR_PLUS ourselves
- Don't export kf5-config to the CMake config file
- Remove unused entry X-KDE-DBus-ModuleName from the kded plugin metadata

### KDE WebKit

- Port KRun::runUrl() &amp; KRun::run() to undeprecated API
- Port KIO::Job::ui() -&gt; KIO::Job::uiDelegate()

### KFileMetaData

- Avoid compiler warnings for taglib headers
- PopplerExtractor: use directly QByteArray() args instead of 0 pointers
- taglibextractor: Restore extracting audio props without tags existing
- OdfExtractor: deal with non-common prefix names
- Don't add -ltag to the public link interface
- implement the lyrics tag for taglibextractor
- automatic tests: do not embed EmbeddedImageData already in the library
- add ability to read embedded cover files
- implement reading of rating tag
- check for needed version of libavcode, libavformat and libavutil

### KGlobalAccel

- Update D-Bus xml file to use "Out*" for signal type Qt annotations

### KHolidays

- holidays/plan2/holiday_jp_* - add missing metainfo
- updated Japanese holidays (in Japanese and English) (bug 365241)
- holidays/plan2/holiday_th_en-gb - updated Thailand (UK English) for 2018 (bug 393770)
- add Venezula holidays (Spanish) (bug 334305)

### KI18n

- In cmake macro file use CMAKE_CURRENT_LIST_DIR consequently instead of mixed use with KF5I18n_DIR
- KF5I18NMacros: Don't install an empty dir when no po files exist

### KIconThemes

- Support choosing .ico files in custom icon file chooser (bug 233201)
- Support Icon Scale from Icon naming specification 0.13 (bug 365941)

### KIO

- Use new method fastInsert everywhere where applicable
- Restore compatibility of UDSEntry::insert, by adding a fastInsert method
- Port KLineEdit::setClearButtonShown -&gt; QLineEdit::setClearButtonEnabled
- Update D-Bus xml file to use "Out*" for signal type Qt annotations
- Remove QT definition duplicated from KDEFrameworkCompilerSettings
- Use a correct emblem icon for readonly files and folders (bug 360980)
- Make it possible to go up to root again, in the file widget
- [Properties dialog] Improve some permissions-related strings (bug 96714)
- [KIO] Add support for XDG_TEMPLATES_DIR in KNewFileMenu (bug 191632)
- update trash docbook to 5.48
- [Properties dialog] Make all field values on general tab selectable (bug 105692)
- Remove unused entry X-KDE-DBus-ModuleName from kded plugins' metadata
- Enable comparing KFileItems by url
- [KFileItem] Check most local URL for whether it's shared
- Fix regression when pasting binary data from clipboard

### Kirigami

- more consistent mouse over color
- don't open submenu for actions without children
- Refactor the Global ToolBar concept (bug 395455)
- Handle enabled property of simple models
- Introduce ActionToolbar
- fix pull to refresh
- Remove doxygen docs of internal class
- Don't link against Qt5::DBus when DISABLE_DBUS is set
- no extra margin for overlaysheets in overlay
- fix the menu for Qt 5.9
- Check the visible property of the action as well
- better look/alignment in compact mode
- don't scan for plugins for each platformTheme creation
- get rid of the "custom" set
- add resetters for all custom colors
- port toolbutton coloring to custom colorSet
- introduce Custom color set
- writable buddyFor to position labels regarding to subitems
- When using a different background color, use highlightedText as text color

### KNewStuff

- [KMoreTools] Enable installing tools via appstream url
- Remove KNS::Engine d-pointer hack

### KService

- Remove unused key X-KDE-DBus-ModuleName from test service metadata

### KTextEditor

- guard updateConfig for disabled status bars
- add context menu to statusbar to toggle show total lines/word count
- Implemented displaying of total lines in kate (bug 387362)
- Make menu-bearing toolbar buttons show their menus with normal click rather than click-and-hold (bug 353747)
- CVE-2018-10361: privilege escalation
- Fix caret width (bug 391518)

### KWayland

- [server] Send frame event instead of flush on relative pointer motion (bug 395815)
- Fix XDGV6 popup test
- Fix stupid copy paste bug in XDGShellV6 Client
- Do not cancel old clipboard selection if it is same as the new one (bug 395366)
- Honor BUILD_TESTING
- Fix some spelling issues suggested by new linter tool
- Add the arclint file in kwayland
- Fixup @since for skip switcher API

### KWidgetsAddons

- [KMessageWidget] Update stylesheet when palette changes
- Update kcharselect-data to Unicode 11.0
- [KCharSelect] Port generate-datafile.py to Python 3
- [KCharSelect] Prepare translations for Unicode 11.0 update

### ModemManagerQt

- Implement support for the Voice and Call interfaces
- Don't set custom domain filter rules

### Oxygen Icons

- Show an icon for hidden files in Dolphin (bug 395963)

### Plasma Framework

- FrameSvg: Update mask frame if image path has been changed
- FrameSvg: Do not wreck shared mask frames
- FrameSvg: Simplify updateSizes
- Icons for Keyboard Indicator T9050
- fix color for media icon
- FrameSvg: Recache maskFrame if enabledBorders has been changed (bug 391659)
- FrameSvg: Draw corners only if both borders in both directions are enabled
- Teach ContainmentInterface::processMimeData how to handle Task Manager drops
- FrameSVG: Delete redundant checks
- FrameSVG: Fix QObject include
- Use QDateTime for interfacing with QML (bug 394423)

### Purpose

- Add Share action to Dolphin context menu
- Properly reset plugins
- Filter out duplicate plugins

### QQC2StyleBridge

- no pixel values in checkindicator
- use RadioIndicator for everybody
- Don't overflow around popups
- on Qt&lt;5.11 the control palette is completely ignored
- anchor button background

### Solid

- Fix device label with unknown size

### Syntax Highlighting

- Fixes for Java comments
- Highlight Gradle files with Groovy syntax too
- CMake: Fix highlighting after strings with a single <code>@</code> symbol
- CoffeeScript: add extension .cson
- Rust: Add keywords &amp; bytes, fix identifiers, and other improvements/fixes
- Awk: fix regex in a function and update syntax for gawk
- Pony: fix single quote escape and a possible infinite loop with #
- Update CMake syntax for the upcoming release 3.12

### Sicherheitsinformation

Der veröffentlichte Quelltext wurde mit GPG mit folgendem Schlüssel signiert: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primärer Fingerprint des Schlüssels: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Sie können im Kommentarabschnitt des <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>Dot-Artikels</a> über diese Veröffentlichung diskutieren und Ideen einbringen.
