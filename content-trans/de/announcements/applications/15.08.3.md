---
aliases:
- ../announce-applications-15.08.3
changelog: true
date: 2015-11-10
description: KDE veröffentlicht die KDE-Anwendungen 15.08.3
layout: application
title: KDE veröffentlicht die KDE-Anwendungen 15.08.3
version: 15.08.3
---
10 November 2015. Heute veröffentlicht KDE die dritte Aktualisierung der <a href='../15.08.0'>KDE-Anwendungen 15.08</a>. Diese Veröffentlichung enthält nur Fehlerkorrekturen und aktualisierte Übersetzungen und ist daher für alle Benutzer eine sichere und problemlose Aktualisierung.

Mehr als 20 aufgezeichnete Fehlerkorrekturen enthalten Verbesserungen für Ark, Dolphin, Kdenlive, kdelibs, kdepim, Kig, Lokalize und Umbrello.

Diese Veröffentlichung enthält die Version der KDE Development Platform %1 mit langfristige Unterstützung.
