---
aliases:
- ../announce-applications-18.04.1
changelog: true
date: 2018-05-10
description: KDE veröffentlicht die KDE-Anwendungen 18.04.1
layout: application
title: KDE veröffentlicht die KDE-Anwendungen 18.04.1
version: 18.04.1
---
10. Mai 2018. Heute veröffentlicht KDE die erste Aktualisierung der <a href='../18.04.0'>KDE-Anwendungen 18.04</a>. Diese Veröffentlichung enthält nur Fehlerkorrekturen und aktualisierte Übersetzungen und ist daher für alle Benutzer eine sichere und problemlose Aktualisierung.

Mehr als 20 aufgezeichnete Fehlerkorrekturen enthalten unter anderem Verbesserungen für Kontact, Cantor, Dolphin, Gwenview, JuK, Okular und Umbrello.

Verbesserungen umfassen:

- Doppelte Einträge in der Orteleiste von Dolphin verursachen jetzt keinen Absturz mehr
- Ein alter Fehler beim erneuten Laden von SVG-Dateien in Gwenview wurde behoben
- Umbrello's C++ import now understands the 'explicit' keyword
