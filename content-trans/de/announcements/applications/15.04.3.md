---
aliases:
- ../announce-applications-15.04.3
changelog: true
date: 2015-07-01
description: KDE veröffentlicht die KDE-Anwendungen 15.04.3
layout: application
title: KDE veröffentlicht die KDE-Anwendungen 15.04.3
version: 15.04.3
---
01. Juli 2015. Heute veröffentlicht KDE die zweite Aktualisierung der <a href='../15.04.0'>KDE-Anwendungen 15.04</a>. Diese Veröffentlichung enthält nur Fehlerkorrekturen und aktualisierte Übersetzungen und ist daher für alle Benutzer eine sichere und problemlose Aktualisierung.

Mehr als 20 aufgezeichnete Fehlerkorrekturen enthalten Verbesserungen für Kdenlive, kdepim, Kopete, ktp-contact-list, Marble, Okteta and Umbrello.

This release also includes Long Term Support versions of Plasma Workspaces 4.11.21, KDE Development Platform 4.14.10 and the Kontact Suite 4.14.10.
