---
aliases:
- ../announce-applications-19.04.1
changelog: true
date: 2019-05-09
description: KDE publie la version 19.04.1 des applications.
layout: application
major_version: '19.04'
release: applications-19.04.1
title: KDE publie KDE Applications 19.04.1
version: 19.04.1
---
{{% i18n_date %}}

Aujourd'hui, KDE a publié la première mise à jour de consolidation pour les <a href='../19.04.0'>applications de KDE %[2 ]s</a>. Cette version ne contient que des corrections de bogues et des mises à jour de traductions, permettant une mise à jour sûre et appréciable pour tout le monde.

A peu près 20 corrections de bogues identifiés, portant sur des améliorations de Kontact, Ark, Cantor, Dolphin, Kdenlive, Spectacle, Umbrello parmi bien d'autres.

Les améliorations incluses sont :

- L'étiquetage des fichiers sur le bureau ne tronque plus le nom de l'étiquette.
- Un plantage dans un module externe de partage de texte dan KMail a été corrigé.
- De nombreuses régressions dans l'éditeur de vidéo Kdenlive ont été corrigées.
