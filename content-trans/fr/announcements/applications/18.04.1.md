---
aliases:
- ../announce-applications-18.04.1
changelog: true
date: 2018-05-10
description: KDE publie les applications de KDE en version 18.04.1
layout: application
title: KDE publie les applications de KDE en version 18.04.1
version: 18.04.1
---
10 mai 2018. Aujourd'hui, KDE a publié la première mise à jour de consolidation pour les <a href='../18.04.0'>applications 18.04 de KDE</a>. Cette version ne contient que des corrections de bogues et des mises à jour de traduction, permettant une mise à jour sûre et appréciable pour tout le monde.

Plus de 20 corrections de bogues apportent des améliorations à Kontact, Cantor, Dolphin, Gwenview, JuK, Okular, Umbrello et bien d'autres.

Les améliorations incluses sont :

- Les entrées dupliquées dans le panneau « Emplacements » de Dolphin ne provoquent plus de plantage.
- Un bogue ancien lors du rechargement de fichiers « SVG » a été corrigé.
- L'importation de C++ de Umbrello comprend maintenant le mot clé « explicit ».
