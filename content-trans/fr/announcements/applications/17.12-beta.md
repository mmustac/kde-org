---
aliases:
- ../announce-applications-17.12-beta
custom_spread_install: true
date: 2017-11-17
description: KDE publie la version 17.12 « bêta » des applications de KDE.
layout: application
release: applications-17.11.80
title: KDE publie la version « bêta » pour les applications de KDE 17.12.
---
17 Novembre 2017. Aujourd'hui, KDE a publié la version « bêta » des nouvelles versions des applications de KDE. Les dépendances et les fonctionnalités sont stabilisées et l'équipe KDE se concentre à présent sur la correction des bogues et sur les dernières finitions.

Veuillez vérifier les <a href='https://community.kde.org/Applications/17.12_Release_Notes'>notes de mises à jour de la communauté</a> pour plus d'informations sur les nouveaux fichiers compressés, maintenant bâtis pour KDE 5, et les problèmes connus. Une annonce plus complète sera disponible avec la mise à jour finale.

Les versions des applications de KDE 17.12 ont besoin de tests intensifs pour maintenir et améliorer la qualité et l'interface utilisateur. Les utilisateurs actuels sont très importants pour maintenir la grande qualité de KDE. En effet, les développeurs ne peuvent tester toutes les configurations possibles. Votre aide est nécessaire pour aider à trouver les bogues suffisamment tôt pour qu'ils puissent être corrigés avant la version finale. Veuillez contribuer à l'équipe en installant la version « bêta » et <a href='https://bugs.kde.org/'>en signalant tout bogue</a>.

#### Installation des paquets binaires des applications KDE 17.12 « bêta »

<em>Paquets</em>Quelques fournisseurs de systèmes d'exploitation Linux / Unix fournissent gracieusement pour certaines versions de leurs distributions, des paquets binaires pour les applications 17.12 « bêta » (en interne 17.11.80). Dans d'autres cas, des bénévoles de la communauté le font aussi. Des paquets binaires supplémentaires, et également des mises à jour des paquets actuels, seront éventuellement mis à disposition dans les prochaines semaines.

<em>Emplacements des paquets</em>. Pour obtenir une liste à jour des paquets binaires disponibles, connus par l'équipe de publication KDE, veuillez visiter le <a href='http://community.kde.org/Binary_Packages</a> Wiki de la communauté</a>.

#### Compilation des applications KDE 17.12 « bêta »

Le code source complet des applications KDE 17.12 « Bêta » peut être <a href='http://download.kde.org/unstable/applications/17.11.80/src/'>librement téléchargé</a>. Toutes les instructions de compilation et d'installation sont disponibles sur la <a href='/info/applications/applications-17.11.80'> page d'informations des applications KDE 17.12 « Bêta »</a>.
