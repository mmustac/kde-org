---
aliases:
- ../../kde-frameworks-5.40.0
date: 2017-11-11
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Considérer les fichiers « DjVu » comme des documents (bogue 369195)
- Fix spelling so WPS Office presentations are recognized correctly

### Icônes « Breeze »

- add folder-stash for the stash Dolphin toolbar icon

### KArchive

- Correction d'une fuite de mémoire potentielle. Correction de la logique.

### KCMUtils

- no margins for qml modules from qwidget side
- Initialize variables (found by coverity)

### KConfigWidgets

- Fix icon of KStandardAction::MoveToTrash

### KCoreAddons

- fix URL detection with double urls like "http://www.foo.bar&lt;http://foo.bar/&gt;"
- Utilisation de « https » pour les « URL » de KDE

### Prise en charge de « KDELibs 4 »

- full docu for disableSessionManagement() replacement
- Make kssl compile against OpenSSL 1.1.0 (bug 370223)

### KFileMetaData

- Correction du nom d'affichage pour la propriété du générateur

### KGlobalAccel

- KGlobalAccel: fix support numpad keys (again)

### KInit

- Correct installation of start_kdeinit when DESTDIR and libcap are used together

### KIO

- Fix display of remote:/ in the qfiledialog
- Implémentation de la prise en charge des catégories dans « KfilesPlacesView »
- HTTP: fix error string for the 207 Multi-Status case
- KNewFileMenu: clean up dead code, spotted by Coverity
- IKWS: Fix possible infinite loop, spotted by Coverity
- KIO::PreviewJob::defaultPlugins() function

### Kirigami

- syntax working on older Qt 5.7 (bug 385785)
- stack the overlaysheet differently (bug 386470)
- Show the delegate highlighted property as well when there's no focus
- Indications de taille préférée pour le séparateur
- Correction de l'utilisation de « Settings.isMobile »
- Allow applications to be somewhat convergent on a desktop-y system
- Make sure the content of the SwipeListItem doesn't overlap the handle (bug 385974)
- Overlaysheet's scrollview is always ointeractive
- Ajout des catégories dans le fichier de bureau de la galerie (bogue 385430)
- Mise à jour du fichier « kirigami.pri »
- Utiliser le module externe non installé pour réaliser des tests
- Rendre obsolète « Kirigami.Label »
- Port gallery example use of Labels to be consistently QQC2
- Port Kirigami.Controls uses of Kirigami.Label
- make the scrollarea interactive on touch events
- Move the git find_package call to where it's used
- Définition par défaut en éléments transparents d'affichage de liste

### KNewStuff

- Supprimer « PreferCache » des requêtes du réseau
- Don't detach shared pointers to private data when setting previews
- KMoreTools: Update and fix desktopfiles (bug 369646)

### KNotification

- Remove check for SNI hosts when chosing whether to use legacy mode (bug 385867)
- Only check for legacy system tray icons if we're going to make one (bug 385371)

### Environnement de développement « KPackage »

- Utiliser les fichiers des services non installés

### KService

- Initialiser des valeurs
- Initialiser certains pointeurs

### KTextEditor

- API dox: fix wrong names of methods and args, add missing \\since
- Avoid (certain) crashes while executing QML scripts (bug 385413)
- Avoid a QML crash triggered by C style indentation scripts
- Accroissement de taille de la marque de fin
- fix some indenters from indenting on random characters
- Correction des alarmes obsolètes

### KTextWidgets

- Initialiser la valeur

### KWayland

- [client] Drop the checks for platformName being "wayland"
- Don't duplicate connect to wl_display_flush
- Protocole extérieur « Wayland »

### KWidgetsAddons

- fix createKMessageBox focus widget inconsistency
- more compact password dialog (bug 381231)
- Définition correcte de la largeur de « KPageListView »

### KWindowSystem

- KKeyServer: fix handling of Meta+Shift+Print, Alt+Shift+arrowkey etc
- Prise en charge de la plate-forme « flatpak »
- Use KWindowSystem's own platform detection API instead of duplicated code

### KXMLGUI

- Utilisation de « https » pour les « URL » de KDE

### NetworkManagerQt

- Paramètres « 8021x » : la propriété « domain-suffix-match » est définie dans le gestionnaire de réseau 1.2.0 et ultérieur
- Support "domain-suffix-match" in Security8021xSetting

### Environnement de développement de Plasma

- Dessin manuel de l'arc de cercle
- [PlasmaComponents Menu] Add ungrabMouseHack
- [FrameSvg] Optimisation de « updateSizes »
- Don't position a Dialog if it's of type OSD

### QQC2StyleBridge

- Amélioration de la compilation en tant que module externe statique
- Faire du bouton radio un vrai bouton radio
- Utiliser « qstyle » pour dessiner le cadran
- Utiliser un format de colonne pour les menus
- Correction de la boîte de dialogue
- Suppression de la propriété de groupe non valable
- Fix formatting of the md file so it matches the other modules
- Comportement de la fermeture de la boîte de dialogue à liste déroulante dans « qqc1 »
- Contournement pour « QQuickWidgets »

### Sonnet

- Ajout d'une méthode « assignByDictionnary »
- Signal if we are able to assign dictionary

### Coloration syntaxique

- Makefile: fix regexpr matching in "CXXFLAGS+"

### ThreadWeaver

- CMake cleanup: Don't hardcode -std=c++0x

### Informations sur la sécurité

Le code publié a été signé en « GPG » avec la clé suivante  pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Empreinte de la clé primaire : 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Vous pouvez discuter et partager vos idées sur cette version dans la section des commentaires de <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>l'article</a>.
