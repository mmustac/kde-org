---
aliases:
- ../../kde-frameworks-5.38.0
date: 2017-09-09
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
Mancanti nell'annuncio dello scorso mese: KF5 include un nuovo framework, Kirigami, un insieme di componenti QtQuick per costruire interfacce utente basate sulle linee guida di KDE per l'interfaccia utente.

### Baloo

- Fix directory based search

### Moduli CMake aggiuntivi

- Set CMAKE_*_OUTPUT_5.38 to run tests without installing
- Include a module for finding qml imports as runtime dependencies

### Integrazione della struttura

- Return high-resolution line edit clear icon
- Fix accepting dialogs with ctrl+return when buttons are renamed

### KActivitiesStats

- Refactor of the query which combines linked and used resources
- Reloading the model when the resource gets unlinked
- Fixed the query when merging linked and used resources

### KConfig

- Fix labels of DeleteFile/RenameFile actions (bug 382450)
- kconfigini: Strip leading whitespace when reading entry values (bug 310674)

### KConfigWidgets

- Deprecate KStandardAction::Help and KStandardAction::SaveOptions
- Fix labels of DeleteFile/RenameFile actions (bug 382450)
- Use "document-close" as icon for KStandardAction::close

### KCoreAddons

- DesktopFileParser: add fallback lookup in ":/kservicetypes5/*"
- Add support for uninstalled plugins in kcoreaddons_add_plugin
- desktopfileparser: Fix non-compliant key/value parsing (bug 310674)

### KDED

- support X-KDE-OnlyShowOnQtPlatforms

### KDocTools

- CMake: Fix target name shortening when build dir has special characters (bug 377573)
- Add CC BY-SA 4.0 International and set it as default

### KGlobalAccel

- KGlobalAccel: port to KKeyServer's new method symXModXToKeyQt, to fix numpad keys (bug 183458)

### KInit

- klauncher: fix appId matching for flatpak apps

### KIO

- Port the webshortcuts KCM from KServiceTypeTrader to KPluginLoader::findPlugins
- [KFilePropsPlugin] Locale-format totalSize during calculation
- KIO: fix long-standing memory leak on exit
- Add mimetype filtering capabilities to KUrlCompletion
- KIO: port the URI filter plugins from KServiceTypeTrader to json+KPluginMetaData
- [KUrlNavigator] Emit tabRequested when place in menu is middle-clicked (bug 304589)
- [KUrlNavigator] Emit tabRequested when places selector is middle-clicked (bug 304589)
- [KACLEditWidget] Allow double clicking to edit entry
- [kiocore] Fix the logic error in previous commit
- [kiocore] Check that klauncher is running or not
- Really rate-limit INF_PROCESSED_SIZE messages (bug 383843)
- Don't clear Qt's SSL CA certificate store
- [KDesktopPropsPlugin] Create destination directory if it doesn't exist
- [File KIO slave] Fix applying special file attributes (bug 365795)
- Remove busy loop check in TransferJobPrivate::slotDataReqFromDevice
- make kiod5 an "agent" on Mac
- Fix proxy KCM not loading manual proxies correctly

### Kirigami

- hide scrollbars when useless
- Add basic example for adjusting column width draggable handle
- ider layers in handles positioning
- fix handle placing when overlaps the last page
- don't show fake handle on the last column
- don't store stuff in the delegates (bug 383741)
- as we already set keyNavigationEnabled, set wraps as well
- better left-alignment for the back button (bug 383751)
- don't take into account the header 2 times when scrolling (bug 383725)
- never wrap the header labels
- address FIXME: remove resetTimer (bug 383772)
- don't scroll away applicationheader in non mobile
- Add a property to hide the PageRow separator matching AbstractListItem
- fix scrolling with originY and bottomtotop flow
- Get rid of warnings about setting both pixel and point sizes
- don't trigger reachable mode on inverted views
- take page footer into account
- add a slightly more complex example of a chat app
- more failsafe to find the right footer
- Check item validity before using it
- Honour layer position for isCurrentPage
- use an animation instead of an animator (bug 383761)
- leave needed space for the page footer, if possible
- better dimmer for applicationitem drawers
- background dimming for applicationitem
- fix properly back button margins
- proper margins for back button
- less warnings in ApplicationHeader
- don't use plasma scaling for icon sizes
- new look for handles

### KItemViews

### KJobWidgets

-  Initialize the "Pause" button state in the widget tracker

### KNotification

- Don't block starting notification service (bug 382444)

### KPackage Framework

- refactor kpackagetool away from stringy options

### KRunner

- Clear previous actions on update
- Add remote runners over DBus

### KTextEditor

- Port Document/View scripting API to QJSValue-based solution
- Show icons in icon border context menu
- Replace KStandardAction::PasteText with KStandardAction::Paste
- Support fractional scaling in generating the sidebar preview
- Switch from QtScript to QtQml

### KWayland

- Treat input RGB buffers as premultiplied
- Update SurfaceInterface outputs when an output global gets destroyed
- KWayland::Client::Surface track output destruction
- Avoid sending data offers from an invalid source (bug 383054)

### KWidgetsAddons

- simplify setContents by letting Qt do more of the work
- KSqueezedTextLabel: Add isSqueezed() for convenience
- KSqueezedTextLabel: Small improvements to API docs
- [KPasswordLineEdit] Set focus proxy to line edit (bug 383653)
- [KPasswordDialog] Reset geometry property

### KWindowSystem

- KKeyServer: fix handling of KeypadModifier (bug 183458)

### KXMLGUI

- Save up a bunch of stat() calls on application start
- Fix KHelpMenu position on Wayland (bug 384193)
- Drop broken mid-button click handling (bug 383162)
- KUndoActions: use actionCollection to set the shortcut

### Plasma Framework

- [ConfigModel] Guard against adding a null ConfigCategory
- [ConfigModel] Allow programmatically adding and removing ConfigCategory (bug 372090)
- [EventPluginsManager] Expose pluginPath in model
- [Icon Item] Don't needlessly unset imagePath
- [FrameSvg] Use QPixmap::mask() instead of deprecated convoluted way via alphaChannel()
- [FrameSvgItem] Create margins/fixedMargins object on demand
- fix check state for menu items
- Force Plasma style for QQC2 in applets
- Install the PlasmaComponents.3/private folder
- Drop remains of "locolor" themes
- [Theme] Use KConfig SimpleConfig
- Avoid some unnecessary theme content lookups
- ignore spurious resize events to empty sizes (bug 382340)

### Evidenziazione della sintassi

- Add syntax definition for Adblock Plus filter lists
- Rewrite the Sieve syntax definition
- Add highlighting for QDoc configuration files
- Add highlight definition for Tiger
- Escape hyphen in rest.xml regular expressions (bug 383632)
- fix: plaintext is highlighted as powershell
- Add syntax highlighting for Metamath
- Rebased Less syntax highlighting on SCSS one (bug 369277)
- Add Pony highlighting
- Rewrite the email syntax definition

### Informazioni di sicurezza

Il codice rilasciato è stato firmato con GPG utilizzando la chiave seguente: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Impronta della chiave primaria: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB

Puoi discutere e condividere idee su questo rilascio nella sezione dei commenti nell'<a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>articolo sul Dot</a>.
