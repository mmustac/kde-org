---
aliases:
- ../../kde-frameworks-5.42.0
date: 2018-01-13
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Modifiche generali

- Fixes for cmake 3.10+ AUTOMOC warnings
- More widespread use of categorized logging, to turn off debug output by default (use kdebugsetting to re-enable it)

### Baloo

- balooctl status: process all arguments
- Fix multiple word tag queries
- Simplify rename conditions
- Fix incorrect UDSEntry display name

### Icone Brezza

- Fix icon name "weather-none" -&gt; "weather-none-available" (bug 379875)
- remove Vivaldi icon cause the origin app icon fits perfect with breeze (bug 383517)
- add Some missing mimes
- Breeze-icons add document-send icon (bug 388048)
- update album artist icon
- add labplot-editlayout support
- remove duplicates and update dark theme
- add gnumeric breeze-icon support

### Moduli CMake aggiuntivi

- Use readelf to find project dependencies
- Introduce INSTALL_PREFIX_SCRIPT to easily set up prefixes

### KActivities

- avoid crash in kactivities if no dbus connection is available

### KConfig

- API docs: explain how to use KWindowConfig from a dialog constructor
- Deprecate KDesktopFile::sortOrder()
- Fix the result of KDesktopFile::sortOrder()

### KCoreAddons

- Extend CMAKE_AUTOMOC_MACRO_NAMES also for own build
- Match license keys by spdx

### KDBusAddons

- Fix absolute path detection for cmake 3.5 on Linux
- Add cmake function 'kdbusaddons_generate_dbus_service_file'

### KDeclarative

- Qml controls for kcm creation

### KDED

- Use cmake function 'kdbusaddons_generate_dbus_service_file' from kdbusaddons to generate dbus service file
- Add used property to service file definition

### Supporto KDELibs 4

- Inform the user if the module can not be registered with org.kde.kded5 and exit with error
- Mingw32 compile fix

### KDocTools

- add entity for Michael Pyne
- add entities for Martin Koller to contributor.entities
- fix debian entry
- add entity Debian to general.entities
- add entity kbackup, it was imported
- add entity latex, we have already 7 entities in different index.docbooks in kf5

### KEmoticons

- Add scheme (file://). It's necessary when we use it in qml and we added all

### KFileMetaData

- remove extractor based on QtMultimedia
- Check for Linux instead of TagLib and avoid building the usermetadatawritertest on Windows
- Restore # 6c9111a9 until a successful build and link without TagLib is possible
- Remove the taglib dependency, caused by a left-over include

### KHTML

- Finally allow to disable debug output by using categorized logging

### KI18n

- do not treat ts-pmap-compile as executable
- Fix a memory leak in KuitStaticData
- KI18n: fix a number of double lookups

### KInit

- Remove impossible to reach code

### KIO

- Properly parse dates in cookies when running in non-English locale (bug 387254)
- [kcoredirlister] Fix sub path creation
- Reflect trash state in iconNameForUrl
- Forward QComboBox signals instead of QComboBox lineedit signals
- Fixed webshortcuts showing their file path instead of their human-readable name
- TransferJob: fix for when the readChannelFinished has already been emitted before start is called() (bug 386246)
- Fix crash, presumably since Qt 5.10? (bug 386364)
- KUriFilter: don't return an error on non-existing files
- Fix creation of paths
- Implement a kfile dialog where we can add custom widget
- Verify that qWaitForWindowActive doesn't fail
- KUriFilter: port away from KServiceTypeTrader
- API dox: use class names in titles of example screenshots
- API dox: also deal with KIOWIDGETS export macros in QCH creation
- fix handling of KCookieAdvice::AcceptForSession (bug 386325)
- Created 'GroupHiddenRole' for KPlacesModel
- forward socket error string to KTcpSocket
- Refactor and remove duplicate code in kfileplacesview
- Emit 'groupHiddenChanged' signal
- Refactoring the hidding/showing animation use within KFilePlacesView
- User can now hide an entire places group from KFilePlacesView (bug 300247)
- Hidding place groups implementation in KFilePlacesModel (bug 300247)
- [KOpenWithDialog] Remove redundant creation of KLineEdit
- Add undo support to BatchRenameJob
- Add BatchRenameJob to KIO
- ix doxygen code block not ending with endcode

### Kirigami

- keep the flickable interactive
- proper prefix for icons as well
- fix form sizing
- read wheelScrollLines from kdeglobals if existing
- add a prefix for kirigami files to avoid conflicts
- some static linking fixes
- move plasma styles to plasma-framework
- Use single quotes for matching characters + QLatin1Char
- FormLayout

### KJobWidgets

- Offer QWindow API for KJobWidgets:: decorators

### KNewStuff

- Limit request cache size
- Require the same internal version as you're building
- Prevent global variables from been using after freeing

### KNotification

- [KStatusNotifierItem] Don't "restore" widget position on its first show
- Use positions of legacy systray icons for Minimize/Restore actions
- Handle positions of LMB clicks on legacy systray icons
- do not make the context menu a Window
- Add explanatory comment
- Lazy-instanciate and lazy-load KNotification plugins

### KPackage Framework

- invalidate the runtime cache on install
- experimental support for rcc files loading in kpackage
- compile against Qt 5.7
- Fix up package indexing and add runtime caching
- new KPackage::fileUrl() method

### KRunner

- [RunnerManager] Don't mess with ThreadWeaver thread count

### KTextEditor

- Fix wildcard matching for modelines
- Fix a regression caused by changing backspace key behavior
- port to non-deprecated API like already used at other place (bug 386823)
- Add missing include for std::array
- MessageInterface: Add CenterInView as additional position
- QStringList initializer list cleanup

### KWallet Framework

- Use correct service executable path for installing kwalletd dbus service on Win32

### KWayland

- Fix naming inconsistency
- Create interface for passing server decoration palettes
- Explicitly include std::bind functions
- [server] Add a method IdleInterface::simulateUserActivity
- Fix regression caused by backward compatibility support in data source
- Add support for version 3 of data device manager interface (bug 386993)
- Scope exported/imported objects to the test
- Fix error in WaylandSurface::testDisconnect
- Add explicit AppMenu protocol
- Fix exclude generated file from automoc feature

### KWidgetsAddons

- Fix crash in setMainWindow on wayland

### KXMLGUI

- API dox: make doxygen cover session related macros &amp; functions again
- Disconnect shortcutedit slot on widget destruction (bug 387307)

### NetworkManagerQt

- 802-11-x: support for PWD EAP method

### Plasma Framework

- [Air theme] Add task bar progress graphic (bug 368215)
- Templates: remove stray * from license headers
- make packageurlinterceptor as noop as possible
- Revert "Don't tear down renderer and other busy work when Svg::setImagePath is invoked with the same arg"
- move kirigami plasma styles here
- disappearing scrollbars on mobile
- reuse KPackage instance between PluginLoader and Applet
- [AppletQuickItem] Only set QtQuick Controls 1 style once per engine
- Don't set a window icon in Plasma::Dialog
- [RTL] - align properly the selected text for RTL (bug 387415)
- Initialize scale factor to the last scale factor set on any instance
- Revert "Initialize scale factor to the last scale factor set on any instance"
- Don't update when the underlying FrameSvg is repaint-blocked
- Initialize scale factor to the last scale factor set on any instance
- Move if check inside #ifdef
- [FrameSvgItem] Don't create unnecessary nodes
- Don't tear down renderer and other busy work when Svg::setImagePath is invoked with the same arg

### Prison

- Also look for qrencode with debug suffix

### QQC2StyleBridge

- simplify and don't try to block mouse events
- if no wheel.pixelDelta, use global wheel scroll lines
- desktop tabbars have different widths for each tab
- ensure a non 0 size hint

### Sonnet

- Don't export internal helper executables
- Sonnet: fix wrong language for suggestions in mixed-language texts
- Remove ancient and broken workaround
- Don't cause circular linking on Windows

### Evidenziazione della sintassi

- Highlighting indexer: Warn about context switch fallthroughContext="#stay"
- Highlighting indexer: Warn about empty attributes
- Highlighting indexer: Enable errors
- Highlighting indexer: report unused itemDatas and missing itemDatas
- Prolog, RelaxNG, RMarkDown: Fix highlighting issues
- Haml: Fix invalid and unused itemDatas
- ObjectiveC++: Remove duplicate comment contexts
- Diff, ObjectiveC++: Cleanups and fix highlighting files
- XML (Debug): Fix incorrect DetectChar rule
- Highlighting Indexer: Support cross-hl context checking
- Revert: Add GNUMacros to gcc.xml again, used by isocpp.xml
- email.xml: add *.email to the extensions
- Highlighting Indexer: Check for infinite loops
- Highlighting Indexer: Check for empty context names and regexps
- Fix referencing of non-existing keyword lists
- Fix simple cases of duplicate contexts
- Fix duplicate itemDatas
- Fix DetectChar and Detect2Chars rules
- Highlighting Indexer: Check keyword lists
- Highlighting Indexer: Warn about duplicate contexts
- Highlighting Indexer: Check for duplicate itemDatas
- Highlighting indexer: Check DetectChar and Detect2Chars
- Validate that for all attributes an itemData exists

### Informazioni di sicurezza

Il codice rilasciato è stato firmato con GPG utilizzando la chiave seguente: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Impronta della chiave primaria: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB

Puoi discutere e condividere idee su questo rilascio nella sezione dei commenti nell'<a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>articolo sul Dot</a>.
