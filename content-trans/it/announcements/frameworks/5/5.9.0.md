---
aliases:
- ../../kde-frameworks-5.9.0
date: '2015-04-10'
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
Nuovo modulo: ModemManagerQt (wrapper Qt per ModemManager API). Alternativamente, aggiorna a Plasma-NM 5.3 Beta durante l'aggiornamento a ModemManagerQt 5.9.0.

### KActivities

- Implemented forgetting a resource
- Build fixes
- Added a plugin to register events for KRecentDocument notifications

### KArchive

- Respect KZip::extraField setting also when writing central header entries
- Remove two erroneous asserts, happening when disk is full, bug 343214

### KBookmarks

- Fix build with Qt 5.5

### KCMUtils

- Use new json-based plugin system. KCMs are searched under kcms/. For now a desktop file still needs to be installed under kservices5/ for compatibility
- Load and wrap the QML-only version of kcms if possible

### KConfig

- Fix assert when using KSharedConfig in a global object destructor.
- kconfig_compiler: add support for CategoryLoggingName in *.kcfgc files, to generate qCDebug(category) calls.

### KI18n

- preload the global Qt catalog when using i18n()

### KIconThemes

- KIconDialog can now be shown using the regular QDialog show() and exec() methods
- Fix KIconEngine::paint to handle different devicePixelRatios

### KIO

- Enable KPropertiesDialog to show free space information of remote file systems as well (e.g. smb)
- Fix KUrlNavigator with high DPI pixmaps
- Make KFileItemDelegate handle non default devicePixelRatio in animations

### KItemModels

- KRecursiveFilterProxyModel: reworked to emit the right signals at the right time
- KDescendantsProxyModel: Handle moves reported by the source model.
- KDescendantsProxyModel: Fix behavior when a selection is made while resetting.
- KDescendantsProxyModel: Allow constructing and using KSelectionProxyModel from QML.

### KJobWidgets

- Propagate error code to JobView DBus interface

### KNotifications

- Added an event() version that takes no icon and will use a default one
- Added an event() version that takes StandardEvent eventId and QString iconName

### KPeople

- Allow extending action metadata by using predefined types
- Fix model not being properly updated after removing a contact from Person

### KPty

- Expose to world whether KPty has been built with utempter library

### KTextEditor

- Add kdesrc-buildrc highlighting file
- syntax: added support for binary integer literals in the PHP highlighting file

### KWidgetsAddons

- Make KMessageWidget animation smooth with high Device Pixel Ratio

### KWindowSystem

- Add a dummy Wayland implementation for KWindowSystemPrivate
- KWindowSystem::icon with NETWinInfo not bound to platform X11.

### KXmlGui

- Preserve translation domain when merging .rc files
- Fix runtime warning QWidget::setWindowModified: The window title does not contain a '[*]' placeholder

### KXmlRpcClient

- Install translations

### Plasma Framework

- Fixed stray tooltips when temporary owner of tooltip disappeared or became empty
- Fix TabBar not properly laid out initially, which could be observed in eg. Kickoff
- PageStack transitions now use Animators for smoother animations
- TabGroup transitions now use Animators for smoother animations
- Make Svg,FrameSvg work qith QT_DEVICE_PIXELRATIO

### Solid

- Refresh the battery properties upon resume

### Buildsystem changes

- Extra CMake Modules (ECM) is now versioned like KDE Frameworks, therefore it is now 5.9, while it was 1.8 previously.
- Many frameworks have been fixed to be useable without searching for their private dependencies. I.e. applications looking up a framework only need its public dependencies, not the private ones.
- Allow configuration of SHARE_INSTALL_DIR, to handle multi-arch layouts better

### Frameworkintegration

- Fix possible crash when destroying a QSystemTrayIcon (triggered by e.g. Trojita), bug 343976
- Fix native modal file dialogs in QML, bug 334963

Puoi discutere e condividere idee su questo rilascio nella sezione dei commenti nell'<a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>articolo sul Dot</a>.
