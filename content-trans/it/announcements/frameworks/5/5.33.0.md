---
aliases:
- ../../kde-frameworks-5.33.0
date: 2017-04-08
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Added description for commands (balooctl)
- Search also in symlinked directories (bug 333678)

### BluezQt

- Provide device type for Low Energy devices

### Moduli CMake aggiuntivi

- Specify qml-root-path as the share directory in the prefix
- Fix ecm_generate_pkgconfig_file compatibility with new cmake
- Only register APPLE_* options if(APPLE)

### KActivitiesStats

- Added presets to the testing application
- Properly moving items to the desired position
- Syncing reordering to other model instances
- If the order is not defined, sort the entries by the id

### KDE Doxygen Tools

- [Meta] Change maintainer in setup.py

### KAuth

- Backend for Mac
- Add support for killing a KAuth::ExecuteJob

### KConfig

- Sanitize shortcut list on read/write from kdeglobals
- Avoid useless reallocs by removing squeeze call on temporary buffer

### KDBusAddons

- KDBusService: Add accessor for the dbus service name we registered

### KDeclarative

- With Qt &gt;= 5.8 use the new API to set scene graph backend
- Don't set acceptHoverEvents in DragArea as we don't use them

### KDocTools

- meinproc5: link to the files, not to the library (bug 377406)

### KFileMetaData

- Make PlainTextExtractor match "text/plain" again

### KHTML

- Error page, correctly load the image (with a real URL)

### KIO

- Make remote file:// URL redirect to smb:// work again
- Keep query encoding when HTTP Proxy is used
- Updated user agents (Firefox 52 ESR, Chromium 57)
- Handle/truncate url display string assigned to job description. Prevents large data: urls from being included in UI notifications
- Add KFileWidget::setSelectedUrl() (bug 376365)
- Fix KUrlRequester save mode by adding setAcceptMode

### KItemModels

- Mention the new QSFPM::setRecursiveFiltering(true) which makes KRecursiveFilterProxyModel obsolete

### KNotification

- Do not remove queued notifications when the fd.o service starts
- Mac platform adaptations

### KParts

- API dox: fix missing note to call setXMLFile with KParts::MainWindow

### KService

- Fix 'Not found: ""' terminal messages

### KTextEditor

- Expose additional internal View's functionality to the public API
- Save a lot of allocation for setPen
- Fix ConfigInterface of KTextEditor::Document
- Added font and on-the-fly-spellcheck options in ConfigInterface

### KWayland

- Add support for wl_shell_surface::set_popup and popup_done

### KWidgetsAddons

- Support building against a qt without a11y enabled
- Fix wrong size hint when animatedShow is called with hidden parent (bug 377676)
- Fix characters in KCharSelectTable getting elided
- Enable all planes in kcharselect test dialog

### NetworkManagerQt

- WiredSetting: return autonegotiate even when it's disabled
- Prevent signals in glib2 be defined by Qt
- WiredSetting: Speed and duplex can be set only when auto-negotiation is off (bug 376018)
- Auto-negotiate value for wired setting should be false

### Plasma Framework

- [ModelContextMenu] Use Instantiator instead of Repeater-and-reparent-hack
- [Calendar] Shrink and elide week names like is done with day delegate (bug 378020)
- [Icon Item] Make "smooth" property actually do something
- Set implicit size from source size for image/SVG URL sources
- add a new property in containment, for an edit mode
- correct maskRequestedPrefix when no prefix is used (bug 377893)
- [Menu] Harmonize openRelative placement
- Most (context) menus have accelerators (Alt+letter shortcuts) now (bug 361915)
- Plasma controls based on QtQuickControls2
- Handle applyPrefixes with an empty string (bug 377441)
- actually delete old theme caches
- [Containment Interface] Trigger context menus on pressing "Menu" key
- [Breeze Plasma Theme] Improve action-overlay icons (bug 376321)

### Evidenziazione della sintassi

- TOML: Fix highlighting of string escape sequences
- Update Clojure syntax highlighting
- A few updates to OCaml syntax
- Hightlight *.sbt files as scala code
- Also use the QML highlighter for .qmltypes files

### Informazioni di sicurezza

Il codice rilasciato è stato firmato con GPG utilizzando la chiave seguente: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Impronta della chiave primaria: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB

Puoi discutere e condividere idee su questo rilascio nella sezione dei commenti nell'<a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>articolo sul Dot</a>.
