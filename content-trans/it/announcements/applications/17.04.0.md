---
aliases:
- ../announce-applications-17.04.0
changelog: true
date: 2017-04-20
description: KDE rilascia KDE Applications 17.04.0
layout: application
title: KDE rilascia KDE Applications 17.04.0
version: 17.04.0
---
20 aprile 2017. KDE Applications 17.04 è qui. Nel complesso, abbiamo lavorato per rendere sia le applicazioni che le librerie sottostanti più stabili e più facili da usare. Stirando le rughe e ascoltando le vostre segnalazioni, abbiamo reso la suite di applicazioni KDE meno soggetta a problemi e molto più amichevole.

Divertiti con le nostre nuove applicazioni!

#### <a href="https://edu.kde.org/kalgebra/">KAlgebra</a>

{{<figure src="/announcements/applications/17.04.0/kalgebra1704.jpg" width="600px" >}}

Gli sviluppatori di KAlgebra hanno intrapreso il percorso della convergenza, avendo portato la versione mobile del programma educativo completo a Kirigami 2.0, il framework preferito per l'integrazione di applicazioni KDE su piattaforme desktop e mobili.

Inoltre, la versione desktop ha anche migrato il motore 3D su GLES, il software che consente al programma di rendere le funzioni 3D sia sul desktop che sui dispositivi mobili. Ciò rende il codice più semplice e più facile da mantenere.

#### <a href="http://kdenlive.org/">Kdenlive</a>

{{<figure src="/announcements/applications/17.04.0/kdenlive1704.png" width="600px" >}}

L'editor video di KDE sta diventando più stabile e più completo con ogni nuova versione. Questa volta, gli sviluppatori hanno riprogettato la finestra di selezione del profilo per semplificare la dimensione dello schermo, la velocità dei fotogrammi e altri parametri del tuo filmato.

Ora puoi anche riprodurre il tuo video direttamente dalla notifica al termine del rendering. Alcuni arresti anomali accaduti quando si  spostano le clip sulla sequenza temporale sono stati corretti e la procedura guidata DVD è stata migliorata.

#### <a href="https://userbase.kde.org/Dolphin">Dolphin</a>

{{<figure src="/announcements/applications/17.04.0/dolphin1704.png" width="600px" >}}

Il nostro esploratore di file preferito e portale per tutto (tranne forse che per gli inferi) ha avuto diversi rinnovamenti e miglioramenti di usabilità per renderlo ancora più potente.

I menu contestuali nel pannello <i>Risorse</i> (per impostazione predefinita a sinistra dell'area di visualizzazione principale) sono stati ripuliti ed è ora possibile interagire con gli oggetti dei metadati nei suggerimenti. I suggerimenti, a proposito, ora funzionano anche su Wayland.

#### <a href="https://www.kde.org/applications/utilities/ark/">Ark</a>

{{<figure src="/announcements/applications/17.04.0/ark1704.png" width="600px" >}}

La popolare app grafica per la creazione, la decompressione e la gestione degli archivi compressi per file e cartelle ora viene fornita con una funzione di <i>ricerca</i> per aiutarti a trovare file in archivi affollati.

Ti consente inoltre di abilitare e disabilitare le estensioni direttamente dalla finestra <i>Configura</i>. Parlando di estensioni, la nuova applicazione Libzip migliora il supporto degli archivi Zip.

#### <a href="https://minuet.kde.org/">Minuet</a>

{{<figure src="/announcements/applications/17.04.0/minuet1704.png" width="600px" >}}

Se stai insegnando o imparando a suonare musica, devi dare un'occhiata a Minuet. La nuova versione offre più esercizi di scale e attività di allenamento dell'orecchio per le scale bebop, armoniche minori/maggiori, pentatoniche e simmetriche.

Puoi anche impostare o eseguire test utilizzando la nuova <i>Modalità di test</i> per rispondere agli esercizi. Puoi monitorare i tuoi progressi eseguendo una sequenza di 10 esercizi e otterrai le statistiche di successo al termine.

### E altro!

<a href='https://okular.kde.org/'>Okular</a>, il visore di documenti di KDE, ha ricevuto almeno una mezza dozzina di cambiamenti che aggiungono funzionalità e accumulano la sua usabilità sugli schermi tattili. <a href='https://userbase.kde.org/Akonadi'>Akonadi</a> e diverse altre applicazioni che compongono <a href='https://www.kde.org/applications/office/kontact/'>Kontact</a> (suite di posta/calendario/groupware di KDE) sono stati sottoposti a revisione, debug e ottimizzati per aiutarti a diventare più produttivo.

<a href='https://www.kde.org/applications/games/kajongg'>Kajongg</a>, <a href='https://www.kde.org/applications/development/kcachegrind/'>KCachegrind</a> e altro (<a href='https://community.kde.org/Applications/17.04_Release_Notes#Tarballs_that_were_based_on_kdelibs4_and_are_now_KF5_based'>Note di rilascio</a>) sono state ora portate su KDE Frameworks 5. Attendiamo con impazienza il vostro riscontro e le vostre idee sulle nuove funzionalità introdotte con questa versione.

<a href='https://userbase.kde.org/K3b'>K3b</a> si è unito al rilascio delle applicazioni KDE.

### Ricerca e risoluzione degli errori

Più di 95 bug sono stati risolti in applicazioni tra cui Kopete, KWalletManager, Marble, Spectacle e altro ancora!

### Elenco completo dei cambiamenti
