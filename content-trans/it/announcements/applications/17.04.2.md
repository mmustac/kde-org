---
aliases:
- ../announce-applications-17.04.2
changelog: true
date: 2017-06-08
description: KDE rilascia KDE Applications 17.04.2
layout: application
title: KDE rilascia KDE Applications 17.04.2
version: 17.04.2
---
8 giugno 2017. Oggi KDE ha rilasciato il secondo aggiornamento di stabilizzazione per <a href='../17.04.0'>KDE Applications 17.04</a>. Questo rilascio contiene solo correzioni di errori e aggiornamenti delle traduzioni, costituisce un aggiornamento sicuro e gradevole per tutti.

Gli oltre 15 errori corretti includono, tra gli altri, miglioramenti a kdepim, ark, dolphin, gwenview e kdenlive.

Questo rilascio include inoltre le versioni con supporto a lungo termine di KDE Development Platform 4.14.33.
