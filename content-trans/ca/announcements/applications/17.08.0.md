---
aliases:
- ../announce-applications-17.08.0
changelog: true
date: 2017-08-17
description: KDE distribueix les aplicacions 17.08.0 del KDE
layout: application
title: KDE distribueix les aplicacions 17.08.0 del KDE
version: 17.08.0
---
17 d'agost de 2017. Les Aplicacions 17.08 del KDE ja són aquí. Hem treballat per fer tant les aplicacions com les biblioteques subjacents més estables i més fàcils d'usar. Hem llimat les arestes i escoltat els vostres comentaris, hem fet que el conjunt de les Aplicacions del KDE sigui menys propens a errors i molt més fàcil d'emprar. Gaudiu de les noves aplicacions!

### Més adaptacions als Frameworks 5 del KDE

Ens complau que les següents aplicacions que es basaven en el kdelibs4 ara es basen en els Frameworks 5 del KDE: kmag, kmousetool, kgoldrunner, kigo, konquest, kreversi, ksnakeduel, kspaceduel, ksudoku, kubrick, lskat, umbrello. Gràcies als desenvolupadors que treballen fort i que ofereixen el seu temps i feina per a fer-ho.

### Novetats a les Aplicacions 17.08 del KDE

#### Dolphin

Els desenvolupadors del Dolphin informen que ara mostra el «Temps de supressió» a la Paperera, i mostra el «Temps de creació» si el sistema operatiu ho admet, com en el BSD.

#### KIO-Extras

Els Kio-Extras ara proporcionen un suport millor per a les comparticions Samba.

#### KAlgebra

Els desenvolupadors del KAlgebra han treballat per millorar el frontal del Kirigami a l'escriptori i implementar-hi la compleció del codi.

#### Kontact

- Al KMailtransport, els desenvolupadors han reactivat el suport del transport akonadi, han creat connectors compatibles i han tornat a crear el suport del transport de correu «sendmail».
- Al SieveEditor, s'han esmenat i tancat molts errors en l'autocreació de scripts. A més de l'esmenat general d'errors, s'ha afegit un editor per línies d'expressions regulars.
- Al KMail, la capacitat d'utilitzar un editor extern s'ha creat com a connector.
- Ara, l'assistent d'importació de l'Akonadi té el «converteix tots els convertidors» com a connector, de manera que els desenvolupadors poden crear amb facilitat convertidors nous.
- Les aplicacions ara depenen de les Qt 5.7. Els desenvolupadors han esmenat molts errors de compilació al Windows. Tot el Kdepim encara no compila al Windows, però els desenvolupadors han fet grans progressos. Per a començar, els desenvolupadors han creat una recepta per a fer-ho. S'ha realitzat una bona esmena d'errors per a modernitzar el codi (C++11). S'ha implementat el Wayland a les Qt 5.9. El Kdepim-runtime afegeix un recurs per a Facebook.

#### Kdenlive

Al Kdenlive, l'equip ha esmenat la fallada «Efecte congelat». En versions recents, era impossible canviar el marc congelat per a l'efecte de congelació. Ara es permet una drecera de teclat per extreure el fotograma. Ara, l'usuari pot desar captures de pantalla de la seva línia de temps amb una drecera de teclat i se suggerirà un nom en funció del número del fotograma <a href='https://bugs.kde.org/show_bug.cgi?id=381325'>https://bugs.kde.org/show_bug.cgi?id=381325</a>. S'ha esmenat l'efecte de les barres de transició en les baixades que no apareixien a la interfície: <a href='https://bugs.kde.org/show_bug.cgi?id=382451'>https://bugs.kde.org/show_bug.cgi?id=382451</a>. S'ha esmenat el problema amb l'àudio dels clics (per ara, cal crear la dependència MLT des del git fins a un llançament d'aquest): <a href='https://bugs.kde.org/show_bug.cgi?id=371849'>https://bugs.kde.org/show_bug.cgi?id=371849</a>.

#### Krfb

Els desenvolupadors han finalitzat l'adaptació del connector X11 a les Qt5, i el krfb torna a funcionar amb un dorsal X11 molt més ràpid que el connector de les Qt. Hi ha una nova pàgina de configuració, la qual permet a l'usuari canviar el connector de framebuffer preferit.

#### Konsole

El Konsole ara permet un desplaçament il·limitat per estendre el límit anterior de 2 GB (32 bits). Ara, el Konsole permet als usuaris introduir qualsevol ubicació per emmagatzemar els fitxers del desplaçament. A més, s'ha esmenat una regressió, el Konsole pot tornar a permetre que la KonsolePart cridi al diàleg Gestiona el perfil.

#### KAppTemplate

Al KAppTemplate, ara hi ha una opció per instal·lar plantilles noves des del sistema de fitxers. S'han eliminat més plantilles del KAppTemplate i s'han integrat als productes relacionats. La plantilla per a un connector del ktexteditor i la plantilla de la kpartsapp (ara adaptades a les Qt5/KF5) esdevenen part del KTextEditor i KParts als Frameworks del KDE des del 5.37.0. Aquests canvis haurien de simplificar la creació de plantilles en les aplicacions del KDE.

### Cacera d'errors

S'han esmenat més de 80 errors a les aplicacions, incloses el paquet Kontact, Ark, Dolphin, K3b, Kdenlive, KGpg, Konsole i més!

### Registre complet de canvis
