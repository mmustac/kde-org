---
aliases:
- ../announce-applications-15.08.0
changelog: true
date: 2015-08-19
description: Es distribueixen les aplicacions 15.08.0 de KDE.
layout: application
release: applications-15.08.0
title: KDE distribueix les aplicacions 15.08.0 del KDE
version: 15.08.0
---
{{<figure src="https://dot.kde.org/sites/dot.kde.org/files/dolphin15.08_0.png" alt=`Dolphin en el nou estil -ara es basa en les Frameworks 5 del KDE-` class="text-center" width="600px" caption=`Dolphin en el nou estil -ara es basa en les Frameworks 5 del KDE-`>}}

19 d'agost de 2015. Avui KDE anuncia el llançament de les aplicacions 15.08 del KDE.

Amb aquest llançament hi ha un total de 107 aplicacions adaptades als <a href='https://dot.kde.org/2013/09/25/frameworks-5'>Frameworks 5 del KDE</a>. L'equip està tractant de donar la millor qualitat a l'escriptori i a aquestes aplicacions. Així que esperem comptar amb vós perquè envieu els vostres comentaris.

Amb aquest llançament hi ha diverses incorporacions noves a la llista d'aplicacions basades en els Frameworks 5 de KDE, incloent-hi <a href='https://www.kde.org/applications/system/dolphin/'>Dolphin</a>, <a href='https://www.kde.org/applications/office/kontact/'>the Kontact Suite</a>, <a href='https://www.kde.org/applications/utilities/ark/'>Ark</a>, <a href='https://games.kde.org/game.php?game=picmi'>Picmi</a>, etc.

### Vista prèvia tècnica de la suite Kontact

En els últims mesos, l'equip PIM del KDE ha posat molt esforç en portar el Kontact a les Qt 5 i als Frameworks 5 del KDE. A més, el rendiment en l'accés a les dades ha obtingut millores considerables mitjançant una capa de comunicació optimitzada. L'equip PIM del KDE està treballant intensament a polir encara més la suite Kontact i resta a l'espera dels vostres comentaris. Per a obtenir més i detallada informació sobre el que ha canviat al PIM del KDE vegeu el <a href='http://www.aegiap.eu/kdeblog/'>blog d'en Laurent Montel</a>.

### Kdenlive i Okular

Aquesta versió del Kdenlive inclou un munt de correccions en l'assistent de DVD, a més d'un gran nombre de correccions d'errors i altres característiques que inclouen la integració d'algunes refactorizacions més grans. Podeu veure més informació sobre els canvis al Kdenlive en la seva <a href='https://kdenlive.org/discover/15.08.0'>extensa llista de canvis</a>. I Okular ara suporta una transició amb esvaïment en el mode de presentació.

{{<figure src="https://dot.kde.org/sites/dot.kde.org/files/ksudoku_small_0.png" alt=`KSudoku amb un trencaclosques amb caràcters` class="text-center" width="600px" caption=`KSudoku amb un trencaclosques amb caràcters`>}}

### Dolphin, educació i jocs

El Dolphin ha estat portat als Frameworks 5 del KDE. El Marbre ha aconseguit millorar la implementació del <a href='https://en.wikipedia.org/wiki/Universal_Transverse_Mercator_coordinate_system'>UTM</a>, així com millorar les anotacions, edició i superposicions del KML.

L'Ark ha tingut un sorprenent nombre de publicacions que inclouen petites correccions. El Kstars ha rebut un gran nombre de publicacions, inclosa una millora en l'algoritme ADU pla, la comprovació per a valors fora dels límits, desar la inversió del meridià, desviació del guiatge, límit HFR de l'enfocament automàtic al fitxer de la seqüència, s'ha afegit la implementació per a classificar i desblocar el telescopi. El KSudoku va millorant. Les publicacions inclouen: afegeixen una IGU i un motor per accedir als trencaclosques de Matedoku o de Killer Sudoku, i afegeix un nou solucionador basat en l'algoritme d'enllaços ballarins -Dancing Links (DLX)- d'en Donald Knuth.

### Altres llançaments

Juntament amb aquest llançament, es publicarà per darrera vegada el Plasma 4 en la seva versió LTS com la versió 4.11.22.
