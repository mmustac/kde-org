---
aliases:
- ../announce-applications-16.04.3
changelog: true
date: 2016-07-12
description: KDE distribueix les aplicacions 16.04.3 del KDE
layout: application
title: KDE distribueix les aplicacions 16.04.3 del KDE
version: 16.04.3
---
12 de juliol de 2016. Avui KDE distribueix la tercera actualització d'estabilització per a les <a href='../16.04.0'>aplicacions 16.04 del KDE</a>. Aquesta publicació només conté esmenes d'errors i actualitzacions de traduccions, proporcionant una actualització segura i millor per a tothom.

Hi ha més de 20 esmenes registrades d'errors que inclouen millores a l'Ark, Cantor, Kate, Kdepim i Umbrello entre altres.

Aquest llançament també inclou les versions de suport a llarg termini de la plataforma de desenvolupament KDE 4.14.22.
