---
aliases:
- ../../kde-frameworks-5.29.0
date: 2016-12-12
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Framework nou

Aquest llançament inclou Prison, un Framework nou per a la generació de codis de barres (incloent-hi codis QR).

### General

S'ha afegit el FreeBSD al «metainfo.yaml» en totes els Frameworks provats que funcionen en el FreeBSD.

### Baloo

- Millora del rendiment en escriure (4 * vegades més ràpid en escriure dades)

### Icones Brisa

- Fa BINARY_ICONS_RESOURCE ON de manera predeterminada
- Afegeix el MIME vnd.rar del «shared-mime-info» 1.7 (error 372461)
- Afegeix la icona del Claws (error 371914)
- Afegeix la icona del GDrive en lloc d'una icona genèrica del núvol (error 372111)
- Soluciona l'error «list-remove-symbolic usa una imatge incorrecta» (error 372119)
- Altres addicions i millores

### Mòduls extres del CMake

- Omet la prova de vinculacions del Python si no està instal·lat el PyQt
- Només afegeix la prova si es troba el Python
- Redueix el requisit mínim del CMake
- Afegeix el mòdul «ecm_win_resolve_symlinks»

### Integració del marc de treball

- Cerca el QDBus, necessari per al manipulador KPackage de l'«appstream»
- Permet que el KPackage tingui dependències del «packagekit» i «appstream»

### KActivitiesStats

- Enviament adequat del recurs de l'esdeveniment enllaçat

### Eines de Doxygen del KDE

- Adaptació al canvi quickgit -&gt; cgit
- Soluciona un error si no s'ha definit el nom del grup. Encara es pot trencar en condicions dolentes

### KArchive

- Afegeix el mètode «errorString()» per proporcionar informació de l'error

### KAuth

- Afegir la propietat «timeout» (error 363200)

### KConfig

- kconfig_compiler - genera codi amb sobreescriptures
- Analitza adequadament les paraules clau de les funcions (error 371562)

### KConfigWidgets

- Assegura que les accions dels menús tenen el MenuRole destinat

### KCoreAddons

- KTextToHtml: soluciona l'error «[1] afegit al final d'un hiperenllaç» (error 343275)
- KUser: Cerca només l'avatar si el «loginName» no és buit

### KCrash

- Alinea amb el «KInit» i no usa el «DISPLAY» al Mac
- No tanca tots els descriptors de fitxer a l'OS X

### KDesignerPlugin

- src/kgendesignerplugin.cpp - afegeix sobreescriptures al codi generat

### KDESU

- Neteja XDG_RUNTIME_DIR en els processos executats amb el «kdesu»

### KFileMetaData

- Cerca realment la «libpostproc» del FFMpeg

### KHTML

- Java: aplica els noms als botons correctes
- Java: defineix els noms al diàleg de permisos

### KI18n

- Verifica adequadament la no igualtat de l'apuntador del «dngettext» (error 372681)

### KIconThemes

- Permet mostrar les icones de totes les categories (error 216653)

### KInit

- Defineix les variables d'entorn des del KLaunchRequest en iniciar processos nous

### KIO

- Adaptat a un enregistrament més categoritzat
- S'ha esmenat la compilació de l'SDK de WinXP
- Permet sumes de verificació en majúscules a la pestanya de sumes de verificació (error 372518)
- No encongeix mai la darrera columna (=data) en el diàleg de fitxer (error 312747)
- Importa i actualitza els docbook del Kcontrol per al codi en els Kio des del mestre «kde-runtime»
- [OS X] Fer que la paperera del KDE usi la paperera de l'OS X
- SlaveBase: afegeix documentació quant a les repeticions dels esdeveniments i notificacions i dels mòduls del «kded»

### KNewStuff

- Afegeix una opció nova de gestió d'arxius («subdir») al «knsrc»
- Consumeix els senyals nous d'error (defineix els errors de tasques)
- Gestiona la desaparició estranya de fitxers quan s'acaben de crear
- Instal·la realment les capçaleres principals, amb majúscules «CamelCase»

### KNotification

- [KStatusNotifierItem] Desa/restaura la posició del giny durant l'ocultació/restauració de la seva finestra (error 356523)
- [KNotification] Permet anotar notificacions amb URL

### Framework del KPackage

- Manté la instal·lació de metadata.desktop (error 372594)
- Carrega manualment les metadades si es passa el camí absolut
- Soluciona una fallada potencial si el paquet no és compatible amb «appstream»
- Permet que el KPackage conegui el X-Plasma-RootPath
- Soluciona la generació del fitxer metadata.json

### KPty

- Més camins de cerca de l'«utempter» (inclòs /usr/lib/utempter/)
- Afegeix un camí de biblioteca per tal que es trobi l'executable de l'«utempter» a l'Ubuntu 16.10

### KTextEditor

- Evita avisos de les Qt quant a un mode de porta-retalls no admès al Mac
- Usa les definicions de sintaxi del KF5::SyntaxHighlighting

### KTextWidgets

- No substitueix les icones de la finestra com a resultat d'una cerca fallada

### KWayland

- [client] Soluciona una desreferència a un apuntador nul al ConfinedPointer i al LockedPointer
- [client] Instal·la pointerconstraints.h
- [servidor] Soluciona una regressió a SeatInterface::end/cancelPointerPinchGesture
- Implementació del protocol PointerConstraints
- [servidor] Redueix la sobrecàrrega dels «pointersForSurface»
- Retorna SurfaceInterface::size a l'espai global del compositor
- [eines/generador] Genera l'enumeració FooInterfaceVersion a la banda del servidor
- [eines/generador] Embolcalla els arguments de les sol·licituds «wl_fixed» en un «wl_fixed_from_double»
- [eines/generador] Genera la implementació de les peticions de la banda del client
- [eines/generador] Genera les factories de recurs de la banda del client
- [eines/generador] Genera crides de retorn i un oient a la banda del client
- [eines/generador] Passa això com un apuntador q a Client::Resource::Private
- [eines/generador] Genera Private::setup(wl_foo *arg) a la banda del client
- Implementació del protocol PointerGestures

### KWidgetsAddons

- Evita una fallada al Mac
- No substitueix les icones amb el resultat d'una cerca que falla
- KMessageWidget: esmena la disposició quan l'ajust de paraules està activat sense accions
- KCollapsibleGroupBox: no oculta els ginys, en canvi, substitueix la política de focus

### KWindowSystem

- [KWindowInfo] Afegeix «pid()» i «desktopFileName()»

### Icones de l'Oxygen

- Afegeix la icona d'«application-vnd.rar» (error 372461)

### Frameworks del Plasma

- Comprova la validesa de les metadades a «settingsFileChanged» (error 372651)
- No inverteix la disposició de la barra de pestanyes si és vertical
- Elimina «radialGradient4857» (error 372383)
- [AppletInterface] No treure mai el focus de «fullRepresentation» (error 372476)
- Esmena el prefix de l'ID de la icona SVG (error 369622)

### Solid

- winutils_p.h: Restaura la compatibilitat amb el WinXP SDK

### Sonnet

- Cerca també l'Hunspell-1.5

### Ressaltat de la sintaxi

- Normalitza els valors de l'atribut llicència de l'XML
- Sincronitza les definicions de sintaxi des del «ktexteditor»
- Soluciona la fusió de la regió de plegat

### Informació de seguretat

El codi publicat s'ha signat amb GPG usant la clau següent: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Empremta digital de la clau primària: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Podeu debatre i compartir idees quant a aquest llançament en la secció de comentaris en <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>l'article del Dot</a>.
