---
aliases:
- ../../kde-frameworks-5.35.0
date: 2017-06-10
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Attica

- Millora de les notificacions d'error

### BluezQt

- Passa una llista explícita d'arguments. Això evita que el QProcess intenti gestionar l'espai que contingui un camí mitjançant un intèrpret d'ordres
- Esmena els canvis de propietat que manquen immediatament després d'afegir un objecte (error 377405)

### Icones Brisa

- Actualitza el MIME «awk» atès que és un llenguatge de script (error 376211)

### Mòduls extres del CMake

- Restaura la prova d'ocultació i visibilitat amb l'Xcode 6.2
- ecm_qt_declare_logging_category: un guàrdia més únic d'inclusió per a les capçaleres
- Afegeix o millora els missatges «Generat. No editeu» i els fa coherents
- Afegeix el mòdul FindGperf nou
- Canvia el camí d'instal·lació predeterminat del «pkgconfig» al FreeBSD

### KActivitiesStats

- Soluciona «kactivities-stats» dins el nivell 3

### Eines de Doxygen del KDE

- No considera la paraula clau Q_REQUIRED_RESULT

### KAuth

- Verifica que qualsevol que ens cridi sigui realment qui diu que és

### KCodecs

- Genera la sortida del «gperf» en temps de construcció

### KCoreAddons

- Assegura un sembrat adequat per fil al KRandom
- No vigila els camins dels QRC (error 374075)

### KDBusAddons

- No inclou el PID en el camí de D-Bus al Flatpak

### KDeclarative

- Emet amb coherència el senyal MouseEventListener::pressed
- Evita una fuita de memòria a l'objecte MimeData (error 380270)

### Compatibilitat amb les KDELibs 4

- Gestiona els espais al camí a CMAKE_SOURCE_DIR

### KEmoticons

- Esmena: «Qt5::DBus» només s'usa de manera privada

### KFileMetaData

- Usa /usr/bin/env per localitzar el Python2

### KHTML

- Genera la sortida del «gperf» de «kentities» en temps de construcció
- Genera la sortida del «gperf» de «doctypes» en temps de construcció

### KI18n

- Amplia la «Guia del programador» amb notes quant a la influència de «setlocale()»

### KIO

- Soluciona un problema quan diversos elements de les aplicacions (p. ex. la vista de fitxers del Dolphin) esdevenen inaccessibles en una configuració multipantalla amb alta densitat de ppp (error 363548)
- [RenameDialog] Fer complir el format de text net
- Identifica els binaris PIE (application/x-sharedlib) com a fitxers executables (error 350018)
- Nucli: Exposa GETMNTINFO_USES_STATVFS a la capçalera de configuració
- PreviewJob: Omet els directoris remots. La vista prèvia costa massa (error 208625)
- PreviewJob: Neteja els fitxers «temp» buits quan falla «get()» (error 208625)
- Accelera els detalls de la pantalla de vista en arbre evitant redimensionaments excessius de les columnes

### KNewStuff

- Usa un «QNAM» únic (i una memòria cau de disc) per als treballs HTTP
- Memòria cau interna per a les dades del proveïdor a la inicialització

### KNotification

- Esmena els KSNI que no poden registrar serveis en el Flatpak
- Usa el nom de l'aplicació en lloc del PID en crear el servei de D-Bus del SNI

### KPeople

- No exporta símbols des de les biblioteques privades
- Esmena l'exportació de símbols al KF5PeopleWidgets i al KF5PeopleBackend
- Limita el nombre d'avisos al GCC

### Framework del KWallet

- Substitueix el «kwalletd4» després de finalitzar la migració
- Assenyala la finalització de l'agent de migració
- Inicia el temporitzador per a l'agent de migració només si és necessari
- Verifica la unicitat d'instància d'aplicació tan aviat com sigui possible

### KWayland

- Afegeix «requestToggleKeepAbove/below»
- Manté QIcon::fromTheme en el fil principal
- Elimina el PID de «changedSignal» a Client::PlasmaWindow
- Afegeix el PID al protocol de gestió de finestres del Plasma

### KWidgetsAddons

- KViewStateSerializer: Esmena una fallada quan la vista es destrueix abans que el serialitzador d'estats (error 353380)

### KWindowSystem

- Millora l'esmena quan NetRootInfoTestWM té el camí amb espais

### KXMLGUI

- Defineix la finestra principal com a pare dels menús emergents independents
- En construir jerarquies de menús, situa els menús pares en els seus contenidors

### Frameworks del Plasma

- Afegeix la icona de safata del VLC
- Plantilles dels plasmoides: usa la imatge que forma part del paquet (una altra vegada)
- Afegeix una plantilla per una miniaplicació QML del Plasma amb una extensió QML
- Torna a crear «plasmashellsurf» en exposar-ho, destrueix en ocultar-ho

### Ressaltat de la sintaxi

- Haskell: ressalta el «quasiquoter» «julius» usant les regles Normal##Javascript
- Haskell: activar el ressaltat «hamlet» també pel «quasiquoter» «shamlet»

### Informació de seguretat

El codi publicat s'ha signat amb GPG usant la clau següent: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Empremta digital de la clau primària: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Podeu debatre i compartir idees quant a aquest llançament en la secció de comentaris en <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>l'article del Dot</a>.
