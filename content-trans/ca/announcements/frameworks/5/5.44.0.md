---
aliases:
- ../../kde-frameworks-5.44.0
date: 2018-03-10
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Balooctl: elimina l'opció «checkDb» (error 380465)
- indexerconfig: descriu diverses funcions
- indexerconfig: exposa la funció «canBeSearched» (error 388656)
- Balooctl monitor: espera la interfície de D-Bus
- fileindexerconfig: presenta «canBeSearched()» (error 388656)

### Icones Brisa

- Elimina «view-media-playlist» de les icones de preferències
- Afegeix la icona «media-album-cover» de 24px
- Afegeix la implementació del Babe QML (22px)
- Actualitza les icones «handle-» per al Kirigami
- Afegeix icones de suports de 64px per l'Elisa

### Mòduls extres del CMake

- Defineix **ANDROID_API**
- Esmena el nom de l'ordre «readelf» en x86
- Cadena d'eines d'Android: afegeix la variable ANDROID_COMPILER_PREFIX, esmena el camí de les inclusions per objectius x86, amplia el camí de cerca de les dependències NDK

### Eines de Doxygen del KDE

- Surt amb error si el directori de sortida no està buit (error 390904)

### KConfig

- Estalvia diverses assignacions de memòria utilitzant l'API correcta
- Exporta «kconf_update» amb les eines

### KConfigWidgets

- Millora KLanguageButton::insertLanguage quan no es passa cap nom
- Afegeix icones per a les KStandardActions Desselecciona i Substitueix

### KCoreAddons

- Neteja «m_inotify_wd_to_entry» abans d'invalidar els apuntadors Entry (error 390214)
- kcoreaddons_add_plugin: elimina sense efecte OBJECT_DEPENDS al fitxer «json»
- Ajuda a l'«automoc» a trobar els fitxers JSON de metadades referenciats al codi
- kcoreaddons_desktop_to_json: anota el fitxer generat al registre de construcció
- Actualitza «shared-mime-info» a 1.3
- Presenta K_PLUGIN_CLASS_WITH_JSON

### KDeclarative

- Esmena la fallada de construcció a armhf/aarch64
- Mata QmlObjectIncubationController
- Desconnecta «render()» en canviar de finestra (error 343576)

### KHolidays

- L'Allen Winter ara és oficialment el nou mantenidor del KHolidays

### KI18n

- API dox: afegeix una nota quant a invocar «setApplicationDomain» després de la creació de QApp

### KIconThemes

- [KIconLoader] Té en compte el «devicePixelRatio» per a les superposicions

### KIO

- No assumeix la disposició de les estructures «msghdr» i «iovec» (error 391367)
- Esmena la selecció de protocol al KUrlNavigator
- Canvia «qSort» per «std::sort»
- [KUrlNavigatorPlacesSelector] Usa KFilePlacesModel::convertedUrl
- [Drop Job] Crea el fitxer de paperera apropiat en enllaçar
- Soluciona una activació no intencionada d'element de menú del fil d'Ariadna (error 380287)
- [KFileWidget] Oculta el marc i la capçalera dels llocs
- [KUrlNavigatorPlacesSelector] Posa les categories en submenús (error 389635)
- Fa ús de la capçalera estàndard d'ajuda de prova del KIO
- Afegeix Ctrl+H a la llista de dreceres per «mostrar/ocultar fitxers ocults» (error 390527)
- Afegeix la implementació semàntica per moure al KIO::UDSEntry
- Esmena el problema «drecera ambigua» introduït al D10314
- Omple el quadre de missatge «No s'ha pogut trobar l'executable» a una lambda en la cua (error 385942)
- Millora la usabilitat del diàleg «Obre amb» afegint una opció per filtrar l'arbre d'aplicacions
- [KNewFileMenu] KDirNotify::emitFilesAdded després de «storedPut» (error 388887)
- Esmena una declaració en cancel·lar el diàleg «rebuild-ksycoca» (error 389595)
- Soluciona l'error núm. 382437 «Una regressió al kdialog provoca una extensió errònia de fitxer» (error 382437)
- Inici més ràpid de «simplejob»
- Repara el copiatge del fitxer a la VFAT sense avisos
- kio_file: Omet la gestió d'error dels permisos inicials durant la còpia de fitxers
- Permet que la semàntica de moure es generi al KFileItem. L'operador existent de constructor de còpia, destructor i assignador de còpia ara també el genera el compilador
- No fer «stat(/etc/localtime)» entre «read()» i «write()» en copiar fitxers (error 384561)
- Remot: no crea entrades amb noms buits
- Afegeix la funcionalitat «supportedSchemes»
- Usa F11 com a drecera per commutar la vista prèvia lateral
- [KFilePlacesModel] Agrupa les comparticions per xarxa sota la categoria «Remot»

### Kirigami

- Mostra el botó d'eina com a marcat quan es mostra el menú
- Indicadors de desplaçament no interactius en el mòbil
- Esmena els submenús de les accions
- Fa possible usar la QQC2.Action
- Fa possible admetre grups d'accions exclusius (error 391144)
- Mostra el text dels botons d'eina d'acció de pàgina
- Fa possible que les accions mostrin submenús
- No té una posició específica de component en el seu pare
- No activa les accions de SwipeListItem a menys que s'exposin
- Afegeix una verificació «isNull()» abans d'establir si el QIcon és una màscara
- Afegeix FormLayout.qml a «kirigami.qrc»
- Esmena els colors de «swipelistitem»
- Comportament millorat per capçaleres i peus de pàgina
- Millora el farciment esquerre de ToolBarApplicationHeader i el comportament d'elisió
- S'assegura que els botons de navegació no van sota l'acció
- Implementació de les propietats capçalera i peu de pàgina a «overlaysheet»
- Elimina un farciment inferior innecessari a OverlaySheets (error 390032)
- Poleix l'aparença de ToolBarApplicationHeader
- Mostra un botó de tancament a l'escriptori (error 387815)
- No és possible tancar el full amb la roda del ratolí
- Només multiplica la mida de la icona si les Qt encara no ho han fet (error 390076)
- Té en compte el peu de pàgina global per gestionar la posició
- Compressió d'esdeveniment de creació i destrucció de barres de desplaçament
- ScrollView: Fa pública la política de la barra de desplaçament i la soluciona

### KNewStuff

- Afegeix Vokoscreen a KMoreTools i l'afegeix a l'agrupació «screenrecorder»

### KNotification

- Usa QWidget per veure si la finestra és visible

### Framework del KPackage

- Ajuda a l'«automoc» a trobar els fitxers JSON de metadades referenciats al codi

### KParts

- Neteja codi antic inaccessible

### KRunner

- Actualitza la plantilla del connector del «krunner»

### KTextEditor

- Afegeix icones per l'exportació de document, eliminació d'adreça d'interès i el format de text en majúscules, minúscules i primera lletra majúscula del KTextEditor

### KWayland

- Implementa l'alliberament de la sortida del «client-freed»
- [server] Gestiona la situació adequadament quan el DataSource d'un arrossegament es destrueix (error 389221)
- [server] No falla quan una subsuperfície esdevé publicada i la seva superfície pare es destrueix (error 389231)

### KXMLGUI

- Reinicia internament QLocale quan hi ha un idioma personalitzat d'aplicació
- No permet configurar accions d'un separador via el menú contextual
- No mostra el menú contextual si el clic dret és fora (error 373653)
- Millora KSwitchLanguageDialogPrivate::fillApplicationLanguages

### Icones de l'Oxygen

- Afegeix la icona de l'Artikulate (error 317527)
- Afegeix la icona «folder-games» (error 318993)
- Soluciona la icona incorrecta de 48px del «calc.template» (error 299504)
- Afegeix les icones «media-playlist-repeat» i «shuffle» (error 339666)
- Oxygen: afegeix icones d'etiquetes com al Brisa (error 332210)
- Enllaça «emblem-mount» a «media-mount» (error 373654)
- Afegeix les icones de xarxa que hi ha disponibles a les icones del Brisa (error 374673)
- Sincronitza l'Oxygen amb les icones del Brisa afegint icones per al plasmoide de l'àudio
- Afegeix «edit-select-none» a l'Oxygen per al Krusader (error 388691)
- Afegeix la icona «rating-unrated» (error 339863)

### Frameworks del Plasma

- Usa el valor nou de «largeSpacing» al Kirigami
- Redueix la visibilitat del text de substitució del PC3 TextField
- No fa transparents els títols al 20%
- [PackageUrlInterceptor] No reescriu «en línia»
- No fa les capçaleres transparents al 20%, per coincidir amb el Kirigami
- No posa la representació completa al missatge emergent si no està reduït
- Ajuda a l'«automoc» a trobar els fitxers JSON de metadades referenciats al codi
- [AppletQuickItem] Precarrega l'expansor de la miniaplicació només si encara no està expandida
- Altres microoptimitzacions de precàrrega
- Estableix el valor predeterminat d'IconItem a «smooth=true»
- Precarrega també l'expansor (el diàleg)
- [AppletQuickItem] Esmena el valor predeterminat de la política de precàrrega si no s'ha definit cap variable d'entorn
- Soluciona l'aparença RTL de les ComboBox (error https://bugreports.qt.io/browse/QTBUG-66446)
- Intenta precarregar determinades miniaplicacions d'una manera intel·ligent
- [Icon Item] Estableix el filtratge a la textura FadingNode
- Inicialitza «m_actualGroup» a NormalColorGroup
- S'assegura que les instàncies de FrameSvg i Svg tenen el «devicePixelRatio» correcte

### Prison

- Actualitza els enllaços a les dependències, i marca l'Android com a admès oficialment
- Fa opcional la dependència de DMTX
- Afegeix la implementació del QML al Prison
- També defineix la mida mínima dels codis de barres 1D

### Purpose

- Esmena el nivell, s'ajusta per al KIO

### QQC2StyleBridge

- Esmena un error de sintaxi al «commit» previ, detectat en llançar el Ruqola
- Mostra un botó d'opció en mostrar un control exclusiu (error 391144)
- Implementa MenuBarItem
- Implementa DelayButton
- Component nou: botó rodó
- Té en compte la posició de la barra d'eines
- Admet colors a les icones dels botons
- Implementa «--reverse»
- Icones completament funcionals al Menú
- Ombres coherents amb l'estil nou del Brisa
- Diversos QStyles semblen que no retornen aquí els «pixelmetrics» coherents
- Primera implementació de les icones rugoses
- No embolicar-se amb la roda del ratolí

### Solid

- Soluciona una fuita i una verificació incorrecta d'apuntador nul al DADictionary
- [UDisks] Soluciona una regressió de muntatge automàtic (error 389479)
- [UDisksDeviceBackend] Evita la cerca múltiple
- Dorsal del Mac/IOKit: implementació per a unitats, discs i volums

### Sonnet

- Usa Locale::name() en lloc de Locale::bcp47Name()
- Cerca la construcció del «libhunspell» al «msvc»

### Ressaltat de la sintaxi

- Implementació bàsica per blocs de codi delimitats de PHP i Python al Markdown
- Permet que WordDetect no distingeixi minúscules i majúscules
- Ressaltat de l'Scheme: elimina els colors en el codi font
- Afegeix el ressaltat de sintaxi per les polítiques CL del SELinux i els contexts de fitxers
- Afegeix l'extensió de fitxer «ctp» al ressaltat de sintaxi del PHP
- Yacc/Bison: esmena el símbol $ i actualitza la sintaxi per al Bison
- awk.xml: afegeix les paraules clau de l'ampliació «gawk» (error 389590)
- Afegeix APKBUILD per a ser ressaltat com a fitxer Bash
- Reverteix «Afegeix APKBUILD per a ser ressaltat com a fitxer Bash»
- Afegeix APKBUILD per a ser ressaltat com a fitxer Bash

### Informació de seguretat

El codi publicat s'ha signat amb GPG usant la clau següent: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Empremta digital de la clau primària: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Podeu debatre i compartir idees quant a aquest llançament en la secció de comentaris en <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>l'article del Dot</a>.
