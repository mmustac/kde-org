---
aliases:
- ../../kde-frameworks-5.23.0
date: 2016-06-13
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Attica

- Fer possible indicar realment els proveïdors de l'URL que s'ha introduït
- Proporcionar «helpers» de QDebug per a diverses classes de l'Attica
- Solucionar la redirecció dels URL absoluts (error 354748)

### Baloo

- Solucionar l'ús d'espais en el «kioslave» «tags» (error 349118)

### Icones Brisa

- Afegir una opció del CMake per construir un recurs Qt binari fora del directori «icons»
- Moltes icones noves i actualitzades
- Actualitzar la icona de xarxa desconnectada per fer més gran la diferència amb la connectada (error 353369)
- Actualitzar la icona de muntar i desmuntar (error 358925)
- Afegir diversos avatars des de plasma-desktop/kcms/useraccount/pics/sources
- Eliminar la icona del Chromium, ja que la icona predeterminada encaixa bé (error 363595)
- Fer més clares les icones del Konsole (error 355697)
- Afegir les icones de correu per al Thunderbird (error 357334)
- Afegir la icona de la clau pública (error 361366)
- Eliminar les icones de «process-working-kde», ja que cal usar les icones del Konqueror (error 360304)
- Actualitzar les icones del Krusader (error 359863)
- Reanomenar les icones del micròfon d'acord amb D1291 (error D1291)
- Afegir diverses icones del tipus MIME «script» (error 363040)
- Afegir la funcionalitat d'activar/desactivar el teclat virtual i el ratolí tàctil per OSD

### Integració del marc de treball

- Eliminar dependències no usades i gestió de les traduccions

### KActivities

- Afegir la propietat «runningActivities» en el «Consumer»

### Eines de Doxygen del KDE

- Gran remodelació del generador de la documentació de l'API

### KCMUtils

- Usar el «QQuickWidget» per als KCM escrits en QML (error 359124)

### KConfig

- Evitar ometre la comprovació del KAuthorized

### KConfigWidgets

- Permetre l'ús del nou estil de sintaxi «connect» amb el KStandardAction::create()

### KCoreAddons

- Imprimir el connector que falla en notificar un avís de «cast»
- [kshareddatacache] Solucionar un ús no vàlid del &amp; per evitar lectures no alineades
- Kdelibs4ConfigMigrator: Ometre tornar a analitzar si no s'ha migrat res
- krandom: Afegir una prova d'ús per detectar l'error 362161 (fallada en el sembrat propi)

### KCrash

- Comprovar la mida del camí del sòcol de domini UNIX abans de copiar-hi res

### KDeclarative

- Implementar l'estat seleccionat
- La importació del KCMShell ara es pot utilitzar per a consultar si es permet obrir realment un KCM

### Compatibilitat amb les KDELibs 4

- Avís si no s'ha implementat KDateTimeParser::parseDateUnicode
- K4TimeZoneWidget: Corregir el camí a les imatges de les banderes

### KDocTools

- Afegir les entitats d'ús comú de tecles a en/user.entities
- Actualitzar la plantilla «man-docbook»
- Actualitzar la plantilla «book» + la plantilla «man» + afegir la plantilla «article»
- Només invocar «kdoctools_create_handbook» per «index.docbook» (error 357428)

### KEmoticons

- Afegir la implementació dels «emojis» a les icones del KEmoticon + Emoji One
- Afegir la implementació per a mides personalitzades de les emoticones

### KHTML

- Solucionar una fuita de memòria potencial informada pel Coverity i simplificar el codi
- El nombre de capes està determinat pel nombre de valors separats per comes en la propietat «background-image»
- Solucionar l'anàlisi de «background-position» en una declaració abreviada
- No crear cap «fontFace» nova si no hi ha un origen vàlid

### KIconThemes

- Fer que KIconThemes no depengui de l'Oxygen (error 360664)
- Concepte d'estat seleccionat per a les icones
- Usar els colors del sistema per a les icones monocromes

### KInit

- Solucionar un error de concurrència en què el fitxer que conté la galeta de l'X11 té permisos erronis durant un període breu
- Solucionar els permisos de /tmp/xauth-xxx-_y

### KIO

- Mostrar un missatge d'error més clar quan KRun(URL) rep un URL sense esquema (error 363337)
- Afegir KProtocolInfo::archiveMimetypes()
- Usa el mode d'icona seleccionat en el diàleg d'obertura de fitxers de la barra lateral
- kshorturifilter: Solucionar una regressió amb un «mailto:» no anteposat quan no hi ha cap programa de correu instal·lat

### KJobWidgets

- Definir l'indicador «dialog» correcte en el diàleg del giny en curs

### KNewStuff

- No inicialitzar KNS3::DownloadManager amb les categories incorrectes
- Amplia l'API pública de KNS3::Entry

### KNotification

- Usar QUrl::fromUserInput per construir un URL de so (error 337276)

### KNotifyConfig

- Usar QUrl::fromUserInput per construir un URL de so (error 337276)

### KService

- Solucionar les aplicacions associades pel tipus MIME amb caràcters amb majúscules
- Posar en minúscules la clau de cerca del tipus MIME, fer-la que no distingeixi majúscules i minúscules
- Solucionar les notificacions de «ksycoca» quan la BD encara no existeix

### KTextEditor

- Esmenar la codificació per omissió a UTF-8 (error 362604)
- Solucionar la configuració del color de l'estil predeterminat «Error»
- Cerca i substitució: Solucionar el color de fons de la substitució (regressió introduïda a la v5.22) (error 363441)
- Esquema nou de color «Brisa fosca», veure https://kate-editor.org/?post=3745
- KateUndoManager::setUndoRedoCursorOfLastGroup(): Passar el cursor com una referència constant
- sql-postgresql.xml: Millorar el ressaltat de sintaxi ignorant els cossos de funció multilínia
- Afegir ressaltat de sintaxi per l'Elixir i el Kotlin
- Ressaltat de la sintaxi VHDL en el «ktexteditor»:Afegir la implementació per les funcions dins les sentències d'arquitectura
- vimode: No fallar quan s'indica un interval per una ordre inexistent (error 360418)
- Eliminar adequadament els caràcters compostos en usar les configuracions regionals de l'Índic

### KUnitConversion

- Solucionar la baixada dels tipus de canvi de les divises (error 345750)

### Framework del KWallet

- Migració del KWalletd: Solucionar la gestió d'errors, que atura la migració en cada inici del sistema.

### KWayland

- [client] No comprovar la versió del recurs per al «PlasmaWindow»
- Presentar un esdeveniment d'estat inicial en el protocol Plasma Window
- [servidor] Activar un error si una sol·licitud transitòria intenta emparentar-se a si mateixa
- [servidor] Gestionar adequadament el cas que una PlasmaWindow no estigui mapada abans que el client la vinculi
- [servidor] Gestionar adequadament el destructor a SlideInterface
- Afegir la implementació d'esdeveniments tàctils en el protocol i la interfície «fakeinput»
- [servidor] Estandarditzar la gestió de sol·licituds del destructor per als «Resources»
- Implementar les interfícies «wl_text_input» i «zwp_text_input_v2»
- [servidor] Evitar la doble supressió de recursos de crida de retorn a SurfaceInterface
- [servidor] Afegir una comprovació de punter nul de recurs a ShellSurfaceInterface
- [server] Comparar ClientConnection en lloc de «wl_client» a SeatInterface
- [servidor] Millorar la gestió quan els clients desconnecten
- server/plasmawindowmanagement_interface.cpp - solucionar un avís -Wreorder
- [client] Afegir un apuntador de context a les connexions en el PlasmaWindowModel
- Moltes esmenes relacionades amb la destrucció

### KWidgetsAddons

- Usar l'efecte d'icona seleccionada per la pàgina KPageView actual

### KWindowSystem

- [Plataforma XCB] Respectar la mida d'icona sol·licitada (error 362324)

### KXMLGUI

- El clic dret a la barra de menús d'una aplicació ja no permetrà deixar de banda

### NetworkManagerQt

- Revertir «Eliminar la implementació del WiMAX en el NM 1.2.0+», ja que trenca l'ABI

### Icones de l'Oxygen

- Sincronitzar les icones meteorològiques amb el Brisa
- Afegir icones d'actualització

### Frameworks del Plasma

- Afegir la implementació de la safata del sistema del Cantata (error 363784)
- Estat seleccionat per a «Plasma::Svg» i «IconItem»
- DaysModel: Reiniciar m_agendaNeedsUpdate quan el connector envia esdeveniments nous
- Actualitzar les icones d'àudio i xarxa per obtenir un millor contrast (error 356082)
- Fer obsolet «downloadPath(const QString &amp;file)» i substituir per «downloadPath()»
- [miniatures d'icona] Sol·licitud per la mida d'icona preferida (error 362324)
- Els Plasmoides ara poden indicar si els ginys estan bloquejats per l'usuari o per restriccions de l'administrador del sistema
- [ContainmentInterface] No intentar emergir un QMenu buit
- Ús del SAX com a substitució del full d'estil Plasma::Svg
- [DialogShadows] Accés a la memòria cau a QX11Info::display()
- Restaurar el tema d'icones Plasma Air del KDE 4
- Recarregar l'esquema de colors seleccionat en canviar els colors

Podeu debatre i compartir idees quant a aquest llançament en la secció de comentaris en <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>l'article del Dot</a>.
