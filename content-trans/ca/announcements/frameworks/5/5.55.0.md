---
aliases:
- ../../kde-frameworks-5.55.0
date: 2019-02-09
layout: framework
libCount: 70
---
### Baloo

- [tags_kio] Desactiva l'accés amb un URL amb barra doble, és a dir, «tags://» (error 400594)
- Instancia QApplication abans de KCrash/KCatalog
- Ignora tots els senyals «deviceAdded» de no emmagatzematge des del Solid
- Usa el K_PLUGIN_CLASS_WITH_JSON, que és més fi
- Elimina les verificacions de les Qt 5.10 ara que es requereixen com a versió mínima

### Icones Brisa

- Afegeix la icona del KCM de Cursors
- Afegeix i reanomena diverses icones del YaST i enllaços simbòlics
- Millora la icona Notification Bell usant el disseny del KAlarm (error 400570)
- Afegeix «yast-upgrade»
- Millores les icones «weather-storm-*» (error 403830)
- Afegeix els enllaços simbòlics «font-otf», tal com els enllaços simbòlics «font-ttf»
- Afegeix les icones «edit-delete-shred» adequades (error 109241)
- Afegeix les icones «trim margins» i «trim to selection» (error 401489)
- Elimina els enllaços simbòlics «edit-delete-shred» per preparar el reemplaçament per les seves icones reals
- Reanomena la icona Activitats del KCM
- Afegeix la icona Activitats del KCM
- Afegeix la icona Escriptoris virtuals del KCM
- Afegeix icones per als KCM de Pantalla tàctil i Vores de la pantalla
- Esmena els noms d'icones relacionades amb les preferències de la compartició de fitxers
- Afegeix la icona «preferences-desktop-effects»
- Afegeix la icona de preferències de tema del Plasma
- Millora el contrast de «preferences-system-time» (error 390800)
- Afegeix la icona nova «preferences-desktop-theme-global»
- Afegeix una icona d'eines
- La icona «document-new» segueix el «ColorScheme-Text»
- Afegeix la icona «preferences-system-splash» per al KCM de Pantalla de presentació
- Inclou «applets/22»
- Afegeix les icones del tipus MIME Kotlin (.kt)
- Millora la icona «preferences-desktop-cryptography»
- Omple el cadenat a «preferences-desktop-user-password»
- Omple coherentment el cadenat a la icona «encrypted»
- Usa una icona del Kile que és similar a l'original

### Mòduls extres del CMake

- FindGperf: a «ecm_gperf_generate» estableix SKIP_AUTOMOC per al fitxer generat
- Mou «-Wsuggest-override» «-Wlogical-op» als paràmetres normals del compilador
- Esmena la generació de vinculacions del Python per a les classes amb constructors de còpia suprimits
- Esmena la generació de mòduls del QMake per a les Qt 5.12.1
- Usa més el «https» als enllaços
- API dox: afegeix entrades que manquen per diversos «find-modules» i «modules»
- FindGperf: millora la «dox» de l'API: exemple d'ús de marcador
- ECMGenerateQmlTypes: esmena la «dox» de l'API: el títol necessita més marcador «---»
- ECMQMLModules: esmena la «dox» de l'API: el títol ha de coincidir amb el nom del mòdul, afegeix un «Since» que mancava
- FindInotify: esmena l'etiqueta «.rst» de la «dox» de l'API, afegeix un «Since» que mancava

### KActivities

- Esmena per a macOS

### KArchive

- Estalvia dues crides a KFilterDev::compressionTypeForMimeType

### KAuth

- Elimina el suport per passar QVariants de l'IGU als ajudants del KAuth

### KBookmarks

- Construeix sense el D-Bus a l'Android
- «Const'ify»

### KCMUtils

- [kcmutils] Afegeix el·lipsis de les etiquetes de cerca al «KPluginSelector»

### KCodecs

- nsSJISProber::HandleData: No falla si «aLen» és 0

### KConfig

- kconfig_compiler: elimina l'operador d'assignació i el constructor per còpia

### KConfigWidgets

- Construeix sense el KAuth ni el D-Bus a l'Android
- Afegeix KLanguageName

### KCrash

- Comentari perquè es requereix el canvi del «ptracer»
- [KCrash] Estableix un sòcol per permetre el canvi del «ptracer»

### KDeclarative

- [KCM Controls GridView] Afegeix animació d'eliminació

### Compatibilitat amb les KDELibs 4

- Esmena les banderes de diversos països per usar tot el mapa de píxels

### KDESU

- Gestiona les contrasenyes incorrectes en usar «sudo» que demani una altra contrasenya (error 389049)

### KFileMetaData

- exiv2extractor: afegeix la implementació per «bmp», «gif», «webp», «tga»
- Esmena una prova que falla amb les dades GPS de l'EXIV
- Prova les dades GPS buides i a zero
- Afegeix la implementació per a més tipus MIME al «taglibwriter»

### KHolidays

- holidays/plan2/holiday_ua_uk - actualitzat per al 2019

### KHTML

- Afegeix metadades JSON a l'executable del connector «khtmlpart»

### KIconThemes

- Construeix sense el D-Bus a l'Android

### KImageFormats

- xcf: esmena l'opacitat fora de límits
- Descomenta les inclusions del «qdebug»
- tga: esmena l'ús de valors no inicialitzats en fitxers trencats
- L'opacitat màxima és 255
- xcf: esmena l'asserció en fitxers amb dues PROP_COLORMAP
- ras: esmena l'asserció perquè la ColorMapLength és massa gran
- pcx: esmena una fallada en fitxers difusos
- xcf: implementa amb robustesa per si PROP_APPLY_MASK no és al fitxer
- xcf: loadHierarchy: obeeix el «layer.type» i no el «bpp»
- tga: no accepta més de 8 bits a l'alfa
- ras: retorna fals si ha fallat en assignar la imatge
- rgb: soluciona un desbordament d'enter en fitxers difusos
- rgb: soluciona un desbordament de memòria intermèdia en monticles en fitxers difusos
- psd: esmena una fallada en fitxers difusos
- xcf: inicialitza el desplaçament x/y
- rgb: esmena una fallada en fitxers difusos
- pcx: esmena una fallada en imatges difuses
- rgb: esmena una fallada en fitxers difusos
- xcf: inicialitza el mode de capa
- xcf: inicialitza l'opacitat de capa
- xcf: estableix la memòria intermèdia a 0 si llegeix menys dades de les esperades
- bzero -&gt; memset
- Esmena diverses lectures i escriptures OOB a «kimg_tga» i «kimg_xcf»
- pic: redimensiona l'ID de la capçalera si no es llegeixen 4 bytes com s'esperaria
- xcf: s'aplica «bzero» a la memòria intermèdia si es llegeixen menys dades de les esperades
- xcf: només crida «setDotsPerMeterX/Y» si es troba «PROP_RESOLUTION»
- xcf: inicialitza «num_colors»
- xcf: inicialitza la propietat de capa visible
- xcf: no fa «cast» d'enter a enumeració si no pot admetre aquest valor enter
- xcf: evita desbordar un enter a la crida de «setDotsPerMeterX/Y»

### KInit

- KLauncher: gestiona la sortida dels processos sense error (error 389678)

### KIO

- Millora els controls de teclat del giny de sumes de verificació
- Afegeix una funció d'ajudant per desactivar redireccions (útil per al kde-open)
- Reverteix «Refactoritza SlaveInterface::calcSpeed» (error 402665)
- No estableix la política CMP0028 del CMake a antiga. No hi ha objectius amb :: excepte que siguin importats.
- [kio] Afegeix el·lipsis de les etiquetes de cerca a la secció Cookies
- [KNewFileMenu] No emet «fileCreated» en crear un directori (error 403100)
- Usa (i suggereix l'ús) el més fi K_PLUGIN_CLASS_WITH_JSON
- Evita blocar «kio_http_cache_cleaner» i assegura la sortida amb la sessió (error 367575)
- Soluciona una prova del «knewfilemenu» que fallava i el motiu subjacent de la fallada

### Kirigami

- Eleva només els botons acolorits (error 403807)
- Esmena l'alçada
- La mateixa política de mida de marges que els altres elements de llista
- Operadors ===
- Implementa el concepte d'elements ampliables
- No neteja en reemplaçar totes les pàgines
- El farciment horitzontal és el doble que el vertical
- Té en compte el farciment per calcular els consells de mida
- [kirigami] No usa els estils fins de tipus de lletra per a les capçaleres (2/3) (error 402730)
- Soluciona la disposició de l'AboutPage en els dispositius més petits
- Fa «flickable» el «stopatBounds» del fil d'Ariadna

### KItemViews

- [kitemviews] Canvia la cerca del Comportament d'Escriptori/Activitats per alinear-se amb altres etiquetes de cerca

### KJS

- Defineix SKIP_AUTOMOC per a diversos fitxers generats, per tractar el CMP0071

### KNewStuff

- Esmena la semàntica de «ghns_exclude» (error 402888)

### KNotification

- Soluciona una fuita de memòria en passar les dades d'icona al Java
- Elimina la dependència de la biblioteca per suportar l'AndroidX
- Afegeix la implementació del canal de notificacions de l'Android
- Fa que les notificacions funcionin a l'Android amb el nivell &lt; 23 de l'API
- Mou les comprovacions de nivell de l'API de l'Android al temps d'execució
- Elimina una declaració «forward» sense ús
- Reconstrueix AAR en canvis de les fonts del Java
- Construeix la banda del Java amb el Gradle, com a AAR en lloc de JAR
- No confia en la integració de l'espai de treball del Plasma a l'Android
- Cerca la configuració d'esdeveniments de notificació als recursos «qrc»

### Framework del KPackage

- Fa que funcionin les traduccions

### KPty

- Soluciona el desfasament «struct/class»

### KRunner

- Elimina l'ús explícit d'«ECM_KDE_MODULE_DIR», forma part d'«ECM_MODULE_PATH»

### KService

- Construeix sense el D-Bus a l'Android
- Suggereix l'ús de K_PLUGIN_CLASS_WITH_JSON

### KTextEditor

- Les Qt 5.12.0 tenen problemes amb la implementació de les expressions regulars al QJSEngine, els sagnadors poden comportar-se incorrectament, les Qt 5.12.1 ho solucionaran
- KateSpellCheckDialog: Elimina l'acció «Spellcheck Selection»
- Actualitza la biblioteca «underscore.js» del JavaScript a la versió 1.9.1
- Soluciona l'error 403422: Torna a permetre canviar la mida del marcador (error 403422)
- SearchBar: Afegeix el botó Cancel·la per aturar les tasques d'execució llarga (error 244424)
- Elimina l'ús explícit d'«ECM_KDE_MODULE_DIR», forma part d'«ECM_MODULE_PATH»
- Revisa la KateGotoBar
- ViewInternal: Esmena «Ves al parèntesi parell» en mode substitució (error 402594)
- Usa HTTPS, si està disponible, als enllaços visibles pels usuaris
- Revisa la KateStatusBar
- ViewConfig: Afegeix una opció per enganxar a la posició del cursor amb el ratolí (error 363492)
- Usa el K_PLUGIN_CLASS_WITH_JSON, que és més fi

### KWayland

- [server] Genera els ID tàctils correctes
- Fa que es compleixin les especificacions del XdgTest
- Afegeix una opció per usar «wl_display_add_socket_auto»
- [server] Envia «org_kde_plasma_virtual_desktop_management.rows» inicial
- Afegeix informació de les files al protocol d'escriptori virtual del Plasma
- [client] Ajusta «wl_shell_surface_set_{class,title}»
- Vigila la supressió de recursos a OuptutConfiguration::sendApplied

### KWidgetsAddons

- [KWidgetsAddons] No usa els estils fins de tipus de lletra per a les capçaleres (3/3) (error 402730)

### KXMLGUI

- Construeix sense el D-Bus a l'Android
- Fa menys invasiu KCheckAccelerators per a les aplicacions que no enllacen directament a KXmlGui

### ModemManagerQt

- Corregeix la implementació de l'operador &gt;&gt; al QVariantMapList

### Frameworks del Plasma

- [Wallpaper templates] Afegeix una entrada Comment= que manca al fitxer «desktop»
- Comparteix les instàncies Plasma::Theme entre ColorScope múltiples
- Fa més lògiques i apropiades visualment les ombres del SVG del rellotge (error 396612)
- [frameworks] No usa els estils fins de tipus de lletra per a les capçaleres (1/3) (error 402730)
- [Dialog] No altera la visibilitat del «mainItem»
- Reinicia el «parentItem» en canvis de «mainItem»

### Purpose

- Usa el K_PLUGIN_CLASS_WITH_JSON, que és més fi

### QQC2StyleBridge

- Corregeix la mida inicial del quadre combinat (error 403736)
- Defineix els quadres combinats emergents com a modals (error 403403)
- Reverteix parcialment 4f00b0cabc1230fdf
- Ajusta les paraules dels consells d'eina llargs (error 396385)
- ComboBox: soluciona el delegat predeterminat
- Pas fals del ratolí per sobre mentre el quadre combinat està obert
- Defineix QStyleOptionState == On del CombooBox en lloc de Sunken per coincidir amb els «qwidgets» (error 403153)
- Esmena el ComboBox
- Dibuixa sempre el consell d'eina a dalt de tot de la resta
- Permet un consell d'eina a MouseArea
- No força la visualització de text a ToolButton

### Solid

- Construeix sense el D-Bus a l'Android

### Sonnet

- No crida aquest codi si només hi ha espai

### Ressaltat de la sintaxi

- Esmena el final de la regió de plegat a les regles amb «lookAhead=true»
- AsciiDoc: Soluciona el ressaltat de la directiva «include»
- Afegeix la implementació d'AsciiDoc
- Esmena un error que causava un bucle infinit en ressaltar els fitxers Kconfig
- Comprova els commutadors de context interminables
- Ruby: soluciona la RegExp després de «: » i soluciona/millora la detecció de HEREDOC (error 358273)
- Haskell: Make = un símbol especial

### Informació de seguretat

El codi publicat s'ha signat amb GPG usant la clau següent: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Empremta digital de la clau primària: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB
