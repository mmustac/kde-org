---
aliases:
- ../../kde-frameworks-5.51.0
date: 2018-10-15
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Afegeix crides a KIO::UDSEntry::reserve als Ioslaves de línies de temps/etiquetes
- [balooctl] Purga la línia «S'està indexant &lt;fitxer&gt;» en memòria intermèdia quan s'inicia la indexació
- [FileContentIndexer] Connecta el senyal de finalitzat des del procés extractor
- [PositionCodec] Evita fallar en cas de dades corrompudes (error 367480)
- Esmena una constant «char» no vàlida
- [Balooctl] Elimina la verificació de directori superior (error 396535)
- Permet eliminar carpetes no existents des de les llistes d'inclusió i d'exclusió (error 375370)
- Usa «String» per emmagatzemar UDS_USER i UDS_GROUP de tipus «String» (error 398867)
- [tags_kio] Esmena els parèntesis. D'alguna manera això ho ha detectat el verificador de codi
- Exclou els fitxers de genomes de la indexació

### BluezQt

- Implementa les API Media i MediaEndpoint

### Icones Brisa

- Soluciona un «stack-use-after-scope» detectat per l'ASAN a CI
- Esmena les icones monocromes que els hi manca els fulls d'estil
- Canvia el «drive-harddisk» a un estil més adaptable
- Afegeix les icones «firewall-config» i «firewall-applet»
- Fa visible el cadenat a la icona «plasmavault» amb el Brisa fosca
- Afegeix el símbol més a «document-new.svg» (error 398850)
- Proporciona icones per escalat 2x

### Mòduls extres del CMake

- Compila les vinculacions de Python amb els mateixos indicadors SIP usats per a PyQt
- Android: Permet passar un camí relatiu com a directori de l'aplicació
- Android: Ofereix adequadament una alternativa a aplicacions que no tenen cap manifest
- Android: Assegura que es carreguen les traduccions Qm
- Esmena les construccions amb l'Android usant el CMake 3.12.1
- l10n: Esmena els dígits coincidents al nom de repositori
- Afegeix QT_NO_NARROWING_CONVERSIONS_IN_CONNECT com a indicador de compilació predeterminat
- Vinculacions: gestió correcta dels codis fonts que contenen UTF-8
- Itera realment sobre CF_GENERATED, en lloc de comprovar l'element 0 tota l'estona

### KActivities

- Esmena referències penjades («dangling») amb «auto» que esdevenen «QStringBuilder»

### KCMUtils

- Gestiona els esdeveniments de retorn
- Redimensiona manualment «KCMUtilDialog» a «sizeHint()» (error 389585)

### KConfig

- Soluciona un problema en llegir les llistes de camins
- El «kcfg_compiler» ara documenta les entrades vàlides pel seu tipus de «Color»

### KFileMetaData

- Elimina la implementació pròpia de conversió de «QString» a «TString» per al «taglibwriter»
- Augmenta la cobertura de proves de «taglibwriter»
- Implementa més etiquetes bàsiques per «taglibwriter»
- Elimina l'ús de la funció pròpia de conversió de QString a TString
- Actualitza la versió requerida de la «taglib» a 1.11.1
- Implementa la lectura de les etiquetes «replaygain»

### KHolidays

- Afegeix els festius de Costa d'Ivori (francès) (error 398161)
- holiday*hk** - esmena la data per al Tuen Ng Festival el 2019 (error 398670)

### KI18n

- Canvia adequadament l'àmbit de CMAKE_REQUIRED_LIBRARIES
- Android: Assegura que se cerquen els fitxers «.mo» en el camí correcte

### KIconThemes

- Inicia el dibuixat dels emblemes a la cantonada inferior dreta

### KImageFormats

- kimg_rgb: optimitza QRegExp i QString::fromLocal8Bit
- [EPS] Esmena una fallada en aturar l'aplicació (s'ha intentat fer persistent la imatge del porta-retalls) (error 397040)

### KInit

- Disminueix la brossa al registre per no verificar l'existència de fitxer amb un nom buit (error 388611)

### KIO

- Permet la redirecció file:// d'un fitxer no local a un URL WebDav del Windows
- [KFilePlacesView] Canvia la icona per l'entrada de menú contextual «Edit» en el plafó de Llocs
- [Places panel] Usa una icona més adequada de xarxa
- [KPropertiesDialog] Mostra la informació de muntatge de les carpetes a / (arrel)
- Soluciona la supressió de fitxers des de DAV (error 355441)
- Evita QByteArray::remove a AccessManagerReply::readData (error 375765)
- No intenta restaurar llocs no vàlids d'usuari
- Fa possible canviar el directori cap amunt, inclús amb barres al final a l'URL
- Les fallades de l'esclau KIO ara es gestionen des del KCrash en lloc d'un codi propi no gaire òptim
- Esmena quan un fitxer és creat des de contingut enganxat del porta-retalls només es mostra després d'un retard
- [PreviewJob] Envia els connectors activats de miniatures a l'esclau de miniatures (error 388303)
- Millora el missatge d'error «Espai insuficient al disc»
- IKWS: usa els «X-KDE-ServiceTypes» no obsolets a la generació del fitxer «desktop»
- Esmena la capçalera de destinació WebDAV en operacions COPY i MOVE
- Avisa l'usuari abans de l'operació de copiar/moure si l'espai disponible no és suficient (error 243160)
- Mou el KCM de SMB a la categoria Paràmetres de la xarxa
- Paperera: esmena l'anàlisi de la memòria cau de les mides dels directoris
- kioexecd: controla la creació o modificació de fitxers temporals (error 397742)
- No dibuixa marcs ni ombres al voltant d'imatges amb transparència (error 258514)
- Soluciona la icona del tipus de fitxer al diàleg de les propietats de fitxer que es representava de manera borrosa en pantalles de ppp alt

### Kirigami

- Obre adequadament el calaix quan s'arrossega amb la nansa
- Marge extra quan la «globaltoolbar» de la «pagerow» és ToolBar
- També s'admet Layout.preferredWidth com a mida del full
- Es desfà de les darreres restes de «controls1»
- Permet la creació de separadors d'accions
- Admet un nombre arbitrari de columnes a CardsGridview
- No destrueix activament els elements de menú (error 397863)
- Les icones a «actionButton» són monocromes
- No fa monocromes les icones quan no ho són
- Restaura la mida arbitrària *1,5 de les icones en el mòbil
- Reciclador delegat: no sol·licita dues vegades l'objecte context
- Usa la implementació interna Material de «ripple»
- Control de l'amplada de la capçalera per «sourcesize» si és horitzontal
- Exposa totes les propietats de BannerImage a Cards
- Usa DesktopIcon inclús en el Plasma
- Carrega correctament els camins file://
- Reverteix «Comença a cercar el context des del mateix delegat»
- Afegeix un cas de prova que delimita el problema de l'àmbit al DelegateRecycler
- Defineix explícitament una alçada per «overlayDrawers» (error 398163)
- Comença a cercar el context des del mateix delegat

### KItemModels

- Usa una referència en un bucle «for» per a tipus amb un constructor de còpia no trivial

### KNewStuff

- Afegeix la implementació per les etiquetes de l'Attica (error 398412)
- [KMoreTools] Proporciona una icona apropiada a l'element de menú «Configura...» (error 398390)
- [KMoreTools] Redueix la jerarquia del menú
- Soluciona «És impossible usar el fitxer «knsrc» per pujades des d'una ubicació no estàndard» (error 397958)
- Fa un enllaç a les eines de proves al Windows
- Soluciona la construcció amb les Qt 5.9
- Afegeix la implementació per les etiquetes de l'Attica

### KNotification

- Soluciona una fallada causada per una gestió dolenta del temps de vida a les notificacions d'àudio basades en «canberra» (error 398695)

### KNotifyConfig

- Esmena el consell del fitxer UI: KUrlRequester ara té QWidget com a classe base

### Framework del KPackage

- Usa una referència en un bucle «for» per a tipus amb un constructor de còpia no trivial
- Mou «Qt5::DBus» als objectius d'enllaç «PRIVATE»
- Emet senyals quan un paquet s'instal·la/desinstal·la

### KPeople

- Esmena els senyals que no s'emeten en fusionar dues persones
- No falla si la persona s'elimina
- Defineix PersonActionsPrivate com a classe, que s'ha declarat abans
- Fa públic l'API PersonPluginManager

### Kross

- Nucli: gestiona millor els comentaris de les accions

### KTextEditor

- Pinta el marcador de plegat de codi només per a les regions de plegat de codi multilínia
- Inicialitza «m_lastPosition»
- Scripting: «isCode()» retorna fals per al text «dsAlert» (error 398393)
- Usa l'script R «hl» per a les proves de sagnat de l'R
- Actualització de l'script de sagnat de l'R
- Esmena els temes de color Solarized clar i fosc (error 382075)
- No requereix Qt5::XmlPatterns

### KTextWidgets

- ktextedit: càrrega poc estricta de l'objecte QTextToSpeech

### Framework del KWallet

- Enregistra els errors de fallada d'obertura de la cartera

### KWayland

- No dona error en silenci si el dany s'envia abans de la memòria intermèdia (error 397834)
- [server] No retorna aviat en fallar al codi de reserva «touchDown»
- [server] Esmena la gestió de la memòria intermèdia d'accés remot quan la sortida no està vinculada
- [server] No intenta crear ofertes de dades sense font
- [server] Interromp l'inici de l'arrossegament en condicions correctes i sense publicar cap error

### KWidgetsAddons

- [KCollapsibleGroupBox] Respecta la durada de l'animació de l'estil del giny (error 397103)
- Elimina la comprovació de versió de les Qt obsoletes
- Compila

### KWindowSystem

- Usa _NET_WM_WINDOW_TYPE_COMBO en lloc de _NET_WM_WINDOW_TYPE_COMBOBOX

### KXMLGUI

- Esmena l'URL del proveïdor OCS al diàleg «quant a»

### NetworkManagerQt

- Usa un valor d'enumeració coincident a AuthEapMethodUnknown per a comparar amb un AuthEapMethod

### Frameworks del Plasma

- Actualitza les cadenes de versió de tema perquè hi ha icones noves a la 5.51
- També eleva la finestra de configuració en reusar-la
- Afegeix un component que manca: RoundButton
- Combina la visualització dels fitxers d'icona OSD i es canvia al tema d'icones del Plasma (error 395714)
- [Plasma Components 3 Slider] Esmena la mida implícita de la nansa
- [Plasma Components 3 ComboBox] Commuta entrades amb la roda del ratolí
- Permet icones de botó quan n'hi ha
- Esmena els noms de les setmanes que no es mostren adequadament al calendari quan la setmana comença amb un dia diferent de dilluns o diumenge (error 390330)
- [DialogShadows] Usa un desplaçament 0 per a les vores desactivades al Wayland

### Prison

- Esmena la representació dels codis asteques amb una relació d'aspecte != 1
- Elimina la suposició quan a la relació d'aspecte del codi de barres a partir de la integració QML
- Esmena errors de renderitzat causats per errors d'arrodoniment al Code 128
- Afegeix la implementació dels codis de barres Code 128

### Purpose

- Fa el CMake 3.0 la versió mínima del CMake

### QQC2StyleBridge

- Petit farciment predeterminat quan hi ha un fons

### Solid

- No mostra un emblema per als discs muntats, només els discs desmuntats
- [Fstab] Elimina el suport per l'AIX
- [Fstab] Elimina el suport del Tru64 (**osf**)
- [Fstab] Mostra un nom de compartició no buit quan el sistema de fitxers arrels està exportat (error 395562)
- També es prefereix proporcionar una etiqueta d'unitat per als dispositius «loop»

### Sonnet

- Soluciona el trencament de l'estimació del llenguatge
- Evita que el ressaltador suprimeixi el text seleccionat (error 398661)

### Ressaltat de la sintaxi

- i18n: esmena l'extracció dels noms de temes
- Fortran: ressalta les alertes als comentaris (error 349014)
- Evita que el context principal pugui ser «#poped»
- Protegeix la transició d'estat sense fi
- YAML: afegeix estils de blocs de literals i plegats (error 398314)
- Logcat i SELinux: millores per als nous esquemes Solarized
- AppArmor: esmena fallades en obrir les regles (al KF5.50) i millores per als esquemes Solarized nous
- Fusiona git://anongit.kde.org/syntax-highlighting
- Actualitza elements a ignorar del «git»
- Usa una referència en un bucle «for» per a tipus amb un constructor de còpia no trivial
- Esmena: ressaltat de correu electrònic per als parèntesis sense tancar a la capçalera «Subject» (error 398717)
- Perl: esmena parèntesis, variables, referències de cadenes i altres (error 391577)
- Bash: esmena l'expansió de paràmetres i claus (error 387915)
- Afegeix els temes de color Solarized clar i fosc

### Informació de seguretat

El codi publicat s'ha signat amb GPG usant la clau següent: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Empremta digital de la clau primària: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Podeu debatre i compartir idees quant a aquest llançament en la secció de comentaris en <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>l'article del Dot</a>.
