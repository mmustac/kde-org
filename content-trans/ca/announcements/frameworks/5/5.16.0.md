---
aliases:
- ../../kde-frameworks-5.16.0
date: 2015-11-13
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Monitor lib: Usar Kformat::spelloutDuration per internacionalitzar la cadena horària
- Usar «KDE_INSTALL_DBUSINTERFACEDIR» per a instal·lar interfícies de D-Bus
- UnindexedFileIndexer: Gestionar fitxers que s'han mogut quan el baloo_file no estava executant-se
- Eliminar Transaction::renameFilePath i afegir-hi DocumentOperation.
- Fer els constructors amb un paràmetre únic explícit
- UnindexedFileIndexer: Indexar només les parts requerides dels fitxers
- Transaction: Afegir un mètode per retornar l'estructura «timeInfo»
- S'ha afegit l'exclusió dels tipus MIME a la configuració del «balooctl»
- Bases de dades: Usar QByteArray::fromRawData en passar dades a un còdec
- Balooctl: Moure l'ordre «status» a la seva pròpia classe
- Balooctl: Mostrar el menú d'ajuda si no es reconeix l'ordre
- Balooshow: Permetre cercar fitxers pel seu inode + devId
- Balooctl monitor: Aturar si el Baloo fineix
- MonitorCommand: Usar els senyals d'iniciat i finalitzat
- Balooctl monitor: Moure a la classe d'ordres adequada
- Afegir una notificació de D-Bus en iniciar/finalitzar la indexació d'un fitxer
- FileIndexScheduler: Matar forçadament els fils en sortir
- Confirmació de WriteTransaction: Evitar la recuperació de la «positionList» a menys que es requereixi
- WriteTransaction: Declaracions extres a «replaceDocument»

### BluezQt

- «isBluetoothOperational» ara també depèn d'un «rfkill» desbloquejat
- Esmenar la determinació de l'estat global del commutador «rfkill»
- API del QML: Marcar les propietats sense senyal de notificació com a constants

### Mòduls extres del CMake

- Avís en lloc d'error si ecm_install_icons no troba cap icona (error 354610)
- Fer possible construir els Frameworks 5 del KDE amb les qt 5.5.x normals instal·lades des de l'instal·lador normal del qt.io al Mac OS
- No netejar les variables de la memòria cau a KDEInstallDirs (error 342717)

### Integració del marc de treball

- Definir un valor per omissió per WheelScrollLines
- Esmenar els paràmetres de WheelScrollLines amb les Qt &gt;= 5.5 (error 291144)
- Canviar al tipus de lletra Noto per al Plasma 5.5

### KActivities

- Esmenar la construcció amb les Qt 5.5
- Moure la inclusió «boost.optional» al lloc que s'utilitza
- Substituir l'ús de «boost.optional» en continuacions per una estructura «optional_view» més reduïda
- S'ha afegit la implementació per a una ordenació personalitzada dels resultats enllaçats
- Permetre que el QML invoqui el KCM d'activitats
- Afegir la implementació per a la supressió d'activitats a les activitats del KCM
- IU de configuració d'activitats noves
- IU nova de configuració que permet afegir descripció i fons de pantalla
- La IU de configuració ara s'ha fet modular de manera adequada

### KArchive

- Esmenar el KArchive per al canvi de comportament en les Qt 5.6
- Esmenar fuites de memòria, ús de memòria més baix

### KAuth

- Gestionar missatges «qInfo» d'intermediaris
- Esperar que finalitzi la crida asíncrona d'inici del «helper» abans de comprovar la resposta (error 345234)
- Esmenar un nom de variable, altrament no hi ha manera que la inclusió pugui funcionar

### KConfig

- Esmenar l'ús d'ecm_create_qm_loader.
- Esmenar una variable d'inclusió
- Usar la variant KDE*INSTALL_FULL*, ja que no hi ha cap ambigüitat
- Permetre que el KConfig usi recursos com els fitxers de configuració de reserva

### KConfigWidgets

- Fer el KConfigWidgets autocontingut, empaquetant el fitxer global en un recurs
- Fer opcionals les «doctools»

### KCoreAddons

- KAboutData: Esmenar «is is» -&gt; «is» a l'aplidoc addCredit(): ocsUserName -&gt; ocsUsername
- KJob::kill(Quiet) també hauria de sortir del cicle d'esdeveniments
- Afegir la implementació per al nom del fitxer «desktop» al «KAboutData»
- Usar el caràcter d'escapada correcte
- Reduir diverses assignacions
- Fer més senzill KAboutData::translators/setTranslators
- Esmenar el codi d'exemple del «setTranslator»
- desktopparser: Ometre la clau Encoding=
- desktopfileparser: Endreçar els comentaris de revisió
- Permetre definir tipus de serveis a kcoreaddons_desktop_to_json()
- desktopparser: Esmenar l'anàlisi de valors dobles i booleans
- Afegir KPluginMetaData::fromDesktopFile()
- desktopparser: Permetre passar camins relatius als fitxers de tipus de servei
- desktopparser: Usar un enregistrament més categoritzat
- QCommandLineParser usa -v per --version, així que cal usar --verbose
- Eliminar molt codi duplicat a desktop{tojson,fileparser}.cpp
- Analitzar els fitxers ServiceType en llegir fitxers .desktop
- Fer que SharedMimeInfo sigui un requisit opcional
- Eliminar la crida a QString::squeeze()
- desktopparser: Evitar descodificacions UTF8 innecessàries
- desktopparser: No afegir altres entrades si l'entrada acaba amb un separador
- KPluginMetaData: Avisar quan una entrada de llista no sigui una llista en JSON
- Afegir mimeTypes() a KPluginMetaData

### KCrash

- Millorar la cerca del Drkonqui i mantenir-lo en silenci per defecte si no es troba

### KDeclarative

- ConfigPropertyMap ara es pot consultar per a opcions de configuració immutables usant el mètode isImmutable(key)
- Desplegar el QJSValue en el mapa de propietats de la configuració
- EventGenerator: Afegir la implementació per enviar esdeveniments de roda
- Esmenar un QuickViewSharedEngine initialSize perdut en inicialitzar.
- Esmenar una regressió crítica a QuickViewSharedEngine en la modificació 3792923639b1c480fd622f7d4d31f6f888c925b9
- Predefinir la mida especificada de la vista precedint a la mida inicial de l'objecte a QuickViewSharedEngine

### KDED

- Fer opcionals les «doctools»

### Compatibilitat amb les KDELibs 4

- No intentar emmagatzemar un QDateTime en memòria «mmap»
- Sincronitzar i adoptar uriencode.cmake de kdoctools.

### KDesignerPlugin

- Afegir KCollapsibleGroupBox

### KDocTools

- Actualitzar les entitats pt_BR

### KGlobalAccel

- No fer «XOR Shift» a KP_Enter (error 128982)
- Capturar totes les tecles per a un símbol (error 351198)
- No recuperar dues vegades els «keysyms» per a cada tecla premuda

### KHTML

- Esmenar la impressió des de KHTMLPart definint correctament el pare «printSetting»

### KIconThemes

- «kiconthemes» ara permet temes incrustats en recursos qt dins el prefix :/icons com ho fan les mateixes Qt per QIcon::fromTheme
- Afegir dependències requerides que mancaven

### KImageFormats

- Reconèixer image/vnd.adobe.photoshop en lloc d'image/x-psd
- Revertir parcialment d7f457a per evitar fallar en sortir de l'aplicació

### KInit

- Fer opcionals les «doctools»

### KIO

- Desar l'URL del servidor intermediari amb l'esquema correcte
- Distribuir les «plantilles noves de fitxer» a la biblioteca «kiofilewidgets» usant un .qrc (error 353642)
- Gestionar adequadament el clic del mig a «navigatormenu»
- Fer desplegable kio_http_cache_cleaner als instal·ladors/paquets de les aplicacions
- KOpenWithDialog: Esmenar la creació de fitxers «desktop» amb un tipus MIME buit
- Llegir la informació del protocol de les metadades del connector
- Permetre el desplegament de «kioslave» local
- Afegir un .protocol al JSON convertit
- Esmenar l'emissió doble de resultat i la manca d'avís en llistar coincidències en una carpeta inaccessible (error 333436)
- Mantenir els enllaços relatius als destins en copiar enllaços simbòlics (error 352927)
- Usar les icones adequades per les carpetes per defecte al directori d'inici d'usuari (error 352498)
- Afegir una interfície que permet al connector mostrar icones de superposició personalitzades
- Fer opcionals les dependències del KNotifications en el KIO (kpac)
- Fer opcionals «doctools» + cartera
- Evitar les fallades del «kio» si el servidor de D-Bus no s'està executant
- Afegir KUriFilterSearchProviderActions, per a mostrar una llista d'accions per cercar text usant dreceres web
- Moure les entrades per al menú «Crea nou» des de kde-baseapps/lib/konq al kio (error 349654)
- Moure konqpopupmenuplugin.desktop des de kde-baseapps al «kio» (error 350769)

### KJS

- Usar la variable global «_timezone» per al MSVC en lloc de «timezone». Esmena la construcció amb el MSVC 2015.

### KNewStuff

- Esmenar el fitxer «desktop» «KDE Partition Manager» i l'URL de la pàgina inicial

### KNotification

- Ara que «kparts» ja no necessita «knotifications», només les coses que realment vulguin notificacions requereixen aquest framework
- Afegir la descripció + finalitat de pronunciació + phonon
- Fer opcional la dependència del phonon, és purament un canvi intern, com s'ha fet per la pronunciació.

### KParts

- Usar deleteLater a Part::slotWidgetDestroyed().
- Eliminar les dependències de KNotifications del KParts
- Usar una funció per consultar ui_standards.rc location en lloc d'escriure-ho en el codi, permetent funcionar els recursos de reserva alternatius

### KRunner

- RunnerManager: Simplificar el codi de càrrega del connector

### KService

- KBuildSycoca: Desar sempre, encara que no es tingui notícia de cap canvi en el fitxer «.desktop» (error 353203)
- Fer opcionals les «doctools»
- kbuildsycoca: Analitzar tots els fitxers «mimeapps.list» mencionats a l'especificació nova.
- Usar un segell de temps més gran en els subdirectoris com a segell de temps de recurs de directori.
- Mantenir els tipus MIME separats en convertir KPluginInfo a KPluginMetaData

### KTextEditor

- Ressaltat: gnuplot: afegir l'extensió «.plt»
- Esmenar el consell de validació, gràcies al «Thomas Jarosch» &lt;thomas.jarosch@intra2net.com&gt;, també afegir un consell quant a la validació de l'hora de compilació
- No fallar quan l'ordre no estigui disponible.
- Esmena de l'error núm. 307107
- El ressaltat de les variables començant per _
- Simplificar l'inici de git2, donat que es requereix una versió força recent (error 353947)
- Empaquetar configuracions per defecte en el recurs
- Ressaltat de la sintaxi (d-g): Usar els estils per defecte en lloc dels colors al codi font
- Millor cerca de scripts, primer usar els locals d'usuari, després els dels recursos, després qualsevol altre, d'aquesta manera l'usuari pot sobreescriure els scripts distribuïts pels locals
- També empaquetar tot el material JS en recursos, només manquen 3 fitxers de configuració i el «ktexteditor» ja es podrà usar com a biblioteca sense cap fitxer empaquetat
- Prova següent: Posar tots els fitxers XML empaquetats en un recurs
- Afegir una drecera de commutació del mode d'entrada (error 347769)
- Empaquetar fitxers XML en el recurs
- Ressaltat de la sintaxi (a-c): Migrar als estils nous per defecte, eliminar els colors en el codi font
- Ressaltat de la sintaxi: Eliminar els colors en el codi font i usar els estils per defecte en el seu lloc
- Ressaltat de la sintaxi: Usar els nous estils per defecte (elimina els colors en el codi font)
- Millor estil per defecte «Importació»
- Presentar «Desa com amb codificació» per a desar un fitxer amb una codificació diferent, usant el menú de codificació agrupat que hi ha i substituint tots els diàlegs de desament pel correcte del sistema operatiu sense perdre aquesta funcionalitat important.
- Empaquetar el fitxer IU en la biblioteca, usant l'extensió al «xmlgui»
- La impressió torna a respectar el tipus de lletra seleccionat i l'esquema de color (error 344976)
- Usar els colors del Brisa per a les línies desades i modificades
- Millora de les vores d'icona amb els colors per defecte de l'esquema «Normal»
- Autobrace: Inserir només una clau quan la lletra següent està buida o no és cap alfanumèric
- Autobrace: Si s'elimina el parèntesi inicial amb el retrocés, eliminar també el final
- Autobrace: Establir la connexió només una vegada
- Autobrace: Eliminar el parèntesi de tancament en condicions concretes
- Esmenar «shortcutoverride» que no es reenvia a la finestra principal
- Error 342659 - El color per defecte del «Ressaltat de parèntesi» és difícil de veure (s'ha esmenat l'esquema normal) (error 342659)
- Afegir els colors per defecte adequats per al color «Número de línia actual»
- Concordança de parèntesis i parèntesis automàtics: Compartició de codi
- Concordança de parèntesis: Evitar les «maxLines» negatives
- Concordança de parèntesis: Encara que l'interval nou coincideixi amb l'antic, això no significa que no es requereixi actualització
- Afegir l'amplada de mig espai per permetre pintar el cursor a l'EOL
- Esmenar problemes de HiDPI en les vores de les icones
- Esmenar l'error núm. 310712: Eliminar els espais finals també en línia amb el cursor (error 310712)
- Mostra només el missatge «Marcatge definit» quan és actiu el mode d'entrada «vi»
- Eliminar &amp; del text del botó (error 345937)
- Esmenar l'actualització del color del número de línia actual (error 340363)
- Implementar la inserció de parèntesis en escriure un parèntesi a una selecció (error 350317)
- Parèntesis automàtics (error 350317)
- Esmenar alerta de ressaltat (error 344442)
- Sense desplaçament de columna amb l'ajust de paraules dinàmic actiu
- Recordar que si l'usuari ha activat el ressaltat de sintaxi a les sessions, no perdre'l en desar després de restaurar (error 332605)
- Esmenar el plegat per a Tex (error 328348)
- Esmenar l'error núm. 327842: El final del comentari a l'estil C no es detectava bé (error 327842)
- Desar/restaurar l'ajust de paraules dinàmic en desar/restaurar la sessió (error 284250)

### KTextWidgets

- Afegir un submenú nou al KTextEdit per commutar entre idiomes de verificació ortogràfica
- Esmenar la càrrega de la configuració per defecte del Sonnet

### Framework del KWallet

- Usar «KDE_INSTALL_DBUSINTERFACEDIR» per a instal·lar interfícies de D-Bus
- Esmenar els avisos del fitxer de configuració del KWallet en connectar-se (error 351805)
- Prefix adequat de la sortida del kwallet-pam

### KWidgetsAddons

- Afegir un giny contenidor que es pugui col·lapsar, KCollapsibleGroupBox
- KNewPasswordWidget: Mancava la inicialització dels colors
- Presentar el KNewPasswordWidget

### KXMLGUI

- kmainwindow: Omplir la informació del traductor quan sigui disponible (error 345320)
- Permetre vincular la tecla del menú contextual (menú dret) a dreceres (error 165542)
- Afegir funció a consulta de les ubicacions estàndards del fitxer XML
- Permetre que el framework «kxmlgui» s'usi sense cap fitxer instal·lat
- Afegir dependències requerides que mancaven

### Frameworks del Plasma

- Esmenar els elements de la TabBar que s'amunteguen en la creació inicial, p. ex. com es pot observar en el Kickoff després d'iniciar el Plasma
- Esmenar que en deixar anar fitxers a l'escriptori/plafó no s'ofereixi una selecció d'accions a fer
- Tenir en compte Take QApplication::wheelScrollLines des de «ScrollView»
- Usar BypassWindowManagerHint només en la plataforma X11
- Suprimir el fons del plafó antic
- Un indicador rotatiu més llegible per mides petites
- Historial amb vista acolorida
- Calendari: Fer que es pugui clicar tota l'àrea de capçalera
- Calendari: No usar el número del dia actual a «goToMonth»
- Calendari: Esmenar l'actualització del resum de la dècada
- Icones del tema Brisa en carregar amb «IconItem»
- Esmenar la propietat «minimumWidth» del botó (error 353584)
- Presentar el senyal «appletCreated»
- Icona Brisa del Plasma: S'ha afegit elements ID SVG al Touchpad
- Icona Brisa del Plasma: Canviar el Touchpad a la mida 22x22px
- Icona Brisa: Afegir icona del giny a les notes
- Un script per a substituir colors en el codi font per fulls d'estils
- Aplicar «SkipTaskbar» a «ExposeEvent»
- No definir «SkipTaskbar» a cada esdeveniment

Podeu debatre i compartir idees quant a aquest llançament en la secció de comentaris en <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>l'article del Dot</a>.
