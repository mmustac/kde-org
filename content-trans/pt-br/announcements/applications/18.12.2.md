---
aliases:
- ../announce-applications-18.12.2
changelog: true
date: 2019-02-07
description: O KDE Lança as Aplicações 18.12.2.
layout: application
major_version: '18.12'
release: applications-18.12.2
title: O KDE Lança as Aplicações do KDE 18.12.2
version: 18.12.2
---
{{% i18n_date %}}

Hoje o KDE lançou a segunda actualização de estabilidade para as <a href='../18.12.0'>Aplicações do KDE 18.12</a>. Esta versão contém apenas correcções de erros e actualizações de traduções, pelo que será uma actualização segura e agradável para todos.

Mais de uma dúzia de correções de erros registradas incluem melhorias no Kontact, Ark, Konsole, Lokalize, Umbrello, entre outros.

As melhorias incluem:

- O Ark não apaga mais os ficheiros gravados dentro do visualizador incorporado</li>
- O livro de endereços agora recorda os aniversários quando reúne os contactos</li>
- Foram corrigidas diversas actualizações em falta na visualização dos diagramas no Umbrello</li>
