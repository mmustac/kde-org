---
aliases:
- ../announce-applications-18.04.3
changelog: true
date: 2018-07-12
description: O KDE disponibiliza o KDE Applications 18.04.3
layout: application
title: O KDE disponibiliza o KDE Applications 18.04.3
version: 18.04.3
---
12 de Julho de 2018. Hoje o KDE lançou a terceira actualização de estabilidade para as <a href='../18.04.0'>Aplicações do KDE 18.04</a>. Esta versão contém apenas correcções de erros e actualizações de traduções, pelo que será uma actualização segura e agradável para todos.

As mais de 20 correcções de erros registadas incluem as melhorias no Kontact, no Ark, no Cantor, no Dolphin, no Gwenview, no KMag, entre outros.

As melhorias incluem:

- Foi reposta a compatibilidade com os servidores de IMAP que não anunciam as suas capacidades
- O Ark consegue agora extrair pacotes ZIP onde faltem registos adequados das pastas
- As notas autocolantes KNotes seguem de novo o cursor do rato ao serem movidas
