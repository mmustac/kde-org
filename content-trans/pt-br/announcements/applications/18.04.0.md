---
aliases:
- ../announce-applications-18.04.0
changelog: true
date: 2018-04-19
description: O KDE disponibiliza o KDE Applications 18.04.0
layout: application
title: O KDE disponibiliza o KDE Applications 18.04.0
version: 18.04.0
---
19 de abril de 2018. Hoje o KDE lançou o KDE Applications 18.04.0.

Nós trabalhamos continuamente em melhorar o software incluso na nossa série aplicativos do KDE e esperamos que você ache úteis todas as novas melhorias e consertos de bugs!

## O que há de novo nas Aplicações do KDE 18.04

### Sistema

{{<figure src="/announcements/applications/18.04.0/dolphin1804.png" width="600px" >}}

A primeira versão importante em 2018 do <a href='https://www.kde.org/applications/system/dolphin/'>Dolphin</a>, o poderoso gestor de ficheiros do KDE, oferece muitas melhorias nos seus painéis:

- As secções do painel de 'Locais' podem agora ser escondidas, se optar por não as mostrar, e está agora disponível uma nova secção 'Rede' para registar os elementos de localizações remotas.
- O painel do 'Terminal' pode ser acoplado a qualquer lado da janela e, se o tentar abrir sem ter o Konsole instalado, o Dolphin irá mostrar um aviso e ajudá-lo-á a instalá-lo.
- O suporte de HiDPI para o painel de 'Informação' foi melhorado.

A área de pastas e os menus também foram actualizados:

- A pasta do Lixo agora mostra um botão 'Esvaziar o Lixo'.
- Foi adicionado um item de menu 'Mostrar o Destino' para ajudar a encontrar os destinos das ligações simbólicas.
- Foi melhorada a integração com o Git, já que o menu de contexto das pastas do Git agora mostra duas acções novas para o 'git log' e o 'git merge'.

Outras melhorias incluem:

- Foi introduzido um novo atalho, dando-lhe a opção para abrir a Barra do Filtro, bastando para tal carregar na tecla da barra (/).
- Poderá agora ordenar e organizar as fotos pela data em que foram tiradas.
- O arrastamento e largada de muitos ficheiros pequenos no Dolphin ficou mais rápido, e os utilizadores poderão agora anular as tarefas de mudança de nomes em lote.

{{<figure src="/announcements/applications/18.04.0/konsole1804.png" width="600px" >}}

Para tornar o trabalho na linha de comandos ainda mais agradável, o <a href='https://www.kde.org/applications/system/konsole/'>Konsole</a>, a aplicação de emulação de terminal do KDE, agora aparece ainda mais bonita:

- Poderá obter esquemas de cores com o KNewStuff.
- A barra de deslocamento funde-se melhor com o esquema de cores activo.
- Por omissão, a barra de páginas só aparece quando for necessária.

As contribuições para o Konsole não ficam por aqui, e foram introduzidas muitas funcionalidades novas:

- Foi adicionado um novo modo apenas para leitura e uma propriedade do perfil para activar/desactivar a cópia de texto como HTML.
- No Wayland, o Konsole agora suporta o menu de arrastamento.
- Tiveram lugar diversas melhorias no que diz respeito ao protocolo ZMODEM: o Konsole pode agora lidar com o indicador de envio B01 do 'zmodem', sendo que pode mostrar a evolução da transferência dos dados, e tendo agora o botão Cancelar na janela a fazer o que é suposto. A transferência de ficheiros maiores é melhor suportada agora, lendo-os para a memória em blocos de 1MB.

Outras melhorias incluem:

- Foi corrigido o deslocamento com a roda do rato, usando a 'libinput', e o percurso pelo histórico da consola, quando se usa a roda do rato, pode ser agora impedido.
- As pesquisas são actualizadas após mudar a opção da pesquisa de ocorrências por expressões regulares e, ao carregar em 'Ctrl' + 'Backspace', o Konsole irá corresponder ao comportamento do 'xterm'.
- Foi corrigido o atalho do '--background-mode'.

### Multimídia

O <a href='https://juk.kde.org/'>JuK</a>, o leitor de música do KDE, agora tem suporte para o Wayland. As novas funcionalidades da interface incluem a capacidade de esconder a barra de menu e de ter uma indicação visual da faixa actualmente em reprodução. Quando a acoplagem na bandeja de sistema estiver desactivada, o JuK não irá mais estoirar ao tentar sair pelo ícone 'fechar' da janela e a interface do utilizador continuará visível. Os erros correspondentes à reprodução automática inesperada do JuK, após acordar de uma suspensão no Plasma 5 e o tratamento da coluna da lista de reprodução, também foram corrigidos.

Para a versão 18.04, os responsáveis pelo <a href='https://kdenlive.org/'>Kdenlive</a>, o editor de vídeo não-linear do KDE, focaram-se na manutenção:

- O dimensionamento dos 'clips' já não danifica mais as transições e imagens-chave.
- Os utilizadores da escala do ecrã nos monitores HiDPI poderão ver agora ícones com melhor definição.
- Foi corrigido um estoiro possível no arranque com algumas configurações específicas.
- O MLT agora é obrigatório usar a versão 6.6.0 ou posterior; a sua compatibilidade foi por outro lado melhorada.

#### Gráficos

{{<figure src="/announcements/applications/18.04.0/gwenview1804.png" width="600px" >}}

Ao longo dos últimos meses, as contribuições do <a href='https://www.kde.org/applications/graphics/gwenview/'>Gwenview</a>, o visualizador e organizador de imagens do KDE tiveram uma grande quantidade de melhorias. Os destaques incluem:

- Foi adicionado o suporte para os controladores MPRIS, para que possa agora controlar as apresentações em ecrã completo com o KDE Connect, com as suas teclas multimédia e o plasmóide do Leitor Multimédia.
- Os botões das miniaturas à passagem do rato podem ser agora desactivados.
- A ferramenta de Recorte recebeu diversas melhorias, dado que a sua configuração agora é recordada ao mudar para outra imagem, sendo que a forma da área de selecção poderá ser bloqueada se usar as teclas 'Shift' ou 'Ctrl', podendo também ser bloqueada a nível das proporções de tamanho da imagem apresentada de momento.
- No modo de ecrã completo, poderá agora sair com a tecla 'Escape' e a paleta de cores irá reflectir o seu tema de cores activo. Se sair do Gwenview neste modo, esta definição será recordada e a nova sessão também será iniciada em todo o ecrã.

A atenção ao detalhe é importante; por isso, o Gwenview foi polido nas seguintes áreas:

- O Gwenview irá mostrar mais localizações de ficheiros legíveis na lista de 'Pastas Recentes', irá mostrar o menu de contexto apropriado para os itens na lista de 'Ficheiros Recentes' e irá esquecer tudo correctamente quando usar a funcionalidade para 'Desactivar o Histórico'.
- Se carregar numa pasta da barra lateral, poderá comutar entre os modos de Navegação e de Visualização, e poderá também recordar o último modo usado ao mudar de pastas, permitindo assim uma navegação mais rápida em colecções de imagens enormes.
- A navegação com o teclado foi ainda mais sistematizada, indicando adequadamente o foco do teclado no modo de Navegação.
- A funcionalidade para 'Ajustar à Largura' foi substituída por uma função 'Preenchimento' mais generalizada.

{{<figure src="/announcements/applications/18.04.0/gwenviewsettings1804.png" width="600px" >}}

Algumas melhorias ainda mais pequenas poderão tornar o trabalho dos utilizadores ainda mais agradável:

- Para melhorar a consistência, as imagens em SVG agora são também ampliadas como as outras, quando a funcionalidade 'Área da Imagem → Aumentar as Imagens Menores' está activa.
- Depois de editar uma imagem ou de anular as alterações, a área da imagem e das miniaturas já não ficarão mais fora de sincronização.
- Ao mudar o nome das imagens, a extensão do nome do ficheiro ficará desligada por omissão e a janela para 'Mover/Copiar/Criar uma Ligação' agora passa a mostrar a pasta actual.
- Foram corrigidos pequenos defeitos visuais, p.ex. na barra do URL, na barra de ecrã completo e nas animações das dicas das miniaturas. Também foram adicionados alguns ícones em falta.
- Por último, o botão à passagem do ecrã completo irá directamente ver a imagem em vez de mostrar o conteúdo da pasta;  por outro lado, a configuração avançada agora permite mais controlo sobre a tentativa de desenho das cores do ICC.

#### Escritório

No <a href='https://www.kde.org/applications/graphics/okular/'>Okular</a>, o visualizador universal de documentos do KDE, o desenho do PDF e a extracção do texto poderão agora ser cancelados se estiver a usar o Poppler na versão 0.63 ou posterior, o que significa que, se tiver um ficheiro PDF complexo e mudar o nível de ampliação enquanto está a desenhar, irá cancelar imediatamente em vez de esperar pelo desenho terminar.

Irá encontrar um suporte melhorado para JavaScript no PDF, para o AFSimple_Calculate e, se tiver o Poppler na versão 0.64 ou posterior, o Okular irá suportar as mudanças por JavaScript no PDF, no estado apenas-para-leitura dos formulários.

A gestão de e-mails de confirmação de marcações no <a href='https://www.kde.org/applications/internet/kmail/'>KMail</a>, o cliente de e-mail poderoso do KDE, foi substancialmente melhorada para suportar as marcações de comboios e usa uma base de dados de aeroportos, baseada no Wikidata, para mostrar os voos com informações de fusos-horários correctas. Para simplificar as cosias para si, foi implementada uma nova extracção para os e-mails que não contêm dados estruturados de marcações.

Outras melhorias incluem:

- A estrutura da mensagem poderá ser vista de novo com o novo 'plugin' 'Experiente'.
- Foi adicionado um 'plugin' no Editor de Sieve para seleccionar os e-mails da base de dados do Akonadi.
- A pesquisa de texto no editor foi melhorada com o suporte para expressões regulares.

#### Utilitários

{{<figure src="/announcements/applications/18.04.0/spectacle1804.png" width="600px" >}}

A melhoria da interface de utilizador do <a href='https://www.kde.org/applications/graphics/spectacle/'>Spectacle</a>, a ferramenta de capturas do ecrã do KDE, foi uma grande área de foco:

- A fila de botões inferior recebeu uma grande remodelação, mostrando agora um botão para abrir o menu de Configuração e um novo botão 'Ferramentas', que revela métodos para abrir a última pasta de imagens usada e lançar um programa de gravação do ecrã.
- O último modo de gravação usado agora é recordado por omissão.
- O tamanho da janela agora adapta-se às proporções da imagem, resultando numa miniatura do ecrã mais agradável e eficiente a nível de espaço.
- A janela de Configuração foi substancialmente simplificada.

Para além disso, os utilizadores serão capazes de simplificar os seus fluxos de trabalho com estas novas funcionalidades:

- Quando for capturada uma janela, o seu título poderá ser adicionado automaticamente ao nome do ficheiro da imagem.
- O utilizador poderá agora escolher se o Spectacle sai ou não automaticamente após qualquer operação de gravação ou cópia.

As correções importantes de erros incluem:

- O arrastamento e largada nas janelas do Chromium agora funcionam como seria de esperar.
- O uso repetitivo dos atalhos de teclado para tirar uma fotografia do ecrã não resulta mais numa janela de aviso de atalho de teclado ambíguo.
- Para as fotografias de regiões rectangulares, o extremo inferior da selecção poderá ser ajustado de forma mais precisa.
- Foi melhorada a fiabilidade da captura das janelas que estão a 'tocar' nos extremos do ecrã, quando a composição está desligada.

{{<figure src="/announcements/applications/18.04.0/kleopatra1804.gif" width="600px" >}}

Com o <a href='https://www.kde.org/applications/utilities/kleopatra/'>Kleopatra</a>, o gestor de certificados e interface universal de encriptação do KDE, pode gerar chaves EdDSA em curva 25519, com uma versão recente do GnuPG. Foi adicionada uma vista de 'Bloco de Notas' para algumas acções de encriptação baseadas em texto, podendo agora assinar/encriptar e descodificar/verificar directamente na aplicação. Na área de 'Detalhes do certificado', irá agora ver uma acção de exportação, a qual poderá usar para exportar como texto a copiar e colar. Mais ainda, poderá importar o resultado com a nova área do 'Bloco de Notas'.

No <a href='https://www.kde.org/applications/utilities/ark/'>Ark</a>, a ferramenta gráfica de compressão/descompressão de ficheiros com suporte para vários formatos, agora é possível parar as compressões ou extracções, caso use a infra-estrutura da 'libzip' para os pacotes ZIP.

### Aplicações que aderiram calendário de lançamento das Aplicações do KDE

O gravador de Webcam do KDE <a href='https://userbase.kde.org/Kamoso'>Kamoso</a> e o programa para cópias de segurança <a href='https://www.kde.org/applications/utilities/kbackup/'>KBackup</a> irão seguir agora os calendários de versões das Aplicações. O mensageiro instantâneo <a href='https://www.kde.org/applications/internet/kopete/'>Kopete</a> também vai ser introduzido de novo, após a sua migração para as Plataformas do KDE 5.

### Aplicações que passaram para o seu calendário próprio de lançamento

O editor hexadecimal <a href='https://community.kde.org/Applications/18.04_Release_Notes#Tarballs_that_we_do_not_ship_anymore'>Okteta</a> passará agora a ter o seu próprio calendário de versões, após um pedido do seu responsável de manutenção.

### Pisoteando bugs

Foram resolvidos mais de 170 erros nas aplicações, incluindo o pacote Kontact, Ark, Dolphin, Gwenview, K3b, Kate, Kdenlive, Konsole, Okular, Umbrello e muito mais!

### Registro completo das alterações
