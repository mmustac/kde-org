---
aliases:
- ../announce-applications-17.12.2
changelog: true
date: 2018-02-08
description: O KDE disponibiliza o KDE Applications 17.12.2
layout: application
title: O KDE disponibiliza o KDE Applications 17.12.2
version: 17.12.2
---
8 de Fevereiro de 2018. Hoje o KDE lançou a segunda actualização de estabilidade para as <a href='../17.12.0'>Aplicações do KDE 17.12</a>. Esta versão contém apenas correcções de erros e actualizações de traduções, pelo que será uma actualização segura e agradável para todos.

As mais de 20 correcções de erros registadas incluem as melhorias no Kontact, no Dolphin, no Gwenview, no KGet, no Okular, entre outros.
