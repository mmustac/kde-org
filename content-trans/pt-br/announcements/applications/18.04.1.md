---
aliases:
- ../announce-applications-18.04.1
changelog: true
date: 2018-05-10
description: O KDE disponibiliza o KDE Applications 18.04.1
layout: application
title: O KDE disponibiliza o KDE Applications 18.04.1
version: 18.04.1
---
10 de Maio de 2018. Hoje o KDE lançou a primeira actualização de estabilidade para as <a href='../18.04.0'>Aplicações do KDE 18.04</a>. Esta versão contém apenas correcções de erros e actualizações de traduções, pelo que será uma actualização segura e agradável para todos.

As mais de 20 correcções de erros registadas incluem as melhorias no Kontact, no Cantor, no Dolphin, no Gwenview, no JuK, no Okular, no Umbrello, entre outros.

As melhorias incluem:

- Os itens duplicados no painel de locais do Dolphin já não provocam mais estoiros
- Foi corrigido um erro no carregamento dos ficheiros SVG no Gwenview
- A importação de C++ do Umbrello agora aceita a palavra-chave 'explicit'
