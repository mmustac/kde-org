---
aliases:
- ../announce-applications-18.12.2
changelog: true
date: 2019-02-07
description: KDEk, Aplikazioak 19.12.2 kaleratu du.
layout: application
major_version: '18.12'
release: applications-18.12.2
title: KDEk, KDE Aplikazioak 18.12.2 kaleratu du
version: 18.12.2
---
{{% i18n_date %}}

Today KDE released the second stability update for <a href='../18.12.0'>KDE Applications 18.12</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

More than a dozen recorded bugfixes include improvements to Kontact, Ark, Konsole, Lokalize, Umbrello, among others.

Improvements include:

- Ark no longer deletes files saved from inside the embedded viewer</li>
- The address book now remembers birthdays when merging contacts</li>
- Several missing diagram display updates were fixed in Umbrello</li>
