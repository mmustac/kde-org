---
aliases:
- ../../kde-frameworks-5.18.0
date: 2016-01-09
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Se han corregido varios problemas de búsqueda relacionados con «mtime».
- PostingDB Iter: No producir una aserción en «MDB_NOTFOUND».
- Estado de Balooctl: Evitar mostrar «Indexando contenido» para las carpetas.
- StatusCommand: Mostrar el estado correcto de las carpetas.
- SearchStore: Manejar con elegancia los valores de términos vacíos (error 356176).

### Iconos Brisa

- Actualizaciones e incorporaciones de iconos
- Iconos de estado de 22 píxeles para 32 píxeles también, ya que son necesarios en la bandeja del sistema.
- Se ha cambiado el valor de «Fixed» a «Scalable» en las carpetas de 32 píxeles en Brisa oscuro.

### Módulos CMake adicionales

- Hacer global el módulo «KAppTemplate» de CMake.
- Silenciar las advertencias CMP0063 con KDECompilerSettings
- ECMQtDeclareLoggingCategory: Incluir &lt;QDebug&gt; en el archivo generado
- Corregir advertencias CMP0054

### KActivities

- Simplificar la carga de QML para KCM (fallo 356832).
- Solución alternativa para el fallo SQL de Qt que no borra correctamente las conexiones (fallo 348194).
- Se ha fusionado un complemento que ejecuta aplicaciones cuando cambia el estado de la actividad.
- Adaptación de KService a KPluginLoader
- Adaptar complementos para usar kcoreaddons_desktop_to_json()

### KBookmarks

- Inicializar completamente «DynMenuInfo» el el valor de retorno.

### KCMUtils

- KPluginSelector::addPlugins: Se ha corregido la aserción si el parámetro 'config' es el predeterminado (fallo 352471).

### KCodecs

- Evitar que se pueda desbordar deliberadamente un búfer lleno.

### KConfig

- Asegurare que el grupo no está escapado correctamente en kconf_update.

### KCoreAddons

- Añadir KAboutData::fromPluginMetaData(const KPluginMetaData &amp;plugin)
- Añadir KPluginMetaData::copyrightText(), extraInformation() y otherContributors()
- Añadir KPluginMetaData::translators() y KAboutPerson::fromJson()
- Se ha corregido el uso tras la liberación en el analizador de archivos del escritorio.
- Hacer que se pueda construir «KPluginMetaData» desde una ruta JSON.
- desktoptojson: Hacer que el archivo de tipo de servicio ausente sea un error para el binario.
- Hacer que sea un error llamar a «kcoreaddons_add_plugin» sin «SOURCES».

### KDBusAddons

- Adaptar a DBus en hilo secundario de Qt 5.6.

### KDeclarative

- [DragArea] Se ha añadido la propiedad «dragActive».
- [Base de datos MIME de KQuickControlsAddons] Exponer el comentario de «QMimeType».

### KDED

- kded: Adaptar a DBus multihilo de Qt 5.6: «messageFilter» debe disparar la carga del módulo en el hilo principal.

### Soporte de KDELibs 4

- kdelibs4support necesita kded (para kdedmodule.desktop)
- Corregir la advertencia CMP0064 al establecer la política CMP0054 a NEW
- No exportar símbolos que también existen en KWidgetsAddons

### KDESU

- No permitir una fuga de memoria en «fd» al crear un «socket».

### KHTML

- Windows: eliminar la dependencia de kdewin

### KI18n

- Documentar la regla del primer argumento para los plurales en QML.
- Reducir los cambios de tipo no deseados.
- Permitir el uso de dobles como índice para las llamadas «i18np*()» en QML.

### KIO

- Corregir «kiod» para DBus multihilo de Qt 5.6: «messageFilter» debe esperar hasta que el módulo esté cargado antes de volver.
- Cambiar el código de error al copiar/mover a un subdirectorio.
- Se ha corregido el problema del bloqueo de la papelera vacía.
- Se ha corregido el botón erróneo para URL remotas en «KUrlNavigator».
- KUrlComboBox: Se ha corregido el retorno de una ruta absoluta de «urls()».
- kiod: Se ha desactivado la gestión de sesiones.
- Añadir completación automática para la entrada '.', que ofrece todos los archivos/carpetas ocultos (fallo 354981).
- ktelnetservice: Corregir por uno en la comprobación de «argc»; parche por Steven Bromley.

### KNotification

- [Notificar con ventana emergente] Enviar junto al ID del evento.
- Establecer motivo predeterminado no vacío para la inhibición del protector de pantalla (fallo 334525).
- Se ha añadido una sugerencia para omitir el agrupamiento de notificaciones (error 356653).

### KNotifyConfig

- [KNotifyConfigWidget] Permitir la selección de un evento específico.

### Framework para paquetes

- Hacer que se puedan proporcionar los metadatos en JSON.

### KPeople

- Se ha corregido un posible doble borrado en «DeclarativePersonData».

### KTextEditor

- Resaltado sintáctico para «pli»: Se han añadido funciones integradas y regiones expansibles.

### Framework KWallet

- kwalletd: Se ha corregido una fuga de memoria en «FILE*».

### KWindowSystem

- Se ha añadido una variante de «xcb» para los métodos estáticos de «KStartupInfo::sendFoo».

### NetworkManagerQt

- Hacer que funcione con versiones antiguas de NetworkManager.

### Framework de Plasma

- [ToolButtonStyle] Indicar siempre el foco activo.
- Usar el indicador «SkipGrouping» para la notificación de widget borrado (error 356653).
- Tratar correctamente los enlaces simbólicos en la ruta de los paquetes.
- Se ha añadido «HiddenStatus» para la ocultación propia de plasmoides.
- Deje de redirigir las ventanas cuando el elemento está deshabilitado u oculto (fallo 356938).
- No emitir «statusChanged» si no ha cambiado.
- Se han corregido los identificadores de los elementos para la orientación Este.
- Contenedor: No emitir «appletCreated» con una miniaplicación nula (error 356428).
- [Interfaz de contenedores] Se ha corregido el desplazamiento errático de alta precisión.
- Leer la propiedad «X-Plasma-ComponentTypes» de «KPluginMetadata» como cadena de texto.
- [Miniaturas de ventanas] No fallar si se ha desactivado la composición.
- Permitir que los contenedores puedan sobrescribir «CompactApplet.qml».

Puede comentar esta versión y compartir sus ideas en la sección de comentarios de <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>el artículo de dot</a>.
