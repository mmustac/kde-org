---
aliases:
- ../../kde-frameworks-5.34.0
date: 2017-05-13
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- balooctl, baloosearch, balooshow: Se ha corregido el orden de creación del objeto QCoreApplication (fallo 378539).

### Iconos Brisa

- Se han añadido iconos para hotspot (https://github.com/KDAB/hotspot).
- Se han mejorado los iconos del sistema de control de versiones (fallo 377380).
- Se ha añadido un icono para plasmate (fallo 376780).
- Se han actualizado los iconos de la sensibilidad del micrófono (fallo 377012).
- Se ha elevado el tamaño por omisión para los iconos del panel a 48.

### Módulos CMake adicionales

- Saneadores: no usar indicadores de tipo GCC en, por ejemplo, MSVC.
- KDEPackageAppTemplates: mejoras en la documentación.
- KDECompilerSettings: pasar -Wvla y -Wdate-time.
- Permitir el uso de versiones antiguas de qmlplugindump.
- Introducción de ecm_generate_qmltypes.
- Permitir que los proyectos puedan incluir el archivo dos veces.
- Se ha corregido el «rx» que satisface nombres de proyectos fuera del uri de git.
- Introducción de la orden de compilación «fetch-translations».
- Mayor uso de -Wno-gnu-zero-variadic-macro-arguments.

### KActivities

- Solo estamos usando el Nivel 1 de Frameworks; así que nos movemos al Nivel 2.
- Se ha eliminado KIO de las dependencias.

### KAuth

- Corrección de seguridad: verificar que quien nos está llamando es realmente quien dice ser.

### KConfig

- Se ha corregido el cálculo de «relativePath» en KDesktopFile::locateLocal() (fallo 345100).

### KConfigWidgets

- Se ha definido el icono para la acción «Donar».
- Se han relajado las restricciones para procesar QGroupBoxes.

### KDeclarative

- No definir «ItemHasContents» en «DropArea».
- No aceptar los eventos que se producen al situar el cursor encima de «DragArea».

### KDocTools

- Método alternativo para MSVC y la carga de catálogos.
- Se ha resuelto un conflicto de visibilidad en meinproc5 (fallo 379142).
- Codificar una cuantas variables más en las rutas (evitar problemas con los espacios).
- Codificar una cuantas variables en las rutas (evitar problemas con los espacios).
- Desactivar temporalmente la documentación local en Windows.
- FindDocBookXML4.cmake, FindDocBookXSL.cmake: buscar en instalaciones caseras.

### KFileMetaData

- Hacer que KArchive sea opcional y no compilar los extractores que lo necesiten.
- Se ha corregido el error de compilación de símbolos duplicados cuando se usa «mingw» en Windows.

### KGlobalAccel

- Compilación: se ha eliminado la dependencia de KService.

### KI18n

- Se ha corregido el manejo del «basename» de los archivos «po» (fallo 379116).
- Se ha corregido el arranque de «ki18n».

### KIconThemes

- No intentar crear iconos con tamaño nulo.

### KIO

- KDirSortFilterProxyModel: se ha recuperado la ordenación natural (fallo 343452).
- Rellenar UDS_CREATION_TIME con el valor de «st_birthtime» en FreeBSD.
- Esclavo HTTP: enviar la página de error tras un fallo de autenticación (fallo 373323).
- kioexec: carga de delegado a un módulo de «kded» (fallo 370532).
- Se ha corregido una prueba de la interfaz de KDirlister que definía el esquema de URL dos veces.
- Borrar módulos de «kiod» al salir.
- Generar un archivo «moc_predefs.h» para «KIOCore» (fallo 371721).
- kioexec: se ha corregido la implementación de --suggestedfilename.

### KNewStuff

- Permitir múltiples categorías con el mismo nombre.
- KNewStuff: mostrar información sobre el tamaño de los archivos en el delegado de la cuadrícula.
- Si se conoce el tamaño de una entrada, mostrarlo en la vista de lista.
- Registrar y declarar «KNSCore::EntryInternal::List» como un metatipo.
- No fracasar en el cambio. ¿Entradas dobles? No, gracias.
- Cerrar siempre el archivo descargado tras su descarga.

### Framework KPackage

- Se ha corregido la ruta de inclusión en KF5PackageMacros.cmake.
- Ignorar las advertencias durante la generación de «appdata» (fallo 378529)

### KRunner

- Plantilla: se ha cambiado la categoría de la plantilla de más alto nivel a «Plasma».

### KTextEditor

- Integración de KAuth para guardar documentos, vol. 2.
- Se ha corregido una aserción al aplicar el plegado de código que cambiaba la posición del cursor.
- Usar el elemento raíz &lt;gui&gt; no desaconsejado en el archivo «ui.rc».
- Añadir marcas a la barra de desplazamiento también en la búsqueda y sustitución integrada.
- Integración de KAuth para guardar documentos.

### KWayland

- Validar que la superficie es válida al enviar el evento de salida en «TextInput».

### KWidgetsAddons

- KNewPasswordWidget: no ocultar la acción de visibilidad en el modo de texto sin formato (fallo 378276).
- KPasswordDialog: no ocultar la acción de visibilidad en el modo de texto sin formato (fallo 378276).
- Se ha corregido KActionSelectorPrivate::insertionIndex().

### KXMLGUI

- «kcm_useraccount» ha muerto, ¡larga vida a «user_manager»!
- Compilaciones reproducibles: disminuir la versión de XMLGUI_COMPILING_OS.
- Corrección: el nombre de DOCTYPE debe coincidir el tipo del elemento raíz.
- Se ha corregido el uso erróneo de «ANY» en «kpartgui.dtd».
- Usar el elemento raíz &lt;gui&gt; no desaconsejado.
- Correcciones en «API dox»: sustituir 0 por «nullptr» o eliminarlo donde no procede.

### NetworkManagerQt

- Se ha corregido un cuelgue al obtener la lista de conexiones activas (fallo 373993).
- Definir el valor por omisión para la negociación automática según la versión de NM que se esté usando.

### Iconos de Oxígeno

- Se ha añadido un icono para «hotspot» (https://github.com/KDAB/hotspot)
- Se ha elevado el tamaño por omisión para los iconos del panel a 48.

### Framework de Plasma

- Volver a cargar el icono cuando cambia «usesPlasmaTheme».
- Instalar los «Componentes de Plasma 3» para que se puedan usar.
- Introducir «units.iconSizeHints.*» para proporcionar sugerencias de tamaño de iconos configurables por el usuario (fallo 378443).
- [TextFieldStyle] Se ha corregido el error «textField no está definido».
- Actualizar el remedio «ungrabMouse» para Qt 5.8.
- Protección contra «la miniaplicación no carga AppletInterface» (fallo 377050)
- Calendario: usar el idioma correcto para los nombres de los meses y de los días.
- Generar archivos «plugins.qmltypes» para los complementos que se instalen.
- Si el usuario ha definido un tamaño implícito, mantenerlo.

### Solid

- Añadir un archivo de cabecera necesario en «msys2».

### Resaltado de sintaxis

- Se ha añadido una extensión para Arduino.
- LaTeX: se ha corregido la terminación incorrecta de los comentarios «iffalse» (fallo 378487).

### Información de seguridad

El código publicado se ha firmado con GPG usando la siguiente clave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;. Huella digital de la clave primaria: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB.

Puede comentar esta versión y compartir sus ideas en la sección de comentarios de <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>el artículo de dot</a>.
