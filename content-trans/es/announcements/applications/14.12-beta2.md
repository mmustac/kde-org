---
aliases:
- ../announce-applications-14.12-beta2
custom_spread_install: true
date: '2014-11-13'
description: KDE lanza Beta 2 de las Aplicaciones 14.12
layout: application
title: KDE lanza la segunda beta para las Aplicaciones 14.12
---
Hoy, 13 de noviembre de 2014, KDE ha lanzado la versión beta de las nuevas versiones de las Aplicaciones. Una vez congeladas las dependencias y las funcionalidades, el equipo de KDE se ha centrado en corregir errores y en pulir aún más la versión.

Debido a las numerosas aplicaciones que se basan en KDE Frameworks 5, es necesario probar la versión 14.12 de manera exhaustiva con el fin de mantener y mejorar la calidad y la experiencia del usuario. Los usuarios reales son críticos para mantener la alta calidad de KDE porque los desarrolladores no pueden probar todas las configuraciones posibles. Contamos con ustedes para ayudar a encontrar errores lo antes posible de forma que se puedan corregir antes de la versión final. Considere la idea de unirse al equipo instalando la beta <a href='https://bugs.kde.org/'>e informando de cualquier error que encuentre</a>.

#### Instalación de paquetes binarios de las Aplicaciones de KDE 14.12 Beta2

<em>Paquetes</em>. Algunos distribuidores del SO Linux/UNIX tienen la gentileza de proporcionar paquetes binarios de Beta2 de 14.12 (internamente, 14.11.90) para algunas versiones de sus distribuciones y en otros casos han sido voluntarios de la comunidad los que lo han hecho posible. En las próximas semanas estarán disponibles paquetes binarios adicionales, así como actualizaciones de los paquetes disponibles en este momento.

<em>Ubicación de paquetes</em>. Para obtener una lista actual de los paquetes binarios disponibles de los que el Equipo de Lanzamiento de KDE tiene conocimiento, visite la <a href='http://community.kde.org/KDE_SC/Binary_Packages'>Wiki de la Comunidad</a>.

#### Las aplicaciones de KDE 14.12 Beta2

La totalidad del código fuente de las Aplicaciones 14.12 Beta2 se puede <a href='http://download.kde.org/unstable/applications/14.11.90/src/'>descargar libremente</a>. Dispone de instrucciones sobre cómo compilar e instalar 14.11.90 en la <a href='/info/applications/applications-14.11.90'>página de información sobre la versión Beta2 de las aplicaciones de KDE 4.11.90</a>.
