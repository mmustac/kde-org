---
aliases:
- ../announce-applications-18.12-rc
date: 2018-11-30
description: KDE lanza la versión candidata de las Aplicaciones 18.12.
layout: application
release: applications-18.11.90
title: KDE lanza la candidata a versión final para las Aplicaciones de KDE 18.12
version_number: 18.11.90
version_text: 18.12 Release Candidate
---
Hoy, 30 de noviembre de 2018, KDE ha lanzado la candidata a versión final de las nuevas Aplicaciones de KDE. Una vez congeladas las dependencias y las funcionalidades, el equipo de KDE se ha centrado en corregir errores y en pulir aún más la versión.

Consulte las <a href='https://community.kde.org/Applications/18.12_Release_Notes'>notas de lanzamiento de la comunidad</a> para obtener información sobre los paquetes y sobre los problemas conocidos. Se realizará un anuncio más completo para la versión final.

Los lanzamientos de las Aplicaciones de KDE 18.12 necesitan una prueba exhaustiva con el fin de mantener y mejorar la calidad y la experiencia del usuario. Los usuarios reales son críticos para mantener la alta calidad de KDE porque los desarrolladores no pueden probar todas las configuraciones posibles. Contamos con ustedes para ayudar a encontrar errores de manera temprana de forma que se puedan corregir antes de la versión final. Considere la idea de unirse al equipo instalando la versión candidata <a href='https://bugs.kde.org/'>e informando de cualquier error que encuentre</a>.
