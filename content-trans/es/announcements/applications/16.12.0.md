---
aliases:
- ../announce-applications-16.12.0
changelog: true
date: 2016-12-15
description: KDE lanza las Aplicaciones de KDE 16.12.0
layout: application
title: KDE lanza las Aplicaciones de KDE 16.12.0
version: 16.12.0
---
Hoy, 15 de diciembre de 2016, KDE presenta las Aplicaciones 16.12 con un impresionante conjunto de actualizaciones relacionadas con una mayor facilidad de acceso, la introducción de funcionalidades de gran utilidad y la solución de problemas menores. Todo ello contribuye a que ahora las Aplicaciones de KDE estén un paso más cerca de ofrecer la configuración perfecta para su dispositivo.

<a href='https://okular.kde.org/'>Okular</a>, <a href='https://konqueror.org/'>Konqueror</a>, <a href='https://www.kde.org/applications/utilities/kgpg/'>KGpg</a>, <a href='https://www.kde.org/applications/education/ktouch/'>KTouch</a> y <a href='https://www.kde.org/applications/education/kalzium/'>Kalzium</a>, entre otros (<a href='https://community.kde.org/Applications/16.12_Release_Notes#Tarballs_that_were_based_on_kdelibs4_and_are_now_KF5_based'>notas del lanzamiento</a>), se han migrado a KDE Frameworks 5. Esperamos con ilusión sus comentarios sobre las funcionalidades más recientes introducidas en esta versión.

En el continuado esfuerzo para hacer que las aplicaciones sean más fáciles de compilar de forma aislada, hemos dividido los «tarballs» de kde-baseapps, kdepim y kdewebdev. Puede encontrar los nuevos «tarballs» creados en <a href='https://community.kde.org/Applications/16.12_Release_Notes#Tarballs_that_we_have_split'>el documento de las notas de lanzamiento</a>

Hemos discontinuado los siguientes paquete: kdgantt2, gpgmepp y kuser. Esto nos ayudará a centrarnos en el resto del código.

### El editor de sonido Kwave se une a las Aplicaciones de KDE

{{<figure src="https://www.kde.org/images/screenshots/kwave.png" width="600px" >}}

<a href='http://kwave.sourceforge.net/'>Kwave</a> es un editor de sonido. Puede grabar, reproducir, importar y editar numerosos tipos de archivos de sonido, incluidos los archivos multicanal. Kwave incluye complementos para transformar archivos de sonido de varios modos y presenta una vista gráfica con funciones completas de ampliación y de desplazamiento.

### El mundo como fondo del escritorio

{{<figure src="https://frinring.files.wordpress.com/2016/08/screenshot_20160804_171642.png" width="600px" >}}

Marble incluye ahora tanto un fondo para el escritorio como un elemento gráfico para Plasma que muestra la hora sobre una vista de satélite de la Tierra, con las zonas de día y noche en tiempo real. Esto ya estaba disponible en Plasma 4, y ahora se ha actualizado para que funcione en Plasma 5.

Puede encontrar más información en el <a href='https://frinring.wordpress.com/2016/08/04/wip-plasma-world-map-wallpaper-world-clock-applet-powered-by-marble/'>blog de Friedrich W. H. Kossebau</a>.

### ¡Gran cantidad de emoticonos!

{{<figure src="/announcements/applications/16.12.0/kcharselect1612.png" width="600px" >}}

KCharSelect posee la capacidad de mostrar el bloque de emoticonos Unicode (y otros bloques de símbolos SMP).

También contiene ahora un menú de marcadores con el que puede marcar como favoritos los caracteres que desee.

### Las matemáticas son mejores con Julia

{{<figure src="https://2.bp.blogspot.com/-BzJNpF5SXZQ/V7skrKcQttI/AAAAAAAAAA8/7KD8g356FfAd9-ipPcWYi6QX5_nCQJFKgCLcB/s640/promo.png" width="600px" >}}

Cantor posee un nuevo motor para Julia, proporcionando a los usuarios la capacidad de usar lo último en cálculos científicos.

Puede encontrar más información en el <a href='https://juliacantor.blogspot.com/2016/08/cantor-gets-support-of-julia-language.html'>blog de Ivan Lakhtanov</a>.

### Archivado avanzado

{{<figure src="https://rthomsen6.files.wordpress.com/2016/11/blog-1612-comp-method.png" width="600px" >}}

Ark dispone de varias funciones nuevas:

- Es posible copiar, mover y cambiar de nombre los archivos y las carpetas contenidos en los archivos comprimidos
- Ahora se pueden seleccionar los algoritmos de compresión y de cifrado al crear archivos comprimidos
- Ark ya puede abrir archivos AR (por ejemplo, las bibliotecas estáticas de Linux \*.a)

Puede encontrar más información en el <a href='https://rthomsen6.wordpress.com/2016/11/26/new-features-in-ark-16-12/'>blog de Ragnar Thomsen</a>.

### ¡Y aún más!

Kopete puede usar la autenticación SASL X-OAUTH2 en el protocolo Jabber y también se han solucionado en él algunos problemas del complemento de cifrado OTR.

Kdenlive dispone de un nuevo efecto rotoscopio, puede descargar contenidos y se ha actualizado el rastreo de movimiento. También proporciona archivos <a href='https://kdenlive.org/download/'>Snap y AppImage</a> para una instalación más sencilla.

KMail y Akregator pueden usar la navegación segura de Google para comprobar si los enlaces sobre los que se pulsa son maliciosos. Los dos han recuperado la función de imprimir (se necesita Qt 5.8).

### Agresivo control de plagas

Se han resuelto más de 130 errores en aplicaciones, que incluyen Dolphin, Akonadi, KAddressBook, KNotes, Akregator, Cantor, Ark y Kdenlive, entre otras.

### Registro de cambios completo
