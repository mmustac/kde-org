{{/*
SPDX-FileCopyrightText: 2021 Pablo Marcos <kde@pablomarcos.me>
SPDX-FileCopyrightText: 2022 Nguyen Hung Phu <phu.nguyen@kdemail.net>
SPDX-License-Identifier: LGPL-3.0-or-later
*/}}
{{- $index := slice -}}
{{- $allFirstSitePages := where .Sites.First.Pages "Layout" "!=" "search" }}
{{- $firstSitePages := slice }}

{{/* frameworks, plasma, and gear info pages, latest 5 each */}}
{{- $infos := where $allFirstSitePages "Section" "=" "info" }}
{{- $infoFrameworks := slice }}
{{- $infoPlasma := slice }}
{{- $infoGear := slice }}
{{- range $infos }}
  {{- if .File }}
  {{- if hasPrefix .File.TranslationBaseName "kde-frameworks" }}
    {{- $infoFrameworks = $infoFrameworks | append . }}
  {{- else if hasPrefix .File.TranslationBaseName "plasma" }}
    {{- $infoPlasma = $infoPlasma | append . }}
  {{- else if hasPrefix .File.TranslationBaseName "releases" }}
    {{- $infoGear = $infoGear | append . }}
  {{- end }}
  {{- end }}
{{- end }}
{{- $firstSitePages = $firstSitePages | append (last 5 (sort $infoFrameworks "Lastmod")) }}
{{- $firstSitePages = $firstSitePages | append (last 5 (sort $infoPlasma "Lastmod")) }}
{{- $firstSitePages = $firstSitePages | append (last 5 (sort $infoGear "Lastmod")) }}

{{- $nonInfos := $allFirstSitePages | symdiff $infos }}

{{/* plasma and gear changelog pages, latest 5 each */}}
{{- $changelogs := slice }}
{{- $nonChangelogs := slice }}
{{- range $nonInfos }}
  {{- if .File }}
  {{- if hasPrefix .File.Dir "announcements/changelogs" }}
    {{- $changelogs = $changelogs | append . }}
  {{- else }}
    {{- $nonChangelogs = $nonChangelogs | append . }}
  {{- end }}
  {{- end }}
{{- end }}
{{- $changelogPlasma := slice }}
{{- $changelogGear := slice }}
{{- range $changelogs }}
  {{- if hasPrefix .File.Dir "announcements/changelogs/plasma" }}
    {{- $changelogPlasma = $changelogPlasma | append . }}
  {{- else if hasPrefix .File.Dir "announcements/changelogs/gear" }}
    {{- $changelogGear = $changelogGear | append . }}
  {{- end }}
{{- end }}
{{- $firstSitePages = $firstSitePages | append (last 5 (sort $changelogPlasma "Lastmod")) }}
{{- $firstSitePages = $firstSitePages | append (last 5 (sort $changelogGear "Lastmod")) }}

{{/* frameworks announcements, latest 5 */}}
{{- $frameworks := slice }}
{{- $nonFrameworks := slice }}
{{- range $nonChangelogs }}
  {{- if hasPrefix .File.Dir "announcements/frameworks" }}
    {{- $frameworks = $frameworks | append . }}
  {{- else }}
    {{- $nonFrameworks = $nonFrameworks | append . }}
  {{- end }}
{{- end }}
{{- $firstSitePages = $firstSitePages | append (last 5 (sort $frameworks "Lastmod")) }}

{{/* the rest */}}
{{- range $nonFrameworks -}}
{{- $firstSitePages = $firstSitePages | append . }}
{{- end }}

{{- $pages := $firstSitePages }}
{{- if ne .Site .Sites.First }}
  {{- $pages = where .Site.Pages "Layout" "!=" "search" | lang.Merge $firstSitePages }}
{{- end }}
{{- range $pages -}}
  {{- $index = $index | append (dict "title" (partial "fn/i18n_title" .) "tags" .Params.tags "categories" .Params.categories "content" (partial "rss_content.html" . | plainify) "permalink" .Permalink "metadata" .Params) -}}
{{- end -}}
{{- $index | jsonify -}}
